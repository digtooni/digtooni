#ifndef CYCLES_H
#define CYCLES_H

// local
#include "database.hpp"
#include "object.hpp"
#include "scene.hpp"

// OpenCV
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/video/background_segm.hpp" 
#include "opencv2/gpu/gpu.hpp" 

// C++
#include <cmath>
#include <ctime>
#include <fstream>
#include <iostream>
#include <list>
#include <string>
#include <thread>

// Video player states
enum Playback { PLAY, PAUSE, NEXT };

class DistanceLine {
    public:
        DistanceLine() {}
        DistanceLine( cv::Point point1, cv::Point point2, float max_dist );
        float convertToRealWorldDist( float projection_distance );
        float projectOntoLine( cv::Point pt );
        float findScalingExponent();


        cv::Point start;
        cv::Point end;
        float max_distance;
        float max_projection_length;
        float scaling_exponent;
};

/*
 * Cyclist tracker object, which takes a video file as input
 * One can access a cyclist count and a list of cyclists currently
 * in the frame along with their properties
 *
 * Parameters:
 * 
 * std::sting& filename: full path to the video file.
 * int minArea         : minimum size in pixels of a cyclist.
 * int maxArea         : maximum size in pixels of a cyclist.
 * double histThresh   : The maximum histogram Bhattacharyya distance
 *                       between the same object in consecutive frames.
 * double didsThresh   : The maximum pixel distance between the same object
 *                       in consecutive frames.
 * checkFrames         : Number of frames for which an object has to satisfy
 *                       the cyclist criteria before being classified a cyclist.
 */
class CyclistTracker {
    public:
        CyclistTracker(int minArea, int maxArea,
                double histThresh, double distThresh, int checkFrames, const std::string &fileName);

        // Use this to launch the cyclist tracker and
        // obtain the cyclist count from the video
        void trackCyclists(bool showVideo, int frameStart, bool undistort_frame);

        std::string getFileName();
        void playVideo();
        int getCyclistCount();

        double minPerimLength;
        double maxPerimLength;
        const double minCyclistArea;
        const double maxCyclistArea;
        const double histThresh;
        const double distThresh;
        const double occlusionLimit;
        std::list<Object> cyclists;
        DistanceLine dist_line;
        bool is_stream;

        // FPS calculation
        std::clock_t time;
        float fps;
        std::vector<float> durations;

    protected:
        void getContours(cv::Ptr<cv::gpu::MOG2_GPU>& bgSubtractor, cv::Mat frame,
                cv::Mat& contourIm, std::vector<std::vector<cv::Point> >& contours);
        void getBlobs(cv::Ptr<cv::gpu::MOG2_GPU>& bgSubtractor, cv::Mat frame,
                cv::Mat& contourIm, std::vector<cv::KeyPoint> &blobs, std::vector<std::vector<cv::Point> > &contours);
        void getObjects(std::vector<Object>& newObjects, std::vector<std::vector<cv::Point> >&
                contours, cv::Mat frame);
        void classifyCyclists(std::vector<Object>& newObjects);
        void drawPaths(cv::Mat& image);
        void incrementObjectCount();
        void incrementCyclistCount();
        void computeSpeed(cv::Mat frame);
        cv::Point2f projectPoint(cv::Point2f);

        const std::string videoPath;
        cv::VideoCapture cap;
        int sliderPos;
        int frameCount;
        Playback vidStatus;
        std::list<Object> objects;
        int objectCount;
        int cyclistCount;
        cv::Rect count_zone;
        double average_speed;
        double clock_start;
	int Max_Package_Size;
	bool correct_distortion;
	std::string DB_Name;
	cv::SimpleBlobDetector blob_detector;
	cv::Mat erosionElement;
	cv::Mat dilationElement;
	double min_area;
	double max_area;
	bool verbose;
	int validation_count;
    cv::RNG rng;
    cv::Mat floorplan;
    std::string record_path;
    cv::VideoWriter video_writer;
    bool record_video;

	cv::Mat perspective_transform;
};

#endif

