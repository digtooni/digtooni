#pragma once

#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>
#include <opencv2/opencv.hpp>

#include <thread>
#include <string.h>

#include "mysql_connection.h"
#include "mysql_driver.h"

#include <vector>

class heatMapUpload
{
    public:

	heatMapUpload() {}
	heatMapUpload(time_t stamp, double x, double y, double density, double frameNumber, float speed,
		int id, bool check);
        time_t TimeStamp;
        double x;
        double y; 
        double density;
        double FrameNumber;
        bool run_check;
	float speed;
	int id;
};


//void SQLDataUpload(trackedObject SQLObjects[100], int ObjNum);
void SQLDataUpload(std::vector<heatMapUpload> &dataUpload, int ObjNum, std::string dbName);
