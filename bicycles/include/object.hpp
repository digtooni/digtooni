#ifndef OBJECT_H
#define OBJECT_H

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <forward_list>
#include <sstream>
#include <stdlib.h>

/*
 * Representation for a moving entity in a video.
 *
 * Parameters:
 *
 * std::vector<cv::Point>& contour: contour which delimits the part of the
 *                                  image corresponding to the entity
 * cv::Mat image                  : image in which the entity is found
 * int ObjectCount                : count of all other objects found so far
 *                                  so as to assign the object a unique
 *                                  identifier
 */
class Object {
    public:
        Object() {}
        Object(std::vector<cv::Point>& contour, cv::Mat image, int ObjectCount, int validation); 
        virtual ~Object() {}

        int getId() const;
        int getLastSeen();
        int getAge();
        cv::Mat getHist();
        cv::Point getCentroid();
        void inherit(Object oldObj);
        void update();
        double histDistance(Object obj2);
        bool cyclistCheck();        // Convert object to cyclist if satisfies criteria
        bool getIsCyclist();      // return the "isCyclist" value
        int getChecker();
        cv::Scalar getColor();
        std::string getName();
        double dist(Object obj2);
        void printHist();
        void setEntered();
        bool getEntered();
        
        // Modify and access position history
        // TODO: Either keep fully public and remove these functions or
        // implement full protected read/write access
        std::forward_list<cv::Point>::iterator pointHistStart();
        std::forward_list<cv::Point>::iterator pointHistEnd();
        void addPoint(const cv::Point pt);

        bool updated;
        std::forward_list<cv::Point> pointHistory;
        float speed;
        bool occluded;
	int validation_count;

    protected:
        // These change for every detection
        cv::Mat image;
        cv::Point centroid;
        cv::Mat colorHist;  // BGR order
        cv::Mat contour;
	//cv::KeyPoint keypoint;
        std::vector<double> direction;
        double aspectRatio;

        // Updated every frame
        int lastSeen;

        // Persistent values
        int id;
        std::string name;
        cv::Scalar drawColor;
        int age;
        bool isCyclist;
        int cyclistChecker;
        bool entered_zone;
};

#endif
