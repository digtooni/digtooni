#ifndef SCENE_H
#define SCENE_H

#include "vector.hpp"

#include "opencv2/imgproc/imgproc.hpp"

#include <iostream>

class Plane
{
    public:
        Plane(Vector3 point, Vector3 norm);
        
        Vector3 point;
        Vector3 norm;

};

class Scene
{
    public:
        Scene( Plane floor);

        Vector3 convertToRealWorld( cv::Point );
        void calibrateScene( cv::Mat );
        void calibrateDistortion();
        bool rayIntersects( Vector3 ray_dir, Vector3 &interect_point );

    private:
        Vector3 camera_pos;
        Plane floor_plane;
};


#endif
