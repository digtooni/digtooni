#include "cyclist_tracker.hpp"

using namespace std;

cv::Mat undistort( cv::Mat image )
{
    cv::Mat camera_matrix;
    cv::Mat dist_coeffs;
    cv::Mat undistorted_image;
 
    cv::FileStorage fs;
    fs.open( "config.xml", cv::FileStorage::READ );
    fs["Camera_Matrix"] >> camera_matrix;
    fs["Distortion_Coefficients"] >> dist_coeffs;
    fs.release();

    cv::undistort( image, undistorted_image, camera_matrix, dist_coeffs );

    return undistorted_image;
}

// Set the frame to the one selected with the trackbar
void onSlideTrackbar(int pos, void* vCap) {
    // dereference the void pointer back to a videoCapture
    cv::VideoCapture cap = *static_cast<cv::VideoCapture*>(vCap);
    cap.set(CV_CAP_PROP_POS_FRAMES, pos);
}

// Utility method to search an object list and find the object 
// with the id in the parameter and delete it.
void removeObjectFromList(const int& id, std::list<Object>& list) {
    bool empty = list.empty();
    if (!empty) {
        for (auto it=list.begin(); it!=list.end(); it++) {
            if (it->getId() == id) {
                it = list.erase(it);
            }
        }
    }
}

DistanceLine::DistanceLine( cv::Point point1, cv::Point point2, float max_dist )
    : start( point1 )
    , end( point2 )
    , max_distance( max_dist )
{
    Vector2 line_vec( end - start );
    max_projection_length = Vector2::Length(line_vec);
    scaling_exponent = findScalingExponent();
}

float DistanceLine::findScalingExponent()
{
    for ( float i = 0.0f; i < 10; i += 0.0001 )
    {
        if ( ( exp( i * max_projection_length ) + 1) > max_distance )
        {
            std::cout << "scaling exponent: " << i << std::endl;
            return i;
        }
    }
    // Should not reach here
    std::cout << "ERROR: scaling exponent not found, check scaling line parameters!" << std::endl;
    return -1;
}

float DistanceLine::convertToRealWorldDist( float projection_length )
{
    return exp( scaling_exponent * projection_length ) + 1;
}

float DistanceLine::projectOntoLine( cv::Point object )
{
    Vector2 origin_to_object( object - start );
    Vector2 line_vec( end - start );

    float projection_length( Vector2::Dot( origin_to_object, line_vec ) / Vector2::Dot( line_vec, line_vec ) * Vector2::Length( line_vec ) );

    float position( convertToRealWorldDist( projection_length ) );

    return position;
}

CyclistTracker::CyclistTracker(int minSize, int maxSize,
                double histThresh, double distThresh, int checkFrames, const std::string &fileName="")
    : videoPath(fileName)
    , sliderPos(0)
    , cyclistCount(0)
    , objectCount(0)
    , minPerimLength(200)
    , maxPerimLength(500)
    , minCyclistArea(minSize)
    , maxCyclistArea(maxSize)
    , histThresh(histThresh=0.8)
    , distThresh(distThresh=80)
    , occlusionLimit(checkFrames=6)
    , average_speed()
    , fps(0)
    , correct_distortion(0)
{
    vidStatus = PLAY;
    vector<cv::Point> points = {cv::Point(576,500), cv::Point(476,0), cv::Point(80,600)};
    vector<double> weights = {1, 0.8, 0};
    count_zone = cv::Rect(750, 0, 800, 550);
    time = std::clock();
    if (!videoPath.empty())
    {
        is_stream = false;
        cap.open(videoPath);    
    }
    else
    {
	std::cout << "No video path provided. Streaming from camera." << std::endl;
        is_stream = true;
        cap.open(0);
    }

    frameCount = (int) cap.get(CV_CAP_PROP_FRAME_COUNT);

    cv::FileStorage fs;
    fs.open( "config.xml", cv::FileStorage::READ );
    fs["Buffer_Size"] >> Max_Package_Size;
    fs["DB_Name"] >> DB_Name;
    fs["Verbose"] >> verbose;
    fs["Validation_Frames"] >> validation_count;

    // Erosion/Dilation parameters
    double erosion_value;
    double dilation_value;
    fs["Erosion"] >> erosion_value;
    fs["Dilation"] >> dilation_value;
    erosionElement = cv::getStructuringElement(cv::MORPH_RECT, 
            cv::Size(erosion_value, erosion_value), cv::Point(0,0));
    dilationElement = cv::getStructuringElement(cv::MORPH_RECT, 
            cv::Size(dilation_value, dilation_value), cv::Point(0,0));

    // Perspective transform
    std::vector<cv::Point2f> cam_points;
    std::vector<cv::Point2f> projected_points;
    cv::Mat cam;
    cv::Mat proj;
    fs["Cam_Points"] >> cam;
    fs["Projected_Points"] >> proj;

    cv::Point2f pt1(cam.at<float>(0,0), cam.at<float>(0,1));
    cv::Point2f pt2(cam.at<float>(1,0), cam.at<float>(1,1));
    cv::Point2f pt3(cam.at<float>(2,0), cam.at<float>(2,1));
    cv::Point2f pt4(cam.at<float>(3,0), cam.at<float>(3,1));

    cam_points.push_back(pt1);
    cam_points.push_back(pt2);
    cam_points.push_back(pt3);
    cam_points.push_back(pt4);

    cv::Point2f pt5(proj.at<float>(0,0), proj.at<float>(0,1));
    cv::Point2f pt6(proj.at<float>(1,0), proj.at<float>(1,1));
    cv::Point2f pt7(proj.at<float>(2,0), proj.at<float>(2,1));
    cv::Point2f pt8(proj.at<float>(3,0), proj.at<float>(3,1));

    projected_points.push_back(pt5);
    projected_points.push_back(pt6);
    projected_points.push_back(pt7);
    projected_points.push_back(pt8);

    perspective_transform = getPerspectiveTransform(cam_points, projected_points);

    //Blob detector parameters
    cv::SimpleBlobDetector::Params params;
    fs["Min_Area"] >> min_area;
    fs["Max_Area"] >> max_area;
    fs["Min_Perim"] >> minPerimLength;
    fs["Max_Perim"] >> maxPerimLength;
    //params.blobColor = 255;
    //params.filterByArea = true;
    //params.filterByCircularity = false;
    //params.filterByConvexity = false;
    //params.filterByInertia = false;

    blob_detector = cv::SimpleBlobDetector(params);

    floorplan = cv::imread("images/BT_1_FP.png");

    rng = cv::RNG(12345);

    fs["Record"] >> record_video;
    if (record_video)
    {
        fs["Video_Output_Path"] >> record_path;
    }
    fs.release();
}

string CyclistTracker::getFileName() {
    return videoPath;
}

int CyclistTracker::getCyclistCount() {
    return cyclistCount;
}

void CyclistTracker::incrementObjectCount() {
    ++objectCount;
}

void CyclistTracker::incrementCyclistCount() {
    ++cyclistCount;
}

cv::Point2f CyclistTracker::projectPoint( cv::Point2f input)
{
    std::vector<cv::Point2f> input_wrapper;
    std::vector<cv::Point2f> output_wrapper;
    input_wrapper.push_back(input);
    cv::perspectiveTransform( input_wrapper, output_wrapper, perspective_transform );
    cv::Point2f output = output_wrapper[0];
    return output;
}

// Simple Video playback
void CyclistTracker::playVideo() {
    cv::namedWindow("Video playback", cv::WINDOW_AUTOSIZE);
    cv::Mat frame;

    while (true) {
        cap >> frame;
        if(!frame.data) {  // no more frames in the video
            break;
        }
        cv::imshow("Video playback", frame);
        if (cv::waitKey(33) >=0) {  // Stop the video on key press
            break;
        }
    }
}

// Extract the contours of moving objects
void CyclistTracker::getContours(cv::Ptr<cv::gpu::MOG2_GPU>& bgSubtractor, cv::Mat frame, 
        cv::Mat& contourIm, vector<vector<cv::Point> >& contours) {
    cv::Mat objectMask;
    cv::RNG rng(12345);
    cv::Mat erosionElement = cv::getStructuringElement(cv::MORPH_RECT, 
            cv::Size(2,2), cv::Point(0,0));
    cv::Mat dilationElement = cv::getStructuringElement(cv::MORPH_RECT, 
            cv::Size(3,3), cv::Point(0,0));

    // Apply background removal
    cv::gpu::GpuMat gpuMask;
    cv::gpu::GpuMat gpuFrame;
    gpuFrame.upload(frame);
    bgSubtractor->operator()(gpuFrame, gpuMask);
    objectMask = cv::Mat(gpuMask);
    gpuMask.release();
    gpuFrame.release();

    // Erode bitmask to remove noise
    cv::erode(objectMask, objectMask, erosionElement);
    //cv::dilate(objectMask, objectMask, dilationElement);

    frame.copyTo(contourIm, objectMask);

    vector<cv::Vec4i> hierarchy;
    /// Find contours
    findContours(objectMask, contours, hierarchy, CV_RETR_TREE,
            CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0));

    //filter contours
    for (int i=0; i<contours.size(); i++) {
        double perim = cv::arcLength(contours[i],1);
        if (perim < minPerimLength) {
            contours.erase(contours.begin()+i);
            --i;
        }
   }
    // Draw contours
    for (int i = 0; i< contours.size(); i++)
    {
        cv::Scalar color = cv::Scalar(rng.uniform(0, 255), rng.uniform(0,255), 
                rng.uniform(0,255) );
        cv::drawContours(contourIm, contours, i, color, 2, 8, hierarchy,
                0, cv::Point() );
    }
    return;
}
// Extract the blobs of moving objects
void CyclistTracker::getBlobs(cv::Ptr<cv::gpu::MOG2_GPU>& bgSubtractor, cv::Mat frame, cv::Mat& blobIm, vector<cv::KeyPoint> &blobs, std::vector<std::vector<cv::Point> > &contours) {

    cv::Mat objectMask;

    // Apply background removal
    cv::gpu::GpuMat gpuMask;
    cv::gpu::GpuMat gpuFrame;
    gpuFrame.upload(frame);
    bgSubtractor->operator()(gpuFrame, gpuMask);
    objectMask = cv::Mat(gpuMask);

    // Erode bitmask to remove noise
    cv::erode(objectMask, objectMask, erosionElement);
    cv::dilate(objectMask, objectMask, dilationElement);

    frame.copyTo(blobIm, objectMask);

    /*
    /// Find blobs
    blob_detector.detect(objectMask, blobs);
    */

    //std::vector<std::vector<cv::Point> > contours;
    vector<cv::Vec4i> hierarchy;
    /// Find contours
    findContours(objectMask, contours, hierarchy, CV_RETR_LIST,
            CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0));

    //filter contours
    for (int i=0; i<contours.size(); i++) {
        double perim = cv::arcLength(contours[i],1);
   	double area = cv::contourArea(contours[i]);
        if (perim < minPerimLength || area < min_area || area > max_area) {
            contours.erase(contours.begin()+i);
            --i;
        }
    }

    // Draw keyopints
    //cv::drawKeypoints(objectMask, blobs, blobIm, cv::Scalar(0, 0, 255), cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS );

// Draw contours
    for (int i = 0; i< contours.size(); i++)
    {
        cv::Scalar color = cv::Scalar(rng.uniform(0, 255), rng.uniform(0,255), 
                rng.uniform(0,255) );
        cv::drawContours(blobIm, contours, i, color, 2, 8, hierarchy,
                0, cv::Point() );
    }
    return;
}

// Using the contours, create "Objects" and add them to a
// vector to be compared to objects from previous frames
void CyclistTracker::getObjects(vector<Object>& newObjects, vector<vector<cv::Point> >& contours,
        cv::Mat frame) {
    for (int i=0; i<contours.size(); i++) {
        if (cv::contourArea(contours[i]) > minCyclistArea &&
                cv::contourArea(contours[i]) < maxCyclistArea) {

            newObjects.push_back(Object(contours[i], frame, objectCount, validation_count));
        }
    }
}

// Update previously seen objects and
// and classify potential cyclists
void CyclistTracker::classifyCyclists(vector<Object>& newObjects) {

    // Mark all of the old objects as "not updated"
    for (auto it=objects.begin(); it!=objects.end(); it++) {
        it->updated = false;
    }

    // Check if new object matches with old object
    // If so, update old object, otherwise add to list
    for (int i=0; i<newObjects.size(); i++) {
        bool exists = false;
        for (auto it=objects.begin(); it!=objects.end(); it++) {
            double histDiff = newObjects[i].histDistance(*it);
            double centroidDist = it->dist(newObjects[i]);
            if ((histDiff < histThresh) && (centroidDist < distThresh)) {
                newObjects[i].inherit(*it);
                // Check if the object is a cyclist (if it already isn't)
                if(!newObjects[i].getIsCyclist()) {
                    newObjects[i].cyclistCheck();
                }
                // Check if the object has entered the count zone
                if(count_zone.contains(newObjects[i].getCentroid())) {
                    newObjects[i].setEntered();
                }
                if (newObjects[i].getIsCyclist()) {
                    // Add updated cyclist to cyclist list
                    removeObjectFromList(newObjects[i].getId(), cyclists);
                    cyclists.push_back(newObjects[i]);
                    // If newly found cyclist, increase count
                    if(!(it->getIsCyclist()) && it->getEntered()) {
                        incrementCyclistCount();
                    }
                    // If cyclist enters zone
                    if(!(it->getEntered()) && newObjects[i].getEntered()) {
                        incrementCyclistCount();
                    }
                }
                // Remove old reference to object
                it = objects.erase(it);
                // Add updated object to object list
                removeObjectFromList(newObjects[i].getId(), objects);
                objects.push_front(newObjects[i]);
                exists = true;
                break;
            }
        }
        if (!exists) {
            // Add new object to object list
            objects.push_back(newObjects[i]);
            incrementObjectCount();
        }
    }

    // Update all of the occluded objects
    for (auto it=objects.begin(); it!=objects.end(); it++) {
        if(!it->updated) {
            it->update();
        }
        // If the object has been outside the scene for too long
        if(it->getLastSeen() > occlusionLimit) {
            // Remove from cyclist list
            for (auto cycleIt=cyclists.begin(); cycleIt!=cyclists.end(); cycleIt++) {
                if (cycleIt->getId() == it->getId()) {
                    cycleIt = cyclists.erase(cycleIt);                    
                }
            }
            // Remove from object list
            it = objects.erase(it);                    
        }
    }
    //cout << "object count: " << objects.size() << endl;
    //cout << "cyclist count: " << cyclists.size() << endl;
    newObjects.clear();
}

// Draw the cyclist paths on the display image
void CyclistTracker::drawPaths(cv::Mat& image) {
    cv::Point lastPoint;
    for (auto it=cyclists.begin(); it!=cyclists.end(); it++) {
        auto pointIt = it->pointHistStart();
        lastPoint = *pointIt;
        ++pointIt;
        while (pointIt != it->pointHistEnd()) {
            cv::line(image, lastPoint, *pointIt, it->getColor(), 4);
            lastPoint = *pointIt;
            ++pointIt;
        }
    }
}

// Compute the speed of the moving entities
void CyclistTracker::computeSpeed(cv::Mat frame)
{
    if ( cyclists.size() < 1 ) {
        //std::cout << "no cyclists yet" << std::endl;
        return;
    }
    int pos_samples = 5;
    for ( auto it = cyclists.begin(); it != cyclists.end(); ++it )
    {
        Object cyclist = *it;
        Object object;

        for ( auto objIt = objects.begin(); objIt != objects.end(); ++objIt )
        {
            if ( objIt->getId() == cyclist.getId() )
            {
                object = *objIt;
                it->occluded = object.occluded;
                break;
            }
        }       
        std::vector<float> distances;

        auto pt_it = object.pointHistStart();
        cv::Point lastPoint = *pt_it;
        ++pt_it;
        int count = 1;
        while ( count < validation_count && count < pos_samples )
        {
            cv::Point newPoint = *pt_it;
            cv::Point last_real_world_pos = projectPoint( lastPoint );
            cv::Point new_real_world_pos = projectPoint( newPoint );
            cv::circle(floorplan, new_real_world_pos, 3, cv::Scalar(0,255,255), 0.5);
            float dist( cv::norm( last_real_world_pos - new_real_world_pos ) );
            distances.push_back( dist );
            lastPoint = newPoint;
            ++count;
            ++pt_it;
        }
        float dist_sum = 0;
        for ( int i = 0; i < distances.size(); ++i )
        {
            dist_sum += distances[i];
        }
        float average_dist(dist_sum / distances.size());
        

        if ( fps > 0 )
        {
            float speed_pps = average_dist / (1.0f / (10.0f / (float) pos_samples));
            float speed_mph = speed_pps * 0.06821 * 0.000621371 * 3600;
            if (speed_mph > 0)
            {
                std::cout << "Speed: " << speed_mph << " mph" << std::endl;
            }
            it->speed = speed_pps;
        }

        std::stringstream label;
        if ( !object.occluded && fps > 0)
        {
            label << it->speed << "pps";
            cv::putText( frame, label.str(), *(cyclist.pointHistStart()),
                cv::FONT_HERSHEY_PLAIN, 1.0, cv::Scalar( 255, 255, 255 ), 2);
        }
    }

    //std::cout << "Average speed: " << average_speed << std::endl;
}

// Main tracking algorithm
void CyclistTracker::trackCyclists(bool showVideo=1, int frameStart=0, bool undistort_frame=0) {
    correct_distortion = undistort_frame;
    bool end = false;
    cap.set(CV_CAP_PROP_POS_FRAMES, frameStart);

    cv::Mat first_frame;
    cap >> first_frame;

    sliderPos = frameStart;
    vector<Object> newObjects;

    cv::Ptr<cv::gpu::MOG2_GPU> bgSubtractor =
        new cv::gpu::MOG2_GPU();
    bgSubtractor->initialize( first_frame.size(), CV_8UC3 );
    bgSubtractor->history = 150;
    bgSubtractor->nShadowDetection = 0;
    bgSubtractor->varThreshold = 9;
    bgSubtractor->fTau = 0.5;

    int start_x, start_y, end_x, end_y;
    float distance;

    cv::FileStorage fs;
    fs.open( "config.xml", cv::FileStorage::READ );
    fs["DistanceLine_Start_X"] >> start_x;
    fs["DistanceLine_Start_Y"] >> start_y;
    fs["DistanceLine_End_X"] >> end_x;
    fs["DistanceLine_End_Y"] >> end_y;
    fs["Line_Length"] >> distance;
    dist_line = DistanceLine( cv::Point( start_x, start_y ), cv::Point( end_x, end_y ), distance ); 
    fs.release();

    if (record_video)
    {
        video_writer.open(record_path, CV_FOURCC('M','J','P','G'), 10, first_frame.size());
    }
    /*
    std::string out_file_name( "bt_sense_out.csv" );
    std::ofstream  out_file;
    out_file.open( out_file_name );
    out_file << videoPath << std::endl;
    */


    if (showVideo) {
        cv::namedWindow("Cyclist Tracker", cv::WINDOW_AUTOSIZE);      
        if (!is_stream) 
        {
                cv::createTrackbar("Position", "Cyclist Tracker", &sliderPos, frameCount,
                onSlideTrackbar, &cap);
        }
    }

    cv::Mat frame, blobIm, vis;
    int frame_count = 0;
    std::vector<heatMapUpload> packages;
    std::clock_t last_check;
    last_check = std::clock();

    double fps_sum;
    double avg_fps;
    vector<vector<cv::Point> > contours;
    std::vector<cv::KeyPoint> keypoints;
    // Iterate through each frame
    while (true) {

        if (vidStatus == PLAY || vidStatus == NEXT) {
            //Trackbar control
            if(showVideo && !is_stream) {
                int currentPos = (int) cap.get(CV_CAP_PROP_POS_FRAMES);
                cv::setTrackbarPos("Position", "Cyclist Tracker", currentPos);
            }
            cap >> frame;

            if (correct_distortion) {
                frame = undistort( frame );
            }

            // Compute FPS
            float duration = ( std::clock() - time ) / ( double ) CLOCKS_PER_SEC;
            time = std::clock();
            if ( durations.size() > 5 )
            {
                durations.erase( durations.begin());
            }
            durations.push_back( duration );

            float duration_sum = 0;

            for ( int i = 0; i < durations.size(); ++i )
            {
                duration_sum += durations[i];
            }
            float average_duration = duration_sum / 5;

            fps = 1 / average_duration;
            fps_sum += fps;
            avg_fps = fps_sum / frame_count;

            if (frame_count % 5 == 0 && verbose)
            {
                    std::cout << "fps: " << fps << std::endl;
                    std::cout << "average fps: " << avg_fps << std::endl;
            }
		

            // Get contours
            //contourIm = cv::Mat::zeros(contourIm.size(), CV_8UC3);
            //getContours(bgSubtractor, frame, contourIm, contours);

	    // Get blobs
            getBlobs(bgSubtractor, frame, blobIm, keypoints, contours);

            // store into objects
            getObjects(newObjects, contours, frame);

            // classify objects into cyclists
            classifyCyclists(newObjects);

            // Draw paths
            drawPaths(frame);

            // compute object speeds
            computeSpeed(frame);

            // Render cyclist count
            std::stringstream current;
            std::stringstream total;
            std::stringstream fps_msg;
            current << "Current count: "  << cyclists.size();
            total << "Total count: "  << getCyclistCount();
            fps_msg << "Avg FPS: "  << avg_fps;
            cv::Point current_pt(20, 50);
            cv::Point total_pt(20, 100);
            cv::Point fps_pt(20, 150);
            cv::Scalar color(255, 255, 255);
            //cv::putText(frame, current.str(), current_pt, cv::FONT_HERSHEY_SIMPLEX, 1, color, 2);
            //cv::putText(frame, total.str(), total_pt, cv::FONT_HERSHEY_SIMPLEX, 1, color, 2);
            //cv::putText(frame, fps_msg.str(), fps_pt, cv::FONT_HERSHEY_SIMPLEX, 1, color, 2);

            // Upload to database
            for ( auto it = cyclists.begin(); it != cyclists.end(); ++it )
            {
                std::stringstream ss;
                Object cyclist = *it;

                if ( !cyclist.occluded && fps > 0 )
                {
                    int frame_number;
                    if (is_stream)
                    {
                        frame_number = frame_count;
                    }
                    else
                    {
                        frame_number = cap.get(CV_CAP_PROP_POS_FRAMES);
                    }

		            cv::Point projected = projectPoint(cyclist.getCentroid());

                    heatMapUpload upload(std::time(0), projected.x / (float) frame.cols, 
                           projected.y / (float) frame.rows,
                           0, frame_number, cyclist.speed, cyclist.getId(), false);

                    packages.push_back(upload);
	
                }
            }
		

	        if (packages.size() >= Max_Package_Size)
            {
                std::thread SQL_thread(SQLDataUpload, std::ref(packages), packages.size(), DB_Name);
                SQL_thread.detach();

		        packages.clear();
            }

            // Upload an indicator that the sensor is still running
            float time_since_check = ( std::clock() - last_check ) / ( double ) CLOCKS_PER_SEC;
            if (time_since_check > 60)
            {
                std::vector<heatMapUpload> check_package;
                heatMapUpload run_check(std::time(0), 0, 0, 0, 0, 0, 0, true);
                check_package.push_back(run_check);
                std::thread SQL_thread(SQLDataUpload, std::ref(check_package), check_package.size(), DB_Name);
                SQL_thread.detach();
                check_package.clear();

                last_check = std::clock();
            }

            // Video playback
            if (showVideo) {
                vis = cv::Mat(frame.rows, frame.cols*2, CV_8UC3);
                blobIm.copyTo(vis(cv::Rect(0, 0, blobIm.cols, blobIm.rows)));
                frame.copyTo(vis(cv::Rect(frame.cols, 0, frame.cols, frame.rows)));
                cv::imshow("Cyclist Tracker", vis);
                cv::imshow("Floor plan", floorplan);
                cv::waitKey(1);
            }

            if (record_video)
            {
                video_writer << frame;
            }

            vis.release();
            blobIm.release();
            frame.release();
            keypoints.clear();
            contours.clear();

            // increment video by one frame then pause
            if (vidStatus == NEXT) {
                vidStatus = PAUSE;
            }
        }
        // Video playback controls
        char keyPress = (char) cv::waitKey(100);
        switch(keyPress) {
            case 'p' : 
                vidStatus = (vidStatus == PLAY) ? PAUSE : PLAY;
                break;
            case 'n' :
                vidStatus = NEXT;
                break;
            case 27 : // Esc key
                end = true;
                break;
        }
        if (end) {
            break;
        }
        ++frame_count;
    }
    cv::destroyWindow("Cyclist Tracker");
}

int main()
{
    bool live_cam = true;
    bool undistort = true;
    bool display_video = false;

    cv::FileStorage fs;
    fs.open( "config.xml", cv::FileStorage::READ );
    fs["Display_Output"] >> display_video;
    fs["Undistort"] >> undistort;
    fs["Live_Stream"] >> live_cam;
    fs.release();

    std::string video("");
    if (!live_cam)
    {
        video = "videos/Sensor1.mp4";
    }

    CyclistTracker tracker(0, 8000, 0.4, 30, 7, video);
    tracker.trackCyclists(display_video, 0, undistort);

    //cv::Mat image( cv::imread( "images/image05.jpg" ) );
    //cv::namedWindow( "window" );

    //image = undistort( image );
    //cv::line( image, cv::Point( 50, 450 ), cv::Point( 50, 40 ), cv::Scalar( 0, 255, 255 ) );

    //cv::imshow( "window", image );
    //cv::waitKey();
   
    return 0;
}

