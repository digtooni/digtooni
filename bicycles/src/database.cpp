#include "database.hpp"

using namespace std;
using namespace sql::mysql;
using namespace sql;

heatMapUpload::heatMapUpload(time_t stamp, double x_, double y_, double density_, double frame, float speed_, int id_, bool check)
    : TimeStamp(stamp)
    , x(x_)
    , y(y_)
    , density(density_)
    , FrameNumber(frame)
    , speed(speed_)
    , id(id_)
    , run_check(check)
{
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
// upload data to SQL server (test)
void SQLDataUpload(std::vector<heatMapUpload> &dataUpload, int ObjNum, std::string db_name)
{

	//try prepared statements
	try {

		// Layout for vehicle tracking data upload
		/*
		sql::Driver *driver;
		sql::Connection *con;
		sql::PreparedStatement *prep_stmt;
		sql::ResultSet *res;

		// Create a connection
		driver = sql::mysql::get_driver_instance();
		//driver = get_driver_instance();
		// Connect to the MySQL test database
		con->setSchema("TestTegraDb");

		prep_stmt = con->prepareStatement("INSERT INTO TrkdObjs2(Timestamp,X_pos,Y_pos,X_speed,Y_speed,ObjType,UniqueID) VALUES (?,?,?,?,?,?,?)");

		for (int iter4 = 0; iter4 < ObjNum; iter4++)
		{
			prep_stmt->setInt(1, SQLUpload[iter4].TimeStamp); //Timestamp
			prep_stmt->setDouble(2, SQLUpload[iter4].x_cord); //X_pos
			prep_stmt->setDouble(3, SQLUpload[iter4].y_cord); //Y_pos
			prep_stmt->setDouble(4, SQLUpload[iter4].x_objectSpeed); //X_speed
			prep_stmt->setDouble(5, SQLUpload[iter4].y_objectSpeed); //Y_speed
			prep_stmt->setString(6, "test!"); //ObjType
			prep_stmt->setInt(7, SQLUpload[iter4].objectID); //UniqueID
			prep_stmt->execute();
		}
		*/

		// Layout for CCTV crowd monitoring data upload
		sql::Driver *driver;
		sql::Connection *con;
		sql::PreparedStatement *prep_stmt;
		sql::ResultSet *res;

		// Create a connection
		driver = sql::mysql::get_driver_instance();
		con = driver->connect("database.server.net", "datahub", "test341");
		// Connect to the MySQL test database
		con->setSchema("cyclists");
		
		// Setup char array large enough for full INSERT statement
		int StatementStrSize = 100 + (100 * ObjNum);
		//char StatementStr[StatementStrSize];
    		char *StatementStr = new char[StatementStrSize];
		
		string StrTimeStamp = to_string(dataUpload[0].TimeStamp);
		string StrX = to_string(dataUpload[0].x);
		string StrY = to_string(dataUpload[0].y);
		string StrDensity = to_string(dataUpload[0].density);
		string StrFrameNum = to_string(dataUpload[0].FrameNumber);
		string StrId = to_string(dataUpload[0].id);
		string StrSpeed = to_string(dataUpload[0].speed);
		string RunCheck = to_string(dataUpload[0].run_check);

		std::stringstream ss;
		ss << "INSERT INTO ";
		ss << db_name;
		ss << "(TrackedTime,X_cord,Y_cord,Density,FrameNumber,objectID,speed,run_check) VALUES ";
		std::string str(ss.str());

		char* insert_command = new char[str.size()+1];		
		insert_command[str.size()]=0;
		memcpy(insert_command, str.c_str(), str.size());

				
		strcpy (StatementStr, insert_command);
		strcat (StatementStr, "(");
		strcat (StatementStr, StrTimeStamp.c_str());
		strcat (StatementStr, ",");
		strcat (StatementStr, StrX.c_str());
		strcat (StatementStr, ",");
		strcat (StatementStr, StrY.c_str());
		strcat (StatementStr, ",");
		strcat (StatementStr, StrDensity.c_str());
		strcat (StatementStr, ",");
		strcat (StatementStr, StrFrameNum.c_str());
		strcat (StatementStr, ",");
		strcat (StatementStr, StrId.c_str());
		strcat (StatementStr, ",");
		strcat (StatementStr, StrSpeed.c_str());
		strcat (StatementStr, ",");
		strcat (StatementStr, RunCheck.c_str());
		strcat (StatementStr, ")");

		//prep_stmt = con->prepareStatement("INSERT INTO TestLTS2(TrackedTime,X_cord,Y_cord,Density) VALUES (?,?,?,?)");

		
		for (int iter4 = 1; iter4 < ObjNum; iter4++)
		{
			/*
			prep_stmt->setInt(1, dataUpload[iter4].TimeStamp); //TrackedTime
			prep_stmt->setDouble(2, dataUpload[iter4].x); //X_cord
			prep_stmt->setDouble(3, dataUpload[iter4].y); //Y_cord
			prep_stmt->setDouble(4, dataUpload[iter4].density); //Density

			prep_stmt->execute();
			
			//dataUpload[iter4].density = 0;

			std::cout << "Sending " << iter4 << "/" << ObjNum << std::endl;
			*/
			
			string StrTimeStamp = to_string(dataUpload[iter4].TimeStamp);
			string StrX = to_string(dataUpload[iter4].x);
			string StrY = to_string(dataUpload[iter4].y);
			string StrDensity = to_string(dataUpload[iter4].density);
			string StrFrameNum = to_string(dataUpload[iter4].FrameNumber);
			string StrId = to_string(dataUpload[iter4].id);
			string StrSpeed = to_string(dataUpload[iter4].speed);
			string RunCheck = to_string(dataUpload[iter4].run_check);
		
			strcat (StatementStr, ", (");
			strcat (StatementStr, StrTimeStamp.c_str());
			strcat (StatementStr, ",");
			strcat (StatementStr, StrX.c_str());
			strcat (StatementStr, ",");
			strcat (StatementStr, StrY.c_str());
			strcat (StatementStr, ",");
			strcat (StatementStr, StrDensity.c_str());
			strcat (StatementStr, ",");
			strcat (StatementStr, StrFrameNum.c_str());
			strcat (StatementStr, ",");
			strcat (StatementStr, StrId.c_str());
			strcat (StatementStr, ",");
			strcat (StatementStr, StrSpeed.c_str());
			strcat (StatementStr, ",");
			strcat (StatementStr, RunCheck.c_str());
			strcat (StatementStr, ")");		
			
			//std::cout << "Sending " << iter4 << "/" << ObjNum << std::endl;
		}
		

		prep_stmt = con->prepareStatement(StatementStr);
		prep_stmt->execute();
		
        if (dataUpload[0].run_check) {
		    std::cout << "RUN CHECK" << std::endl;
        }
        else
        {
		    std::cout << "DATA SENT" << std::endl;
        }
		
		
		//LINE BELOW CAUSES SEGMENTATION FAULT - unknown why
		//delete res;
    delete StatementStr;
		delete prep_stmt;
		delete con;


	}
	catch (sql::SQLException &e) {
		cout << "# ERR: SQLException in " << __FILE__;
		cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
		cout << "# ERR: " << e.what();
		cout << " (MySQL error code: " << e.getErrorCode();
		cout << ", SQLState: " << e.getSQLState() << " )" << endl;
	}


	//////////////////////////////////////////////////////////////////////////////////////////////////////////
}
