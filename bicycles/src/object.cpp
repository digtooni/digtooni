#include "object.hpp"

#include <iostream>

Object::Object(std::vector<cv::Point>& cntr, cv::Mat frame, int objectCount, int validation)
    : contour(cntr)
    , image(frame)
    , age(0)
    , lastSeen(0)
    , occluded(false)
    , updated(false)
    , id(objectCount)
    , cyclistChecker(0)
    , isCyclist(false)
    , entered_zone(false)
    , speed(0)
    , validation_count(validation)
{
    // Assign a name
    std::stringstream ss;
    ss << "Object " << id;
    name = ss.str(); 

    // Assign a random color for drawing
    cv::RNG rng(rand() % 10000);
    drawColor = cv::Scalar(rng.uniform(0, 255), rng.uniform(0,255), 
                rng.uniform(0,255));

    // Get the moments
    cv::Moments moments = cv::moments(contour, true); 
    //  Get the center of mass
    centroid = cv::Point(moments.m10/moments.m00, moments.m01/moments.m00);

    // Compute the bounding rectangle
    cv::Rect boundRect;
    std::vector<cv::Point> contourPoly;
    cv::approxPolyDP(cv::Mat(contour), contourPoly, 3, true);
    boundRect = cv::boundingRect(cv::Mat(contourPoly));

    //Set the aspect ratio
    aspectRatio =  boundRect.width / (double) boundRect.height;
    
    // Obtain image region containing only pixels
    // within the contour boundaries 
    cv::Mat contourRegion;
    cv::Mat roi;
    cv::Mat contourMask;

    std::vector<std::vector<cv::Point> > contourContainer;
    contourContainer.push_back(contour);
    cv::Mat mask = cv::Mat::zeros(frame.size(), CV_8UC1);
    cv::drawContours(mask, contourContainer, 0, cv::Scalar(255), CV_FILLED);

    frame.copyTo(roi, mask);
    contourRegion = roi(boundRect);
    mask.copyTo(roi);
    contourMask = roi(boundRect);

    // compute color histogram

    // Establish the number of channels and bins
    // per channel
    int channels[] = {0, 1, 2};
    int histSize[] = {8, 8, 8};

    /// Set the ranges ( for B,G,R) )
    float range[] = {0, 255} ;
    const float* histRange[] = { range, range, range };

    // initialise the histogram matrices
    for (int i=0;i<3;i++) { 
        colorHist.push_back(cv::Mat(frame.size(),CV_8UC3)); 
    }

    /// Compute the histograms:
    cv::calcHist(&contourRegion, 1, channels, contourMask, colorHist, 3, histSize, 
            histRange, true);
} 

// Return the number of frames since the
// object was last seen in the video
int Object::getLastSeen() {
    return lastSeen;
}

void Object::setEntered() {
    entered_zone = true;
}

bool Object::getEntered() {
    return entered_zone;
}

// Update the history of an occluded
// object
void Object::update() {
    ++age;
    ++lastSeen;
    occluded = true;
    addPoint(getCentroid());
    updated = true;
}

// After having matched with an object from a
// previous frame, inherit the properties from
// that object
void Object::inherit(Object oldObj) {
    // inherit id
    id = oldObj.id;
    isCyclist = oldObj.getIsCyclist();

    // update the name
    std::stringstream ss;
    if (isCyclist) {
        ss << "Cyclist " << id;
    } else {
        ss << "Object " << id;
    }
    name = ss.str();

    drawColor = oldObj.getColor();
    age = oldObj.getAge();
    cyclistChecker = oldObj.cyclistChecker;//getChecker();
    entered_zone = oldObj.getEntered();

    // retrieve point history
    pointHistory = oldObj.pointHistory;
    addPoint(getCentroid());

    ++age;
    lastSeen = 0;
    occluded = false;
    updated = true;
}

bool Object::getIsCyclist() {
    return isCyclist;
}

int Object::getChecker() {
    return cyclistChecker;
}

int Object::getId() const {
    return id;
}

int Object::getAge() {
    return age;
}

cv::Mat Object::getHist() {
    return colorHist;
}

cv::Point Object::getCentroid() {
    return centroid;
}

cv::Scalar Object::getColor() {
    return drawColor;
}

std::string Object::getName() {
    return name;
}

std::forward_list<cv::Point>::iterator Object::pointHistStart() {
    return pointHistory.begin();
}

std::forward_list<cv::Point>::iterator Object::pointHistEnd() {
    return pointHistory.end();
}

// Add the current position to the point history
void Object::addPoint(const cv::Point pt) {
    pointHistory.push_front(pt);
}

// Distance in pixels between this object
// and another object
double Object::dist(Object obj2) {
    cv::Vec2d v1(centroid.x, centroid.y);
    cv::Vec2d v2(obj2.getCentroid().x, obj2.getCentroid().y);

    return cv::norm(v1 - v2);
}

// Compute the Bhattacharrya distance between
// this and another object's histograms
double Object::histDistance(Object obj2) {
    cv::Mat hist2 = obj2.getHist();
    cv::normalize(colorHist, colorHist, 1, 0, cv::NORM_L1);
    cv::normalize(hist2, hist2, 1, 0, cv::NORM_L1);
    double dist = cv::compareHist(colorHist, hist2, CV_COMP_BHATTACHARYYA);
    return dist;
}

// Check whether this object qualifies
// as a cyclist
bool Object::cyclistCheck() {
    if(true) {//aspectRatio > 0.5) { 
        cyclistChecker++;
    }
    if (cyclistChecker >= validation_count && contourArea(contour) > 2000 && contourArea(contour) < 11000) {
        std::stringstream ss;
        ss << "Cyclist " << id;
        name = ss.str();

        isCyclist = true;
    }
    return isCyclist;
}
