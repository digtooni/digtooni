#include "scene.hpp"

Plane::Plane( Vector3 point, Vector3 norm )
    : point( point )
    , norm( norm )
{

}

Scene::Scene( Plane floor )
    : floor_plane( floor )
{
    camera_pos = Vector3( 0, 0, 0 );
}

bool Scene::rayIntersects ( Vector3 ray_dir, Vector3 &intersect_point )
{
    if ( Vector3::Dot( ray_dir, floor_plane.norm ) == 0 )
    {
        return false;
    }
    float intersect_dist = Vector3::Dot( ( floor_plane.point - camera_pos ), floor_plane.norm )
        / Vector3::Dot( ray_dir, floor_plane.norm );
    
    if ( intersect_dist < 0 )
    {
        return false;
    }
    
    intersect_point = camera_pos + ( ray_dir * intersect_dist );
    return true;
}

void Scene::calibrateScene( cv::Mat )
{
    // User must interact with GUI and select 2 points
    cv::Point pt1;
    cv::Point pt2;
}
