#pragma once

#include <string>
#include <vector>
#include <cstdlib>
#include <fstream>
#include <mysql_driver.h>
#include <mysql_connection.h>
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>
#include <mutex>


// data sentence structure
struct data_sentence
{
	time_t time;
	int BusID;
	double lat;
	double lon;
	int passenger_in;
	int passenger_out;
	int occupancy;
};

// Uploader class
class DatabaseUploader{

public:

  DatabaseUploader(const std::string &url, const std::string &username, const std::string &password, const std::string &schema);
  ~DatabaseUploader();

  void WriteLogfile(data_sentence sentence);	//write the sentence to log file
  bool StackEmpty();	//check if the upload stack is empty
  //void SQLDataUpload(time_t time_stamp);
  void SetupStatement();
  void addSentence(data_sentence sentence); // add data sentence to the stack
  
  void update_internet_status(); // ping google to update internet status
	
  bool getinternet_status(); // returns the current internet status
  void Upload();	// try to upload to the database
protected:

  // dataase url username password schema
  const std::string &url;
  const std::string &username;
  const std::string &password;
  const std::string &schema;
  
  // Database objects
  sql::Driver *driver;
  sql::Connection *con;
  sql::PreparedStatement *prep_stmt;
  sql::ResultSet *res;
  //char *StatementStr;
  std::string statement;	//statement to be uploaded
  
  bool internet_status;	// current internet status
  int log_count;	// log entries counter
  std::string logfile_name;	// the name of the log file currently writing to
  std::ofstream logfile;	// log file output file stream
  
 // std::vector<data_sentence> data_buffer;
  std::vector<data_sentence> upload_stack;	// the stack of sentences that needs to be uploaded
};
