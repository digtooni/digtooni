#ifndef OPENCV_LOADED
#define OPENCV_LOADED
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/video/background_segm.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/gpu/gpu.hpp"   
#endif

//
#ifndef VECTOR_
#define VECTOR_
#include <vector>
#endif

enum door_status_t {OPEN, CLOSE};

class door_detector
{

	public:
		door_detector();
		~door_detector();
		bool init(std::string cascade_dir, cv::Mat img,cv::Rect door_ROI, cv::Size door_minSize, cv::Size door_maxSize, int thres = 7);  //initialise the parameters in the class
		door_status_t getdoor_status(); // returns door stats if it's opened or closed
		void update_door_status(cv::Mat img); // update the door status by detecting the door in the image in the argument img
		void draw_door(cv::Mat &img);	// draw the box around the block that the door has been detected
	private:
		door_status_t status; // curent status of the door
		int miss_count;	//counts how many frames the door has not been detected
		cv::CascadeClassifier door_classifier; // the cascadeclassifier
		cv::Rect ROI;	// ROI used to crop the region where the door should be
		cv::Size minSize;	//minimum size of the door to search for in ROI
		cv::Size maxSize;	//maximum size of the door to search for in ROI
		std::vector<cv::Rect> door;	// the detected door in the current frame
		int threshold; 	// threshold for deciding if the door has opened or closed (ie opened if miss_count goes above threshold)
};
