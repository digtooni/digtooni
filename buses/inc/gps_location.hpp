#include "libgpsmm.h"
#include <time.h>

class gps_service
{
	public:
		gps_service();	//constructor
		~gps_service();	//destructor
		double getlat();	//returns lat value
		double getlon();	//returns lon value
		double getspeed();	//returns the current speed
		tm* getdate();	// return the time in stuct tm
		time_t gettime();	//	return the UNIX time
		int refresh();	// refresh GPS information
		void extract_data(struct gps_data_t* collect);	// extract the data from the recieved data packet from GPSD
	private:
		gpsmm* gps_rec;	// the gpsmm class (see gpsd for documentation)
		bool gpsd_init;	// true if the gps has been initialised (ie GPSD is running)
		double lat;	
		double lon;
		double speed;
		time_t timestamp;	//UNIX time
		tm* time_tm;		//struct tm time
		struct gps_data_t* newdata;	// the collected data from GPSD
};
