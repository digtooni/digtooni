#ifndef OPENCV_ 
#include "opencv2/opencv.hpp"
#define OPENCV_
#endif
// 
class stereo_cam
{
	public:
		stereo_cam(); // constructor
		~stereo_cam(); // destructor
		bool init_camera(int camera1, int camera2); // initialise the stereo camera
		bool init_videoinput(std::string file1, std::string file2);	//initialise the input to be stereo videos
		bool init_export(std::string out1, std::string out2);	//initialise the videos export directory
		bool init(int camera1, int camera2,std::string out1, std::string out2); //combined initialiser for stereo camera camera1,2 and export directory out1,2
		void start_record();	// set record_status to 1
		void stop_record();	   	// set record_status to 0
		void setfps(int fps);	// set fps to the given value (not really working)
		bool nextframe();		// fetch the next frame from the camera and undistort the images if the calib file has been loaded
		void load_calib(std::string dir);	// load calibration parameters from xml file
		
		//GET FUNCTIONS
		cv::Mat getUndistortedimg(int cam);	// get the undistorted images stored in this class
		bool getrecord_status();	// returns the record status
		cv::Size getimgsize(int cam);	//returns the recording image size
		cv::Mat getimg(int cam);	// returns the images from each camera
	
	private:
		std::string input_video1;	//directory of video1
		std::string input_video2;	//directory of video2
		std::string output_video1;	//directory of exporting video1
		std::string output_video2;	//directory of exporting video2
		
		cv::VideoCapture cap1;	//videocapture class of video1
		cv::VideoCapture cap2;	//videocapture class of video2
		
		cv::VideoWriter writer1; //videowriter class of video1
		cv::VideoWriter writer2;	//videowriter class of video2
		
		cv::Size cap1size;	//size of video1
		cv::Size cap2size;	//size of video2
		
		bool record_status; // 0 is not recording, 1 is recording
		double framerate;	// the current framerate
		
		cv::Mat img1;	//image in videocapture cap1
		cv::Mat img2;	//image in videocapture cap2
		
		bool calib_loaded;  	// 1 if the parameters have been loaded from XML file, 0 if they are not
		
		//CALIBRATION PARAMETERS
		std::string calib_dir;	//directory
		cv::Mat image_dims;		//input image dimensions
		cv::Mat_<double> l_intrinsic, l_distortion;	// left camera distortion parameters
		cv::Mat_<double> r_intrinsic, r_distortion;	// right camera distortion parameters
		int image_width;	
		int image_height;
		cv::Mat rotation;	// rotation angle of the stereo camera
		cv::Mat translation;	// translation vector representing distance between 2 camera
		
		//UNDISTORTION TRANSFROMATION MATRICES
		cv::Mat R1,R2,P1,P2,Q;	// matrices used to map images to undistort
		cv::Mat l_map1,l_map2,r_map1,r_map2; 
		cv::Mat undist1, undist2;	// undistorted image
};
