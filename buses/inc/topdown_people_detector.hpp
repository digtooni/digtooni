#ifndef OPENCV_
#include "opencv2/opencv.hpp"
#define OPENCV_
#endif

#include "stereo_cam.hpp"

#ifndef VECTOR_
#define VECTOR_
#include <vector>
#endif

#include <time.h> 
#include <sstream>

/*Class to store information about people in the frame*/
class people
{
	public:
		cv::Point centre; // centre of the contour detected
		std::vector<cv::Point> contours; //the contour detected
		std::vector<cv::Point> trajectory; //history of centres of the contours of this person
		int miss_count; // when the tracker cannot track this person this count will be incremented
		
};

/*Class that calculate depth map, track and count*/
class people_detector
{
	public:
		people_detector(); //constructor
		~people_detector(); //destructor
		//people_detector(const std::string &calibration_filename,sgbm parameter);
		
		bool init(int cam1, int cam2);	//initialise this object to stereo cameras
		bool init(std::string vid1, std::string vid2); //initialise this object to stereo videos
		bool init(stereo_cam cam);		//initialise this object with stereo_cam class
		
		void load_calib(std::string dir); // load calibration data from XML file
		void load_SGBM_settings(std::string dir); // load the settings for depthmap construction from XML file
		void load_tracker_settings(std::string dir); // load the parameters for tracking people in/out from XML file
		void trackandcount();	//function to track people, count and store the in/out counts in this class object (this calls detect from inside this function)
		std::vector<people> detect(); // detect people in the depthmap and returns the list of people as a vector
		void drawpeople(cv::Mat InputImage); // draw people and their trajectories on the InputImage
		
		cv::Mat getUndistortedimg(int cam);	// returns the undistorted image
		cv::Mat getCroppedUndistortedimg(int cam);	// returns the cropped undistorted image
		cv::Mat getdepth_img();	//returns the calculated depthmap
		cv::Mat getimg(int cam); // returns the image corresponding to the camera number
		cv::Size getSize(int cam);	// returns the size of the images
		cv::Size getCroppedSize(); // returns the size of the cropped undistorted images
		cv::Rect getROI();	// returns the ROI that is being used to cropped undistorted images
		cv::Mat getpeople_mask();	// the foreground mask from thesholding the depthmap 
		double getrecorded_framerate(); //	returns the frame rate the camera is recording
		bool AnyPeople();	//return 1 if there are people detected in the frame and 0 if there isn't
		
		int getIN();	// returns the current IN count
		int getOUT(); 	// returns the current OUT count
		int getOccupancy(); // returns the current occupancy
		void setOccupancy(int val); // set occupancy to the value of val
		
		bool nextframe();	//take a new frame from stereo cameras/videos
		cv::Mat CalcDepthmap(); //calculates depth map and stores it within the class
		
		void putInfoOn(cv::Mat &OutputImage, struct tm* timeinfo); // put information (dates framerate and counts) on the output image
		
		double getfps();	//returns the framerate

	private:
		stereo_cam camera;	// the stereo camera to take in stereo images from cameras/videos
		cv::Mat depth_img;	// stores the depth image
		cv::StereoSGBM sgbm; // the sgbm method depthmap constructor
		bool sgbm_ready;	// indicate if the sgbm parameters has been loaded
		cv::Rect crop_undistortedROI;	// ROI used to crop undistorted images
		
		cv::Mat people_mask;	// cvMat which store foreground mask from depthmap
		cv::Mat canny_mask;		// the canny mask of people_mask 
		std::vector<people> list;	//list of the people in the frame in vector form
		int in;		// IN count
		int out;	// OUT count
		int occupancy;  // occupancy count
		int last_action; // last action (in=1, out=-1)
		bool newaction;	 // bool to indicate if there's people waking in/out in the frame
		int counter_action;	// counts to 20 to reset last action to out
		
		clock_t start; // timer
		double recorded_framerate; // frame rate
		int frame_counter; // counter which counts to 20 to calculates framerate
		
		//SGBM_parameters
		int min_dispar,max_dispar; // disparity search range
		int SAD,P1,P2;
		int alpha; //used to scale CV_32FC to CV_8U
		
		//Tracking parameters
		int search_range;
		int detect_minY,detect_maxY;
		int depthmap_thres;
		int min_contour_size;
		int miss_tolerance;
		int min_dist_people;
		int trajectory_threshold;
};
