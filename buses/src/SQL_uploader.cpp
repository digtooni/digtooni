
#include <opencv2/opencv.hpp>

#include <string.h>
#include <iomanip>
#include "../inc/SQL_uploader.hpp"




using namespace std;
using namespace sql::mysql;
using namespace sql;

DatabaseUploader::DatabaseUploader(const std::string &database_url, const std::string &username, const std::string &password, const std::string &schema) : url(database_url), username(username), password(password), schema(schema), driver(0x0), con(0x0), prep_stmt(0x0) {

  internet_status =0;
  log_count= 0;
  time_t rawtime;
  struct tm* timeinfo;
  time (&rawtime);
  timeinfo = localtime(&rawtime);
  std::stringstream ss;
  ss << std::setw(2) << std::setfill('0') << timeinfo->tm_mday;
  ss << std::setw(2) << std::setfill('0') << timeinfo->tm_mon+1;
  ss << timeinfo->tm_year%100;
  ss << "_";
  ss << std::setw(2) << std::setfill('0') << timeinfo->tm_hour;
  ss << std::setw(2) << std::setfill('0') << timeinfo->tm_min;
  ss << std::setw(2) << std::setfill('0') << timeinfo->tm_sec; 
  std::string tmp;
  ss >> tmp;
  logfile_name = "log/bus" + tmp + ".txt";
  logfile.open(logfile_name, std::ios::out|std::ios::app);
  logfile << std::setw(20) << std::setfill(' ') << "UNIX Time";
  logfile << std::setw(20) << std::setfill(' ') << "BusID";
  logfile << std::setw(20) << std::setfill(' ') << "Latitude";
  logfile << std::setw(20) << std::setfill(' ') << "Longitude";
  logfile << std::setw(20) << std::setfill(' ') << "Passenger in";
  logfile << std::setw(20) << std::setfill(' ') << "Passenger out";
  logfile << std::setw(20) << std::setfill(' ') << "Occupancy";
  logfile << "\n";
  logfile.close();
}

DatabaseUploader::~DatabaseUploader(){

  if (res != 0x0){
    delete res;
    res = 0x0;
  }
  //if (StatementStr != 0x0){
  //  delete[] StatementStr;
  //  StatementStr = 0x0;
  //}
  if (prep_stmt != 0x0){
    delete prep_stmt;
    prep_stmt = 0x0;
  }
  if (con != 0x0){
    delete con;
    con = 0x0;
  }

}

void DatabaseUploader::WriteLogfile(data_sentence sentence)
{
	//open logfile and append the sentence to the file
	logfile.open(logfile_name, std::ios::out|std::ios::app);
	logfile << std::setw(20) << std::setfill(' ') << sentence.time;
	logfile << std::setw(20) << std::setfill(' ') << sentence.BusID;
	logfile << std::setw(20) << std::setfill(' ') << sentence.lat;
	logfile << std::setw(20) << std::setfill(' ') << sentence.lon;
	logfile << std::setw(20) << std::setfill(' ') << sentence.passenger_in;
	logfile << std::setw(20) << std::setfill(' ') << sentence.passenger_out;
	logfile << std::setw(20) << std::setfill(' ') << sentence.occupancy;
	logfile << "\n";
	logfile.close();
	log_count++;
	// if the number of entries in the log file exceeds 100 then create a new log file
	if(log_count==100)
	{
	  log_count= 0;
	  time_t rawtime;
	  struct tm* timeinfo;
	  time (&rawtime);
	  timeinfo = localtime(&rawtime);
	  std::stringstream ss;
	  ss << std::setw(2) << std::setfill('0') << timeinfo->tm_mday;
	  ss << std::setw(2) << std::setfill('0') << timeinfo->tm_mon+1;
	  ss << timeinfo->tm_year%100;
	  ss << "_";
	  ss << std::setw(2) << std::setfill('0') << timeinfo->tm_hour;
	  ss << std::setw(2) << std::setfill('0') << timeinfo->tm_min;
	  ss << std::setw(2) << std::setfill('0') << timeinfo->tm_sec; 
	  std::string tmp;
	  ss >> tmp;
	  logfile_name = "log/bus" + tmp + ".txt";
	  logfile_name = "log/bus" + tmp + ".txt";
	  logfile.open(logfile_name, std::ios::out|std::ios::app);
	  logfile << std::setw(20) << std::setfill(' ') << "UNIX Time";
	  logfile << std::setw(20) << std::setfill(' ') << "BusID";
	  logfile << std::setw(20) << std::setfill(' ') << "Latitude";
	  logfile << std::setw(20) << std::setfill(' ') << "Longitude";
	  logfile << std::setw(20) << std::setfill(' ') << "Passenger in";
	  logfile << std::setw(20) << std::setfill(' ') << "Passenger out";
	  logfile << std::setw(20) << std::setfill(' ') << "Occupancy";
	  logfile << "\n";
	  logfile.close();
	}
}
// return if upload stack is empty
bool DatabaseUploader::StackEmpty()
{
	return(upload_stack.empty());
}
//set up the statement to be uploaded
void DatabaseUploader::SetupStatement(){
  

  //StatementStr = new char[100 + (20 * parking_spaces.size())];
  // creating header
  statement = "";
  statement += "INSERT INTO Route7493(time_stamp, BusID, lat, lon, passenger_in, passenger_out, occupancy) VALUES ";
  // add each sentence to the statement
  if(!upload_stack.empty())
  {
	  for (int i = 0; i < upload_stack.size(); i++){

		std::string timestamp = std::to_string(upload_stack[i].time);
		std::string BusID = std::to_string(upload_stack[i].BusID); //
		std::string lat = std::to_string(upload_stack[i].lat);
		std::string lon = std::to_string(upload_stack[i].lon);
		std::string passenger_in = std::to_string(upload_stack[i].passenger_in);
		std::string passenger_out = std::to_string(upload_stack[i].passenger_out);
		std::string occupancy = std::to_string(upload_stack[i].occupancy);
		if (i != 0)
		  statement += ", (";
		else
		  statement += "(";
		statement += timestamp;
		statement += ",";
		statement += BusID;
		statement += ",";
		statement += lat;
		statement += ",";
		statement += lon;
		statement += ",";
		statement += passenger_in;
		statement += ",";
		statement += passenger_out;
		statement += ",";
		statement += occupancy;
		statement += ")";
		
	  }
	  //std::cout << statement <<std::endl;
  }

}
//add sentence to the stack
void DatabaseUploader::addSentence(data_sentence sentence)
{
	upload_stack.push_back(sentence);
}
// ping google.co.uk for internet status
void DatabaseUploader::update_internet_status()
{
	std::string target = (std::string) "ping -q -c 1 "+"www.google.co.uk > /dev/null 2>&1"; //XML pinging URL
	int ret = std::system(target.c_str());
	if(ret==0)
		internet_status = 1;
	else
		internet_status = 0;
}

// get function for internet status	
bool DatabaseUploader::getinternet_status()
{
	  return internet_status;
}
// try to upload data
void DatabaseUploader::Upload() {
	try {

    sql::Driver *driver = sql::mysql::get_driver_instance();
    sql::Connection *con = driver->connect("database.server.net", "user", "password");
    //driver->connect(url, username, password);
    // Connect to the MySQL test database
    con->setSchema("buses");

    sql::PreparedStatement *prep_stmt = con->prepareStatement(statement);
		prep_stmt->execute();
		
    std::cout << "DATA SENT" << std::endl;
	upload_stack.clear();
    //if (StatementStr){
    // std::cout << "deleting StatementStr" << std::endl;
    //  delete[] StatementStr;
    // StatementStr = 0x0;
    //  std::cout << "done" << std::endl;
    //}
    
    if (prep_stmt != 0x0){
      delete prep_stmt;
      prep_stmt = 0x0;
    }
    if (con != 0x0){
      delete con;
      con = 0x0;
    }
    }
	catch (sql::SQLException &e) {
		cout << "# ERR: SQLException in " << __FILE__;
		cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
		cout << "# ERR: " << e.what();
		//cout << " (MySQL error code: " << e.getErrorCode();
		//cout << ", SQLState: " << e.getSQLState() << " )" << endl;
  }

}
/*
void DatabaseUploader::SQLDataUpload(const std::vector<ParkingSpace> &detected_spaces, int frame_number, int frames_averaged, time_t time_stamp){

  if (detected_spaces.size() == 0) return;
    
  SetupStatement(detected_spaces, frame_number, frames_averaged, time_stamp);

  std::thread SQL_thread(SQLDataUploadThread, std::ref(statement));

  SQL_thread.join();
  //SQLDataUploadThread(StatementStr);

  //SQL_thread.detach();

}*/
