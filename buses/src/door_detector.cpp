#include "../inc/door_detector.hpp"

door_detector::door_detector()
{
}

door_detector::~door_detector()
{
	
}
//initialise the class
bool door_detector::init(std::string cascade_dir, cv::Mat img, cv::Rect door_ROI, cv::Size door_minSize, cv::Size door_maxSize, int thres)
{
	// load the cascade file
	door_classifier.load(cascade_dir);
	if(door_classifier.empty())
		return 0;
	//initialise the door_ROI and size search range
	ROI = door_ROI;
	minSize = door_minSize;
	maxSize = door_maxSize;
	//try to detect the door if detected, set status to close if not set status to open
	door_classifier.detectMultiScale(img(ROI),door,1.1,5,CV_HAAR_DO_CANNY_PRUNING|CV_HAAR_SCALE_IMAGE|CV_HAAR_DO_ROUGH_SEARCH,minSize,maxSize);
	if(!door.empty()&&door.size()>0)
	{
		status = CLOSE;
		miss_count = 0;
	}
	else
	{
		status = OPEN;
		miss_count = 0;
	}
	//initialise the threshold value
	threshold = thres;
	//
}

door_status_t door_detector::getdoor_status()
{
	return(status);
}
//try to detect the door in img and update the door status
void door_detector::update_door_status(cv::Mat img)
{
	//clear the detected door boxes in last frame
	door.clear();
	//try to detect
	door_classifier.detectMultiScale(img(ROI),door,1.1,3,CV_HAAR_DO_CANNY_PRUNING|CV_HAAR_SCALE_IMAGE|CV_HAAR_DO_ROUGH_SEARCH,minSize,maxSize);
	// if detected and the current status is open then increment the miss_count, if the current status is close reset the miss count
	if(!door.empty()&&door.size()>0)
	{
		if(status == OPEN)
			miss_count++;
		else if(status == CLOSE)
			miss_count = 0;
		//status = CLOSE;
		//miss_count = 0;
	}
	// if not detected and the current status is close then increment the miss_count, if the current status is open reset the miss count
	else
	{
		if(status==CLOSE)
			miss_count++;
		else if(status == OPEN)
			miss_count = 0;
		//status = OPEN;
		//miss_count++;
	}
	// if miss count goes above the threshold then toggle the status
	if(miss_count>threshold)
	{
		if(status==OPEN)
			status = CLOSE;
		else if(status == CLOSE)
			status = OPEN;
		miss_count = 0;
	}
}
// draw boxes around the detected door
void door_detector::draw_door(cv::Mat &img)
{
	for(int i=0;i<door.size();i++)
		cv::rectangle(img,door[i]+ROI.tl(),cv::Scalar(0,0,255));
}
