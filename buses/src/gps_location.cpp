#include "../inc/gps_location.hpp"
#include <stdlib.h>
#include <iostream>
//extract information from gps data
void gps_service::extract_data(struct gps_data_t* collect)
{
    if (collect->set & TIME_SET)
    {
		timestamp = collect->fix.time;
		time_tm = localtime(&timestamp);
	}
    if (collect->set & LATLON_SET)
    {
		lat = collect->fix.latitude;
		lon =  collect->fix.longitude;
	}
    if (collect->set & SPEED_SET)
    {
		speed = collect->fix.speed;
	}
 //
}
//CONSTRUCTOR
gps_service::gps_service()
{
	//construct gpsmm class connected to localhost and using Default GPSD port
	static gpsmm tmp("localhost", DEFAULT_GPSD_PORT);
	gps_rec = &tmp;
	//check if gpsd is running
	if (gps_rec->stream(WATCH_ENABLE|WATCH_JSON) == NULL) {
       gpsd_init = false; // NO GPSD RUNNING
    }
    else
		gpsd_init = true;
	//initialise everything to 0
	lat = 0;
	lon = 0;
	speed = 0;
	timestamp = 0;
	time_tm = NULL;
	//fetch the data once
	refresh();
}
//DESTRUCTOR
gps_service::~gps_service()
{
	//free(gps_rec);
	free(time_tm);
}

double gps_service::getlat()
{
	return(lat);
}
double gps_service::getlon()
{
	return(lon);
}
double gps_service::getspeed()
{
	return(speed);
}
tm* gps_service::getdate()
{
	return time_tm;
}
time_t gps_service::gettime()
{
	return timestamp;
}
//refresh the data from the antenna
int gps_service::refresh() 
{
	// try to get gpsd to run if it is not currently running
	if(!gpsd_init)
	{
		if (gps_rec->stream(WATCH_ENABLE|WATCH_JSON) == NULL)
		{
			gpsd_init = false; // NO GPSD RUNNING
		}
		else
		{
			gpsd_init = true;
		}
		return 1;
	}
	// if it is already running then wait for new signal and extract data from the incoming signal
	else
	{
		if (!gps_rec->waiting(5000000))
			return 2; // NO SIGNAL COMING IN

		if ((newdata = gps_rec->read()) == NULL) {
			return 3; //READ ERROR
		} 
		else 
		{
			extract_data(newdata);
		}
		//free(newdata);
		return 0; // SUCCESS REFRESH
	}
}
