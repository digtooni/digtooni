#include "opencv2/opencv.hpp"
#include "../inc/topdown_people_detector.hpp"
#include "../inc/door_detector.hpp"
#include "../inc/gps_location.hpp"
#include "../inc/SQL_uploader.hpp"
#include <iostream>
#include <vector>
#include <ctime>
#include <thread>
//
/*void gps_thread(gps_service* location)
{
	bool err =0;
	clock_t timer=clock();
	while(!err)
	{
		int retval = location->refresh();
		if(retval == 1||retval ==3)
			system("killall gpsd");
	}
}*/
void counter_thread(people_detector* detector,cv::VideoWriter* vid_out,gps_service* location,bool display_enable,bool GPS_enable, bool export_vid)
{
	//init the output image to be 4 frames of the image size
	cv::Size dispSize(detector->getCroppedSize().width *2+150,detector->getCroppedSize().height *2);
	cv::Mat disp(dispSize, CV_8UC3);
	//set the ROI for each frame
	cv::Rect frame1(cv::Point(0,0), (detector->getROI()).size());
	cv::Rect frame2(frame1.tl()+cv::Point(frame1.width,0), frame1.size());
	cv::Rect frame3(frame1.tl()+cv::Point(0,frame1.height), frame1.size());
	cv::Rect frame4(frame1.br(), frame1.size());
	bool debug = 0;
	bool already_reset = 0;
	int status = 0; //0 = on the route, 1 wolverton, 2 depot , 3 = bletchly
	clock_t occupancy_timer = clock();
	clock_t start=clock();
	while(detector->nextframe())
	{
		//check if the bus is at one of the final stops if yes reset occupancy to 0 after 15 seconds
		if(location->getlat()>52.62&&location->getlat()<52.063&&location->getlon()>(-0.8085)&&location->getlon()<(-0.8075))
		{
			if(status ==1)
			{
				if((clock()-occupancy_timer)/CLOCKS_PER_SEC > 15&&!already_reset)
				{
					detector->setOccupancy(0);
					already_reset = 1;
				}
			}
			else
			{
				status = 1;
				occupancy_timer = clock();
			}
		}
		else if(location->getlat()>52.06&&location->getlat()<52.067&&location->getlon()>(-0.8213)&&location->getlon()<(-0.8203))
		{
			if(status ==2)
			{
				if((clock()-occupancy_timer)/CLOCKS_PER_SEC > 15&&!already_reset)
				{
					detector->setOccupancy(0);
					already_reset = 1;
				}
			}
			else
			{
				status = 2;
				occupancy_timer = clock();
			}
		}
		else if(location->getlat()>51.990&&location->getlat()<52.000&&location->getlon()>(-0.739)&&location->getlon()<(-0.729))
		{
			if(status ==3)
			{
				if((clock()-occupancy_timer)/CLOCKS_PER_SEC > 15&&!already_reset)
				{
					detector->setOccupancy(0);
					already_reset = 1;
				}
			}
			else
			{
				status = 3;
				occupancy_timer = clock();
			}
		}
		else
		{
			if(status ==0)
			{
				if((clock()-occupancy_timer)/CLOCKS_PER_SEC > 15&&already_reset)
				{
					already_reset = 0;
				}
			}
			else
			{
				status = 0;
				occupancy_timer = clock();
			}
		}
		
		//detect and track using depth map
        detector->trackandcount();
        //copy camera 1 to frame 1
		detector->getCroppedUndistortedimg(1).copyTo(disp(frame1));
		//copy camera 2 to frame 2
		detector->getCroppedUndistortedimg(2).copyTo(disp(frame2));
		// draw people and their trajectories on frame 1
		detector->drawpeople(disp(frame1));
		//convert depth map to RGB
		cv::Mat depth_col,mask_col;
		cv::cvtColor(detector->getdepth_img(),depth_col,CV_GRAY2BGR);
		cv::cvtColor(detector->getpeople_mask(),mask_col,CV_GRAY2BGR);
		//set frame 3 to be raw depthmap and frame 4 to be processed depthmap
		depth_col.copyTo(disp(frame3));
		mask_col.copyTo(disp(frame4));
		//put information on output display
		detector->putInfoOn(disp,location->getdate());
		//if exporting the video write the frame to the export directory
		if(export_vid&&vid_out->isOpened())
			vid_out->write(disp);
		// if display is enable show the image on the screen
		if(display_enable)
		{
			cv::imshow("bus",disp);
			if(debug) // if debug need to press a key to continue to next frame
			{
				if(cv::waitKey()=='d')
					debug = !debug;
			}
			switch(char c = cv::waitKey(2))
			{
				case 'd':	debug = !debug;
							break;
				case 27	:	exit(0);
							break;
				default	:	break;
			}
		}
		// if not displaying then print out the in formation every 10 seconds
		else if(!export_vid&&(clock()-start)/CLOCKS_PER_SEC > 10)
		{
			std::cout << "frame rate: " << detector->getrecorded_framerate() << std::endl;
			std::cout << "IN: " << detector->getIN() << "\tOUT: " << detector->getOUT() << "\tOCCUPANCY: " << detector->getOccupancy() << std::endl;
			if(GPS_enable)
			{
				std::cout << "lat: " << location->getlat() << std::endl;
				std::cout << "lon: " << location->getlon() << std::endl;
			}
			std::cout << "*******************************************" << std::endl;
			start = clock();
		}
	}
	std::cout << "End of capture:\nIN: " << detector->getIN() << "\tOUT: " << detector->getOUT() << std::endl;
	exit(1);
}
 
void SQL_thread(people_detector* detector, gps_service* location, DatabaseUploader* uploader,bool GPS_enable, bool logging_enable, bool SQL_upload_enable, int BusID, int GPS_upload_freq, int min_inactive_period_before_upload )
{
	//timer initialisation
	clock_t timer = clock();
	clock_t timer_GPS = clock();
	//init data_sentence
	data_sentence SQL_sentence;
	SQL_sentence.time = location->gettime();
	SQL_sentence.BusID = 1;
	SQL_sentence.lat  = location->getlat();
	SQL_sentence.lon  = location->getlon();
	SQL_sentence.passenger_in = detector->getIN();
	SQL_sentence.passenger_out = detector->getOUT();
	SQL_sentence.occupancy= detector->getIN()-detector->getOUT();	
	
	int last_updated_in=0;
	int last_updated_out=0;
	for(int round =1; round>0;round++)
	{
		//check if detector detect anything
		if(!detector->AnyPeople())
		{
			//std::cout << SQL_sentence.time;
			//make a data sentence every 30 seconds to update GPS location
			if((clock()-timer_GPS)/CLOCKS_PER_SEC>GPS_upload_freq)
			{
				data_sentence tmp;
				tmp.time = location->gettime();
				tmp.BusID = BusID;
				tmp.lat  = location->getlat();
				tmp.lon  = location->getlon();
				tmp.passenger_in = 0;
				tmp.passenger_out = 0;
				tmp.occupancy= -1;
				if(logging_enable)
				{
					//std::cout <<"Logging location" <<std::endl ; 
					uploader->WriteLogfile(tmp); // log the location
				}
				//push into upload stack
				uploader->addSentence(tmp);
				//check internet status and tries to upload
				uploader->update_internet_status();
				if(SQL_upload_enable&&uploader->getinternet_status()&&!uploader->StackEmpty())
				{
					uploader->SetupStatement();
					
					//try upload and remove from stack if success
					uploader->Upload();
				}
				//reset timer
				timer_GPS=clock();	
			}
			//if nothing has been detected for the last 10 seconds put the information into a data sentence and tries to upload
			if((clock()-timer)/CLOCKS_PER_SEC > min_inactive_period_before_upload)
			{
				//save into log file if there's someone get on/off the bus
				if(SQL_sentence.passenger_in>0||SQL_sentence.passenger_out>0)
				{
					if(logging_enable)
					{
						std::cout <<"Logging passenger count" <<std::endl ; 
						uploader->WriteLogfile(SQL_sentence);
					}
					//push into upload stack
					uploader->addSentence(SQL_sentence);
					SQL_sentence.passenger_in = 0;
					SQL_sentence.passenger_out = 0;
					last_updated_in = detector->getIN();
					last_updated_out = detector->getOUT();
				}
				//check upload stack if there's anyting then try to upload
				//
				uploader->update_internet_status();
				if(SQL_upload_enable&&uploader->getinternet_status()&&!uploader->StackEmpty())
				{
					uploader->SetupStatement();
					
				//try upload and remove from stack if success
					uploader->Upload();
				}
			}
			else
			{
				// if not 10 seconds yet then refresh the info on SQL_sentence
				SQL_sentence.time = location->gettime();
				SQL_sentence.BusID = BusID;
				SQL_sentence.lat  = location->getlat();
				SQL_sentence.lon  = location->getlon();
				SQL_sentence.passenger_in = detector->getIN()-last_updated_in;
				SQL_sentence.passenger_out = detector->getOUT()-last_updated_out;
				SQL_sentence.occupancy= -1;
			}
		}
		else
		{
			//if yes reset timer
			timer = clock();
			
			
			/*SQL_sentence.time = location->gettime();
			SQL_sentence.BusID = 1;
			SQL_sentence.lat  = location->getlat();
			SQL_sentence.lon  = location->getlon();
			SQL_sentence.passenger_in = detector->getIN()-last_updated_in;
			SQL_sentence.passenger_out = detector->getOUT()-last_updated_out;
			SQL_sentence.occupancy= -1;*/
			//std::cout <<"not detecting \n";
			//uploader->update_internet_status();
		}
		//GPS service every 70 loops
		if(GPS_enable&&round ==70)
		{
			round=1;
			int retval = location->refresh();
			if(retval == 1||retval == 3)
				system("killall gpsd"); // kill gpsd if it returns bad values
		}
	}
}
 // main function
int main(int argc, char** argv)
{

	bool display_enable, export_vid,GPS_enable, logging_enable, SQL_upload_enable;
	int BusID,GPS_upload_freq,min_inactive_period_before_upload;
	cv::FileStorage fs;
	//load settings from xml file
	try
	{
		fs.open("settings/general_settings.xml", cv::FileStorage::READ);
		
		fs["display_enable"] >> display_enable;
		fs["export_video"]>>export_vid;
		fs["GPS_enable"]>>GPS_enable;		
		fs["Logging"]>>logging_enable;
		fs["Upload_data"]>>SQL_upload_enable;	
		fs["BusID"] >> BusID;
		fs["GPS_upload_freq"] >> GPS_upload_freq;
		fs["Min_inactive_period_before_upload"]	>> min_inactive_period_before_upload;
	}
	catch(cv::Exception& e)
	{
		std::cerr << "Error while reading from general_settings.xml file.\n" << e.msg << "\n";
		exit(0);
		//SAFE_EXIT();
	}	
	fs.release();
	
	// constuction of each class objects
	gps_service location;
	DatabaseUploader uploader("database.server.net", "user", "password","buses");
	cv::VideoWriter vid_out;
	people_detector detector;
	detector.load_calib("settings/calib.xml");
	detector.load_SGBM_settings("settings/SGBM_settings.xml");
	detector.load_tracker_settings("settings/tracker_settings.xml");
	
	//load videos if there is an argument
	if(argc ==2)
	{
		std::string idx(argv[1]);
		std::string in1 = "Recorded_videos/camera1_"+idx+".avi";
		std::string in2 = "Recorded_videos/camera2_"+idx+".avi";
		std::cout << "Processing video " + idx << std::endl;
		detector.init(in1,in2);
		if(export_vid&&!vid_out.open("/home/Bus_"+idx+".avi", CV_FOURCC('M','J','P','G'),7.0,cv::Size(630,320)))
		std::cout << "cannot initialise video writer" << std::endl;
	}
	else if(argc==3&&isdigit(argv[1][0])&&isdigit(argv[2][0]))
	{
		//load the specified camera corresponding to the ID
		detector.init(argv[1][0] -'0',argv[2][0] -'0');
	}
	else
		//load camera 0 and 1 by default
		detector.init(0,1);
	
	//start multithreading
	std::thread thread1(counter_thread,&detector,&vid_out,&location,display_enable,GPS_enable,export_vid);
	//std::thread thread2(gps_thread,&location);
	std::thread thread3(SQL_thread,&detector, &location, &uploader,GPS_enable,logging_enable,SQL_upload_enable,BusID,GPS_upload_freq,min_inactive_period_before_upload);
	thread3.join();
	thread1.join();
	//if(GPS_enable)
		//thread2.join();
	
	//end of multithreading
	return 1;
}
