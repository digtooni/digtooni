#include "../inc/stereo_cam.hpp"
//
stereo_cam::stereo_cam()
{
	//Default values when initialised
	framerate = 10;
	record_status = false;
	calib_loaded = false;
}
stereo_cam::~stereo_cam()
{
}
//initialise the class to the stereo camera
bool stereo_cam::init_camera(int camera1, int camera2)
{
	//init camera1
	bool tmp1 = cap1.open(camera1);
	cap1.set(CV_CAP_PROP_FRAME_WIDTH,320);
	cap1.set(CV_CAP_PROP_FRAME_HEIGHT,240);
	cap1.set(CV_CAP_PROP_FPS,7);
	//cap1.set(CV_CAP_PROP_FOURCC,CV_FOURCC('M','J','P','G'));
	
	//init camera2
	bool tmp2 = cap2.open(camera2);
	cap2.set(CV_CAP_PROP_FRAME_WIDTH,320);
	cap2.set(CV_CAP_PROP_FRAME_HEIGHT,240);
	cap2.set(CV_CAP_PROP_FPS,7);
	//cap2.set(CV_CAP_PROP_FORMAT,);
	
	// if any failed return 0;
	if(!tmp1||!tmp2)
		return 0;
	
	//clear the directory of input videos(in case when the class was initialised to read from video)
	input_video1.clear();
	input_video2.clear();
	
	//fetch image from camera1 and store in img1
	cv::Mat frame1;
	cap1 >> frame1;
	if(frame1.empty())
		return 0;
	frame1.copyTo(img1);					
	cap1size = img1.size();
	
	//fetch image from camera2 and store in img2
	cv::Mat frame2;
	cap2 >> frame2;
	if(frame2.empty())
		return 0;
	frame2.copyTo(img2);
	cap2size = img2.size();
	
	return 1;
}
//initialise class to the videos file
bool stereo_cam::init_videoinput(std::string file1, std::string file2)
{
	//open file1 and file2 if any fails return 0
	bool tmp1 = cap1.open(file1);
	bool tmp2 = cap2.open(file2);
	if(!tmp1||!tmp2)
		return 0;
	//store the directories in input_video1 and input_video2
	input_video1 = file1;
	input_video2 = file2;
	
	//fetch an image from video1
	cv::Mat frame1;
	cap1 >> frame1;
	if(frame1.empty())
		return 0;
	frame1.copyTo(img1);					
	cap1size = img1.size();
	
	//fetch an image from video2
	cv::Mat frame2;
	cap2 >> frame2;
	if(frame2.empty())
		return 0;
	frame2.copyTo(img2);
	cap2size = img2.size();
	
	return 1;
}
//initialise videowriter to export video to out1,out2
bool stereo_cam::init_export(std::string out1, std::string out2)
{
	// if the camera has not been initialised return 0
	if(!cap1.isOpened()||!cap2.isOpened())
		return 0;
	writer1.open(out1, CV_FOURCC('M','J','P','G'), framerate ,cap1size);
	writer2.open(out2, CV_FOURCC('M','J','P','G'), framerate ,cap2size);
	return 1;
}
//combined initialiser for both camera and video exporter
bool stereo_cam::init(int camera1, int camera2,std::string out1, std::string out2)
{
	init_camera(camera1,camera2);
	bool retval = init_export(out1,out2);
	return (retval);
}
void stereo_cam::start_record()
{
	record_status = 1;
}
void stereo_cam::stop_record()
{
	record_status = 0;
}
void stereo_cam::setfps(int fps)
{
	framerate = fps;
}
//fetch next frame from cam/vid and export if videowriter has been initialised
bool stereo_cam::nextframe()
{
	//fetch next img from cam 1
	cv::Mat frame1;
	cap1 >> frame1;
	//cap1 >> blank;
	if(frame1.empty())
		return 0;
	frame1.copyTo(img1);					
	cap1size = img1.size();
	
	//fetch next img from cam 2
	cv::Mat frame2;
	cap2 >> frame2;
	//cap2 >> blank;
	if(frame2.empty())
		return 0;
	frame2.copyTo(img2);
	cap2size = img2.size();
		
	// if recording and videowriters have been initialised then export videos
	if(record_status&&writer1.isOpened()&&writer2.isOpened())
	{
		writer1 << img1;
		writer2 << img2;
	}	
	
	// if calibration file has been loaded then undistort images and stored them in undist 1,2
	if(calib_loaded)
	{
		cv::remap(img1,undist1,l_map1,l_map2,cv::INTER_LINEAR);
		cv::remap(img2,undist2,r_map1,r_map2,cv::INTER_LINEAR);
	}
	return 1;
}
// load calibration parameters from XML file
void stereo_cam::load_calib(std::string dir)
{
	cv::FileStorage fs;  

  try{
    fs.open(dir.c_str(), cv::FileStorage::READ);

    fs["Image_Dimensions"] >> image_dims;
    image_width = image_dims.at<int>(0);
    image_height = image_dims.at<int>(1);

    fs["Left_Camera_Matrix"] >> l_intrinsic;
    fs["Left_Distortion_Coefficients"] >> l_distortion;

    fs["Right_Camera_Matrix"] >> r_intrinsic;
    fs["Right_Distortion_Coefficients"] >> r_distortion;
    
	rotation = cv::Mat(3,3,CV_64FC1);
	translation = cv::Mat(3,1,CV_64FC1);
    fs["Extrinsic_Camera_Rotation"] >> rotation;
   
    fs["Extrinsic_Camera_Translation"] >> translation;


  }
  catch(cv::Exception& e){

    std::cerr << "Error while reading from camara calibration file.\n" << e.msg << "\n";
	exit(0);
    //SAFE_EXIT();
	}
	//
	calib_dir = dir;
	calib_loaded =1;
	cv::stereoRectify(l_intrinsic,l_distortion,r_intrinsic,r_distortion,cv::Size(image_width,image_height),rotation,translation,R1,R2,P1,P2,Q);
	cv::initUndistortRectifyMap(l_intrinsic,l_distortion,R1,P1,cv::Size(320,240),CV_32FC1,l_map1,l_map2);
	cv::initUndistortRectifyMap(r_intrinsic,r_distortion,R2,P2,cv::Size(320,240),CV_32FC1,r_map1,r_map2);
}

cv::Mat stereo_cam::getUndistortedimg(int cam)
{
	if(cam ==1)
		return undist1;
	else if(cam ==2)
		return undist2;
	else
		return cv::Mat();
}
bool stereo_cam::getrecord_status()
{
	return (record_status);
}
cv::Size stereo_cam::getimgsize(int cam)
{
	if(cam==1)
		return cap1size;
	else if (cam==2)
		return cap2size;
	else
		return cv::Size();
}
cv::Mat stereo_cam::getimg(int cam)
{
	if(cam==1)
		return img1;
	else if (cam==2)
		return img2;
	else
		return cv::Mat();
}
