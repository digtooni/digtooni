#include "../inc/stereo_cam.hpp"
#include <iostream>
#include <sstream>
#include <time.h>
// recorder main function
int main(int argc, char** argv)
{
	if(argc>3||argc ==2)
	{
		std::cout << "Usage: ./recorder [cam1/vid1_dir][cam2/vid2_dir]" << std::endl;
		exit(0);
	}
	
	stereo_cam recorder;
	//if no argument load camera 0 and 1
	bool load;
	if(argc == 1)
		load = recorder.init_camera(0,1);
	//else load camera number specified or video specified
	else if(argc == 3&&strlen(argv[1])==1&&isdigit(argv[1][0])&&strlen(argv[2])==1&&isdigit(argv[2][0]))
		load = recorder.init_camera(argv[1][0]-'0', argv[2][0] -'0');
	else
		load = recorder.init_videoinput(argv[1], argv[2]);
	// if failed to load, exit
	if(!load)
	{
		std::cout << "Cannot initialise capture..." << std::endl;
		exit(0);
	}
	// call user to input starting index number
	int idx;
	std::cout << "Input starting index number: ";
	std::cin >> idx;
	std::stringstream ss;
	ss << idx;
	std::string index;
	ss >> index;
	//initialised exporting directories
	std::string out1 = "Recorded_videos/camera1_"+index+".avi";
	std::string out2 = "Recorded_videos/camera2_"+index+".avi";
	if(!recorder.init_export(out1,out2))
	{
		std::cout << "output directory not found..." << std::endl;
		exit(0);
	}
	clock_t start;
	int frame_count=0;
	bool display = 1;
	double recorded_framerate =0;
	//main loop
	while(recorder.nextframe())
	{
		
		if(display)
		{
			//display the images from the camera
			cv::Mat img1 = recorder.getimg(1);
			cv::Mat img2 = recorder.getimg(2);
			if(recorder.getrecord_status())
			{
				cv::putText(img1,"RECORDING", cv::Point(0,15),cv::FONT_HERSHEY_PLAIN, 1, cv::Scalar(50,0,255));
				cv::putText(img2,"RECORDING", cv::Point(0,15),cv::FONT_HERSHEY_PLAIN,1, cv::Scalar(50,0,255));
			}
			cv::imshow("CAMERA1",img1);
			cv::imshow("CAMERA2",img2);
			cv::destroyWindow("Blank");
		}
		else
		{
			cv::namedWindow("Blank");
			cv::destroyWindow("CAMERA1");
			cv::destroyWindow("CAMERA2");
		}
		
		char c = cv::waitKey(50);
		switch(c)
		{
			case 'd': //toggle display on/off
						if(display)
						{
							display=0;
						}
						else
						{
							display=1;
							
						}
			case 's': //toggle between start/stop recording
						if(recorder.getrecord_status())
						{
							recorder.stop_record();
						}
						else
						{
							recorder.start_record();
							//increment the index number
							if(idx>0)
							{
								std::stringstream ss1;
								ss1 << idx;
								std::string index1;
								ss1 >> index1;
								out1 = "Recorded_videos/camera1_"+index1+".avi";
								out2 = "Recorded_videos/camera2_"+index1+".avi";
								idx++;
								if(!recorder.init_export(out1,out2))
								{
									std::cout << "output directory not found..." << std::endl;
									exit(1);
								}
							}
						}
						break;
			case 'p': //pause
						switch(cv::waitKey())
						{}
						break;
			case 27 : 	//exit with escape key
						exit(1);
						break;
		}
		// time the process to calculate framerate
		if(frame_count==20)
		{
			frame_count=1;
			clock_t diff = clock()-start;
			recorded_framerate = 20.0*CLOCKS_PER_SEC/diff;
			//std::cout << recorded_framerate<< std::endl;
			start = clock();
		}
		else
		{
			frame_count++;
		}
	}
	return 1;
}

