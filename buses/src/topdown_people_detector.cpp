#include "../inc/topdown_people_detector.hpp"
#include <iostream>

//CONSTRUCTOR
people_detector::people_detector()
{
	//initialise the parameters inside the class
	frame_counter = 0;
	recorded_framerate = 0;
	start = clock();
	if(!list.empty())
		list.clear();
	in = 0;
	out = 0;
	occupancy = 0;
	sgbm_ready = false;
	last_action = 1; //in=1, out=-1
	newaction = 0;
	counter_action=0;
}
//DESTRUCTOR
people_detector::~people_detector()
{
	
}
//initialise the class to cam1 cam2
bool people_detector::init(int cam1, int cam2)
{
	return(camera.init_camera(cam1,cam2));
}
//initialise the class to vid1, vid2
bool people_detector::init(std::string vid1, std::string vid2)
{
	return(camera.init_videoinput(vid1,vid2));
}
//initialise using stereo_cam class
bool people_detector::init(stereo_cam cam)
{
	stereo_cam* ptr;
	ptr = &camera;
	*ptr = cam;
	free(ptr);
	return 1;
}
//load calibration file
void people_detector::load_calib(std::string dir)
{
	camera.load_calib(dir);
}
//load SGBM parameters
void people_detector::load_SGBM_settings(std::string dir)
{	
	cv::FileStorage fs;  
	try
	{
		fs.open(dir.c_str(), cv::FileStorage::READ);
		
		fs["crop_size"] >> crop_undistortedROI;
		fs["mininum_disparity"]>>min_dispar;
		fs["maximum_disparity"]>>max_dispar;
		fs["SAD_windows"] >> SAD;
		fs["P1"] >> P1;
		fs["P2"] >> P2;
		fs["Alpha"] >> alpha;
		
	}
	catch(cv::Exception& e)
	{
		std::cerr << "Error while reading from sgbm_settings.xml file.\n" << e.msg << "\n";
		exit(0);
		//SAFE_EXIT();
	}	
	sgbm = cv::StereoSGBM(min_dispar*16,max_dispar*16,SAD,P1*1*SAD*SAD,P2*1*SAD*SAD,0);
	sgbm_ready = 1;
}
//load traker settings
void people_detector::load_tracker_settings(std::string dir)
{
	cv::FileStorage fs;
	try
	{
		fs.open(dir.c_str(), cv::FileStorage::READ);
		
		fs["search_range"] >> search_range;
		fs["detect_minY"]>>detect_minY;
		fs["detect_maxY"]>>detect_maxY;
		fs["depthmap_thres"] >> depthmap_thres;
		fs["min_contour_size"] >> min_contour_size;
		fs["miss_tolerance"] >> miss_tolerance;
		fs["min_dist_people"] >> min_dist_people;
		fs["trajectory_threshold"] >> trajectory_threshold;
	}
	catch(cv::Exception& e)
	{
		std::cerr << "Error while reading from sgbm_settings.xml file.\n" << e.msg << "\n";
		exit(0);
		//SAFE_EXIT();
	}	
}
// Track and count people getting on/off
void people_detector::trackandcount()
{
	//detect people in the new frame
	std::vector<people> candid = detect();
	//compare candid (people in new frame) and list(people in last frame) to track people
	if(!list.empty())
	{
		//setup a blank vector to store the new centre of the people detected in the last frame
		std::vector<cv::Point> newcentre;
		std::vector<int> num;
		for(int i=0; i<list.size();i++)
		{
			newcentre.push_back(cv::Point(0,0));
			num.push_back(0);
		}
		if(!candid.empty())
		{
		for(int i=0; i<candid.size();i++)
		{
			//search for the closest centre of the detected person in the old frame
			int min_inlist = -1;
			double min_dist = 200;
			for(int j=0; j<list.size();j++)
			{
				//Performing velocity projection
				double alpha = 0.7;
				cv::Point proj = list[j].centre;
				if(!list[j].trajectory.empty())
					proj = (1+alpha)*list[j].centre - alpha*list[j].trajectory[list[j].trajectory.size()-1] ;
				double dist = cv::norm(candid[i].centre - list[j].centre);
				//double dist = cv::norm(centres[i] - people[j].centre);
				//double dist = sqrt(pow(2*(centres[i].x - people[j].centre.x),2)+pow(centres[i].y - people[j].centre.y,2));
				
				//check direction of trajectory 
				bool direction_correct = 1;
				//if trajectory is not empty check that there is no track back
				if(!list[j].trajectory.empty())
				{
					double last_movement = list[j].centre.y - list[j].trajectory[list[j].trajectory.size()-1].y;
					double current_movement =candid[i].centre.y - list[j].centre.y;
					//direction_correct = (last_movement*current_movement>=0||(std::abs(last_movement) <20 ||std::abs(current_movement)<20)&&std::abs(current_movement)<130);
					direction_correct = (std::abs(current_movement)<130);
				}
				//if the distance to the old centre or projection point is within seach range choose the closest one
				if((dist<search_range||(cv::norm(candid[i].centre - proj)<110&&direction_correct))&&dist<min_dist)
				{
						min_dist = dist;
						min_inlist = j;
				}
			}
			// if there is a matching centre in the last frame then assign the new one to the newcentre vector and erase that point on vector candid
			if(min_dist <200)
			{
				newcentre[min_inlist] = newcentre[min_inlist] + candid[i].centre;
				//centres[i].x /= 2;
				//centres[i].y /= 2;
				num[min_inlist]++;
				candid.erase(candid.begin()+i);
				i--;
			}
		}
		}
		//update the list of people in the frame
		for(int i=0; !list.empty()&&i<list.size();i++)
		{
			//assign new centre and push the old centre to trjectory
			if(num[i]>0)
			{
				list[i].miss_count = 0;
				newcentre[i].x /= num[i];
				newcentre[i].y /= num[i];
				list[i].trajectory.push_back(list[i].centre);
				list[i].centre = newcentre[i];
				
			}
			else
			{
				//increment miss_count and count if miss more than tolerance
				list[i].miss_count++;
				if(list[i].miss_count > miss_tolerance)
				{
					if(list[i].trajectory.empty())
					{
						/*if(last_action==1)
						{
							in++;
							newaction = 1;
						}
						else
						{
							out++;
							newaction = 1;
						}*/
					}
					//walking from bottom to top and end up on the upper half of the image
					else if(list[i].centre.y-list[i].trajectory[0].y <= -60&&list[i].centre.y<80)
					{
						in++;
						occupancy++;
						last_action = 1;
						newaction = 1;
					}
					//walking from top to bottom and end up on the lower half of the image
					else if (list[i].centre.y-list[i].trajectory[0].y >= 20 && list[i].centre.y>80)
					{	
						out++;
						occupancy--;
						last_action = -1;
						newaction = 1;
					}
					//remove the person
					list.erase(list.begin()+i);
					i--;
				}
				// no trajectory so assumed to be noise
				else if(list[i].trajectory.empty())
				{}
				//check for IN/OUT
				else if(list[i].centre.y - list[i].trajectory[0].y >= trajectory_threshold)
				{
					out++;
					occupancy--;
					last_action = -1;
					newaction = 1;
					list.erase(list.begin()+i);
					i--;
				}
				else if (list[i].trajectory[0].y - list[i].centre.y >= trajectory_threshold)
				{
					in++;
					occupancy++;
					last_action = 1;
					list.erase(list.begin()+i);
					newaction = 1;
					i--;
				}
				
				
			}
			
		
		}
		}
		//if there are points left on candid vector (newly detected people)
		if(!candid.empty())
		{
			// record detected coordinates
			for(int i=0; i<candid.size();i++)
			{
				if(candid[i].centre.y < detect_minY || candid[i].centre.y>detect_maxY) // XML
				{
					//add to the list of detected people
					list.push_back(candid[i]);
				}
			}
		}
}
//function to detect people
std::vector<people> people_detector::detect()
{
	//setup the kernel
	cv::Mat di_element = cv::getStructuringElement(cv::MORPH_RECT,cv::Size(5,5),cv::Point(0,0));
	cv::Mat mask;
	depth_img.copyTo(mask);
	//cv::threshold(mask,mask,20,255,cv::THRESH_BINARY_INV);
	//cv::threshold(mask,mask,200,255,cv::THRESH_BINARY);
	
	//Blur and threshold to dilate
	cv::blur(mask, mask, cv::Size(5,5));
	cv::threshold(mask,mask,depthmap_thres,255,cv::THRESH_BINARY);
	
	//dilate and erode
	cv::dilate(mask,mask,di_element,cv::Point(-1,-1),2);
	cv::erode(mask,mask,di_element,cv::Point(-1,-1),4);
	
	mask.copyTo(people_mask);
	int thresh = 10;
	// Detect edges using canny
	mask.copyTo(canny_mask);
	cv::Canny(canny_mask, canny_mask, thresh, thresh * 2, 3);
	
	// Find contours
	std::vector<cv::Vec4i> hierarchy;
	std::vector<std::vector<cv::Point> > contours;
	cv::findContours(canny_mask, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0));

	// Set up ellipse/hull vectors
	std::vector<cv::RotatedRect> minEllipse(contours.size());
	std::vector<std::vector<cv::Point> > hull(contours.size());
	
	std::vector<people> candid;
	
	for(int i = 0; i < contours.size(); i++)
	{

		if (contours[i].size() > min_contour_size) // filter small contours out
		//if (area0 > 200)
		{
			//std::cout << cv::contourArea(contours[i]) << std::endl;
			cv::Scalar color = cv::Scalar(127,0,255);
			cv::convexHull(cv::Mat(contours[i]), hull[i], false);
			
			cv::Rect tmp3 = boundingRect(hull[i]);
			people ppl;
			ppl.centre = cv::Point(tmp3.x,tmp3.y)+cv::Point(tmp3.width/2,tmp3.height/2);
			ppl.contours = contours[i];
			ppl.trajectory.clear();
			ppl.miss_count =0;
			candid.push_back(ppl);
			//cv::drawContours( fg_mask, hull, i, color, 2, 8, hierarchy, 0, cv::Point() );
			
			if (hull[i].size() > 4)
			{
				minEllipse[i] = cv::fitEllipse(cv::Mat(hull[i]));
			}

		}
	}
	//if 2 of the detected centres are closer than min_dist_people then it's likely that it's the same person, thus using the point in the middle instead
	bool stop = 0;
	while(!stop)
	{
		stop = 1;
		for(int i=0; i<candid.size();i++)
		{
			for(int j=i; j<candid.size();j++)
			{
				if(i!=j&&cv::norm(candid[i].centre - candid[j].centre) < min_dist_people)
				{
						//calculate the point in the middle of the 2 points and use as a new centre
						stop =0;
						candid[i].centre = candid[i].centre + candid[j].centre;
						candid[i].centre.x /= 2;
						candid[i].centre.y /= 2;
						candid.erase(candid.begin()+j);
						j--;
				}
			}
			
		}
	}
	
	return candid;
}
void people_detector::drawpeople(cv::Mat InputImage)
{
	std::vector<std::vector<cv::Point> > contours;
	std::vector<cv::Vec4i> hierarchy;
	for(int i=0;i<list.size();i++)
	{
		//draw the current position
		contours.push_back(list[i].trajectory);
		cv::circle(InputImage,list[i].centre,3,cv::Scalar(150,0,0),-1);
		cv::rectangle(InputImage,cv::Rect(list[i].centre-cv::Point(10,10),cv::Size(20,20)),cv::Scalar(150,0,0));
		
		//draw search_range
		/*cv::circle(InputImage,list[i].centre,50,cv::Scalar(0,150,0),1);
		double alpha = 0.7;
		cv::Point proj = list[i].centre;
		if(!list[i].trajectory.empty())
			proj = (1+alpha)*list[i].centre - alpha*list[i].trajectory[list[i].trajectory.size()-1] ;
		cv::circle(InputImage,proj,120,cv::Scalar(0,150,0),1);*/
	}
	//draw trajectories
	for(int i=0; i<list.size();i++)
	{
		if(!list[i].trajectory.empty())
		{
			for(int j=0;j<list[i].trajectory.size();j++)
			{
				if(j<list[i].trajectory.size()-1)
					cv::line(InputImage,list[i].trajectory[j],list[i].trajectory[j+1],cv::Scalar(255,0,0));
				else
					cv::line(InputImage,list[i].trajectory[j],list[i].centre,cv::Scalar(255,0,0));
			}
		}
	}
}
cv::Mat people_detector::getUndistortedimg(int cam)
{
	return(camera.getUndistortedimg(cam));
}
cv::Mat people_detector::getCroppedUndistortedimg(int cam)
{
	cv::Mat tmp = camera.getUndistortedimg(cam);
	cv::Mat retMat;
	(tmp(crop_undistortedROI)).copyTo(retMat);
	return (retMat);
}
cv::Mat people_detector::getdepth_img()
{
	return(depth_img);
}
cv::Mat people_detector::getimg(int cam)
{
	return(camera.getimg(cam));
}
cv::Size people_detector::getSize(int cam)
{
	return(camera.getimgsize(cam));
}
cv::Size people_detector::getCroppedSize()
{
	return (crop_undistortedROI.size());
}
cv::Rect people_detector::getROI()
{
	return(crop_undistortedROI);
}
cv::Mat people_detector::getpeople_mask()
{
	return(people_mask);
}
double people_detector::getrecorded_framerate()
{
	return recorded_framerate;
}
bool people_detector::AnyPeople()
{
	if(list.empty())
		return 0;
	else
		return 1;
}
int people_detector::getIN()
{
	return in;
}
int people_detector::getOUT()
{
	return out;
}
int people_detector::getOccupancy()
{
	return occupancy;
}
void people_detector::setOccupancy(int val)
{
	occupancy = val;
}
bool people_detector::nextframe()
{
	//calculate framerate
	if(frame_counter==20)
	{
		frame_counter=1;
		clock_t diff = clock()-start;
		recorded_framerate = 20.0*CLOCKS_PER_SEC/diff; 
		start = clock();
	}
	else
		frame_counter++;
	bool retval= camera.nextframe();
	
	//if sgbm parameters is loaded then calculate depth map
	if(sgbm_ready)
	{
		depth_img = CalcDepthmap();
			
	}
	//if there's new action then reset the counter but if there's no action for 20 frames set last action to getting off the bus (more likely that the first person is getting of the bus)
	if(newaction)
	{
		counter_action =0;
	}
	else
	{
		counter_action++;
	}
	if(counter_action>20)
		last_action = -1;
		
	return(retval);
}
cv::Mat people_detector::CalcDepthmap()
{
	cv::Mat left_grey,right_grey,result;
	cv::Mat left = camera.getUndistortedimg(1);
	//undist1.copyTo(left);
	cv::cvtColor(left(crop_undistortedROI),left_grey,CV_BGR2GRAY);
	cv::Mat right = camera.getUndistortedimg(2);
	//undist2.copyTo(right);
	cv::cvtColor(right(crop_undistortedROI),right_grey,CV_BGR2GRAY);
	//resize to speed up
	cv::resize(left_grey,left_grey,cv::Size(60,40));
	cv::resize(right_grey,right_grey,cv::Size(60,40));
	//calculate depth map and resize back to original size
	sgbm(left_grey,right_grey,result);
	cv::resize(result,result,cv::Size(240,160));
	//convert to CV_8U type
	result.convertTo(result, CV_8U, 255/(alpha*16.));
	cv::Mat retMat;
	result.copyTo(retMat);
	return (retMat);
}
double people_detector::getfps()
{
return recorded_framerate;
}
void people_detector::putInfoOn(cv::Mat &OutputImage, struct tm* timeinfo)
{
	// add time and fps stamp
	if(timeinfo==NULL)
	{
		time_t rawtime;
		time(&rawtime);
		timeinfo = localtime(&rawtime);
	}
	char buffer[80];
	strftime(buffer,80, "%c",timeinfo);
	
	std::stringstream ss;
	ss << recorded_framerate << " ";
	std::string time_stamp1,time_stamp2;
	time_stamp1.resize(10);
	for(int i =0; i<10; i++)
	{
		time_stamp1[i] = buffer[i];
	}
	for(int i =11; buffer[i]!= '\0'; i++)
	{
		time_stamp2[i-11] = buffer[i];
	}
	std::string fps_tmp;
	ss >> fps_tmp;
	ss << in << " ";
	std::string in_tmp;
	ss >> in_tmp;
	
	ss << out;
	std::string out_tmp;
	ss >> out_tmp;	
	
	cv::Size dim= OutputImage.size();
	cv::Rect box(cv::Point(dim.width-150,(int) dim.height/2-30),cv::Point(dim.width,(int) dim.height/2+30));
	cv::rectangle(OutputImage, box ,cv::Scalar(88,88,88),CV_FILLED);
	cv::putText(OutputImage, "FPS : " + fps_tmp , box.tl()+cv::Point(0,15),cv::FONT_HERSHEY_PLAIN, 1, cv::Scalar(0,0,255));
	cv::putText(OutputImage, time_stamp1 , box.tl()+cv::Point(0,30),cv::FONT_HERSHEY_PLAIN, 1, cv::Scalar(0,0,255));
	cv::putText(OutputImage, time_stamp2 , box.tl()+cv::Point(0,45),cv::FONT_HERSHEY_PLAIN, 1, cv::Scalar(0,0,255));
	cv::putText(OutputImage, "IN : " + in_tmp , box.tl()+cv::Point(0,60),cv::FONT_HERSHEY_PLAIN, 1, cv::Scalar(0,0,255));
	cv::putText(OutputImage, "OUT : " + out_tmp , box.tl()+cv::Point(70,60),cv::FONT_HERSHEY_PLAIN, 1, cv::Scalar(0,0,255));
}
