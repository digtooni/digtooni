#pragma once

#include <string>
#include <opencv2/opencv.hpp>

class Camera{

public:
  /**
  * Construct a camera with a calibration file. This file should be in the opencv calibration xml format.
  * @param[in] calibration_filename The url of the calibration file.
  */
  explicit Camera(const std::string &calibration_filename);

  /**
  * Construct a camera directly specifying the intrinsic and distortion parameters.
  * @param[in] distortion The distortion parameters of the camera.
  * @param[in] image_width The width of the image plane in pixels.
  * @param[in] image_width The height of the image plane in pixels.
  */
  Camera(const cv::Mat &intrinsic, const cv::Mat &distortion, const int image_width, const int image_height);

  /**
  * Construct a camera setting its parameters to /f$f_{x} = 1000, f_{y} = 1000, c_{x} = 0.5\times ImageWidth, c_{y} = 0.5\times ImageHeight \f$.
  */
  Camera(){}

  /**
  * Destructor for the camera.
  */
  virtual ~Camera(){};

  /**
  * Project a 3D point onto the image plane without rounding its coordinates to a specific pixel.
  * @param point The 3D point to project.
  * @return The projected point.
  */
  cv::Point2d ProjectPoint(const cv::Point3d &point) const;

  /**
  * Project a 3D point directly to an image pixel by nearest neighbour interpolation.
  * @param point The 3D point to project.
  * @return The projected point.
  */
  cv::Point2i ProjectPointToPixel(const cv::Point3d &point) const;

  /**
  * Unproject a pixel to a ray into space.
  * @param[in] point The pixel coordinates.
  * @return The ray.
  */
  cv::Point3d UnProjectPoint(const cv::Point2i &point) const;

  /**
  * Camera focal length in x pixel dimensions.
  * @return The focal length.
  */
  float Fx() const { return fx_; }

  /**
  * Camera focal length in y pixel dimensions.
  * @return The focal length.
  */
  float Fy() const { return fy_; }

  /**
  * Camera principal point in the x dimension.
  * @return The principal point.
  */
  float Px() const { return px_; }

  /**
  * Camera principal point in the y dimension.
  * @return The principal point.
  */
  float Py() const { return py_; }

  /**
  * Get window size.
  * @return The size of the camera's view.
  */
  cv::Size GetWindowSize() const { return cv::Size(image_width_, image_height_); }

  /**
  * Get image width.
  * @return The image width.
  */
  int Width() const { return image_width_; }

  /**
  * Get image height.
  * @return The image height.
  */
  int Height() const { return image_height_; }

  /**
  * Generate the camera matrix.
  * @return The camera matrix.
  */
  cv::Mat CameraMatrix() const;

  cv::Mat Distortion() const  { return distortion_params_; }

  cv::Mat Rotation() const { return camera_pose_(cv::Rect(0, 0, 3, 3)).clone(); }

  cv::Mat RotationVector() const { cv::Mat rotation_vector; cv::Rodrigues(Rotation(), rotation_vector); return rotation_vector; }

  cv::Mat Translation() const { return camera_pose_(cv::Rect(3, 0, 1, 3)).clone(); }

protected:

  float fx_; /**< The camera focal length in units of horizontal pixel length. */
  float fy_; /**< The camera focal length in units of vertical pixel length. */
  float px_; /**< The camera horizontal principal point in units of horizontal pixel length. */
  float py_; /**< The camera horizontal principal point in units of vertical pixel length. */
  cv::Mat distortion_params_; /**< The camera distortion parameters. */

  int image_width_; /**< The width of the image plane in pixels. */
  int image_height_; /**< The height of the image plane in pixels. */

  cv::Mat camera_pose_; /**< The camera's pose w.r.t. the grid coordinate system .*/

};
