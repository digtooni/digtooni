#pragma once

#include <string>
#include <mysql_driver.h>
#include <mysql_connection.h>
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>
#include <mutex>

#include "parking_space.hpp"

class DatabaseUploader{

public:

  DatabaseUploader(const std::string &url, const std::string &username, const std::string &password, const std::string &schema);
  ~DatabaseUploader();

  void SQLDataUpload(const std::vector<ParkingSpace> &parking_spaces, int frame_number, int frames_averaged, time_t time_stamp, int camera_id, bool using_svm);

  bool NeedsRefresh(const int64 frame_number) const;

  int GetFramesBetweenUploads() const { return frames_between_uploads; }

protected:

  void SetupStatement(const std::vector<ParkingSpace> &spaces, int frame_number, int frames_averaged, time_t time_stamp, int camera_id);
  
  //setup statement for 'occupancy changed' style detection using e.g. Background Subtractor
  void SetupStatement2(const std::vector<ParkingSpace> &spaces, int frame_number, int frames_averaged, time_t time_stamp, int camera_id);
  
  void SendData(std::string &StatementStr);

  const std::string url;
  const std::string username;
  const std::string password;
  const std::string schema;

  sql::Driver *driver;
  sql::Connection *con;
  sql::PreparedStatement *prep_stmt;
  sql::ResultSet *res;
  //char *StatementStr;
  std::string statement;

  int frames_between_uploads;

};
