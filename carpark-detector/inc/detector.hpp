#pragma once

#include <opencv2/ml/ml.hpp>
#include <opencv2/objdetect/objdetect.hpp>

class CarDetector {

public:

  void DetectWholeFrame(cv::Mat &frame, std::vector<cv::Rect> &detections);

  explicit CarDetector(const std::string &detector_file);

  void RunDetection(cv::Mat &frame, std::vector<cv::Rect> &detections);
  void RunDetection(cv::Mat &frame, const cv::Rect &roi, std::vector<cv::Rect> &detections);

protected:

  void Detect(const cv::Mat &frame, std::vector<cv::Rect> &detections);

  cv::CascadeClassifier detector_;

  float scale_increase_;
  float frame_scale_;
  cv::Size min_size_;
  cv::Size max_size_;
  int min_neighbours_;

};
