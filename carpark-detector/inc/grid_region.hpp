#pragma once

#include <vector>
#include <opencv2/opencv.hpp>

#include "camera.hpp"
#include "helpers.hpp"

struct GridRegion {

  GridRegion(size_t id_, const cv::Point2f &tl, const cv::Point2f &tr, const cv::Point2f &bl, const cv::Point2f &br){

    id = id_;

    points.push_back(tl);
    points.push_back(tr);
    points.push_back(bl);
    points.push_back(br);

  }

  void SetupQuickIntersectionImage(const cv::Size image_size, const Camera &camera){

    cv::Point2f ptl, ptr, pbl, pbr;
    GetTransformedCoordinates(camera, ptl, ptr, pbl, pbr);
    std::vector<cv::Point2f> pts;
    pts.push_back(ptl);  pts.push_back(ptr); pts.push_back(pbl); pts.push_back(pbr);
    cv::Rect bounding = cv::boundingRect(pts);

    cv::Mat tmp = cv::Mat::zeros(image_size, CV_8UC1);
    for (int r = 0; r < tmp.rows; ++r){
      for (int c = 0; c < tmp.cols; ++c){

        if (r < bounding.tl().y || r > bounding.br().y || c < bounding.tl().x || c > bounding.br().x){
          continue;
        }
        tmp.at<unsigned char>(r, c) = 255 * ContainsPoint(camera, cv::Point(c, r));

      }
    }

    quick_intersection_test = tmp;

  }

  cv::Vec2f GetUpVector(const Camera &camera) const {

    std::vector<cv::Point3f> world_coordinates(2);
    world_coordinates[0] = cv::Point3f(tl().x, tl().y, 0);
    world_coordinates[1] = cv::Point3f(tl().x, tl().y, -1);

    std::vector < cv::Point2f> image_coordinates(2);
    cv::projectPoints(world_coordinates, camera.RotationVector(), camera.Translation(), camera.CameraMatrix(), camera.Distortion(), image_coordinates);

    return cv::Vec2f(image_coordinates[1].x - image_coordinates[0].x, image_coordinates[1].y - image_coordinates[0].y);

  }

  bool IsInView(const Camera &camera){

    cv::Point2f tl, br, tr, bl;
    GetTransformedCoordinates(camera, tl, tr, bl, br, true);
    int num_in_view = 0;
    if (cv::Rect(0, 0, camera.Width(), camera.Height()).contains(tl)){
      num_in_view++;
    }
    if (cv::Rect(0, 0, camera.Width(), camera.Height()).contains(tr)){
      num_in_view++;
    }
    if (cv::Rect(0, 0, camera.Width(), camera.Height()).contains(bl)){
      num_in_view++;
    }
    if (cv::Rect(0, 0, camera.Width(), camera.Height()).contains(br)){
      num_in_view++;
    }

    return (num_in_view > 1);

  }

  void GetTransformedCoordinates(const Camera &camera, cv::Point2f &top_left, cv::Point2f &top_right, cv::Point2f &bottom_left, cv::Point2f &bottom_right, bool use_distortion = true) const {

    std::vector<cv::Point3f> world_coordinates(4);
    world_coordinates[0] = cv::Point3f(tl().x, tl().y, 0);
    world_coordinates[1] = cv::Point3f(tr().x, tr().y, 0);
    world_coordinates[2] = cv::Point3f(bl().x, bl().y, 0);
    world_coordinates[3] = cv::Point3f(br().x, br().y, 0);

    std::vector < cv::Point2f> image_coordinates(4);
    if (use_distortion)
      cv::projectPoints(world_coordinates, camera.RotationVector(), camera.Translation(), camera.CameraMatrix(), camera.Distortion(), image_coordinates);
    else
      cv::projectPoints(world_coordinates, camera.RotationVector(), camera.Translation(), camera.CameraMatrix(), cv::Mat::zeros(5, 1, CV_32FC1), image_coordinates);

    top_left = image_coordinates[0];
    top_right = image_coordinates[1];
    bottom_left = image_coordinates[2];
    bottom_right = image_coordinates[3];

  }

  std::vector<cv::Point> GetProjectedRectangleAngle(const Camera &camera) const {

    auto points = GetCarBox(camera);

    std::vector<cv::Point> pts;
    for (auto pt : points){
      pts.push_back(pt.first);
      pts.push_back(pt.second);
    }

    std::vector<cv::Point> ret_pts;
    cv::convexHull(pts, ret_pts);

    return ret_pts;

  }

  std::vector< std::pair <cv::Point, cv::Point> > GetCarBox(const Camera &camera) const {

    std::vector<cv::Point3f> lower_points(4);
    lower_points[0] = cv::Point3f(tl().x, tl().y, 0);
    lower_points[1] = cv::Point3f(tr().x, tr().y, 0);
    lower_points[2] = cv::Point3f(bl().x, bl().y, 0);
    lower_points[3] = cv::Point3f(br().x, br().y, 0);

    std::vector < cv::Point2f> projected_lower_points(4);

    cv::projectPoints(lower_points, camera.RotationVector(), camera.Translation(), camera.CameraMatrix(), camera.Distortion(), projected_lower_points);

    std::vector<cv::Point3f> upper_points(4);
    float height_factor = 0.25f;
    upper_points[0] = cv::Point3f(tl().x, tl().y, height_factor);
    upper_points[1] = cv::Point3f(tr().x, tr().y, height_factor);
    upper_points[2] = cv::Point3f(bl().x, bl().y, height_factor);
    upper_points[3] = cv::Point3f(br().x, br().y, height_factor);

    std::vector<cv::Point2f> projected_upper_points(4);
    cv::projectPoints(upper_points, camera.RotationVector(), camera.Translation(), camera.CameraMatrix(), camera.Distortion(), projected_upper_points);

    std::vector< std::pair< cv::Point, cv::Point> > rets;
    rets.push_back(std::make_pair(cv::Point((int)projected_lower_points[0].x,
      (int)projected_lower_points[0].y),
      cv::Point((int)projected_lower_points[1].x,
      (int)projected_lower_points[1].y)));

    rets.push_back(std::make_pair(cv::Point((int)projected_lower_points[1].x,
      (int)projected_lower_points[1].y),
      cv::Point((int)projected_lower_points[2].x,
      (int)projected_lower_points[2].y)));

    rets.push_back(std::make_pair(cv::Point((int)projected_lower_points[2].x,
      (int)projected_lower_points[2].y),
      cv::Point((int)projected_lower_points[3].x,
      (int)projected_lower_points[3].y)));

    rets.push_back(std::make_pair(cv::Point((int)projected_lower_points[3].x,
      (int)projected_lower_points[3].y),
      cv::Point((int)projected_lower_points[0].x,
      (int)projected_lower_points[0].y)));


    rets.push_back(std::make_pair(cv::Point((int)projected_upper_points[0].x,
      (int)projected_upper_points[0].y),
      cv::Point((int)projected_upper_points[1].x,
      (int)projected_upper_points[1].y)));

    rets.push_back(std::make_pair(cv::Point((int)projected_upper_points[1].x,
      (int)projected_upper_points[1].y),
      cv::Point((int)projected_upper_points[2].x,
      (int)projected_upper_points[2].y)));

    rets.push_back(std::make_pair(cv::Point((int)projected_upper_points[2].x,
      (int)projected_upper_points[2].y),
      cv::Point((int)projected_upper_points[3].x,
      (int)projected_upper_points[3].y)));

    rets.push_back(std::make_pair(cv::Point((int)projected_upper_points[3].x,
      (int)projected_upper_points[3].y),
      cv::Point((int)projected_upper_points[0].x,
      (int)projected_upper_points[0].y)));

    for (int pi = 0; pi < 4; ++pi){
      rets.push_back(std::make_pair(cv::Point((int)projected_lower_points[pi].x,
        (int)projected_lower_points[pi].y),
        cv::Point((int)projected_upper_points[pi].x,
        (int)projected_upper_points[pi].y)));
    }

    return rets;
  }

  bool ContainsPoint(const Camera &camera, const cv::Point &pt) const {

    if (!quick_intersection_test.empty()){

      return quick_intersection_test.at<unsigned char>(pt) == 255;

    }

    cv::Point2f ptl, ptr, pbl, pbr;
    GetTransformedCoordinates(camera, ptl, ptr, pbl, pbr);

    return
      PointInTriangle(pt, ptl, ptr, pbl)
      ||
      PointInTriangle(pt, ptr, pbl, pbr);

  }

  void InitDetectionStatus() { detections.push_back(false); }
  void SetDetected() { detections.back() = true; }

  bool RegionContainsObject(){

    if (detections.size() == 0) return false;

    return detections.back();

    /*
    if (detections.size() < 4){
      return detections.back();
    }

    int detection_count = 0;
    int detection_size = (int)detections.size();
    for (int i = detection_size - 4; i >= 0 && i < detections.size(); ++i){
      if (detections[i]) detection_count++;
    }

    if (detection_count >= 3) return true;
    else return false;
    */
  }

  size_t id;

  std::vector<bool> detections;

  cv::Point2f tl() const { return points[0]; }
  cv::Point2f tr() const { return points[1]; }
  cv::Point2f bl() const { return points[2]; }
  cv::Point2f br() const { return points[3]; }

  std::vector<cv::Point2f> points;

  cv::Mat quick_intersection_test;

};