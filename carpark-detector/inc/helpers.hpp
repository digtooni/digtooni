#pragma once

#include <opencv2/opencv.hpp>
#include "../inc/temporal.hpp"
#include <time.h>
  
void getContoursFromImage(cv::Mat &fgmask, std::vector<std::vector<cv::Point> > &contours, std::vector<cv::RotatedRect> &minEllipse);

inline std::string get_time(){

   time_t t = time(NULL);
   tm datetime = *localtime(&t);
     
   std::stringstream ss;
   ss << datetime.tm_year + 1900 << "-" << datetime.tm_mon + 1 << "-" << datetime.tm_mday << " " << datetime.tm_hour << ":" << datetime.tm_min << ":" << datetime.tm_sec;

   return ss.str();

}

inline cv::Mat getInitialFrame(cv::VideoCapture &cap){

	cv::Mat frame;
	cv::Mat frameBlr;
	cap >> frame;
	return frame;
	
}

template<typename Precision>
inline bool PointInImage(const cv::Point_<Precision> &pt, const cv::Mat &image){

  return cv::Rect(0, 0, image.cols, image.rows).contains(cv::Point((int)pt.x,(int)pt.y));

}


template<typename Precision>
inline bool IsHorizontal(const cv::Point_<Precision> &a, const cv::Point_<Precision> &b){
  return a.y == b.y;
}

template<typename Precision>
inline bool IsVertical(const cv::Point_<Precision> &a, const cv::Point_<Precision> &b){
  return a.x == b.x;
}

template<typename Precision>
inline double Gradient(const cv::Point_<Precision> &a, const cv::Point_<Precision> &b){
  return (float)(a.y - b.y) / (a.x - b.x);
}

template<typename Precision>
inline double YIntersect(const cv::Point_<Precision> &a, const double gradient){
  return a.y - (gradient*a.x);
}

inline cv::Rect GetSafeROI(const cv::Rect &roi, const cv::Mat frame){

  cv::Rect rroi = roi;

  if (rroi.tl().x < 0) rroi.x = 0;
  if (rroi.tl().y < 0) rroi.y = 0;

  while (rroi.br().x >= frame.cols) rroi.width--;
  while (rroi.br().y >= frame.rows) rroi.height--;

  return rroi;

}

inline void RotateFrame(const cv::Mat & src, cv::Mat & dst, const double degrees){
  cv::Mat frame, frameRotated;

  int diagonal = (int)sqrt(src.cols * src.cols + src.rows * src.rows);
  int newWidth = diagonal;
  int newHeight = diagonal;

  int offsetX = (newWidth - src.cols) / 2;
  int offsetY = (newHeight - src.rows) / 2;
  cv::Mat targetMat(newWidth, newHeight, src.type(), cv::Scalar(0));
  cv::Point2f src_center(targetMat.cols / 2.0f, targetMat.rows / 2.0f);

  src.copyTo(frame);

  frame.copyTo(targetMat.rowRange(offsetY, offsetY +
    frame.rows).colRange(offsetX, offsetX + frame.cols));
  cv::Mat rot_mat = cv::getRotationMatrix2D(src_center, degrees, 1.0);
  cv::warpAffine(targetMat, frameRotated, rot_mat, targetMat.size());

  cv::Rect bound_Rect(frame.cols, frame.rows, 0, 0);
  int x1 = offsetX;
  int x2 = offsetX + frame.cols;
  int x3 = offsetX;
  int x4 = offsetX + frame.cols;
  int y1 = offsetY;
  int y2 = offsetY;
  int y3 = offsetY + frame.rows;
  int y4 = offsetY + frame.rows;
  cv::Mat co_Ordinate = (cv::Mat_<double>(3, 4) << x1, x2, x3, x4,
    y1, y2, y3, y4,
    1, 1, 1, 1);

  cv::Mat RotCo_Ordinate = rot_mat * co_Ordinate;

  for (int i = 0; i < 4; ++i) {
    if (RotCo_Ordinate.at<double>(0, i) < bound_Rect.x)
      bound_Rect.x = (int)RotCo_Ordinate.at<double>(0, i);
    if (RotCo_Ordinate.at<double>(1, i) < bound_Rect.y)
      bound_Rect.y = RotCo_Ordinate.at<double>(1, i);
  }

  for (int i = 0; i < 4; ++i) {
    if (RotCo_Ordinate.at<double>(0, i) > bound_Rect.width)
      bound_Rect.width = (int)RotCo_Ordinate.at<double>(0, i);
    if (RotCo_Ordinate.at<double>(1, i) > bound_Rect.height)
      bound_Rect.height = RotCo_Ordinate.at<double>(1, i);
  }

  bound_Rect.width = bound_Rect.width - bound_Rect.x;
  bound_Rect.height = bound_Rect.height - bound_Rect.y;

  if (bound_Rect.x < 0)
    bound_Rect.x = 0;
  if (bound_Rect.y < 0)
    bound_Rect.y = 0;
  if (bound_Rect.width > frameRotated.cols)
    bound_Rect.width = frameRotated.cols;
  if (bound_Rect.height > frameRotated.rows)
    bound_Rect.height = frameRotated.rows;

  cv::Mat ROI = frameRotated(bound_Rect);
  ROI.copyTo(dst);
}

inline float GetAngleBetweenVectors(const cv::Vec2f &a, const cv::Vec2f &b){

  //cv::Vec2f a_normed = a / std::sqrt((a[0] * a[0]) + (a[1] * a[1]));
  //cv::Vec2f b_normed = b / std::sqrt((b[0] * b[0]) + (b[1] * b[1]));
  //return acos(a_normed[0] * b_normed[0] + a_normed[1] * b_normed[1]);

  return atan2(b[1], b[0]) - atan2(a[1], a[0]);

}

inline cv::Mat getNewWarpPersp(cv::Point2f TmpsrcTri[4], cv::Point2f TmpdstTri[4]){
  cv::Mat warp_mat(2, 3, CV_32FC1);
  warp_mat = getPerspectiveTransform(TmpsrcTri, TmpdstTri);
  return warp_mat;
}

inline float l2_distance(const cv::Vec2f &a, const cv::Vec2f &b){
  
  return std::sqrt((a[0] - b[0])*(a[0] - b[0]) + (a[1] - b[1])*(a[1] - b[1]));
  
}

inline float LineSign(cv::Point2f p1, cv::Point2f p2, cv::Point2f p3) {
  return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
}

inline bool PointInTriangle(cv::Point2f pt, cv::Point2f v1, cv::Point2f v2, cv::Point2f v3) {
  bool b1, b2, b3;
  b1 = LineSign(pt, v1, v2) < 0.0f;
  b2 = LineSign(pt, v2, v3) < 0.0f;
  b3 = LineSign(pt, v3, v1) < 0.0f;
  return ((b1 == b2) && (b2 == b3));
}

inline bool PointInBox(const cv::Rect &r, const cv::Point2f &pt){

  return r.contains(pt);
  
}

inline bool PointInBox(const cv::RotatedRect &r, const cv::Point2f &pt){
  
  cv::Point2f pts[4];
  r.points(pts);
  
  return
    PointInTriangle(pt, pts[0], pts[1], pts[2])
    ||
    PointInTriangle(pt, pts[1], pts[2], pts[3]);

}

void drawGridOnFrame(cv::Mat &frame);

void CallBackFunc(int event, int x, int y, int flags, void* userdata);

template<typename Precision>
bool FindGeneralIntersection(const cv::Point_<Precision> &a, const cv::Point_<Precision> &b, const cv::Point_<Precision> &c, const cv::Point_<Precision> &d, cv::Point_<Precision> &intersection){

  const double gradient_ab = Gradient<Precision>(a, b);
  const double yintersect_ab = YIntersect<Precision>(a, gradient_ab);
  const double gradient_cd = Gradient<Precision>(c, d);
  const double yintersect_cd = YIntersect<Precision>(c, gradient_cd);

  double intersection_x = (yintersect_cd - yintersect_ab) / (gradient_ab - gradient_cd);
  double intersection_y = (gradient_ab*intersection_x) + yintersect_ab;

  intersection = cv::Point_<Precision>((Precision)intersection_x, (Precision)intersection_y);

  bool i1 = intersection.x >= std::min(a.x, b.x) && intersection_x <= std::max(a.x, b.x) && intersection_y >= std::min(a.y, b.y) && intersection_y <= std::max(a.y, b.y);
  bool i2 = intersection.x >= std::min(c.x, d.x) && intersection_x <= std::max(c.x, d.x) && intersection_y >= std::min(c.y, d.y) && intersection_y <= std::max(c.y, d.y);

  return i1 && i2;

}

template<typename Precision>
inline bool FindHorizontalIntersection(const cv::Point_<Precision> &horizontal_start, const cv::Point_<Precision> &horizontal_end, const cv::Point_<Precision> &line_start, const cv::Point_<Precision> &line_end, cv::Point_<Precision> &intersection){

  const double gradient = Gradient<Precision>(line_start, line_end);
  double intersect_x = (horizontal_start.y / gradient) - YIntersect<Precision>(line_start, gradient);
  intersection = cv::Point_<Precision>(intersect_x, horizontal_start.y);
  //do max-min checks as the start and end points might be the wrong way around
  return (intersection.x >= std::min(horizontal_start.x, horizontal_end.x) && intersection.x <= std::max(horizontal_start.x, horizontal_end.x) && intersection.x >= std::min(line_start.x, line_end.x) && intersection.x <= std::max(line_end.x, line_end.x));

}

template<typename Precision>
inline bool FindVerticalIntersection(const cv::Point_<Precision> &vertical_start, const cv::Point_<Precision> &vertical_end, const cv::Point_<Precision> &line_start, const cv::Point_<Precision> &line_end, cv::Point_<Precision> &intersection){

  const double gradient = Gradient<Precision>(line_start, line_end);
  double intersect_y = gradient*vertical_start.x + YIntersect<Precision>(line_start, gradient);
  intersection = cv::Point_<Precision>(vertical_start.x, intersect_y);
  //do max-min checks as the start and end points might be the wrong way around
  return (intersection.y >= std::min(vertical_end.y, vertical_start.y) && intersection.y <= std::max(vertical_end.y, vertical_start.y) && intersection.y >= std::min(line_start.y, line_end.y) && intersection.y <= std::max(line_end.y, line_start.y));

}


template<typename Precision>
inline bool FindIntersection(const cv::Point_<Precision> &a1, const cv::Point_<Precision> &a2, const cv::Point_<Precision> &b1, const cv::Point_<Precision> &b2, cv::Point_<Precision> &intersection){

  if (IsHorizontal(a1, a2) && !IsHorizontal(b1, b2)){

    return FindHorizontalIntersection<Precision>(a1, a2, b1, b2, intersection);

  }

  if (IsVertical(a1, a2) && !IsVertical(b1, b2)) {

    return FindVerticalIntersection<Precision>(a1, a2, b1, b2, intersection);

  }

  if (IsHorizontal(b1, b2) && !IsHorizontal(a1, a2)){

    return FindHorizontalIntersection<Precision>(b1, b2, a1, a2, intersection);

  }

  if (IsVertical(b1, b2) && !IsVertical(a1, a2)){

    return FindVerticalIntersection<Precision>(b1, b2, a1, a2, intersection);

  }

  return FindGeneralIntersection<Precision>(a1, a2, b1, b2, intersection);

}
