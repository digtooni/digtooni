#ifndef __OCCUPANCY_DETECTOR_HPP__
#define __OCCUPANCY_DETECTOR_HPP__

#include<vector>
#include<opencv2/opencv.hpp>
#include <array>

#include "parking_space.hpp"
#include "detector.hpp"
#include "camera.hpp"
#include "helpers.hpp"
#include "grid_temporal_model.hpp"

class OccupancyDetector {

public:

  OccupancyDetector(const std::string &grid_file, const std::string &detector_file, int camera_id, bool display_to_screen);

  void MatchDetectionsToFloorPlan(cv::Mat &frame, const std::vector<cv::Rect> &detections);
  void MatchDetectionsToFloorPlan2(cv::Mat &frame, const std::vector<cv::Rect> &detections);
  void AddCamera(const Camera &cam);
  Camera *GetCamera(const size_t index);

  std::vector<ParkingSpace> DetectOccupancy(cv::Mat &frame, bool use_rotations);
  void Draw(cv::Mat &canvas);
  void DrawRegion(cv::Mat &canvas, const GridRegion &region);

  bool DisplayToScreen() const { return display_to_screen_; }
  int GetCameraID() const { return camera_id_; }

protected:


  std::vector<cv::Point> GetAllGridPoints() const;

  std::array<cv::Point, 4> TransformBoundaryPoints(cv::Point tl, cv::Point tr, cv::Point bl, cv::Point br, cv::Size im_size, const float &angle);
  void DetectOnFrame(cv::Mat &frame, std::vector<cv::Rect> &detections);
  void LoadGridFromFile(const std::string &grid_file);
  cv::Rect GetRotatedSearchSpace(const cv::Rect unrotated_rectangle, const cv::Size orig_image_size, const cv::Size updated_image_size, float degrees);
  cv::Rect GetRotatedSearchSpace(const std::vector<cv::Point> &unrotated_rectangle, const cv::Size orig_image_size, const cv::Size updated_image_size, float degrees);
  cv::RotatedRect GetRectFromRotatedWindow(std::array<cv::Point, 4> &boundary_points, const cv::Rect &rect_in_rotated_frame, const cv::Size orig_image_size, const cv::Size updated_image_size, float degrees);

  CarDetector detector_;
  std::vector<GridRegion> grid_regions_;

  std::vector<Camera> cameras_;

  bool display_to_screen_;
  int camera_id_;

};


#endif