#pragma once
#include <ctime>
#include <opencv2/opencv.hpp>
#include <mutex>

class Timer {

public:

  Timer() : start(0), fps(10) , start_alt(time(0)) {}

  void Start() {

    float fps = GetAverageFPS();
    std::cout << "average FPS = " << fps << std::endl;

    mutex_.lock();
    start = cv::getTickCount();
    mutex_.unlock();

  }

  void Stop() {

    mutex_.lock();
    fps = cv::getTickFrequency() / (cv::getTickCount() - start);
    mutex_.unlock();
    
  }

  int64 GetCurrentTime() const {
    return time(NULL);
  }

  double GetCurrentFps() const {
    return fps;
  }

  static int64 GetFrameCount() {
    mutex_.lock();
    int64 ret = frame_count;
    mutex_.unlock();
    return ret;
  }

  static void IncreaseFrameCount() {
    mutex_.lock();
    ++frame_count;
    mutex_.unlock();
  }

  float GetAverageFPS() const {
    mutex_.lock();
    float ret = float(frame_count) / (time(0) - start_alt);
    mutex_.unlock();
    return ret;
  }

private:

  static int64 frame_count;
  int64 start; 
  double fps;

  double fps_average;
  double start_alt;

  static std::mutex mutex_;

};
