#pragma once

#include <string>
#include <opencv2/highgui/highgui.hpp>
#include <fstream>

#include <thread>
#include <mutex>

#include "helpers.hpp"



class VideoCaptureWrapper {


public:

  enum CaptureType { CAMERA, VIDEOFILE };

  explicit VideoCaptureWrapper(const std::string &videofile);
  VideoCaptureWrapper();
  virtual ~VideoCaptureWrapper();
  bool isOpened() const;
  cv::Mat ReadFrame();

protected:

  void Init();
  
  //don't assign or copy this object
  VideoCaptureWrapper(const VideoCaptureWrapper& vcw) {
      
  }
  VideoCaptureWrapper &operator=(const VideoCaptureWrapper& rhs) { 
    return *this;
  }

  void CheckCaptureStatus();

  std::mutex lock_;
  bool check_capture_status_;
  bool camera_changed_;
  
  static std::ofstream DEBUG_LOG_FILE;

  CaptureType capture_type_;
  cv::VideoCapture cap_;
  size_t camera_id_;
  
};
