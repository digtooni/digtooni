#pragma once

#include <opencv2/highgui/highgui.hpp>

class VideoWriterWrapper {


public:

  enum WriterType { WRITE_VIDEO, WRITE_IMAGES };

  VideoWriterWrapper(const std::string &path_to_write, WriterType writer_type) : writer_type_(writer_type) , path_to_write_(path_to_write), frame_count_(0) {
  
  }

  void OpenVideoFile(const std::string &video_file, const cv::Size &size){
    writer_.open(video_file, CV_FOURCC('M','J','P','G'), 25, cv::Size(size));
    if (!writer_.isOpened()) throw std::runtime_error("Error, could not open video file " + video_file);
  }

  void WriteFrame(const cv::Mat &frame){

    if (writer_type_ == WRITE_VIDEO){
      writer_ << frame;
    }
    else{
      std::stringstream ss;
      ss << path_to_write_ << "/frame" << frame_count_ << ".jpg";
      frame_count_++;
      cv::imwrite(ss.str(), frame);
    }

  }


protected:

  WriterType writer_type_;
  cv::VideoWriter writer_;
  size_t frame_count_;
  std::string path_to_write_;

};