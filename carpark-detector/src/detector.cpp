#include "../inc/detector.hpp"
#include "../inc/config_reader.hpp"

#include <opencv2/imgproc/imgproc.hpp>
#include <fstream>
#include <opencv2/highgui/highgui.hpp>
#include <boost/filesystem.hpp>


void CarDetector::Detect(const cv::Mat &frame, std::vector<cv::Rect> &detections){

#ifdef VERBOSE
  std::cout << "Running detection on frame size: (" << frame.cols << ", " << frame.rows << ")" << std::endl;
#endif

  std::vector<double> confidences;

  cv::Mat frame_gray;
  cvtColor(frame, frame_gray, CV_BGR2GRAY);

  detector_.detectMultiScale(frame_gray, detections, scale_increase_, min_neighbours_, CV_HAAR_SCALE_IMAGE, min_size_, max_size_);

}
  
std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
  std::stringstream ss(s);
  std::string item;
  while (std::getline(ss, item, delim)) {
    elems.push_back(item);
  }
  return elems;
}

std::vector<std::string> split(const std::string &s, char delim) {
  std::vector<std::string> elems;
  split(s, delim, elems);
  return elems;
}

CarDetector::CarDetector(const std::string &config_file){

  ConfigReader cfg(config_file);

  min_size_.width = cfg.get_element_as_type<size_t>("min-size-x");
  min_size_.height = cfg.get_element_as_type<size_t>("min-size-y");

  max_size_.width = cfg.get_element_as_type<size_t>("max-size-x");
  max_size_.height = cfg.get_element_as_type<size_t>("max-size-y");

  scale_increase_ = cfg.get_element_as_type<float>("scale-increase");
  min_neighbours_ = cfg.get_element_as_type<float>("min-neighbours");
  frame_scale_ = cfg.get_element_as_type<float>("frame-scale");
  
  std::string model_file_path = cfg.get_element("model-file");
  
  detector_.load(model_file_path);

}


void CarDetector::RunDetection(cv::Mat &frame, std::vector<cv::Rect> &detections){

#ifdef VERBOSE
  std::cout << "Running detection on frame size: (" << frame.cols << ", " << frame.rows << ")" << std::endl;
#endif

  cv::Mat large_frame;
  cv::resize(frame, large_frame, cv::Size(0, 0), frame_scale_, frame_scale_, CV_INTER_CUBIC);

  Detect(frame, detections);

  std::vector<cv::Rect> rescaled_locations;

  for (auto &location : detections)
    rescaled_locations.push_back(cv::Rect(cv::Point(location.tl().x / frame_scale_, location.tl().y / frame_scale_), cv::Size(location.width / frame_scale_, location.height / frame_scale_)));

  detections = rescaled_locations;

}
  
void CarDetector::RunDetection(cv::Mat &frame, const cv::Rect &roi, std::vector<cv::Rect> &detections){
  
  cv::Mat frame_roi = frame(roi);

  RunDetection(frame_roi, detections);

  std::vector<cv::Rect> repositioned_locations;

  for (auto &location : detections)
    repositioned_locations.push_back(cv::Rect(cv::Point(roi.tl().x, roi.tl().y) + cv::Point(location.tl().x, location.tl().y), cv::Size(location.width , location.height)));

  detections = repositioned_locations;

	
}


