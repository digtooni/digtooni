#include "../inc/helpers.hpp"
#include "../inc/database.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <ctime>
#include <thread>
#include <iostream>

#include "../inc/temporal.hpp"

using namespace cv;
using namespace ttrk;

int thresh = 100; //threshold for canny

void getContoursFromImage(cv::Mat &fgmask, std::vector<std::vector<cv::Point> > &contours, std::vector<cv::RotatedRect> &minEllipse){


  // COMMENT OUT ERODE FUNCTION IF NOISE IS NOT AN ISSUE. This uses significant processing time
  //Apply erode to fgmask to remove speckles
  //Create a structuring element
  int erosion_size = 1;
  Mat elementEr = getStructuringElement(cv::MORPH_RECT, cv::Size(2 * erosion_size + 1, 2 * erosion_size + 1), cv::Point(erosion_size, erosion_size));
  // Apply erode
  erode(fgmask, fgmask, elementEr);


  //Apply dilation to fgmask to generate single blobs
  //Create a structuring element
  int dilate_size = 5;
  Mat elementDi = getStructuringElement(cv::MORPH_RECT, cv::Size(2 * dilate_size + 1, 2 * dilate_size + 1), cv::Point(dilate_size, dilate_size));
  // Apply dilation
  //dilate(fgmask, fgmask, elementDi);

  /*
  // Apply further erode to reduce impact of over-sizing due to dilate (but now objects should be merged together)
  //Apply erode to fgmask to remove speckles
  //Create a structuring element
  int erosion_size2 = 4;
  Mat elementEr2 = getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(2 * erosion_size2 + 1, 2 * erosion_size2 + 1), cv::Point(erosion_size2, erosion_size2));
  // Apply erode
  erode(fgmask, fgmask, elementEr2);
  */

  cv::Mat canny_fgmask;
  // Detect edges using canny
  Canny(fgmask, canny_fgmask, thresh, thresh * 2, 3);
  // Find contours
  vector<Vec4i> hierarchy;
  findContours(canny_fgmask, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));

  Mat contour_fgmask = Mat::zeros(canny_fgmask.size(), CV_8UC3);

  int j = 0;
  double area0;

  // Set up ellipse vectors
  minEllipse = vector<RotatedRect>(contours.size());
  vector<vector<Point> >hull(contours.size());

  for (size_t i = 0; i < contours.size(); i++)
  {
    area0 = contourArea(contours[i]);

    if (contours[i].size() > 10)
      //if (area0 > 20)
    {
      Scalar color = CV_RGB(0, 0, 255);
      convexHull(Mat(contours[i]), hull[i], false);
      if (hull[i].size() > 4)
      {
        minEllipse[i] = fitEllipse(Mat(hull[i]));
      }

    }
    //std::cout << "Contour : " << contours[i].size() << std::endl;
  }
}

// Get new dimensions for Perspective Transform (4 points)
cv::Mat inputCommonPointsPersp(cv::Mat &frame, cv::Mat &floorPlan)
{

	cv::Point2f srcTri[4];
	cv::Point2f dstTri[4];
  
  srcTri[0] = cv::Point2f(247, 510);
  srcTri[1] = cv::Point2f(320, 131);
  srcTri[2] = cv::Point2f(808, 122);
  srcTri[3] = cv::Point2f(706, 496);

  dstTri[0] = cv::Point2f(129, 512);
  dstTri[1] = cv::Point2f(86, 25);
  dstTri[2] = cv::Point2f(698, 13);
  dstTri[3] = cv::Point2f(500, 503);
  
	//for (int i = 0; i < 4; ++i)
	//{
	//	// Create window for Frame
	//	namedWindow("Frame", 1);
	//	// Set the call back function for mouse click event
	//	Point p1;
	//	setMouseCallback("Frame", CallBackFunc, &p1);
	//	// Show the image
	//	imshow("Frame", frame);
	//	//Wait until user press key
	//	std::cout << "Press Any Key to continue when happy with selection" << std::endl;
	//	waitKey(0);
	//	srcTri[i] = p1;

	//	// Create window for FloorPlan
	//	namedWindow("FloorPlan", 1);
	//	// Set the call back function for mouse click event
	//	Point p2;
	//	setMouseCallback("FloorPlan", CallBackFunc, &p2);
	//	// Show the image
	//	imshow("FloorPlan", floorPlan);
	//	//Wait until user press key
	//	std::cout << "Press Any Key to continue when happy with selection" << std::endl;
	//	waitKey(0);
	//	dstTri[i] = p2;

	//	destroyWindow("Frame");
	//	destroyWindow("FloorPlan");
	//}

	std::cout << "Selected position S1: (" << srcTri[0].x << ", " << srcTri[0].y << ")" << std::endl;
	std::cout << "Selected position S2: (" << srcTri[1].x << ", " << srcTri[1].y << ")" << std::endl;
	std::cout << "Selected position S3: (" << srcTri[2].x << ", " << srcTri[2].y << ")" << std::endl;
	std::cout << "Selected position S4: (" << srcTri[3].x << ", " << srcTri[3].y << ")" << std::endl;

	std::cout << "Selected position D1: (" << dstTri[0].x << ", " << dstTri[0].y << ")" << std::endl;
	std::cout << "Selected position D2: (" << dstTri[1].x << ", " << dstTri[1].y << ")" << std::endl;
	std::cout << "Selected position D3: (" << dstTri[2].x << ", " << dstTri[2].y << ")" << std::endl;
	std::cout << "Selected position D4: (" << dstTri[3].x << ", " << dstTri[3].y << ")" << std::endl;

	cv::Mat warp_mat_temp = getNewWarpPersp(srcTri, dstTri);
	return warp_mat_temp;

}

void CallBackFunc(int event, int x, int y, int flags, void* ptr)
{
	if (event == EVENT_LBUTTONDOWN)
	{
		std::cout << "Selected position: (" << x << ", " << y << ")" << std::endl;
		std::cout << "Press Any Key to continue when happy with selection" << std::endl;
		Point*p = (Point*)ptr;
		p->x = x;
		p->y = y;
	}
}



bool isClassifiedRegion(const cv::RotatedRect &rr, const cv::Mat &frame, cv::Mat &flood){

  if (rr.center.x < 0 || rr.center.y < 0 || rr.center.x >= frame.cols || rr.center.y >= frame.cols){
    return false;
  }
  bool val_at_center = frame.at<unsigned char>(rr.center);

  if (!val_at_center) return false;

  cv::floodFill(frame, flood, rr.center, cv::Scalar(255));
  return true;
}



void KalmanFilterTracker::Init(std::vector<float> &start_pose, float dt) {

	//static float dt = 1.0f / 25; //frame rate of 25Hz

	//params (x,y,dx,dy) - euler angles allows linear parametrization
	///filter_.init(12, 6, 0, CV_32F); //dynamic params, measurement params, control params
	filter_.init(4, 2, 0, CV_32F); //dynamic params, measurement params, control params

	filter_.transitionMatrix = cv::Mat::eye(4, 4, CV_32F);
	filter_.transitionMatrix.at<float>(0, 2) = dt;
	filter_.transitionMatrix.at<float>(1, 3) = dt;
	/*
	for (int i = 3; i<6; i++){
		filter_.transitionMatrix.at<float>(i - 3, i) = dt;
		filter_.transitionMatrix.at<float>(i + 3, i + 6) = dt;
	}
	*/

	filter_.measurementMatrix = cv::Mat::zeros(2, 4, CV_32F);
	filter_.measurementMatrix.at<float>(0, 0) = 1;
	filter_.measurementMatrix.at<float>(1, 1) = 1;

	/*
	for (int i = 0; i < 3; i++){
		filter_.measurementMatrix.at<float>(i, i) = 1;
		filter_.measurementMatrix.at<float>(i + 3, i + 6) = 1;
	}
	*/

	//cv::Vec3f eulers = CiToCv<float>(ConvertQuatToEulers(ci::Quatf(start_pose[3], start_pose[4], start_pose[5], start_pose[6])));

	filter_.statePost.at<float>(0) = start_pose[0];
	filter_.statePost.at<float>(1) = start_pose[1];
	filter_.statePost.at<float>(2) = 0;
	filter_.statePost.at<float>(3) = 0;

	cv::setIdentity(filter_.processNoiseCov, cv::Scalar::all(1)); //uncertainty in the model
	cv::setIdentity(filter_.measurementNoiseCov, cv::Scalar::all(1e-2)); //uncertainty in the measurement
	cv::setIdentity(filter_.errorCovPost, cv::Scalar::all(1));

}

void KalmanFilterTracker::UpdatePoseWithMotionModel(std::vector<cv::Mat> &pose_measurement, float dt, std::vector<cv::Point3f> &next_pose, int Index){

	/*
	ci::Matrix44f pose = model->GetBasePose();
	ci::Matrix33f rotation = pose.subMatrix33(0, 0);

	ci::Quatf rq(rotation);

	ci::Vec3f eulers = ConvertQuatToEulers(rq);

	ci::Vec3f translation = pose.getTranslate().xyz();

	std::vector<float> measurement_vector = { translation[0], translation[1], translation[2], eulers[0], eulers[1], eulers[2] };

	cv::Mat pose_measurement;
	for (auto i = 0; i < measurement_vector.size(); ++i){
		pose_measurement.push_back(measurement_vector[i]);
	}
	*/

	filter_.transitionMatrix.at<float>(0, 2) = dt;
	filter_.transitionMatrix.at<float>(1, 3) = dt;

	const cv::Mat prediction = filter_.predict(); //compute prior P(w_{t}|x_{1},...,x_{t-1}) (using Chapman-Kolomogorov e.q.)
	const cv::Mat estimation = filter_.correct(pose_measurement[Index]); //compute posterior by combining the measurement likelihood with the prior
	
	cv::Vec2f updated_translation(estimation.at<float>(0), estimation.at<float>(1));

	next_pose[Index].x = estimation.at<float>(0);
	next_pose[Index].y = estimation.at<float>(1);

	/*
	//don't use the velocities in this part
	cv::Vec3f updated_translation(estimation.at<float>(0), estimation.at<float>(1), estimation.at<float>(2));
	cv::Vec3f updated_rotation_as_euler(estimation.at<float>(6), estimation.at<float>(7), estimation.at<float>(8));
	*/

	//model->SetBasePose(Pose(ci::Quatf(ttrk::ConvertEulersToQuat(CvToCi<float>(updated_rotation_as_euler))), updated_translation));

}