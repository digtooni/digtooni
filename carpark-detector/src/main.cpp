#include <iostream>
#include <string>
#include <math.h>
#include <algorithm>

#include <signal.h>

#include "../inc/window_viewer.hpp"
#include "../inc/helpers.hpp"
#include "../inc/database.hpp"
#include "../inc/occupancy_detector.hpp"
#include "../inc/timer.hpp"
#include "../inc/thread_helpers.hpp"
#include "../inc/video_writer.hpp"
#include "../inc/video_capture.hpp"

#include <opencv2/opencv.hpp>
#include <fstream>

using namespace std;
using namespace cv;
using namespace cv::gpu;
bool setup(int argc, const char **argv, VideoCaptureWrapper *&cap, VideoWriterWrapper *&writer, OccupancyDetector *&detector, DatabaseUploader *&dbu, ThreadSafeVideoDisplay *&display);

static ThreadWrapper *main_thread = nullptr;

void sigint(int ) {
  if (main_thread){
    main_thread->run = false;
  }
}


int main(int argc, const char** argv){

  signal(SIGINT, sigint);

  DatabaseUploader *dbu = 0x0;
  Timer main_timer;
  OccupancyDetector *detector = 0x0;
  VideoCaptureWrapper *cap = 0x0;
  VideoWriterWrapper *writer = 0x0;
  ThreadSafeVideoDisplay *display = nullptr;

  //set everything up
  if (!setup(argc, argv, cap, writer, detector, dbu, display)){
    fprintf(stderr, "\nError in setting up!\n");
    if (dbu) delete dbu;
    if (detector) delete detector;
    if (cap) delete cap;
    if (main_thread) delete main_thread;
    return 1;
  }

  if (dbu == nullptr) throw std::runtime_error("Error, database not initialised!");
  if (detector == nullptr) throw std::runtime_error("Error, detector not initilialized");

  printf("Use ctrl+c to exit safely...\n");

  main_thread = new ThreadWrapper(cap);

  
  if(detector->DisplayToScreen()){
    //cv::namedWindow("window");
  }

  main_thread->InitDetector(detector);
  main_thread->InitDatabase(dbu);

  main_thread->RunThread(display);
  display->DisplayVideo();

  main_thread->thread_instance->join();
  display->run = false;
   
  
  printf("Finished... cleaning up.\n");


  //clean up after
  if (dbu) delete dbu;
  if (detector) delete detector;
  if (cap) delete cap;

  printf("Done. Exiting. \n");
  return 0;

}
bool setup(int argc, const char **argv, VideoCaptureWrapper *&cap, VideoWriterWrapper *&writer, OccupancyDetector *&detector, DatabaseUploader *&dbu, ThreadSafeVideoDisplay *&display) {

  cv::CommandLineParser cmd(argc, argv,
    "{ o | output_videofile | empty            | output video file, if empty then displays to screen }"
    "{ i | input_videofile  | empty            | input video file, if empty then uses the camera }"
    "{ g | grid_file        | empty            | the xml grid definition file }"
    "{ h | help             | false            | print help message }"
    "{ d | detector_file    | empty            | path to detector file }"
    "{ c | camera_cfg       | camera.xml       | camera config file }"
    "{ k | camera_id        | -1               | the ID for the camera }" );

	if (cmd.get<bool>("help")){
		cout << "Usage : carparks [options]" << endl;
		cout << "Avaible options:" << endl;
		cmd.printParams(); 
		return false;
	}

  const std::string input_videofile = cmd.get<std::string>("input_videofile");
	
  if (input_videofile == "empty"){
	  cap = new VideoCaptureWrapper();
	}
	else{
    cap = new VideoCaptureWrapper(input_videofile);
	}
	if (!cap->isOpened()){
    cerr << "can not open camera or video file at:" << input_videofile << endl;
	  return false;
	}
    
  Camera cam(cmd.get<std::string>("camera_cfg"));

  bool display_to_screen = false;
  std::string output_videofile = cmd.get<std::string>("output_videofile");
  if (output_videofile != "empty"){
    
    if (output_videofile[output_videofile.length() - 4] == '.'){
      writer = new VideoWriterWrapper(output_videofile, VideoWriterWrapper::WriterType::WRITE_VIDEO);
      writer->OpenVideoFile(output_videofile, cam.GetWindowSize());
    }
    else{
      writer = new VideoWriterWrapper(output_videofile, VideoWriterWrapper::WriterType::WRITE_IMAGES);
    }

    display = ThreadSafeVideoDisplay::InitForVideoWriter("", cam.GetWindowSize());
  }
  else{
    display = ThreadSafeVideoDisplay::InitForWindowDisplay(2, 1, cam.GetWindowSize(), "Tracker");
    display_to_screen = true;
  }

  try{



    std::vector < std::string > detector_files;

    std::string detector_file = cmd.get<std::string>("detector_file");

    int camera_id = cmd.get<int>("camera_id");

    std::string grid_file = cmd.get<std::string>("grid_file");

    detector = new OccupancyDetector(grid_file, detector_file, camera_id, display_to_screen);
    detector->AddCamera(cam); 

    dbu = new DatabaseUploader("database.net", "username", "password", "DB name");
    
  }catch(std::runtime_error &e){
    std::cerr << e.what();
    return false;
  }

  return true;
}
