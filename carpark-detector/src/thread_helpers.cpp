#include "../inc/thread_helpers.hpp"
#include <chrono>

size_t FixedSizeVideoWriter::MAX_NUM_FRAMES = 500;


bool FixedSizeVideoWriter::Open(const std::string &filename, const size_t frames_per_second, const cv::Size size_of_frame){

  fps = frames_per_second;
  frame_size = size_of_frame;

  const size_t index = filename.find_last_of('.');
  main_filename = filename.substr(0, index);
  extension = filename.substr(index + 1, 3);
  
  file_index = 0;
  frame_count = 0;
  image_save_count = 0;

  writer = new cv::VideoWriter();

  return writer->open(GenerateNewFilename(), CV_FOURCC('M', 'J', 'P', 'G'), fps, frame_size);

}

void FixedSizeVideoWriter::WriteFrame(const std::string &prefix, const cv::Mat &frame){

  if (frame_count < MAX_NUM_FRAMES){
    ///COMMENTED OUT WRITER TO SAVE IMAGES INSTEAD OF VIDEO
    //*writer << frame;
    
    // write as a series of still images instead of video
    std::stringstream strstr;
    strstr << image_save_count;
    std::string imgFileName = prefix + strstr.str() + ".jpg";
    cv::imwrite(imgFileName, frame);
    
    frame_count++;
    image_save_count++;
    return;
  }
  else{
    delete writer;
    writer = new cv::VideoWriter();
    writer->open(GenerateNewFilename(), CV_FOURCC('M', 'J', 'P', 'G'), fps, frame_size);
    frame_count = 1;
  }

}

std::string FixedSizeVideoWriter::GenerateNewFilename(){
  std::stringstream ss;
  ss << file_index;
  file_index++;
  return main_filename + "_" + ss.str() + "." + extension;
}

ThreadSafeVideoDisplay *ThreadSafeVideoDisplay::InitForWindowDisplay(const size_t num_x_wins, const size_t num_y_wins, const cv::Size individual_win_size, const std::string &name){
  ThreadSafeVideoDisplay *v = new ThreadSafeVideoDisplay();
  v->SetupWindowDisplay(num_x_wins, num_y_wins, individual_win_size, name);
  return v;
}

ThreadSafeVideoDisplay *ThreadSafeVideoDisplay::InitForVideoWriter(const std::string &video_file_name, const cv::Size image_size){
  ThreadSafeVideoDisplay *v = new ThreadSafeVideoDisplay();
  v->SetupVideoWriter(video_file_name, image_size);
  v->SetupAltVideoWriter(video_file_name, image_size);
  return v;
}

ThreadSafeVideoDisplay::ThreadSafeVideoDisplay() :run(true), wv(nullptr), use_display_window(true), current_frame_index(0) {}

ThreadSafeVideoDisplay::~ThreadSafeVideoDisplay() {
  if (wv != nullptr) delete wv;
}

cv::Mat ThreadSafeVideoDisplay::GetNextFrame() {

  static cv::Mat frame;
  FrameAndIndex TempFM;
  mutex.lock();
  
  if (frames_to_show.size() > 5){
    TempFM = frames_to_show.back();
    frames_to_show.clear();
    frames_to_show.push_back(TempFM);
    current_frame_index = frames_to_show[0].second;
  }
  
  if (frames_to_show.size() > 0){
    if (frames_to_show[0].second == current_frame_index){
      frame = frames_to_show[0].first;
      current_frame_index++;
    }
  }
  mutex.unlock();
  return frame;

}


void ThreadSafeVideoDisplay::PushNewAltFrame(cv::Mat &fm){

  mutex.lock();
  alt_frame = fm;
  mutex.unlock();

}

void ThreadSafeVideoDisplay::PushNewFrame(FrameAndIndex &fm){

  mutex.lock();

  frames_to_show.push_back(fm);
  std::sort(frames_to_show.begin(), frames_to_show.end(), [](const FrameAndIndex a, const FrameAndIndex b){ return a.second < b.second; });

  while (frames_to_show.size() > 0 && frames_to_show[0].second < current_frame_index){
    frames_to_show = std::vector<FrameAndIndex>(frames_to_show.begin() + 1, frames_to_show.end());
  }

  mutex.unlock();

}

void ThreadSafeVideoDisplay::SetupVideoWriter(const std::string &filename, const cv::Size size){
  time_t FileTimeStamp = time(0);
  const std::string StrFileTimeStamp = std::to_string(FileTimeStamp);
  const std::string FileNameTm = std::string("carpark") + StrFileTimeStamp + ".avi";
  
  if (!writer.Open(FileNameTm, 2, size)){
    throw std::runtime_error("Error, could not open video file for writing");
  }
  use_display_window = false;
}

void ThreadSafeVideoDisplay::SetupAltVideoWriter(const std::string &filename, const cv::Size size){
  time_t FileTimeStamp = time(0);
  const std::string StrFileTimeStamp = std::to_string(FileTimeStamp);
  const std::string FileNameTm = std::string("carpark_alt") + StrFileTimeStamp + ".avi";

  if (!alt_writer.Open(FileNameTm, 2, size)){
    throw std::runtime_error("Error, could not open video file for writing");
  }
}

void ThreadSafeVideoDisplay::SetupWindowDisplay(const size_t num_x_wins, const size_t num_y_wins, const cv::Size individual_win_size, const std::string &name){
  wv = new WindowViewer(num_x_wins, num_y_wins, individual_win_size, name);
  use_display_window = true;
}

inline bool CompareImages(const cv::Mat &im1, const cv::Mat &im2){
  
  if (im1.size() != im2.size() || im1.channels() != im2.channels()){
    return false;
  }

  for (int r = 0; r < im1.rows; ++r){
    for (int c = 0; c < im2.cols; ++c){

      if (im1.type() == CV_8UC3){
        if (im1.at<cv::Vec3b>(r, c) != im2.at<cv::Vec3b>(r, c)){
          return false;
        }
      }
      else if (im1.type() == CV_8UC1){
        if (im1.at<unsigned char>(r, c) != im2.at<unsigned char>(r, c)){
          return false;
        }
      }
      else if (im1.type() == CV_32FC1){
        if (im1.at<float>(r, c) != im2.at<float>(r, c)){
          return false;
        }
      }
      else{
        throw std::runtime_error("Error, incompatible types!");
      }   
    }
  }



  return true;

}

void ThreadSafeVideoDisplay::DisplayVideo() {
  
  cv::Mat previous_frame, previous_alt_frame;

  while (run){

    std::clock_t startcputime = std::clock();

    cv::Mat f = GetNextFrame();
    if (!f.empty()){
      if (use_display_window){

        mutex.lock();

        wv->UpdateSubWindow(0, 0, f);
        if (!alt_frame.empty()){
          wv->UpdateSubWindow(1,0, alt_frame);
        }
        if (!wv->RefreshFrame())
          break;

        mutex.unlock();
      }
      else{

        mutex.lock();

        if (!previous_frame.empty() && !CompareImages(previous_frame, f)){
          writer.WriteFrame("main", f);
          previous_frame = f;
        }
        
        if (!alt_frame.empty() && !CompareImages(previous_alt_frame, alt_frame)){
          alt_writer.WriteFrame("alt", alt_frame);
          previous_alt_frame = alt_frame;
        }

        mutex.unlock();

      }
    }

  }
}

FrameAndIndex ThreadSafeVideoCapture::Read() {

  
  mutex.lock();
  //for (int i = 0; i < 50; ++i)
  cv::Mat m = cap->ReadFrame();
  if (m.size() == cv::Size(800, 450)){
    cv::resize(m, m, cv::Size(960, 540), 0.0, 0.0, cv::INTER_CUBIC);
    printf("Warning. Resizing frames!\n");
  }
  FrameAndIndex fm;
  fm.first = m;
  fm.second = Timer::GetFrameCount();
  Timer::IncreaseFrameCount();
  mutex.unlock();
  return fm;

}

std::mutex ThreadSafeVideoCapture::mutex;

ThreadWrapper::ThreadWrapper(VideoCaptureWrapper *cap_) : cap(cap_), viewer(nullptr), thread_instance(nullptr), detector(nullptr), dbu(nullptr) { }
ThreadWrapper::~ThreadWrapper() { if (thread_instance) delete thread_instance; }

void ThreadWrapper::RunThread(ThreadSafeVideoDisplay *viewer_){

  run = true;

  viewer = viewer_;

  if (dbu == nullptr) throw std::runtime_error("Error, database not initialised!");

  if (detector == nullptr) throw std::runtime_error("Error, detector not initilialized");

  if (thread_instance == nullptr) thread_instance = new std::thread(&ThreadWrapper::RunThreadInternal, this);

}

void ThreadWrapper::InitDetector(OccupancyDetector *detector_){ detector = detector_; }
void ThreadWrapper::InitDatabase(DatabaseUploader *dbu_){ dbu = dbu_; }

void ThreadWrapper::RunThreadInternal(){

  

  while (run){

    timer.Start();

    FrameAndIndex frame = cap.Read();

    if (frame.first.empty()) {
      printf("Got empty frame... Exiting.\n\n");
      break;
    }

    
    auto detections = detector->DetectOccupancy(frame.first, false);

    if (timer.GetFrameCount() > 5)
      //dbu->SQLDataUpload(detections, timer.GetFrameCount(), timer.GetCurrentFps(), timer.GetCurrentTime(), detector->GetCameraID(), true);

    char key = 0;
    if (detector->DisplayToScreen()){
      viewer->PushNewFrame(frame);
    }

    timer.Stop();

  }

  viewer->run = false;

}
