#pragma once

#include <opencv2/ml/ml.hpp>
#include <opencv2/objdetect/objdetect.hpp>

class Detector {

public:

  Detector();

  bool IsLoaded() const { return loaded_; }

protected:

  bool loaded_;
  
};

class SingleCarDetector {

public:

  explicit SingleCarDetector(const std::string &detector_files);

  void DetectOnSubWindow(const cv::Mat &frame, const cv::Rect &roi, std::vector<cv::Rect> &detections);
  void DetectWholeFrame(cv::Mat &frame, std::vector<cv::Rect> &detections);

  void Detect(const cv::Mat &frame, std::vector<cv::Rect> &detections);
  
  cv::Size DetectorSize() const { return hog_.winSize; }

  float GetDetectorSizeScore(const cv::Size &window_size);


protected:
 
  cv::HOGDescriptor hog_;
  float hit_threshold_;

  size_t stride_x_;
  size_t stride_y_;
  size_t padding_x_;
  size_t padding_y_;

  float shift_threshold_;
  float scale_increase_;
  float max_scale_;
  float frame_scale_;

};


class CarDetector {

public:

  void DetectWholeFrame(cv::Mat &frame, std::vector<cv::Rect> &detections);

  SingleCarDetector &GetBestDetectorSize(const cv::Size &window_size);

  CarDetector(const std::vector<std::string> &detector_files, const float rotation_angle);

  void RunDetection(cv::Mat &frame, std::vector<cv::Rect> &detections, size_t detector_index);
  void RunDetection(cv::Mat &frame, std::vector<cv::Rect> &detections);
  void RunDetection(cv::Mat &frame, const cv::Rect &roi, std::vector<cv::Rect> &detections);

  float GetAngle() const { return rotation_angle_; }

protected:

  float rotation_angle_;
  std::vector<SingleCarDetector> detectors_;

};
