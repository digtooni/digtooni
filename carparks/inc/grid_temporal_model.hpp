#pragma once

#include <boost/circular_buffer.hpp>

#include "grid_region.hpp"

class GridTemporalModel {

public:

  explicit GridTemporalModel(GridRegion &region) : region_(region), occupancy_(10) { }

  void Update(bool occupancy){}

protected:

  GridRegion &region_;
  boost::circular_buffer<bool> occupancy_;

};