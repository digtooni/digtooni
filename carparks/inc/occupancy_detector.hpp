#ifndef __OCCUPANCY_DETECTOR_HPP__
#define __OCCUPANCY_DETECTOR_HPP__

#include<vector>
#include<opencv2/opencv.hpp>
#include <array>

#include "parking_space.hpp"
#include "detector.hpp"
#include "camera.hpp"
#include "helpers.hpp"
#include "background_sub.hpp"
#include "grid_temporal_model.hpp"

class OccupancyDetector {

public:

  OccupancyDetector(const std::string &grid_file, const std::vector<std::string> &detector_files, const float rotation_angle, bool use_main_detector, int camera_id, bool display_to_screen);

  void MatchDetectionsToFloorPlan(cv::Mat &frame, const std::vector<cv::Rect> &detections);
  void MatchDetectionsToFloorPlan2(cv::Mat &frame, const std::vector<cv::Rect> &detections);
  void OccupancyDetector::MatchDetectionsToFloorPlan2(cv::Mat &frame, const std::vector<cv::RotatedRect> &detections);
  void AddCamera(const Camera &cam);
  Camera *GetCamera(const size_t index);

  std::vector<ParkingSpace> DetectOccupancy(cv::Mat &frame);
  void Draw(cv::Mat &canvas);
  void DrawRegion(cv::Mat &canvas, const GridRegion &region);

  void SetupBackupDetector(const cv::Mat &first_frame, const cv::Mat &background_image);

  bool DisplayToScreen() const { return display_to_screen_; }
  int GetCameraID() const { return camera_id_; }

  bool UsingMainDetector() const { return use_main_detector_; }

  BackgroundSubtractorWrapper &BSW() { return bsw_; }

protected:

  std::vector<cv::Point> RotatePointsInFrame(const std::vector<cv::Point> &points_to_rotate, const cv::Size image_size, const float angle);

  std::vector<cv::Point> GetAllGridPoints() const;

  std::array<cv::Point, 4> TransformBoundaryPoints(cv::Point tl, cv::Point tr, cv::Point bl, cv::Point br, cv::Size im_size, const float &angle);
  void DetectOnFrameBackgroundSubtractor(cv::Mat &frame, std::vector<cv::Rect> &detections);
  void DetectOnFrameWithRotations(cv::Mat &frame, std::vector<cv::Rect> &detections);
  void DetectOnFrame(cv::Mat &frame, std::vector<cv::Rect> &detections);
  void DetectOnFrameNoRotation(cv::Mat &frame, std::vector<cv::Rect> &detections);

  void LoadGridFromFile(const std::string &grid_file);
  cv::Rect GetRotatedSearchSpace(const cv::Rect unrotated_rectangle, const cv::Size orig_image_size, const cv::Size updated_image_size, float degrees);
  cv::Rect GetRotatedSearchSpace(const std::vector<cv::Point> &unrotated_rectangle, const cv::Size orig_image_size, const cv::Size updated_image_size, float degrees);
  cv::RotatedRect GetRectFromRotatedWindow(std::array<cv::Point, 4> &boundary_points, const cv::Rect &rect_in_rotated_frame, const cv::Size orig_image_size, const cv::Size updated_image_size, float degrees);

  CarDetector detector_;
  std::vector<GridRegion> grid_regions_;

  BackgroundSubtractorWrapper bsw_;

  std::vector<Camera> cameras_;

  bool use_main_detector_;
  bool display_to_screen_;
  int camera_id_;

};


#endif