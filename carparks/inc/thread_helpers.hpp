#pragma once

#include "../inc/timer.hpp"
#include "../inc/window_viewer.hpp"
#include "../inc/occupancy_detector.hpp"
#include "../inc/database.hpp"

#include <thread>
#include <unordered_map>

typedef std::pair<cv::Mat, size_t> FrameAndIndex;


class FixedSizeVideoWriter {

public:

  FixedSizeVideoWriter() : writer(nullptr) {}
  ~FixedSizeVideoWriter() { if (writer) delete writer; writer = nullptr; }

  bool Open(const std::string &filename, const size_t frames_per_second, const cv::Size size_of_frame);

  void WriteFrame(const cv::Mat &frame);

  std::string GenerateNewFilename();

protected:

  FixedSizeVideoWriter(const FixedSizeVideoWriter &w) {}
  FixedSizeVideoWriter operator=(const FixedSizeVideoWriter &rhs){ return *this; }

  std::string main_filename;
  std::string extension;

  size_t fps;
  cv::Size frame_size;

  size_t frame_count;
  size_t image_save_count;
  size_t file_index;

  static size_t MAX_NUM_FRAMES;
  
  cv::VideoWriter *writer;

};


struct ThreadSafeVideoDisplay {

  static ThreadSafeVideoDisplay *InitForWindowDisplay(const size_t num_x_wins, const size_t num_y_wins, const cv::Size individual_win_size, const std::string &name);
  static ThreadSafeVideoDisplay *InitForVideoWriter(const std::string &video_file_name, const cv::Size image_size);

  ThreadSafeVideoDisplay();
  ~ThreadSafeVideoDisplay();

  cv::Mat GetNextFrame();
  void PushNewFrame(FrameAndIndex &fm);
  void SetupVideoWriter(const std::string &filename, const cv::Size size);
  void SetupWindowDisplay(const size_t num_x_wins, const size_t num_y_wins, const cv::Size individual_win_size, const std::string &name);
  void DisplayVideo();

  bool run;

  bool use_display_window;
  size_t current_frame_index;
  std::vector<FrameAndIndex> frames_to_show;
  std::mutex mutex;
  WindowViewer *wv;
  FixedSizeVideoWriter writer;

};

struct ThreadSafeVideoCapture{

  ThreadSafeVideoCapture(cv::VideoCapture *cap_) : cap(cap_) {}
  FrameAndIndex Read();

  static std::mutex mutex;
  cv::VideoCapture *cap;

};


struct ThreadWrapper {

  ThreadWrapper(cv::VideoCapture *cap_);
  ~ThreadWrapper();

  void RunThread(ThreadSafeVideoDisplay *viewer_);
  void InitDetector(OccupancyDetector *detector);
  void InitDatabase(DatabaseUploader *dbu_);
  void RunThreadInternal();

  ThreadSafeVideoCapture cap;
  ThreadSafeVideoDisplay *viewer;
  std::thread *thread_instance;
  OccupancyDetector *detector;
  Timer timer;
  DatabaseUploader *dbu;

};

