#pragma once
#include <ctime>
#include <opencv2/opencv.hpp>
#include <mutex>

class Timer {

public:

  Timer() : start(0), fps(10) , start_alt(time(0)) {}

  void Start() {

    mutex_.lock();
    std::cout << "average FPS = " << GetAverageFPS() << std::endl;
    start = cv::getTickCount();
    mutex_.unlock();

  }

  void Stop() {

    mutex_.lock();
    fps = cv::getTickFrequency() / (cv::getTickCount() - start);
    mutex_.unlock();
    
  }

  int64 GetCurrentTime() const {
    return time(NULL);
  }

  double GetCurrentFps() const {
    return fps;
  }

  static int64 GetFrameCount() {
    return frame_count;
  }

  static void IncreaseFrameCount() {
    ++frame_count;
  }

  float GetAverageFPS() const {
    return float(frame_count) / (time(0) - start_alt);
  }

private:

  static int64 frame_count;
  int64 start; 
  double fps;

  double fps_average;
  double start_alt;

  static std::mutex mutex_;

};
