#ifndef __WINDOW_VIEWER__
#define __WINDOW_VIEWER__

#include <opencv2/opencv.hpp>
#include <stdexcept>

/****************

index (x,y) is as follows:

  | (0,0), (0,1), (0,2), (0,3) |
  | (1,0), (1,1), (1,2), (1,3) |
  | (2,0), (2,1), (2,2), (2,3) |


*****************/

//eventually add this 

class WindowViewer {

public:
  WindowViewer(const size_t num_x_wins, const size_t num_y_wins, const cv::Size individual_win_size, const std::string &name) : num_x_wins(num_x_wins), num_y_wins(num_y_wins), individual_win_size(individual_win_size), name(name){

    window = cv::Mat::zeros(num_y_wins * individual_win_size.height, num_x_wins * individual_win_size.width, CV_8UC3);
  
    cv::namedWindow(name);

  }

  void UpdateSubWindow(const size_t x_idx, const size_t y_idx, const cv::Mat &frame, bool draw_border = false){

    if (x_idx >= num_x_wins || y_idx >= num_y_wins || frame.size() != individual_win_size){
      throw std::runtime_error("Unsupported frame size.\n");
    }

    if (frame.type() != CV_8UC3 && frame.type() != CV_8UC1){
      throw std::runtime_error("Need to add new frame type.\n");
    }

    const size_t start_col = x_idx * individual_win_size.width;
    const size_t end_col = (x_idx + 1) * individual_win_size.width;
    const size_t start_row = y_idx * individual_win_size.height;
    const size_t end_row = (y_idx + 1) * individual_win_size.height;

    for (size_t r_window = start_row, r_frame = 0; r_window < end_row; ++r_window, ++r_frame){
      for (size_t c_window = start_col, c_frame = 0; c_window < end_col; ++c_window, ++c_frame){
        if (draw_border && ((r_frame == 0 || r_frame == frame.rows - 1) || (c_frame == 0 || c_frame == frame.cols - 1))){
          window.at<cv::Vec3b>(r_window, c_window) = cv::Vec3b(255, 255, 255);
          continue;
        }
        if (frame.type() == CV_8UC3)
          window.at<cv::Vec3b>(r_window, c_window) = frame.at<cv::Vec3b>(r_frame, c_frame);
        else if (frame.type() == CV_8UC1){
          const unsigned char pix = frame.at<unsigned char>(r_frame, c_frame);
          window.at<cv::Vec3b>(r_window, c_window) = cv::Vec3b(pix, pix, pix);
        }        
      }
    }
  }

  bool RefreshFrame(){

    imshow(name, window);
    return cv::waitKey(5) != 'q';

  }


protected:

  //std::vector< std::vector< cv::Mat > > grid_of_windows;

  size_t num_x_wins;
  size_t num_y_wins;
  cv::Size individual_win_size;

  std::string name;
  
  cv::Mat window;

};


#endif
