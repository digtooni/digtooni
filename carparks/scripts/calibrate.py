import cv2
import numpy as np

# remember because of the masking you need to have the front row of the grid at the FRONT of teh camera (i.e. near the bottom)

#camera 5
#space 196

#object_pts = np.asarray([
#[155.1, 116.6,0],
#[180.95, 116.6,0],
#[155.1, 172.6,0],
#[180.95, 172.6,0]
#])


#camera 2 
#space 175

object_pts = np.asarray([
[0, 0,0],
[25.77, 0.0, 0],
[0, 56.0, 0],
[25.77, 56, 0]
])

object_pts = object_pts.astype(np.float32)

image_pts = np.asarray([
[134, 115],
[178, 102],
[185, 141],
[237, 125]
])

image_pts = np.ascontiguousarray(image_pts,dtype=np.float32)

#go pro
K = np.asarray([[ 427.86514, 0, 312.76355 ],[0, 429.27123, 239.80280 ],[0.0, 0.0, 1.0]])
dist = np.asarray([-0.36033,   0.11724,   0.00255,   -0.00424,  0.00000])
zero_dist = np.zeros(shape=(5,),dtype=np.float32)

#R_guess = np.asarray([[0.99941799, -0.00760613, -0.03325403],[-0.0335048, -0.40209671, -0.91498397],[ -0.00641185, 0.91556561, -0.40211753]])
#R_guess = R_guess.astype(np.float32)
#R_guess_vec =  cv2.Rodrigues(R_guess)[0]

#T_guess = np.asarray([-8.8639819, 7.36200574, 11.09270213])
#T_guess = T_guess.astype(np.float32)

#print object_pts
#print image_pts
#print K
#print R_guess_vec
#print T_guess

#no distrotion and ransac
retval, R,T = cv2.solvePnP(object_pts, image_pts, K, dist)#, rvec =R_guess_vec, tvec=T_guess, useExtrinsicGuess=True)

guesses = cv2.projectPoints(object_pts,R,T,K,dist)[0]
#print guesses
num_pts = image_pts.shape[0]
dists = []
dist = 0
for a,b in zip(image_pts,guesses):
  print a 
  print b
  
  x = np.linalg.norm(a-b)
  dists.append(x)
  dist += x
  
print "Total error = " + str(dist) + "\n"
print "Average error = " + str(dist/num_pts) + "\n"
#print "Errors = " + ", ".join(dists)
print cv2.Rodrigues(R)[0]
print T
