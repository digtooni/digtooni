import os

def create_space(file, name, origin_x, origin_y, x_shift, y_shift):

  file.write("<{0} type_id=\"opencv-matrix\">\n\t".format(name))
  file.write("<rows>4</rows>\n\t")
  file.write("<cols>2</cols>\n\t")  
  file.write("<dt>f</dt>\n\t")
  file.write("<data>\n\t")
  file.write("\t{0} {1}\n\t".format(origin_x, origin_y))
  file.write("\t{0} {1}\n\t".format(origin_x + x_shift, origin_y))
  file.write("\t{0} {1}\n\t".format(origin_x, origin_y + y_shift))
  file.write("\t{0} {1}\n\t".format(origin_x + x_shift, origin_y + y_shift))
  file.write("</data>\n")
  file.write("</{0}>\n".format(name))

def create_start(file):

   file.write("<?xml version=\"1.0\"?>\n<opencv_storage>\n")
   
def create_end(file):
  
  file.write("</opencv_storage>")
  

def create_camera_1():
  
  
  f = open(r'Z:\mk_smart\mk_installation1\MK_Installation1\Sensor1\grid.xml', 'w')
  create_start(f)
  
  origin_x = 0
  origin_y = 0  
  x_shift = 25.77
  y_shift = 56
  for i in range(1,43):
    
    create_space(f, "ParkingSpace{0}".format(i), origin_x, origin_y, x_shift, y_shift)
    origin_x += x_shift
  
  origin_x = 305
  origin_y = 126.5 
  x_shift = 28.09
  y_shift = 58
  for i in range(217,229):
    
    create_space(f, "ParkingSpace{0}".format(i), origin_x, origin_y, x_shift, y_shift)
    origin_x += x_shift
  
  create_end(f)
  del f

def create_camera_2():

  f = open(r'Z:\mk_smart\mk_installation1\MK_Installation1\Sensor2\grid.xml', 'w')
  create_start(f)
  
  origin_x = 0
  origin_y = 0  
  x_shift = 28.09
  y_shift = 53.06
  for i in range(174,188):
    
    create_space(f, "ParkingSpace{0}".format(i), origin_x, origin_y, x_shift, y_shift)
    origin_x += x_shift
  
  origin_x = 358.2
  origin_y = 116.0
  x_shift = -25.58
  y_shift = 56
  for i in range(160,174):
    
    create_space(f, "ParkingSpace{0}".format(i), origin_x, origin_y, x_shift, y_shift)
    origin_x += x_shift
    
  create_end(f)
  del f
  
def create_camera_3():

  f = open(r'Z:\mk_smart\mk_installation1\MK_Installation1\Sensor3\grid.xml', 'w')
  create_start(f)
  
  origin_x = 0
  origin_y = 0  
  x_shift = 28.09
  y_shift = 53.06
  for i in range(174,188):
    
    create_space(f, "ParkingSpace{0}".format(i), origin_x, origin_y, x_shift, y_shift)
    origin_x += x_shift
  
  origin_x = 358.2
  origin_y = 116.0
  x_shift = -25.58
  y_shift = 56
  for i in range(160,174):
    
    create_space(f, "ParkingSpace{0}".format(i), origin_x, origin_y, x_shift, y_shift)
    origin_x += x_shift
    
  create_end(f)
  del f 
 
def create_camera_4():

  f = open(r'Z:\mk_smart\mk_installation1\MK_Installation1\Sensor4\grid.xml', 'w')
  create_start(f)
  
  origin_x = 0
  origin_y = 0  
  x_shift = 26
  y_shift = 53.06
  for i in range(151, 160):
    
    create_space(f, "ParkingSpace{0}".format(i), origin_x, origin_y, x_shift, y_shift)
    origin_x += x_shift
  
  origin_x = 0
  origin_y = 114.0
  x_shift = 25.85
  y_shift = 56
  for i in range(196,187,-1):
    
    create_space(f, "ParkingSpace{0}".format(i), origin_x, origin_y, x_shift, y_shift)
    origin_x += x_shift
    
  origin_x = 286 
  origin_y = 114.0
  x_shift = 25.85
  y_shift = 56
  for i in range(184,188):
    
    create_space(f, "ParkingSpace{0}".format(i), origin_x, origin_y, x_shift, y_shift)
    origin_x += x_shift
    
  create_end(f)
  del f
  
def create_camera_5():

  f = open(r'Z:\mk_smart\mk_installation1\MK_Installation1\Sensor5\grid.xml', 'w')
  create_start(f)
  
  origin_x = 0
  origin_y = 0  
  x_shift = 26
  y_shift = 53.06
  #for i in range(145, 160):
  #  
  #  create_space(f, "ParkingSpace{0}".format(i), origin_x, origin_y, x_shift, y_shift)
  # origin_x += x_shift
  
  origin_x = 0
  origin_y = 116.6
  x_shift = 25.85
  y_shift = 56
  for i in range(202,188,-1):
    
    create_space(f, "ParkingSpace{0}".format(i), origin_x, origin_y, x_shift, y_shift)
    origin_x += x_shift
    
  origin_x = 0
  origin_y = 116.6 + 114
  x_shift = 25.85
  y_shift = 56
  for i in range(203,217):
    
    create_space(f, "ParkingSpace{0}".format(i), origin_x, origin_y, x_shift, y_shift)
    origin_x += x_shift
    
  create_end(f)
  del f
  
def create_camera_6():

  f = open(r'Z:\mk_smart\mk_installation1\MK_Installation1\Sensor6\grid.xml', 'w')
  create_start(f)
  
  origin_x = 0
  origin_y = 0  
  x_shift = 26
  y_shift = 53.06
  for i in range(145, 160):
    
    create_space(f, "ParkingSpace{0}".format(i), origin_x, origin_y, x_shift, y_shift)
    origin_x += x_shift
  
  origin_x = 0
  origin_y = 116.6
  x_shift = 25.85
  y_shift = 56
  for i in range(202,187,-1):
    
    create_space(f, "ParkingSpace{0}".format(i), origin_x, origin_y, x_shift, y_shift)
    origin_x += x_shift
    
  
  origin_x = 25.85
  origin_y = 116.6 + 114
  x_shift = 25.85
  y_shift = 56
  for i in range(203,206):
    
    create_space(f, "ParkingSpace{0}".format(i), origin_x, origin_y, x_shift, y_shift)
    origin_x += x_shift
  
  create_end(f)
  del f
 
def create_camera_7():

  f = open(r'Z:\mk_smart\mk_installation1\MK_Installation1\Sensor7\grid.xml', 'w')
  create_start(f)
  
  origin_x = 0
  origin_y = 0
  x_shift = -56.9
  y_shift = 25.24
  for i in range(144, 134,-1):
    
    create_space(f, "ParkingSpace{0}".format(i), origin_x, origin_y, x_shift, y_shift)
    origin_y += y_shift
  
  origin_x = -132.6
  origin_y = -242.7
  x_shift = -31.4
  y_shift = 73.2
  for i in range(112,113):
    
    create_space(f, "ParkingSpace{0}".format(i), origin_x, origin_y, x_shift, y_shift)
    origin_y += y_shift
  
  for i in range(115,116):
    
    create_space(f, "ParkingSpace{0}".format(i), origin_x, origin_y, x_shift, y_shift)
    origin_y += y_shift
  
  
  y_shift = 54.6
  for i in range(116,117):
    
    create_space(f, "ParkingSpace{0}".format(i), origin_x, origin_y, x_shift, y_shift)
    origin_y += y_shift
  
  y_shift = 59.6
  for i in range(117,118):
    
    create_space(f, "ParkingSpace{0}".format(i), origin_x, origin_y, x_shift, y_shift)
    origin_y += y_shift
  
  y_shift = 65.8
  for i in range(118,119):
    
    #create_space(f, "ParkingSpace{0}".format(i), origin_x, origin_y, x_shift, y_shift)
    origin_y += y_shift
   
  y_shift = 67.7
  for i in range(119,124):
    
    #create_space(f, "ParkingSpace{0}".format(i), origin_x, origin_y, x_shift, y_shift)
    origin_y += y_shift
    
  create_end(f)
  del f
  
def create_camera_8():

  f = open(r'Z:\mk_smart\mk_installation1\MK_Installation1\Sensor8\grid.xml', 'w')
  create_start(f)
  
  origin_x = 0
  origin_y = 0  
  x_shift = 53.06
  y_shift = 24.96
  for i in range(124, 144):
    
    create_space(f, "ParkingSpace{0}".format(i), origin_x, origin_y, x_shift, y_shift)
    origin_y += y_shift
  
  origin_x = 132
  origin_y = 7
  x_shift = 32.2
  y_shift = 56
  for i in range(123,111,-1):
    
    create_space(f, "ParkingSpace{0}".format(i), origin_x, origin_y, x_shift, y_shift)
    origin_y += y_shift
    
  create_end(f)
  del f

def create_camera_9():

  f = open(r'Z:\mk_smart\mk_installation1\MK_Installation1\Sensor9\grid.xml', 'w')
  create_start(f)
  
  origin_x = 0
  origin_y = 0  
  x_shift = 53.06
  y_shift = 24.96
  for i in range(80, 112):
    
    create_space(f, "ParkingSpace{0}".format(i), origin_x, origin_y, x_shift, y_shift)
    origin_y += y_shift
  
  origin_x = 113.2
  origin_y = 0
  x_shift = 55.2
  y_shift = 25.59
  for i in range(79,42,-1):
    
    create_space(f, "ParkingSpace{0}".format(i), origin_x, origin_y, x_shift, y_shift)
    origin_y += y_shift
    
  create_end(f)
  del f

if __name__ == "__main__":

  create_camera_1()
  create_camera_2()
  create_camera_3()
  create_camera_4()
  create_camera_5()
  create_camera_6()
  create_camera_7()
  create_camera_8()
  create_camera_9()
  
