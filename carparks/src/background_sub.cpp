#include "../inc/background_sub.hpp"
#include <opencv2/gpu/gpu.hpp>

void BackgroundSubtractorWrapper::Update(const cv::Mat &new_frame, int64 frameNumberProc){

#ifdef SYS_TEGRA
  d_frame.upload(new_frame);
#endif

  //Apply blur to frame before processing (helps to remove issues caused by camera shake, but reduces resolution and small object detection)
  cv::blur(new_frame, frameBlr, cv::Size(5, 5));

#ifdef SYS_TEGRA
  //upload the blurred frame the gpu
  cv::gpu::GpuMat d_frameBlr;
  d_frameBlr.upload(frameBlr);
#endif
 

  //update the model
  switch (method_)
  {
#ifdef SYS_TEGRA
  case FGD_STAT:
    fgd_stat.update(d_frameBlr);
    d_fgmask = fgd_stat.foreground;
    d_bgimg = fgd_stat.background;
    break;
#endif
  case SIMPLE:
    mog2_1(frameBlr, fgmask, 0.05f);
    mog2_1.getBackgroundImage(bgimg);
    break;


  case MOG:
	  // Choose between whether to run mog_1 or 2 depending on switchingCounter value
	  if (switchingCounter < switchingFrameCount){
#ifdef SYS_TEGRA
	    if (frameNumberProc < switchingFrameCount){
	      mog_1(d_frameBlr, d_fgmask, 0.001f);
	      mog_1.getBackgroundImage(d_bgimg);

	      mog_2(d_frameBlr, d_fgmaskSpare, 0.001f);
	    }
	    else{
	      mog_1(d_frameBlr, d_fgmask, 0.001f);
	      mog_1.getBackgroundImage(d_bgimg);
	      
	      mog_2(d_bgimg, d_fgmaskSpare, 0.005f);
	    }
#else
	    if (frameNumberProc < 5){
	      mog_1(frameBlr, fgmask, 0.001f);
	      mog_1.getBackgroundImage(bgimg);
	      
	      mog_2(frameBlr, fgmaskSpare, 0.001f);
	    }
	    else{
	      mog_1(frameBlr, fgmask, 0.001f);
	      mog_1.getBackgroundImage(bgimg);
	      
	      mog_2(bgimg, fgmaskSpare, 0.005f);
	    }
#endif
	    switchingCounter++;
	    std::cout << "Using DETECTOR 1" << std::endl;
	  }
	  else if (switchingCounter < (switchingFrameCount * 2)){
#ifdef SYS_TEGRA
	    mog_2(d_frameBlr, d_fgmask, 0.001f);
	    mog_2.getBackgroundImage(d_bgimg);
	    mog_1(d_bgimg, d_fgmaskSpare, 0.005f);
#else
	    mog_2(frameBlr, fgmask, 0.001f);
	    mog_2.getBackgroundImage(bgimg);
	    mog_1(bgimg, fgmaskSpare, 0.005f);
#endif
	    switchingCounter++;
	    std::cout << "Using DETECTOR 2" << std::endl;
	    if (switchingCounter >= (switchingFrameCount * 2)){ switchingCounter = 0; }
	  }
	  if (switchingCounter < 0){ switchingCounter = 0; }
	  
	  break;

  case MOG2:
	  // Choose between whether to run mog2_ 1 or 2 depending on switchingCounter value
	  if (switchingCounter < switchingFrameCount){
		  if (frameNumberProc < 5){
#ifdef SYS_TEGRA
		    mog2_1(d_frameBlr, d_fgmask);
		    mog2_1.getBackgroundImage(d_bgimg);
		    mog2_2(d_frameBlr, d_fgmaskSpare);
#else
		    mog2_1(frameBlr, fgmask);
		    mog2_1.getBackgroundImage(bgimg);
		    mog2_2(frameBlr, fgmaskSpare);
#endif
		  }
		  else{
#ifdef SYS_TEGRA
		    mog2_1(d_frameBlr, d_fgmask);
		    mog2_1.getBackgroundImage(d_bgimg);
		    mog2_2(d_bgimg, d_fgmaskSpare);
#else
		    mog2_1(frameBlr, fgmask);
		    mog2_1.getBackgroundImage(bgimg);
		    mog2_2(bgimg, fgmaskSpare);
#endif
		  }
		  switchingCounter++;
		  std::cout << "Using DETECTOR 1" << std::endl;
	  }
	  else if (switchingCounter < (switchingFrameCount * 2)){
#ifdef SYS_TEGRA
	    mog2_2(d_frameBlr, d_fgmask);
	    mog2_2.getBackgroundImage(d_bgimg);
	    mog2_1(d_bgimg, d_fgmaskSpare);
#else
	    mog2_2(frameBlr, fgmask);
	    mog2_2.getBackgroundImage(bgimg);
	    mog2_1(bgimg, fgmaskSpare);
#endif
	    switchingCounter++;
	    std::cout << "Using DETECTOR 2" << std::endl;
	    if (switchingCounter >= (switchingFrameCount * 2)){ switchingCounter = 0; }
	  }
	  if (switchingCounter < 0){ switchingCounter = 0; }
	  
	  break;
	  
#ifdef SYS_TEGRA
  case GMG:
    gmg(d_frame, d_fgmask);
    break;
#endif
  }
  
#ifdef SYS_TEGRA
  //create a foreground image from mask
  d_fgimg.create(d_frame.size(), d_frame.type());
  d_fgimg.setTo(cv::Scalar::all(0));
  d_frame.copyTo(d_fgimg, d_fgmask); 
  //download the mask and image from the gpu
  d_fgmask.download(fgmask);
  d_fgimg.download(fgimg);
  if (!d_bgimg.empty())
    d_bgimg.download(bgimg);
#endif

}

void BackgroundSubtractorWrapper::initialiseMogBackgroundSubtractorSimple(cv::BackgroundSubtractorMOG2 &mog_2){
  
  mog2_1.set("nShadowDetection", 0);
  mog2_1.set("fTau", 0.1);
  mog2_1.set("backgroundRatio", 0.9);
  mog2_1.set("varThresholdGen", 25);
  mog2_1.set("fCT", 0.05);

}

void BackgroundSubtractorWrapper::Setup(const cv::Mat &first_frame, const cv::Mat &initial_blank_background){

  cv::Mat blurred_background;
  cv::blur(initial_blank_background, blurred_background, cv::Size(10, 10));
#ifdef SYS_TEGRA
  d_frame = cv::gpu::GpuMat(first_frame);
  d_initialBG = cv::gpu::GpuMat(blurred_background);
#endif

  //create the gpu matrices
  
  switch (method_)
  {
#ifdef SYS_TEGRA
  case FGD_STAT:
    fgd_stat.create(d_frame);
    break;
#endif
  case SIMPLE:
    initialiseMogBackgroundSubtractorSimple(mog2_1);
    mog2_1(first_frame, fgmask, 0.5f);
    break;

  case MOG:
    initialiseMogBackgroundSubtractor1(mog_1);
    initialiseMogBackgroundSubtractor2(mog_2);
#ifdef SYS_TEGRA 
    mog_1(d_frame, d_fgmask, 0.01f);
    mog_2(d_frame, d_fgmask, 0.01f);
#else
    mog_1(first_frame, fgmask, 0.01f);
    mog_2(first_frame, fgmask, 0.01f);
#endif
    break;
  case MOG2:
    initialiseMog2BackgroundSubtractor1(mog2_1);
    initialiseMog2BackgroundSubtractor2(mog2_2);
#ifdef SYS_TEGRA
    mog2_1(d_frame, d_fgmask);
    mog2_2(d_frame, d_fgmaskSpare);
#else
    mog2_1(first_frame, fgmask);
    mog2_2(first_frame, fgmaskSpare);
#endif
    break;
#ifdef SYS_TEGRA
  case GMG:
    initialiseGMGBackgroundSubtractor(gmg);
    gmg.initialize(d_frame.size());
    break;
#endif
  }

  switchingCounter = 0;
  // Constant for how many processed frames used before switching
  switchingFrameCount = 20;

}


#ifdef SYS_TEGRA
void BackgroundSubtractorWrapper::initialiseMogBackgroundSubtractor1(cv::gpu::MOG_GPU &mog_1){
#else
void BackgroundSubtractorWrapper::initialiseMogBackgroundSubtractor1(cv::BackgroundSubtractorMOG &mog_1){
#endif

}

#ifdef SYS_TEGRA
void BackgroundSubtractorWrapper::initialiseMogBackgroundSubtractor2(cv::gpu::MOG_GPU &mog_2){
#else
void BackgroundSubtractorWrapper::initialiseMogBackgroundSubtractor2(cv::BackgroundSubtractorMOG &mog_2){
#endif

}

#ifdef SYS_TEGRA
void BackgroundSubtractorWrapper::initialiseMog2BackgroundSubtractor1(cv::gpu::MOG2_GPU &mog2_1){
#else
void BackgroundSubtractorWrapper::initialiseMog2BackgroundSubtractor1(cv::BackgroundSubtractorMOG2 &mog2_1){
#endif
	///mog2_1.backgroundRatio = 0.99f;
	//mog2_1.varThreshold = 15.0f;
	//mog2_1.fVarInit = 15;
#ifdef SYS_TEGRA
	mog2_1.fCT = 0.05f;
#else
	//mog2_1.bShadowDetection = false;
	mog2_1.set("nShadowDetection", 0);
	mog2_1.set("backgroundRatio", 0.85);
	mog2_1.set("varThresholdGen", 16);
	mog2_1.set("fCT", 0);
#endif

#ifdef SYS_TEGRA
	// Set shadow detection threshold
	mog2_1.fTau = 0.3f;

	// Set shadow output to black to eliminate shadows from object
	mog2_1.nShadowDetection = 0;
#endif

}

#ifdef SYS_TEGRA
void BackgroundSubtractorWrapper::initialiseMog2BackgroundSubtractor2(cv::gpu::MOG2_GPU &mog2_2){
#else
void BackgroundSubtractorWrapper::initialiseMog2BackgroundSubtractor2(cv::BackgroundSubtractorMOG2 &mog2_2){
#endif
	///mog2_2.backgroundRatio = 0.99f;
	//mog2_2.varThreshold = 15.0f;
	//mog2_2.fVarInit = 15;
#ifdef SYS_TEGRA
	mog2_2.fCT = 0.05f;
#else
	//mog2_2.bShadowDetection = false;
	mog2_2.set("nShadowDetection", 0);
	mog2_2.set("backgroundRatio", 0.85);
	mog2_2.set("varThresholdGen", 16);
	mog2_2.set("fCT", 0);
#endif
#ifdef SYS_TEGRA
  // Set shadow detection threshold
	mog2_2.fTau = 0.3f;

	// Set shadow output to black to eliminate shadows from object
	mog2_2.nShadowDetection = 0;
#endif


}

#ifdef SYS_TEGRA
void BackgroundSubtractorWrapper::initialiseGMGBackgroundSubtractor(cv::gpu::GMG_GPU &gmg){

	gmg.numInitializationFrames = 40;

}
#endif

#ifdef SYS_TEGRA
void initialiseMogBackgroundSubtractor1(cv::gpu::MOG_GPU &mog_1){
#else
void initialiseMogBackgroundSubtractor1(cv::BackgroundSubtractorMOG &mog_1){
#endif

}

#ifdef SYS_TEGRA
void initialiseMogBackgroundSubtractor2(cv::gpu::MOG_GPU &mog_2){
#else
void initialiseMogBackgroundSubtractor2(cv::BackgroundSubtractorMOG &mog_2){
#endif

}

#ifdef SYS_TEGRA
void initialiseMog2BackgroundSubtractor1(cv::gpu::MOG2_GPU &mog2_1){
#else
void initialiseMog2BackgroundSubtractor1(cv::BackgroundSubtractorMOG2 &mog2_1){
#endif

  ///mog2_1.backgroundRatio = 0.99f;
  //mog2_1.varThreshold = 15.0f;
  //mog2_1.fVarInit = 15;
#ifdef SYS_TEGRA
  mog2_1.fCT = 0.05f;
#else
  //mog2_1.bShadowDetection = false;
	mog2_1.set("nShadowDetection", 0);
	mog2_1.set("backgroundRatio", 0.85);
	mog2_1.set("varThresholdGen", 16);
	mog2_1.set("fCT", 0);
#endif

#ifdef SYS_TEGRA
  // Set shadow detection threshold
  mog2_1.fTau = 0.3f;
  // Set shadow output to black to eliminate shadows from object
  mog2_1.nShadowDetection = 0;
#endif

}

#ifdef SYS_TEGRA
void initialiseMog2BackgroundSubtractor2(cv::gpu::MOG2_GPU &mog2_2){
#else
void initialiseMog2BackgroundSubtractor2(cv::BackgroundSubtractorMOG2 &mog2_2){
#endif
  ///mog2_2.backgroundRatio = 0.99f;
  //mog2_2.varThreshold = 15.0f;
  //mog2_2.fVarInit = 15;
#ifdef SYS_TEGRA
  mog2_2.fCT = 0.05f;
#else
  //mog2_2.bShadowDetection = false;
	mog2_2.set("nShadowDetection", 0);
	mog2_2.set("backgroundRatio", 0.85);
	mog2_2.set("varThresholdGen", 16);
	mog2_2.set("fCT", 0);
#endif

#ifdef SYS_TEGRA
  // Set shadow detection threshold
  mog2_2.fTau = 0.3f;

  // Set shadow output to black to eliminate shadows from object
  mog2_2.nShadowDetection = 0;
#endif


}

#ifdef SYS_TEGRA
void initialiseGMGBackgroundSubtractor(cv::gpu::GMG_GPU &gmg){

  gmg.numInitializationFrames = 40;

}
#endif
