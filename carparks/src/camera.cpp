#include "../inc/camera.hpp"
#include <stdexcept>

Camera::Camera(const std::string &calibration_filename){

  cv::FileStorage fs;

  cv::Mat cam_matrix;
  fs.open(calibration_filename, cv::FileStorage::READ);
  if(!fs.isOpened()){
    throw std::runtime_error("Error, could not open camera calibration file: " + calibration_filename + "\n");
  }

  fs["Camera_Matrix"] >> cam_matrix;

  fx_ = cam_matrix.at<float>(0, 0);
  fy_ = cam_matrix.at<float>(1, 1);
  px_ = cam_matrix.at<float>(0, 2);
  py_ = cam_matrix.at<float>(1, 2);

  fs["Distortion_Coefficients"] >> distortion_params_;

  cv::Mat image_dims;
  fs["Image_Dimensions"] >> image_dims;
  image_width_ = image_dims.at<int>(0);
  image_height_ = image_dims.at<int>(1);

  fs["Camera_Pose"] >> camera_pose_;

}

Camera::Camera(const cv::Mat &intrinsic, const cv::Mat &distortion, const int image_width, const int image_height) :distortion_params_(distortion), image_width_(image_width), image_height_(image_height){

  fx_ = (float)intrinsic.at<double>(0, 0);
  fy_ = (float)intrinsic.at<double>(1, 1);
  px_ = (float)intrinsic.at<double>(0, 2);
  py_ = (float)intrinsic.at<double>(1, 2);

}

cv::Mat Camera::CameraMatrix() const {

  cv::Mat cm = cv::Mat::eye(3, 3, CV_32FC1);
  cm.at<float>(0, 0) = fx_;
  cm.at<float>(1, 1) = fy_;
  cm.at<float>(0, 2) = px_;
  cm.at<float>(1, 2) = py_;
  return cm;

}

cv::Point2i Camera::ProjectPointToPixel(const cv::Point3d &point) const {
  cv::Point2d pt = ProjectPoint(point);
  return cv::Point2i(std::round(pt.x), std::round(pt.y));
}

cv::Point2d Camera::ProjectPoint(const cv::Point3d &point_in_camera_coords) const {

  std::vector<cv::Point2d> projected_point;
  static cv::Mat rot = cv::Mat::eye(3, 3, CV_64FC1);
  static cv::Mat tran = cv::Mat::zeros(3, 1, CV_64FC1);

  cv::projectPoints(std::vector<cv::Point3d>(1, point_in_camera_coords), rot, tran, CameraMatrix(), distortion_params_, projected_point);
  if (projected_point.size() != 1) throw(std::runtime_error("Error, projected points size != 1.\n"));

  return projected_point.front();

}
