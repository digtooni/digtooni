
#include <opencv2/opencv.hpp>

#include <thread>
#include <string.h>

#include "../inc/database.hpp"

using namespace std;
using namespace sql::mysql;
using namespace sql;

DatabaseUploader::DatabaseUploader(const std::string &database_url, const std::string &username, const std::string &password, const std::string &schema) : url(database_url), username(username), password(password), schema(schema), driver(0x0), con(0x0), prep_stmt(0x0) {

  frames_between_uploads = 6;

}

DatabaseUploader::~DatabaseUploader(){

  if (res != 0x0){
    delete res;
    res = 0x0;
  }
  //if (StatementStr != 0x0){
  //  delete[] StatementStr;
  //  StatementStr = 0x0;
  //}
  if (prep_stmt != 0x0){
    delete prep_stmt;
    prep_stmt = 0x0;
  }
  if (con != 0x0){
    delete con;
    con = 0x0;
  }

}

bool DatabaseUploader::NeedsRefresh(const int64 frame_number) const {

  return (frame_number > 1 && (frame_number % frames_between_uploads) == 0);
     
}


void DatabaseUploader::SetupStatement2(const std::vector<ParkingSpace> &parking_spaces, int frame_number, int frames_averaged, time_t time_stamp, int camera_id_){


  //StatementStr = new char[100 + (20 * parking_spaces.size())];

  statement = "";
  statement += "INSERT INTO BGSub_raw(time_stamp, square_id, occupancy_change, camera_id) VALUES ";

  for (size_t i = 0; i < parking_spaces.size(); ++i){

    std::string timestamp = std::to_string(time_stamp);
    std::string square_id = std::to_string(parking_spaces[i].id);
    std::string occupancy_score = std::to_string(parking_spaces[i].score);
    std::string camera_id = std::to_string(camera_id_);

    //if (i != 0){
      if (parking_spaces[i].score != -1){
        statement += "(";
        statement += timestamp;
        statement += ",";
        statement += square_id;
        statement += ",";
        statement += occupancy_score;
        statement += ",";
        statement += camera_id;
        statement += "), ";
      }
    //}
    /*
    else
    {
      if (parking_spaces[i].score == 1){
        statement += "(";
        statement += timestamp;
        statement += ",";
        statement += square_id;
        statement += ",";
        statement += occupancy_score;
        statement += ",";
        statement += camera_id;
        statement += ")";
      }
    }
    */
  }
  // remove last two characters from 'statement' string
  statement.pop_back();
  statement.pop_back();
  
  //std::cout << statement << std::endl;

}


void DatabaseUploader::SetupStatement(const std::vector<ParkingSpace> &parking_spaces, int frame_number, int frames_averaged, time_t time_stamp, int camera_id_){
  

  //StatementStr = new char[100 + (20 * parking_spaces.size())];

  statement = "";
  statement += "INSERT INTO parkingspace(time_stamp, square_id, occupancy_score, camera_id) VALUES ";

  for (size_t i = 0; i < parking_spaces.size(); ++i){

    std::string timestamp = std::to_string(time_stamp);
    std::string square_id = std::to_string(parking_spaces[i].id);
    std::string occupancy_score = std::to_string(parking_spaces[i].score);
    std::string camera_id = std::to_string(camera_id_);

    if (i != 0)
      statement += ", (";
    else
      statement += "(";
    statement += timestamp;
    statement += ",";
    statement += square_id;
    statement += ",";
    statement += occupancy_score;
    statement += ",";
    statement += camera_id;
    statement += ")";

  }

}

void DatabaseUploader::SendData(std::string &StatementStr) {
  
	try {

    sql::Driver *driver = sql::mysql::get_driver_instance();
    sql::Connection *con = driver->connect(url, username, password);
    // Connect to the MySQL test database
    con->setSchema(schema);

    sql::PreparedStatement *prep_stmt = con->prepareStatement(StatementStr);
		prep_stmt->execute();
		
    std::cout << "DATA SENT" << std::endl;

    //if (StatementStr){
    // std::cout << "deleting StatementStr" << std::endl;
    //  delete[] StatementStr;
    // StatementStr = 0x0;
    //  std::cout << "done" << std::endl;
    //}
    
    if (prep_stmt != 0x0){
      delete prep_stmt;
      prep_stmt = 0x0;
    }
    if (con != 0x0){
      delete con;
      con = 0x0;
    }
    }
	catch (sql::SQLException &e) {
		cout << "# ERR: SQLException in " << __FILE__;
		cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
		cout << "# ERR: " << e.what();
		//cout << " (MySQL error code: " << e.getErrorCode();
		//cout << ", SQLState: " << e.getSQLState() << " )" << endl;
  }

}


void SQLDataUploadThread(std::string &StatementStr) {

  try {

    sql::Driver *driver = sql::mysql::get_driver_instance();
    sql::Connection *con = driver->connect("database.server.net", "user", "password");
    //driver->connect(url, username, password);
    // Connect to the MySQL test database
    con->setSchema("MK_CarPark1");


    sql::PreparedStatement *prep_stmt = con->prepareStatement(StatementStr);
    prep_stmt->execute();

    std::cout << "DATA SENT" << std::endl;

    //if (StatementStr){
    // std::cout << "deleting StatementStr" << std::endl;
    //  delete[] StatementStr;
    // StatementStr = 0x0;
    //  std::cout << "done" << std::endl;
    //}

    if (prep_stmt != 0x0){
      delete prep_stmt;
      prep_stmt = 0x0;
    }
    if (con != 0x0){
      delete con;
      con = 0x0;
    }
  }
  catch (sql::SQLException &e) {
    cout << "# ERR: SQLException in " << __FILE__;
    cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
    cout << "# ERR: " << e.what();
    //cout << " (MySQL error code: " << e.getErrorCode();
    //cout << ", SQLState: " << e.getSQLState() << " )" << endl;
  }

}

//return true if there is a change, false if the same
bool DatabaseUploader::CheckForChanges(const std::vector<ParkingSpace> &spaces) const{

  if (spaces.size() != cache_of_spaces_.size()){
    throw std::runtime_error("Error, cache of spaces should never be different size to upload spaces.");
  }
  for (size_t i = 0; i < spaces.size(); ++i){

    if (spaces[i].id != cache_of_spaces_[i].id){
      throw std::runtime_error("Error, cache of spaces should be the same order as upload spaces.");
    }

    if (spaces[i].score != cache_of_spaces_[i].score){
      return true;
    }


  }

  return false;

}
void DatabaseUploader::SQLDataUpload(const std::vector<ParkingSpace> &detected_spaces, int frame_number, int frames_averaged, time_t time_stamp, int camera_id, bool using_svm){

  if (detected_spaces.size() == 0) return;

  if (cache_of_spaces_.empty()){

    cache_of_spaces_ = detected_spaces;

  }


  if (!CheckForChanges(detected_spaces)){
    return;
  }
  
  cache_of_spaces_ = detected_spaces;
  
  if (using_svm)
    SetupStatement(detected_spaces, frame_number, frames_averaged, time_stamp, camera_id);
  else
    SetupStatement2(detected_spaces, frame_number, frames_averaged, time_stamp, camera_id);
    
  //SendData(statement);
  std::thread SQL_thread(SQLDataUploadThread, std::ref(statement));
  //SQL_thread.join();
  SQL_thread.detach();

}
