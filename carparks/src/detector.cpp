#include "../inc/detector.hpp"
#include "../inc/config_reader.hpp"

#include <opencv2/imgproc/imgproc.hpp>
#include <fstream>
#include <opencv2/highgui/highgui.hpp>
#include <boost/filesystem.hpp>

cv::Point MovePointForCacheStride(const cv::Point &point, const cv::Size cache_stride){

	cv::Point r = point;
	while (r.x % cache_stride.width) r.x++;
	while (r.y % cache_stride.height) r.y++;
	return r;
}

void CarDetector::DetectWholeFrame(cv::Mat &frame, std::vector<cv::Rect> &detections){
    
  detectors_[0].DetectWholeFrame(frame, detections);

}

void SingleCarDetector::DetectWholeFrame(cv::Mat &frame, std::vector<cv::Rect> &detections){

#ifdef VERBOSE
  std::cout << "Running detection on frame size: (" << frame.cols << ", " << frame.rows << ")" << std::endl;
#endif

  cv::Mat large_frame;
  cv::resize(frame, large_frame, cv::Size(0, 0), frame_scale_, frame_scale_, CV_INTER_CUBIC);
  
  cv::Rect window(0, 0, large_frame.cols, large_frame.rows);

  Detect(frame, detections);

  std::vector<cv::Rect> repositioned_locations;

  for (auto &location : detections)
    repositioned_locations.push_back(cv::Rect(cv::Point(window.tl().x / frame_scale_, window.tl().y / frame_scale_) + cv::Point(location.tl().x / frame_scale_, location.tl().y / frame_scale_), cv::Size(location.width / frame_scale_, location.height / frame_scale_)));

  detections = repositioned_locations;

}

void SingleCarDetector::Detect(const cv::Mat &frame, std::vector<cv::Rect> &detections){

  

#ifdef VERBOSE
  std::cout << "Running detection on frame size: (" << frame.cols << ", " << frame.rows << ")" << std::endl;
#endif

  std::vector<double> confidences;

  cv::Mat frame_gray;
  cvtColor(frame, frame_gray, CV_BGR2GRAY);

  hog_.detectMultiScale(frame_gray, detections, confidences, shift_threshold_ * hit_threshold_, cv::Size(stride_x_, stride_y_), cv::Size(padding_x_, padding_y_), scale_increase_, max_scale_, false);

}

void SingleCarDetector::DetectOnSubWindow(const cv::Mat &frame, const cv::Rect &roi, std::vector<cv::Rect> &detections){
  
  std::vector<cv::Rect> locations;
  
  if (!cv::Rect(0, 0, frame.cols, frame.rows).contains(roi.tl()) || !cv::Rect(0, 0, frame.cols, frame.rows).contains(roi.br())){
    return;
  }
  
  cv::Mat image_roi = frame(roi).clone();
  cv::Mat nroi;
  cv::resize(image_roi, nroi, cv::Size(0, 0), frame_scale_, frame_scale_, CV_INTER_CUBIC);
  image_roi = nroi;

  while (image_roi.rows < hog_.winSize.height || image_roi.cols < hog_.winSize.width){
    cv::Mat tmp;
    cv::resize(image_roi, tmp, cv::Size(0, 0), 1.1, 1.1, CV_INTER_CUBIC);
    image_roi = tmp;
  }

  Detect(image_roi, detections);

  std::vector<cv::Rect> repositioned_locations;
  
  for (auto &location : detections)
    repositioned_locations.push_back(cv::Rect(roi.tl() + cv::Point(location.tl().x / frame_scale_, location.tl().y / frame_scale_), cv::Size(location.width / frame_scale_, location.height / frame_scale_)));

  detections = repositioned_locations;
  
}
  
std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
  std::stringstream ss(s);
  std::string item;
  while (std::getline(ss, item, delim)) {
    elems.push_back(item);
  }
  return elems;
}

std::vector<std::string> split(const std::string &s, char delim) {
  std::vector<std::string> elems;
  split(s, delim, elems);
  return elems;
}

cv::Size GetDetectorSizeFromFilename(const std::string &filename){

  std::string cleaned_filename;

  std::vector<std::string> split_root_dir = split(filename, '/');
  if (split_root_dir.size() == 1){
    split_root_dir = split(filename, '\\');
    if (split_root_dir.size() == 1)
      cleaned_filename = filename;
    else
      cleaned_filename = split_root_dir[split_root_dir.size() - 1];
  }
  else{
    cleaned_filename = split_root_dir[split_root_dir.size() - 1];
  }

  std::vector<std::string> vals = split(cleaned_filename, '_');

  if (vals.size() != 4) throw std::runtime_error("Error filename should split to 4 vals.\n");

  for(auto &val : vals){
    std::cout << "val = " << val << std::endl;
  }
  
  std::string middle = vals[1].substr(8, std::string::npos);

  std::vector<std::string> num_vals = split(middle, 'x');

  std::stringstream ss; 
  ss << num_vals[0];

  cv::Size detector_size;

  ss >> detector_size.width;

  std::stringstream ss1;

  ss1 << num_vals[1];

  ss1 >> detector_size.height;

  return detector_size;

}

SingleCarDetector::SingleCarDetector(const std::string &config_file){

  ConfigReader cfg(config_file);

  hog_.winSize.width = cfg.get_element_as_type<int>("width");
  hog_.winSize.height = cfg.get_element_as_type<int>("height");

  stride_x_ = cfg.get_element_as_type<size_t>("stride-x");
  stride_y_ = cfg.get_element_as_type<size_t>("stride-y");
  padding_x_ = cfg.get_element_as_type<size_t>("padding-x");
  padding_y_ = cfg.get_element_as_type<size_t>("padding-y");

  hog_.nlevels = cfg.get_element_as_type<size_t>("num-scales");
  shift_threshold_ = cfg.get_element_as_type<float>("shift-threshold");
  scale_increase_ = cfg.get_element_as_type<float>("scale-increase");
  max_scale_ = cfg.get_element_as_type<float>("max-scale");
  frame_scale_ = cfg.get_element_as_type<float>("frame-scale");

  if ((hog_.winSize.width - hog_.blockSize.width) % hog_.blockStride.width != 0){
    throw std::runtime_error("Error, incompatible block and window size!\n");
  }
  if ((hog_.winSize.height - hog_.blockSize.height) % hog_.blockStride.height != 0){
    throw std::runtime_error("Error, incompatible block and window size!\n");
  }

  namespace fs = boost::filesystem;

  fs::path abs_config_file_path(config_file);// , fs::current_path());
  std::string descriptor_vector_file = (abs_config_file_path.parent_path() / fs::path("descriptor_vector.dat")).string();

  std::ifstream ifs(descriptor_vector_file);

  std::vector<float> descriptor_vector;

  if (!ifs.is_open()){
    throw std::runtime_error("Error, could not open " + descriptor_vector_file);
  }

  //hit threshold is on the first line
  ifs >> hit_threshold_;

  while (!ifs.eof()){
    float x;
    ifs >> x;
    descriptor_vector.push_back(x);
  }

  hog_.setSVMDetector(descriptor_vector);

}

float SingleCarDetector::GetDetectorSizeScore(const cv::Size &window_size){

  float a = std::min((float)window_size.width, (float)hog_.winSize.width) / std::max((float)window_size.width, (float)hog_.winSize.width);
  float b = std::min((float)window_size.height, (float)hog_.winSize.height) / std::max((float)window_size.height, (float)hog_.winSize.height);

  return (a + b) / 2;

}

SingleCarDetector &CarDetector::GetBestDetectorSize(const cv::Size &window_size){

  size_t best_detector_idx = -1;
  float best_detector_score = std::numeric_limits<float>::min();

  for (size_t i = 0; i < detectors_.size(); ++i){
    float detector_score = detectors_[i].GetDetectorSizeScore(window_size);
    if (detector_score > best_detector_score){
      best_detector_score = detector_score;
      best_detector_idx = i;
    }
  }

  if (best_detector_idx == -1){
    throw std::runtime_error("Error, this should not be possible!");
  }

  return detectors_[best_detector_idx];

}


CarDetector::CarDetector(const std::vector<std::string> &detector_files, const float rotation_angle) : rotation_angle_(rotation_angle){

  for (const auto &df : detector_files) detectors_.push_back(SingleCarDetector(df));
  
}

void CarDetector::RunDetection(cv::Mat &frame, std::vector<cv::Rect> &detections_in, size_t detector_index){

  std::vector<cv::Rect> detections;
  detectors_[detector_index].Detect(frame, detections);

  detections_in = detections;

}

void CarDetector::RunDetection(cv::Mat &frame, std::vector<cv::Rect> &detections_in){
  
  std::vector<cv::Rect> detections;
  for (auto &detector : detectors_){
    detector.Detect(frame, detections);
  }
  
  detections_in = detections;
  
}

void CarDetector::RunDetection(cv::Mat &frame, const cv::Rect &roi, std::vector<cv::Rect> &detections_in){

  SingleCarDetector &detector = GetBestDetectorSize(roi.size());

  //std::cout << "For roi of size (" << roi.size().width << ", " << roi.size().height << ")" << " choosing detector of size (" << detector.DetectorSize().width << ", " << detector.DetectorSize().height << ")" << std::endl;

  detector.DetectOnSubWindow(frame, roi, detections_in);
	
}

Detector::Detector() : loaded_(false) {

  
}
