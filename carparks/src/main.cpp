#include <iostream>
#include <string>
#include <math.h>
#include <algorithm>

#include <signal.h>

#include "../inc/window_viewer.hpp"
#include "../inc/helpers.hpp"
#include "../inc/database.hpp"
#include "../inc/occupancy_detector.hpp"
#include "../inc/timer.hpp"
#include "../inc/thread_helpers.hpp"
#include "../inc/video_writer.hpp"
#include "../inc/video_capture.hpp"

#include <opencv2/opencv.hpp>
#include <fstream>

using namespace std;
using namespace cv;
using namespace cv::gpu;
bool setup(int argc, const char **argv, VideoCaptureWrapper *&cap, VideoWriterWrapper *&writer, VideoWriterWrapper *&alt_writer, OccupancyDetector *&detector, DatabaseUploader *&dbu);

static volatile bool run = true;

void sigint(int ) {
  run = false;
}


int main(int argc, const char** argv){

  signal(SIGINT, sigint);

  DatabaseUploader *dbu = 0x0;
  Timer main_timer;
  OccupancyDetector *detector = 0x0;
  VideoCaptureWrapper *cap = 0x0;
  VideoWriterWrapper *writer = 0x0;
  VideoWriterWrapper *alt_writer = 0x0;

  //set everything up
  if (!setup(argc, argv, cap, writer, alt_writer, detector, dbu)){
    fprintf(stderr, "\nError in setting up!\n");
    if (dbu) delete dbu;
    if (detector) delete detector;
    if (writer) delete writer;
    if (alt_writer) delete alt_writer;
    if (cap) delete cap;
    return 1;
  }

  if (dbu == nullptr) throw std::runtime_error("Error, database not initialised!");
  if (detector == nullptr) throw std::runtime_error("Error, detector not initilialized");

  printf("Use ctrl+c to exit safely...\n");

  if(detector->DisplayToScreen()){
    cv::namedWindow("window");
  }

  while (run){

    main_timer.Start();

    cv::Mat frame = cap->ReadFrame();    
    Timer::IncreaseFrameCount();

    if (frame.empty()) break;

    auto detections = detector->DetectOccupancy(frame);

    if (main_timer.GetFrameCount() > 5)
      //dbu->SQLDataUpload(detections, main_timer.GetFrameCount(), main_timer.GetCurrentFps(), main_timer.GetCurrentTime(), detector->GetCameraID(), detector->UsingMainDetector());

    if (writer){  
      writer->WriteFrame(frame);
      if (alt_writer){
        cv::Mat bsframe = detector->BSW().GetForegroundMask();
        alt_writer->WriteFrame(bsframe);
      }
    }

    char key = 0;
    if (detector->DisplayToScreen()){
      cv::imshow("window", frame);
      key = cv::waitKey(5);
    }

    main_timer.Stop();
    if(key == 'q') break;

  }
  
  printf("Finished... cleaning up.\n");


  //clean up after
  if (dbu) delete dbu;
  if (detector) delete detector;
  if (writer) delete writer;
  if (alt_writer) delete alt_writer;
  if (cap) delete cap;

  printf("Done. Exiting. \n");
  return 0;

}
bool setup(int argc, const char **argv, VideoCaptureWrapper *&cap, VideoWriterWrapper *&writer, VideoWriterWrapper *&alt_writer, OccupancyDetector *&detector, DatabaseUploader *&dbu) {

  cv::CommandLineParser cmd(argc, argv,
    "{ c | camera           | false            | use camera }"
    "{ o | save_directory    | empty            | directory to save the output files (videos images etc) }"
    "{ k | save_video       | false            | save the output as a video file rather than single images}"
    "{ f | file             | Vid3.mp4         | input video file }"
    "{ g | grid_file        | empty            | the xml grid definition file }"
    "{ h | help             | false            | print help message }"
    "{ d | detector         | empty            | file containing paths to detectors }"
    "{ q | camera_cfg       | camera.xml       | camera config file }"
    "{ b | backup_detector  | false            | use backup detector }"
    "{ i | background_image | empty            | background image for backup detector }"
    "{ s | display          | false            | output the display to the screen }"
    "{ z | camera_id        | -1               | the ID for the camera }"
    "{ r | rotation_angle   | 0                | the rotation angle for the detector (in degrees)}");

	if (cmd.get<bool>("help")){
		cout << "Usage : carparks [options]" << endl;
		cout << "Avaible options:" << endl;
		cmd.printParams(); 
		return false;
	}

	bool useCamera = cmd.get<bool>("camera");
	string videoFile = cmd.get<std::string>("file");
	
	if (useCamera){
	  cap = new VideoCaptureWrapper();
	}
	else{
	  cap = new VideoCaptureWrapper(videoFile);
	}
	if (!cap->isOpened()){
	    cerr << "can not open camera or video file at:" << videoFile << endl;
	    return false;
	}

  try{

    Camera cam(cmd.get<std::string>("camera_cfg"));

    std::vector < std::string > detector_files;

    std::string detector_file = cmd.get<std::string>("detector");
    std::ifstream ifs(detector_file);

    while (ifs.good()){

      std::string model_config;
      ifs >> model_config;
      if(model_config.length() == 0)break;
      detector_files.push_back(model_config);

    }

    bool display_to_screen = cmd.get<bool>("display");

    int camera_id = cmd.get<int>("camera_id");

    std::string grid_file = cmd.get<std::string>("grid_file");

    bool use_backup_detector = cmd.get<bool>("backup_detector");

    detector = new OccupancyDetector(grid_file, detector_files, cmd.get<float>("rotation_angle"), !use_backup_detector, camera_id, display_to_screen);
    detector->AddCamera(cam);

    cv::Mat first_frame = cap->ReadFrame();

    if (use_backup_detector){
    
      std::string s = cmd.get<std::string>("background_image");
      cv::Mat initial_blank_background = cv::imread(s);
      if (initial_blank_background.empty()){
        initial_blank_background = first_frame.clone();
      }

      detector->SetupBackupDetector(first_frame, initial_blank_background);

    }

    
    std::string save_dir = cmd.get<std::string>("save_directory");
    save_dir.erase(save_dir.find_last_not_of("\n\r\t") + 1);
    if (save_dir != "empty"){
      
      if (cmd.get<bool>("save_video")){
        writer = new VideoWriterWrapper(save_dir + "/output_video.avi", VideoWriterWrapper::WriterType::WRITE_VIDEO);
        writer->OpenVideoFile(save_dir + "/output_video.avi", first_frame.size());
        
        if (detector->UsingMainDetector() == 0){

          std::string alt_video_file = save_dir + "/output_background_subtractor_video.avi";
          alt_writer = new VideoWriterWrapper(alt_video_file, VideoWriterWrapper::WriterType::WRITE_VIDEO);
          alt_writer->OpenVideoFile(alt_video_file, first_frame.size());

        }

      }
      else{

        writer = new VideoWriterWrapper(save_dir + "/frame_", VideoWriterWrapper::WriterType::WRITE_IMAGES);
        if (detector->UsingMainDetector() == 0){
          alt_writer = new VideoWriterWrapper(save_dir + "/background_subtractor_frame_", VideoWriterWrapper::WriterType::WRITE_IMAGES);

        }
      }
      
     

      //writer.open(output_videofile, CV_FOURCC('M','J','P','G'), 10, detector->GetCamera(0)->GetWindowSize());
      std::cerr << "done" << std::endl;
    }

    dbu = new DatabaseUploader("", "", "", "");
    
  }catch(std::runtime_error &e){
    std::cerr << e.what();
    return false;
  }

  return true;
}
