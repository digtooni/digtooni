#include "../inc/occupancy_detector.hpp"
#include "../inc/helpers.hpp"
#include "../inc/parking_space.hpp"

#include <array>
#include <iomanip>

OccupancyDetector::OccupancyDetector(const std::string &grid_file, const std::vector<std::string> &detector_files, const float rotation_angle, bool use_main_detector, int camera_id, bool display_to_screen) : detector_(detector_files, rotation_angle), bsw_(SIMPLE), use_main_detector_(use_main_detector), camera_id_(camera_id), display_to_screen_(display_to_screen) {

  LoadGridFromFile(grid_file);

}

void OccupancyDetector::LoadGridFromFile(const std::string &grid_file){

  cv::FileStorage fs;

  fs.open(grid_file, cv::FileStorage::READ);

  //cv::FileNode data = fs["ParkingSpace1"];

  for (int i = 0; i < 229; ++i){

    std::stringstream ss;
    ss << "ParkingSpace" << i;

    if (fs[ss.str()].empty()) continue;

    cv::Mat space;

    fs[ss.str()] >> space;

    grid_regions_.push_back(GridRegion(i, cv::Point2f(space.at<float>(0, 0), space.at<float>(0, 1)), cv::Point2f(space.at<float>(1, 0), space.at<float>(1, 1)), cv::Point2f(space.at<float>(2, 0), space.at<float>(2, 1)), cv::Point2f(space.at<float>(3, 0), space.at<float>(3, 1))));


  }
}

void OccupancyDetector::AddCamera(const Camera &cam) {
  cameras_.push_back(cam);
}

Camera *OccupancyDetector::GetCamera(const size_t index){

  if (index > cameras_.size())
    return 0x0;
  else
    return &cameras_[index];
}

void OccupancyDetector::DetectOnFrameBackgroundSubtractor(cv::Mat &frame, std::vector<cv::Rect> &detections){

  std::vector<cv::Point> grid_points;
  cv::convexHull(GetAllGridPoints(), grid_points);

  cv::Rect detection_region = cv::boundingRect(grid_points);

  static size_t frame_num = 0;
  bsw_.Update(frame, frame_num++);


  cv::Mat background_image = bsw_.GetForegroundMask();

  for (auto &grid_square : grid_regions_){
    grid_square.InitDetectionStatus();
  }


  //std::vector<std::vector<cv::Point>> contours; std::vector<cv::RotatedRect> ellipses;
  //getContoursFromImage(background_image, contours, ellipses);

  //for (auto &contour : contours){

  //  float max_score = -1;
  //  int best_gr = -1;

  //  int score = 0;

  //  for (size_t gr = 0; gr < grid_regions_.size(); ++gr){
  //    
  //    for (auto &point : contour){

  //      if (grid_regions_[gr].ContainsPoint(cameras_[0], point)){

  //        score++;

  //      }

  //    
  //    }

  //    if (score > max_score){

  //      max_score = score;
  //      best_gr = gr;

  //    }
  //  }

  //  if (max_score > (0.5 * contour.size())){

  //    grid_regions_[best_gr].SetDetected();
  //    DrawRegion(frame, grid_regions_[best_gr]);

  //  }
  //  

  //}

  //return;

  // cv::imshow("background", background_image);


  for (auto &region : grid_regions_){

    //if (!g->RunDetectionInSquare()) continue;

#ifdef VERBOSE
    std::cout << "Running detection in square: [" << i << ", " << j << "] -> " << detection_idx << "\n";
#endif

    std::vector<std::pair<cv::Point, cv::Point> > pts = region.GetCarBox(cameras_[0]);
    std::vector<cv::Point> pts_; for (auto p : pts) {
      if (cv::Rect(0, 0, frame.cols, frame.rows).contains(p.first)){
        pts_.push_back(p.first);
      }
      if (cv::Rect(0, 0, frame.cols, frame.rows).contains(p.second)){
        pts_.push_back(p.second);
      }
    }

    if (pts_.size() == 0) continue;

    cv::Rect space = GetSafeROI(cv::boundingRect(pts_), frame);
    cv::Mat roi;
    try{
      roi = background_image(space);
    }
    catch (cv::Exception){
      continue;
    }

    size_t num_pixels[4]; for (int np = 0; np < 4; ++np) num_pixels[np] = 0;
    size_t total_pixels[4]; for (int tp = 0; tp < 4; ++tp) total_pixels[tp] = (roi.rows * roi.cols) / 4;
    for (int r = 0; r < roi.rows; ++r){
      for (int c = 0; c < roi.cols; ++c){

        if (roi.at<unsigned char>(r, c) > 127){

          if (r < roi.rows / 2 && c < roi.cols / 2){
            num_pixels[0]++;
          }
          else if (r < roi.rows / 2 && c >= roi.cols / 2){
            num_pixels[1]++;
          }
          else if (r >= roi.rows / 2 && c < roi.cols / 2){
            num_pixels[2]++;
          }
          else {
            num_pixels[3]++;
          }

        }

        //if (region.ContainsPoint(cameras_[0], cv::Point(space.tl().x + c, space.tl().y + r))){
        //  num_pixels += roi.at<unsigned char>(r, c) > 127;
        //  total_pixels++;
        //}
      }
    }

    int count_over_threshold = 0;
    for (int np = 0; np < 4; ++np){
      if (num_pixels[np] > total_pixels[np] * 0.05) count_over_threshold++;
    }

    if (count_over_threshold > 2){
      //if (num_pixels > (total_pixels * 0.15)){
      region.SetDetected();
      DrawRegion(frame, region);
    }

  }

}

void OccupancyDetector::SetupBackupDetector(const cv::Mat &first_frame, const cv::Mat &background_image){

  bsw_.Setup(first_frame, background_image);

}

std::vector<ParkingSpace> OccupancyDetector::DetectOccupancy(cv::Mat &frame){

  static bool first = true;
  if (first){

    for (auto &grid_square : grid_regions_){

      grid_square.SetupQuickIntersectionImage(frame.size(), cameras_[0]);

    }

  }
  first = false;

  std::vector<cv::Rect> detections;

  cv::Mat image = frame.clone();

  if (use_main_detector_){

    if (detector_.GetAngle() == 0){
      DetectOnFrameNoRotation(frame, detections);
    }
    else{
      DetectOnFrame(frame, detections);
    }
    std::vector<ParkingSpace> spaces;

    for (auto &grid_square : grid_regions_){

      ParkingSpace ps;


      if (!grid_square.IsInView(cameras_[0])){
        ps.score = -1.0;
      }
      else{
        if (grid_square.RegionContainsObject()){
          ps.score = 1.0;
        }
        else{
          ps.score = 0.0;
        }
      }

      spaces.push_back(ps);

    }

    return spaces;

  }
  else{

    DetectOnFrameBackgroundSubtractor(frame, detections);

    std::vector<ParkingSpace> spaces;

    for (auto &grid_square : grid_regions_){

      //the detection status has changed
      ParkingSpace ps;
      ps.id = grid_square.id;

      if (!grid_square.IsInView(cameras_[0])){
        ps.score = -1.0;
      }
      else{
        if (grid_square.RegionContainsObject()){
          ps.score = 1.0;
        }
        else{
          ps.score = 0.0;
        }
      }

      spaces.push_back(ps);

    }

    return spaces;

  }

}


//if the point is too far out of the image opencv sometimes messes up draws it in random places...
inline bool check_sane(const cv::Point2f &point, const cv::Size &fsize){

  return !((point.x < -100) || (point.x > fsize.width + 100) || (point.y < -100) || (point.y > fsize.height + 100));

}

inline bool EditPoints(cv::Point2f &p1, cv::Point2f &p2, cv::Mat &canvas){

  if (!cv::Rect(0, 0, canvas.cols, canvas.rows).contains(p1) && !cv::Rect(0, 0, canvas.cols, canvas.rows).contains(p2)){
    return false;
  }

  cv::Point2f intersection;
  if (FindIntersection(p1, p2, cv::Point2f(0, 0), cv::Point2f(canvas.cols, 0), intersection)){
    if (!cv::Rect(0, 0, canvas.cols, canvas.rows).contains(p1)){
      p1 = intersection;
    }
    else{
      p2 = intersection;
    }
  }

  if (FindIntersection(p1, p2, cv::Point2f(0, 0), cv::Point2f(0, canvas.rows), intersection)){
    if (!cv::Rect(0, 0, canvas.cols, canvas.rows).contains(p1)){
      p1 = intersection;
    }
    else{
      p2 = intersection;
    }
  }

  if (FindIntersection(p1, p2, cv::Point2f(canvas.cols, 0), cv::Point2f(canvas.cols, canvas.rows), intersection)){
    if (!cv::Rect(0, 0, canvas.cols, canvas.rows).contains(p1)){
      p1 = intersection;
    }
    else{
      p2 = intersection;
    }
  }

  if (FindIntersection(p1, p2, cv::Point2f(0, canvas.rows), cv::Point2f(canvas.cols, canvas.rows), intersection)){
    if (!cv::Rect(0, 0, canvas.cols, canvas.rows).contains(p1)){
      p1 = intersection;
    }
    else{
      p2 = intersection;
    }
  }

  return true;

}

void OccupancyDetector::DrawRegion(cv::Mat &canvas, const GridRegion &region){

  cv::Point2f tl, tr, bl, br;
  region.GetTransformedCoordinates(cameras_[0], tl, tr, bl, br, false);

  cv::Point2f tl2, tr2, bl2, br2;
  region.GetTransformedCoordinates(cameras_[0], tl2, tr2, bl2, br2);

  if (1){//PointInImage(tl, canvas) && PointInImage(tr, canvas) && PointInImage(br, canvas) && PointInImage(bl, canvas) && PointInImage(tl2, canvas) && PointInImage(tr2, canvas) && PointInImage(br2, canvas) && PointInImage(bl2, canvas)){

    if (EditPoints(tl2, tr2, canvas))
      cv::line(canvas, tl2, tr2, cv::Scalar(0, 255, 0), 1);

    if (EditPoints(br2, tr2, canvas))
      cv::line(canvas, br2, tr2, cv::Scalar(0, 255, 0), 1);

    if (EditPoints(br2, bl2, canvas))
      cv::line(canvas, br2, bl2, cv::Scalar(0, 255, 0), 1);

    if (EditPoints(bl2, tl2, canvas))
      cv::line(canvas, bl2, tl2, cv::Scalar(0, 255, 0), 1);

  }
  else{

    if (EditPoints(tl, tr, canvas))
      cv::line(canvas, tl, tr, cv::Scalar(0, 255, 0), 1);

    if (EditPoints(br, tr, canvas))
      cv::line(canvas, br, tr, cv::Scalar(0, 255, 0), 1);

    if (EditPoints(br, bl, canvas))
      cv::line(canvas, br, bl, cv::Scalar(0, 255, 0), 1);

    if (EditPoints(bl, tl, canvas))
      cv::line(canvas, bl, tl, cv::Scalar(0, 255, 0), 1);

  }

}

void OccupancyDetector::Draw(cv::Mat &canvas){

  for (auto &region : grid_regions_){

    DrawRegion(canvas, region);

  }
}

std::vector<cv::Point> OccupancyDetector::GetAllGridPoints() const {

  std::vector<cv::Point> points;

  for (auto region : grid_regions_){

    auto grid_points = region.GetProjectedRectangleAngle(cameras_[0]);
    points.insert(points.end(), grid_points.begin(), grid_points.end());

  }

  return points;

}

void OccupancyDetector::DetectOnFrameWithRotations(cv::Mat &frame, std::vector<cv::Rect> &detections){

  std::vector<cv::RotatedRect> detections_on_frame;

  /**
  *
  * Run detection on frame wiht no rotation. Then iterative over the grid squares and check with are heavily rotated. Run rotated detection on these frame.,acefhnorstw
  *
  */

  std::vector<cv::Point> grid_points;
  cv::convexHull(GetAllGridPoints(), grid_points);

  cv::Rect detection_region = cv::boundingRect(grid_points);

  if (detection_region.tl().x < 0) detection_region.x = 0;
  if (detection_region.tl().y < 0) detection_region.y = 0;

  while (detection_region.br().x >= frame.cols) detection_region.width--;
  while (detection_region.br().y >= frame.rows) detection_region.height--;

  std::vector<cv::Rect> detection_on_frame_upright;
  //detector_.RunDetection(frame, detection_region, detection_on_frame_upright);

  //for (auto &r : detection_on_frame_upright){
  //  cv::rectangle(frame, r, cv::Scalar(255, 0, 255));
  //}

  ////count number of detections
  //for (auto region : grid_regions_){

  //  //if (!region.RunDetectionOnRegion()) continue;

  //}


  double start_tick_count = cv::getTickCount();
  //std::cout << "Starting detection on " << detection_count << " locations.\n";


  for (auto &region : grid_regions_){

    //if (!g->RunDetectionInSquare()) continue;

#ifdef VERBOSE
    std::cout << "Running detection in square: [" << i << ", " << j << "] -> " << detection_idx << "\n";
#endif


    const cv::Vec2f &up_vector = region.GetUpVector(cameras_[0]);

    //'up' in pixel space is down if we count from top right
    const float angle_rads = GetAngleBetweenVectors(cv::Vec2f(0, -1), up_vector);
    const float angle_degrees = angle_rads * 180 / 3.141592;

    //if (std::abs(angle_degrees) < 10) continue;

    //rotate the frame and keep track of the edge bounds
    cv::Mat rotated_frame;
    RotateFrame(frame, rotated_frame, angle_degrees);
    std::array<cv::Point, 4> boundary_points_in_rotated_frame;
    boundary_points_in_rotated_frame = TransformBoundaryPoints(cv::Point(0, 0), cv::Point(frame.size().width - 1, 0), cv::Point(0, frame.size().height - 1), cv::Point(frame.size().width - 1, frame.size().height - 1),
      frame.size(), angle_degrees);

    //get the search space in the original image and rotate it to the new image
    cv::Rect rotated_search_area = GetRotatedSearchSpace(region.GetProjectedRectangleAngle(cameras_[0]), frame.size(), rotated_frame.size(), angle_degrees);

    if (rotated_search_area.area() == 0) {
#ifdef VERBOSE
      std::cout << "Skipping as frame not in view.\n";
#endif
      continue;
    }

#ifdef VERBOSE
    std::cout << "Starting detection on square." << std::endl;
    double detection_start_time = (double)cv::getTickCount();
#endif

    //get detections (which are normal rectangles in the rotated frame)
    detector_.RunDetection(rotated_frame, rotated_search_area, detections);

#ifdef VERBOSE
    std::cout << "Done detection on square in time: " << ((double)cv::getTickCount() - detection_start_time) / cv::getTickFrequency() << "." << std::endl;
#endif

    for (auto &detection : detections){

      cv::rectangle(rotated_frame, detection, cv::Scalar(255, 0, 0));
      cv::RotatedRect rr;
      //rotate the detection back into the original frame
      rr = GetRectFromRotatedWindow(boundary_points_in_rotated_frame, detection, rotated_frame.size(), frame.size(), -angle_degrees);
      if (rr.center == cv::Point2f(0, 0)) continue;
      detections_on_frame.push_back(rr);

    }


  }





#ifdef DEBUG
  DrawGrid(frame);
#endif

  std::vector<cv::Rect> detections_to_draw;
  //std::vector<cv::RotatedRect> detections_to_draw;

  for (auto &rr : detections_on_frame){
    cv::Point2f pts[4];
    rr.points(pts);
    std::vector<cv::Point> pts_;
    for (int ii = 0; ii < 4; ++ii) pts_.push_back(pts[ii]);
    //cv::polylines(frame, pts_, true, cv::Scalar(255, 255, 0));

    cv::Rect r = cv::boundingRect(pts_);
    detections_to_draw.push_back(r);

  }

  MatchDetectionsToFloorPlan2(frame, detections_on_frame);

  double end_tick_count = (double)cv::getTickCount();
  std::cout << "Finished detection on frame in " << (end_tick_count - start_tick_count) / cv::getTickFrequency() << std::endl;


}

void OccupancyDetector::MatchDetectionsToFloorPlan2(cv::Mat &frame, const std::vector<cv::RotatedRect> &detections){


  for (auto &grid_square : grid_regions_){
    grid_square.InitDetectionStatus();
  }

  for (auto &detection : detections){

    GridRegion *matched_square = 0x0;

    float best_distance = std::numeric_limits<float>::max();

    cv::Point center = detection.center;// detection.tl() + cv::Point(detection.width / 2, detection.height / 2);

    for (auto &grid_square : grid_regions_){

      if (grid_square.ContainsPoint(cameras_[0], center)){
        matched_square = &grid_square;
        break;
      }

    }

    cv::Point2f pts[4];
    detection.points(pts);
    std::vector<cv::Point> pts_;
    for (int ii = 0; ii < 4; ++ii) pts_.push_back(pts[ii]);
    cv::polylines(frame, pts_, true, cv::Scalar(255, 255, 0));

    if (matched_square == 0x0){
      continue;
    }
    else{
      matched_square->SetDetected();
    }


  }

  for (auto &grid_square : grid_regions_){
    if (grid_square.RegionContainsObject()){
      DrawRegion(frame, grid_square);
    }
  }

  //#endif

}


void OccupancyDetector::MatchDetectionsToFloorPlan2(cv::Mat &frame, const std::vector<cv::Rect> &detections){


  for (auto &grid_square : grid_regions_){
    grid_square.InitDetectionStatus();
  }

  for (auto &detection : detections){

    GridRegion *matched_square = 0x0;

    float best_distance = std::numeric_limits<float>::max();

    cv::Point center = detection.tl() + cv::Point(detection.width / 2, detection.height / 2);

    for (auto &grid_square : grid_regions_){

      if (grid_square.ContainsPoint(cameras_[0], center)){
        matched_square = &grid_square;
        break;
      }

    }

    cv::rectangle(frame, detection, cv::Scalar(255, 0, 0));

    if (matched_square == 0x0){
      continue;
    }
    else{
      matched_square->SetDetected();
    }


  }

  for (auto &grid_square : grid_regions_){
    if (grid_square.RegionContainsObject()){
      DrawRegion(frame, grid_square);
    }
  }

  //#endif

}

void OccupancyDetector::MatchDetectionsToFloorPlan(cv::Mat &frame, const std::vector<cv::Rect> &detections){


  for (auto &grid_square : grid_regions_){
    grid_square.InitDetectionStatus();
  }

  for (auto &detection : detections){

    GridRegion *matched_square = 0x0;

    cv::Point bl1(detection.tl().x, detection.br().y);
    cv::Point br1(detection.br().x, detection.br().y);

    float best_distance = std::numeric_limits<float>::max();
    float sanity_distance = 0.8*std::abs(bl1.x - br1.x);

    for (auto &grid_square : grid_regions_){

      cv::Point2f tl, tr, bl2, br2;
      grid_square.GetTransformedCoordinates(cameras_[0], tl, tr, bl2, br2);
      //grid_square.GetTransformedCoordinates(cameras_[0], bl2, br2, tl, tr);


      float distance = std::sqrt((bl1.x - bl2.x)*(bl1.x - bl2.x) + (bl1.y - bl2.y)*(bl1.y - bl2.y));
      distance += std::sqrt((br1.x - br2.x)*(br1.x - br2.x) + (br1.y - br2.y)*(br1.y - br2.y));



      if (distance < best_distance){// && distance < sanity_distance){
        best_distance = distance;
        matched_square = &grid_square;
      }

    }

    cv::rectangle(frame, detection, cv::Scalar(255, 0, 0));

    if (matched_square == 0x0){
      continue;
    }
    else{
      matched_square->SetDetected();
    }


  }

  for (auto &grid_square : grid_regions_){
    if (grid_square.RegionContainsObject()){
      DrawRegion(frame, grid_square);
    }
  }

  //#endif

}

void OccupancyDetector::DetectOnFrame(cv::Mat &frame, std::vector<cv::Rect> &detections){

  double start_tick_count = cv::getTickCount();
  std::cout << "Starting detection on frame.\n";

  cv::Mat rotated_frame;
  RotateFrame(frame, rotated_frame, detector_.GetAngle());

  std::vector<cv::Point> grid_points = GetAllGridPoints();
  std::vector<cv::Point> on_frame_grid_points;
  for (auto &gp : grid_points) { if (cv::Rect(0, 0, frame.cols, frame.rows).contains(gp)) on_frame_grid_points.push_back(gp); }

  std::vector<cv::Point> rotated_grid_points = RotatePointsInFrame(on_frame_grid_points, frame.size(), detector_.GetAngle());
  std::vector<cv::Point> on_rotated_frame_grid_points;
  for (auto &gp : rotated_grid_points) { if (cv::Rect(0, 0, rotated_frame.cols, rotated_frame.rows).contains(gp)) on_rotated_frame_grid_points.push_back(gp); }


  cv::convexHull(on_rotated_frame_grid_points, grid_points);
  cv::Rect detection_region = cv::boundingRect(on_rotated_frame_grid_points);

  if (detection_region.tl().x < 0) detection_region.x = 0;
  if (detection_region.tl().y < 0) detection_region.y = 0;

  while (detection_region.br().x >= rotated_frame.cols) detection_region.width--;
  while (detection_region.br().y >= rotated_frame.rows) detection_region.height--;

  std::array<cv::Point, 4> boundary_points_in_rotated_frame;
  boundary_points_in_rotated_frame = TransformBoundaryPoints(cv::Point(0, 0), cv::Point(frame.size().width - 1, 0), cv::Point(0, frame.size().height - 1), cv::Point(frame.size().width - 1, frame.size().height - 1),
    frame.size(), detector_.GetAngle());
  //boundary_points_in_rotated_frame = TransformBoundaryPoints(detection_region.tl(), cv::Point(detection_region.br().x, detection_region.tl().y), 
  //cv::Point(detection_region.tl().x, detection_region.br().y), cv::Point(detection_region.br().x, detection_region.br().y), rotated_frame.size(), detector_.GetAngle());

  //get the search space in the original image and rotate it to the new image
  //cv::Rect rotated_search_area = GetRotatedSearchSpace(region.GetProjectedRectangleAngle(cameras_[0]), frame.size(), rotated_frame.size(), angle_degrees);

  //get detections (which are normal rectangles in the rotated frame)
  detector_.RunDetection(rotated_frame, detection_region, detections);


  std::vector<cv::RotatedRect> detections_on_frame;

  for (auto &detection : detections){

    cv::rectangle(rotated_frame, detection, cv::Scalar(255, 0, 0));
    cv::RotatedRect rr;
    //rotate the detection back into the original frame
    rr = GetRectFromRotatedWindow(boundary_points_in_rotated_frame, detection, rotated_frame.size(), frame.size(), -detector_.GetAngle());
    if (rr.center == cv::Point2f(0, 0)) continue;
    detections_on_frame.push_back(rr);

  }



  //detection_region = cv::Rect(5, 5, frame.cols-10, frame.rows-10);

  //detector_.RunDetection(frame, detection_region, detections);

  //#ifdef DEBUG
  double end_tick_count = (double)cv::getTickCount();
  std::cout << "Finished detection on frame in " << (end_tick_count - start_tick_count) / cv::getTickFrequency() << std::endl;
  //#endif

  //#ifdef DEBUG
  //Draw(frame);
  MatchDetectionsToFloorPlan2(frame, detections_on_frame);

}


void OccupancyDetector::DetectOnFrameNoRotation(cv::Mat &frame, std::vector<cv::Rect> &detections){
  double start_tick_count = cv::getTickCount();
  std::cout << "Starting detection on frame.\n";

  std::vector<cv::Point> grid_points = GetAllGridPoints();
  std::vector<cv::Point> on_frame_grid_points;
  for (auto &gp : grid_points) { if (cv::Rect(0, 0, frame.cols, frame.rows).contains(gp)) on_frame_grid_points.push_back(gp); }

  cv::convexHull(on_frame_grid_points, grid_points);

  cv::Rect detection_region = cv::boundingRect(grid_points);

  if (detection_region.tl().x < 0) detection_region.x = 0;
  if (detection_region.tl().y < 0) detection_region.y = 0;

  while (detection_region.br().x >= frame.cols) detection_region.width--;
  while (detection_region.br().y >= frame.rows) detection_region.height--;

  //detection_region = cv::Rect(5, 5, frame.cols-10, frame.rows-10);

  detector_.RunDetection(frame, detection_region, detections);

  //#ifdef DEBUG
  double end_tick_count = (double)cv::getTickCount();
  std::cout << "Finished detection on frame in " << (end_tick_count - start_tick_count) / cv::getTickFrequency() << std::endl;
  //#endif

  //#ifdef DEBUG
  //Draw(frame);
  MatchDetectionsToFloorPlan2(frame, detections);
}


cv::RotatedRect OccupancyDetector::GetRectFromRotatedWindow(std::array<cv::Point, 4> &boundary_points, const cv::Rect &rect_in_rotated_frame, const cv::Size orig_image_size, const cv::Size updated_image_size, float degrees){

  cv::Point image_center(orig_image_size.width / 2.0f, orig_image_size.height / 2.0f);

  cv::Mat rot_mat = cv::getRotationMatrix2D(image_center, degrees, 1.0);

  cv::Point rotated_center = rect_in_rotated_frame.tl() + cv::Point(rect_in_rotated_frame.width / 2.0f, rect_in_rotated_frame.height / 2.0f);

  if (!cv::Rect(cv::Point(0, 0), orig_image_size).contains(rotated_center)) return cv::RotatedRect();

  cv::Mat z = cv::Mat::zeros(orig_image_size, CV_8UC1);
  z.at<unsigned char>(rotated_center) = 255;
  cv::Mat uz;
  RotateFrame(z, uz, degrees);
  cv::Point unrotated_center(-1, -1);
  for (int r = 0; r < uz.rows; ++r){
    for (int c = 0; c < uz.cols; ++c) {
      cv::Point p(c, r);
      if (uz.at<unsigned char>(p) > 0) {
        unrotated_center = p;
        r = uz.rows;
        c = uz.cols;
      }
    }
  }

  auto boundary_points_in_rotated_frame = TransformBoundaryPoints(boundary_points[0], boundary_points[1], boundary_points[3], boundary_points[2], orig_image_size, degrees);

  unrotated_center = unrotated_center - boundary_points_in_rotated_frame[0];

  //cv::Point2f unrotated_center;
  //unrotated_center.x = (rot_mat.at<double>(0, 0) * rotated_center.x + rot_mat.at<double>(0, 1) * rotated_center.y) + rot_mat.at<double>(0, 2);
  //unrotated_center.y = (rot_mat.at<double>(1, 0) * rotated_center.x + rot_mat.at<double>(1, 1) * rotated_center.y) + rot_mat.at<double>(1, 2);

  //unrotated_center.x += image_center.x;
  //unrotated_center.y -= image_center.y;

  if (!cv::Rect(cv::Point(0, 0), orig_image_size).contains(unrotated_center)) return cv::RotatedRect();

  //rotates clockwise
  cv::RotatedRect rr;
  rr.angle = -degrees;
  rr.center = unrotated_center;
  rr.size.width = rect_in_rotated_frame.width;
  rr.size.height = rect_in_rotated_frame.height;

  return rr;

}

cv::Rect OccupancyDetector::GetRotatedSearchSpace(const std::vector<cv::Point> &unrotated_rectangle, const cv::Size orig_image_size, const cv::Size updated_image_size, float degrees){

  cv::Point center(orig_image_size.width / 2.0f, orig_image_size.height / 2.0f);
  cv::Mat rot_mat = cv::getRotationMatrix2D(center, degrees, 1.0);

  cv::Mat z = cv::Mat::zeros(orig_image_size, CV_8UC1);

  for (const auto &pt : unrotated_rectangle){
    if (!cv::Rect(cv::Point(0, 0), orig_image_size).contains(pt)){
      return cv::Rect(0, 0, 0, 0);
    }
    else{
      z.at<unsigned char>(pt) = 255;
    }
  }
  cv::Mat uz;
  RotateFrame(z, uz, degrees);
  cv::Point rotated_center(-1, -1);

  std::vector<cv::Point> new_pts;
  for (int r = 0; r < uz.rows; ++r){
    for (int c = 0; c < uz.cols; ++c) {
      cv::Point p(c, r);
      if (uz.at<unsigned char>(p) > 0) {
        new_pts.push_back(p);
      }
    }
  }

  cv::Rect brect = cv::boundingRect(new_pts);

  return brect;


}

cv::Rect OccupancyDetector::GetRotatedSearchSpace(const cv::Rect unrotated_rectangle, const cv::Size orig_image_size, const cv::Size updated_image_size, float degrees){

  cv::Point center(orig_image_size.width / 2.0f, orig_image_size.height / 2.0f);

  cv::Mat rot_mat = cv::getRotationMatrix2D(center, degrees, 1.0);

  cv::Point unrotated_center = unrotated_rectangle.tl() + cv::Point(unrotated_rectangle.width / 2.0f, unrotated_rectangle.height / 2.0f);

  if (!cv::Rect(cv::Point(0, 0), orig_image_size).contains(unrotated_center)) return cv::Rect(0, 0, 0, 0);

  //HACK
  cv::Mat z = cv::Mat::zeros(orig_image_size, CV_8UC1);
  z.at<unsigned char>(unrotated_center) = 255;
  cv::Mat uz;
  RotateFrame(z, uz, degrees);
  cv::Point rotated_center(-1, -1);
  for (int r = 0; r < uz.rows; ++r){
    for (int c = 0; c < uz.cols; ++c) {
      cv::Point p(c, r);
      if (uz.at<unsigned char>(p) > 0) {
        rotated_center = p;
        r = uz.rows;
        c = uz.cols;
      }
    }
  }

  if (rotated_center == cv::Point(-1, -1)) return cv::Rect(0, 0, 0, 0);

  //cv::Mat rotated_center_;
  //cv::warpAffine(cv::Mat(unrotated_center), rotated_center_, rot_mat, cv::Size(1,2));
  //cv::Point rotated_center(rotated_center_);

  float x_scale = updated_image_size.width / orig_image_size.width;
  float y_scale = updated_image_size.height / orig_image_size.height;

  float updated_rect_width = unrotated_rectangle.width*x_scale;
  float updated_rect_height = unrotated_rectangle.height*y_scale;

  cv::Rect rotated_rect(rotated_center - cv::Point(updated_rect_width / 2.0f, updated_rect_height / 2.0f), rotated_center + cv::Point(updated_rect_width / 2.0f, updated_rect_height / 2.0f));

  return rotated_rect;

}

std::vector<cv::Point> OccupancyDetector::RotatePointsInFrame(const std::vector<cv::Point> &points_to_rotate, const cv::Size image_size, const float angle){

  cv::Mat test_image = cv::Mat::zeros(image_size, CV_8UC1);
  for (const auto &pt : points_to_rotate){
    test_image.at<unsigned char>(pt) = 255;
  }

  cv::Mat output_image;
  RotateFrame(test_image, output_image, angle);

  std::vector<cv::Point> transformed_points;
  for (int r = 0; r < output_image.rows; ++r){
    for (int c = 0; c < output_image.cols; ++c){

      if (output_image.at<unsigned char>(r, c) > 0){
        transformed_points.push_back(cv::Point(c, r));
      }

    }
  }

  return transformed_points;

}

std::array<cv::Point, 4> OccupancyDetector::TransformBoundaryPoints(cv::Point tl, cv::Point tr, cv::Point bl, cv::Point br, cv::Size im_size, const float &angle){

  cv::Mat test_image = cv::Mat::zeros(im_size, CV_8UC3);

  test_image.at<cv::Vec3b>(tl) = cv::Vec3b(255, 0, 0);
  test_image.at<cv::Vec3b>(tr) = cv::Vec3b(0, 255, 0);
  test_image.at<cv::Vec3b>(br) = cv::Vec3b(0, 0, 255);
  test_image.at<cv::Vec3b>(bl) = cv::Vec3b(255, 255, 255);

  cv::Mat output_image;
  RotateFrame(test_image, output_image, angle);

  std::array<cv::Point, 4> transformed_points;
  for (int r = 0; r < output_image.rows; ++r){
    for (int c = 0; c < output_image.cols; ++c){

      cv::Vec3b pix = output_image.at<cv::Vec3b>(r, c);
      if (pix[0] > 0 && pix[1] > 0 && pix[2] > 0){
        transformed_points[3] = cv::Point(c, r);
      }
      else if (pix[0] > 0){
        transformed_points[0] = cv::Point(c, r);
      }
      else if (pix[1] > 0){
        transformed_points[1] = cv::Point(c, r);
      }
      else if (pix[2] > 0){
        transformed_points[2] = cv::Point(c, r);
      }
    }
  }

  return transformed_points;

}
