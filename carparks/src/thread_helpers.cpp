#include "../inc/thread_helpers.hpp"


size_t FixedSizeVideoWriter::MAX_NUM_FRAMES = 500;


bool FixedSizeVideoWriter::Open(const std::string &filename, const size_t frames_per_second, const cv::Size size_of_frame){

  fps = frames_per_second;
  frame_size = size_of_frame;

  const size_t index = filename.find_last_of('.');
  main_filename = filename.substr(0, index);
  extension = filename.substr(index + 1, 3);
  
  file_index = 0;
  frame_count = 0;
  image_save_count = 0;

  writer = new cv::VideoWriter();

  return writer->open(GenerateNewFilename(), CV_FOURCC('M', 'J', 'P', 'G'), fps, frame_size);

}

void FixedSizeVideoWriter::WriteFrame(const cv::Mat &frame){

  if (frame_count < MAX_NUM_FRAMES){
    ///COMMENTED OUT WRITER TO SAVE IMAGES INSTEAD OF VIDEO
    //*writer << frame;
    
    // write as a series of still images instead of video
    std::stringstream strstr;
    strstr << image_save_count;
    std::string imgFileName = strstr.str() + ".jpg";
    cv::imwrite(imgFileName, frame);
    
    frame_count++;
    image_save_count++;
    return;
  }
  else{
    delete writer;
    writer = new cv::VideoWriter();
    writer->open(GenerateNewFilename(), CV_FOURCC('M', 'J', 'P', 'G'), fps, frame_size);
    frame_count = 1;
  }

}

std::string FixedSizeVideoWriter::GenerateNewFilename(){
  std::stringstream ss;
  ss << file_index;
  file_index++;
  return main_filename + "_" + ss.str() + "." + extension;
}

ThreadSafeVideoDisplay *ThreadSafeVideoDisplay::InitForWindowDisplay(const size_t num_x_wins, const size_t num_y_wins, const cv::Size individual_win_size, const std::string &name){
  ThreadSafeVideoDisplay *v = new ThreadSafeVideoDisplay();
  v->SetupWindowDisplay(num_x_wins, num_y_wins, individual_win_size, name);
  return v;
}

ThreadSafeVideoDisplay *ThreadSafeVideoDisplay::InitForVideoWriter(const std::string &video_file_name, const cv::Size image_size){
  ThreadSafeVideoDisplay *v = new ThreadSafeVideoDisplay();
  v->SetupVideoWriter(video_file_name, image_size);
  return v;
}

ThreadSafeVideoDisplay::ThreadSafeVideoDisplay() :run(true), wv(nullptr), use_display_window(true), current_frame_index(0) {}

ThreadSafeVideoDisplay::~ThreadSafeVideoDisplay() {
  if (wv != nullptr) delete wv;
}

cv::Mat ThreadSafeVideoDisplay::GetNextFrame() {

  static cv::Mat frame;
  FrameAndIndex TempFM;
  mutex.lock();
  
  if (frames_to_show.size() > 5){
    TempFM = frames_to_show.back();
    frames_to_show.clear();
    frames_to_show.push_back(TempFM);
    current_frame_index = frames_to_show[0].second;
  }
  
  if (frames_to_show.size() > 0){
    if (frames_to_show[0].second == current_frame_index){
      frame = frames_to_show[0].first;
      current_frame_index++;
    }
  }
  mutex.unlock();
  return frame;

}

void ThreadSafeVideoDisplay::PushNewFrame(FrameAndIndex &fm){

  mutex.lock();

  frames_to_show.push_back(fm);
  std::sort(frames_to_show.begin(), frames_to_show.end(), [](const FrameAndIndex a, const FrameAndIndex b){ return a.second < b.second; });

  while (frames_to_show.size() > 0 && frames_to_show[0].second < current_frame_index){
    frames_to_show = std::vector<FrameAndIndex>(frames_to_show.begin() + 1, frames_to_show.end());
  }

  mutex.unlock();

}

void ThreadSafeVideoDisplay::SetupVideoWriter(const std::string &filename, const cv::Size size){
  time_t FileTimeStamp = time(0);
  const std::string StrFileTimeStamp = std::to_string(FileTimeStamp);
  const std::string FileNameTm = std::string("carparks") + StrFileTimeStamp + ".avi";
  
  if(!writer.Open(filename, 2, size)){
    throw std::runtime_error("Error, could not open video file for writing");
  }
  use_display_window = false;
}

void ThreadSafeVideoDisplay::SetupWindowDisplay(const size_t num_x_wins, const size_t num_y_wins, const cv::Size individual_win_size, const std::string &name){
  wv = new WindowViewer(num_x_wins, num_y_wins, individual_win_size, name);
  use_display_window = true;
}

void ThreadSafeVideoDisplay::DisplayVideo() {
  while (run){
    cv::Mat f = GetNextFrame();
    if (!f.empty()){
      if (use_display_window){
        wv->UpdateSubWindow(0, 0, f);
        if (!wv->RefreshFrame())
          break;
      }
      else{
        writer.WriteFrame(f);
      }
    }
  }
}

FrameAndIndex ThreadSafeVideoCapture::Read() {

  cv::Mat m;
  mutex.lock();
  //for (int i = 0; i < 50; ++i)
  cap->read(m);
  if (m.size() == cv::Size(800, 450)){
    cv::resize(m, m, cv::Size(960, 540), 0.0, 0.0, cv::INTER_CUBIC);
    printf("Warning. Resizing frames!\n");
  }
  FrameAndIndex fm;
  fm.first = m;
  fm.second = Timer::GetFrameCount();
  Timer::IncreaseFrameCount();
  mutex.unlock();
  return fm;

}

std::mutex ThreadSafeVideoCapture::mutex;

ThreadWrapper::ThreadWrapper(cv::VideoCapture *cap_) : cap(cap_), viewer(nullptr), thread_instance(nullptr), detector(nullptr), dbu(nullptr) { }
ThreadWrapper::~ThreadWrapper() { if (detector) delete detector; if (thread_instance) delete thread_instance; }

void ThreadWrapper::RunThread(ThreadSafeVideoDisplay *viewer_){

  viewer = viewer_;

  if (dbu == nullptr) throw std::runtime_error("Error, database not initialised!");

  if (detector == nullptr) throw std::runtime_error("Error, detector not initilialized");

  if (thread_instance == nullptr) thread_instance = new std::thread(&ThreadWrapper::RunThreadInternal, this);

}

void ThreadWrapper::InitDetector(OccupancyDetector *detector_){ detector = detector_; }
void ThreadWrapper::InitDatabase(DatabaseUploader *dbu_){ dbu = dbu_; }

void ThreadWrapper::RunThreadInternal(){

  while (true){

    timer.Start();

    static bool first = true;
    
    FrameAndIndex frame;
    frame = cap.Read();

    if (frame.first.empty()) {
      printf("Got empty frame... Exiting.\n\n");
      break;
    }
    

    //static cv::VideoWriter writer("z:/mk_5.avi", CV_FOURCC('D', 'I', 'B', ' '), 1, cv::Size(640, 480));
    auto detections = detector->DetectOccupancy(frame.first);
    //writer.write(frame.first);

    dbu->SQLDataUpload(detections, frame.second, 1, 1.0, 1, detector->UsingMainDetector());

    timer.Stop();

    viewer->PushNewFrame(frame);

  }

  viewer->run = false;

}
