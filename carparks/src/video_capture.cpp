#include "../inc/video_capture.hpp"

std::ofstream VideoCaptureWrapper::DEBUG_LOG_FILE;

VideoCaptureWrapper::VideoCaptureWrapper(const std::string &videofile){

  cap_.open(videofile);
  if (!cap_.isOpened()){
    throw std::runtime_error("Error, could not open video file: " + videofile + "\n");
  }

  capture_type_ = VIDEOFILE;

}

VideoCaptureWrapper::VideoCaptureWrapper() {

  camera_id_ = 0;

  if (!DEBUG_LOG_FILE.is_open())
    DEBUG_LOG_FILE.open("camera_error.log", std::ios_base::out);

  cap_.open(camera_id_);


  if(!cap_.isOpened()){
    throw std::runtime_error("Error, could not open camera capture!\n");
  }

  std::stringstream ss;
  ss << get_time() << " Trying camera " << camera_id_ << "\n";
  ss << get_time() << " Successfully opened camera " << camera_id_ << "\n";
  ss << std::endl;
  
  DEBUG_LOG_FILE << ss.str();
  DEBUG_LOG_FILE.flush();
  capture_type_ = CAMERA;

  
 }

void VideoCaptureWrapper::Init(){

  camera_id_ = 0;

  if (!DEBUG_LOG_FILE.is_open())
    DEBUG_LOG_FILE.open("camera_error.log", std::ios_base::out);

  std::stringstream ss;

  cv::Mat frame;
  while (true) {

    cap_.open(camera_id_);

    ss << get_time() << " Trying camera " << camera_id_ << "\n";

    cap_ >> frame;
    if (!frame.empty()) break;
    ss << get_time() << " Failed to open camera " << camera_id_ << "\n";
    
    camera_id_++;
  }

  ss << get_time() << " Successfully opened camera " << camera_id_ << "\n";
  ss << std::endl;
  
  DEBUG_LOG_FILE << ss.str();
  DEBUG_LOG_FILE.flush();
  capture_type_ = CAMERA;

}

VideoCaptureWrapper::~VideoCaptureWrapper(){

  if (DEBUG_LOG_FILE.is_open())
    DEBUG_LOG_FILE.close();

  check_capture_status_ = false;
  
}

bool VideoCaptureWrapper::isOpened() const {
  return cap_.isOpened();
}

cv::Mat VideoCaptureWrapper::ReadFrame(){
  cv::Mat f;

  if (capture_type_ == VIDEOFILE){
    cap_ >> f;
  }
  else{

    Init();
    cap_ >> f;

  }

  if (!f.empty() && f.size() != cv::Size(640, 480)){
    cv::Mat fr;
    cv::resize(f, fr, cv::Size(640, 480));
    f = fr;
  }

  return f;

}
