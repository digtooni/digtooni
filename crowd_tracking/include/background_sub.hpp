#pragma once

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/gpu/gpu.hpp>
#include <opencv2/gpu/gpumat.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>

#define SYS_TEGRA

enum BS_Method
{
	FGD_STAT,
	MOG,
	MOG2,
	GMG
};

#ifdef SYS_TEGRA
void initialiseMogBackgroundSubtractor1(cv::gpu::MOG_GPU &mog_1);
void initialiseMogBackgroundSubtractor2(cv::gpu::MOG_GPU &mog_2);
void initialiseMog2BackgroundSubtractor1(cv::gpu::MOG2_GPU &mog2_1);
void initialiseMog2BackgroundSubtractor2(cv::gpu::MOG2_GPU &mog2_2);
void initialiseGMGBackgroundSubtractor(cv::gpu::GMG_GPU &gmg);
#else
void initialiseMogBackgroundSubtractor1(cv::BackgroundSubtractorMOG &mog_1);
void initialiseMogBackgroundSubtractor2(cv::BackgroundSubtractorMOG &mog_2);
void initialiseMog2BackgroundSubtractor1(cv::BackgroundSubtractorMOG2 &mog2_1);
void initialiseMog2BackgroundSubtractor2(cv::BackgroundSubtractorMOG2 &mog2_2);
#endif

class BackgroundSubtractorWrapper {

public:

  explicit BackgroundSubtractorWrapper(BS_Method method) : method_(method) {}

  void Setup(const cv::Mat &first_frame, const cv::Mat &initial_blank_background);

  void Update(const cv::Mat &new_frame, int64 frameNumberProc, cv::Mat &initial_blank_background);

  cv::Mat GetForegroundMask() { return fgmask; }
  cv::Mat GetForegroundImg() { return fgimg; }
  cv::Mat GetBackgroundImg() { return bgimg; }

protected:

#ifdef SYS_TEGRA
  void initialiseMogBackgroundSubtractor1(cv::gpu::MOG_GPU &mog_1);
  void initialiseMogBackgroundSubtractor2(cv::gpu::MOG_GPU &mog_2);
  void initialiseMog2BackgroundSubtractor1(cv::gpu::MOG2_GPU &mog2_1);
  void initialiseMog2BackgroundSubtractor2(cv::gpu::MOG2_GPU &mog2_2);
  void initialiseGMGBackgroundSubtractor(cv::gpu::GMG_GPU &gmg);
#else
  void initialiseMogBackgroundSubtractor1(cv::BackgroundSubtractorMOG &mog_1);
  void initialiseMogBackgroundSubtractor2(cv::BackgroundSubtractorMOG &mog_2);
  void initialiseMog2BackgroundSubtractor1(cv::BackgroundSubtractorMOG2 &mog2_1);
  void initialiseMog2BackgroundSubtractor2(cv::BackgroundSubtractorMOG2 &mog2_2);
#endif

  BS_Method method_;

#ifdef SYS_TEGRA //GPU support

  cv::gpu::MOG_GPU mog_1;
  cv::gpu::MOG_GPU mog_2;
  cv::gpu::MOG2_GPU mog2_1;
  cv::gpu::MOG2_GPU mog2_2;
  cv::gpu::GMG_GPU gmg;
  cv::gpu::FGDStatModel fgd_stat;
  
  cv::gpu::GpuMat d_fgmask; //foreground mask
  cv::gpu::GpuMat d_fgmaskSpare; //spare foreground mask for second mog detector
  cv::gpu::GpuMat d_fgimg; //foreground image (with background removed)
  cv::gpu::GpuMat d_bgimg; //background image
  cv::gpu::GpuMat d_frameBlr; //blurred image

  cv::gpu::GpuMat d_frame;
  cv::gpu::GpuMat d_initialBG;
#else
  cv::BackgroundSubtractorMOG mog_1;
  cv::BackgroundSubtractorMOG mog_2;
  cv::BackgroundSubtractorMOG2 mog2_1;
  cv::BackgroundSubtractorMOG2 mog2_2;
#endif


  cv::Mat fgmask; //mask for the foreground
  cv::Mat fgmaskSpare; // spare foreground mask for second mog detector
  cv::Mat fgimg; //foreground image
  cv::Mat bgimg; //background image
  cv::Mat blr_fgmask; //blurred foreground mask
  cv::Mat frameBlr; //blurred frame

  int switchingCounter;
  // Constant for how many processed frames used before switching
  int switchingFrameCount;


};
