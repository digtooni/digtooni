#ifndef CROWD_H
#define CROWD_H

#include "config_reader.hpp"
#include "tracker.hpp"
#include "training_model.hpp"

#ifdef SYS_TEGRA
#include "opencv2/gpu/gpu.hpp"
#endif

#include <fstream>
#include <iostream>
#include <sstream>

enum Playback { PLAY, PAUSE, NEXT };

class CrowdDetector
{
    public:
        enum Mode : int {RUN = 0, TRAIN = 1, RETRAIN = 2, MINE_FPS = 3};

        CrowdDetector(const std::string &config_path, const std::string &vid);
        void setVidNumber(int number);
        void setOutputFiles();
        void run();

        std::string video_path;
        std::string video_dir;
        std::string counts_output;
        std::string positions_output;
        int video_number;
        std::string output_path;
        std::stringstream model_config;
        bool train;
        bool retrain;
        bool mine_for_false_positives;
        int frameStart;
        bool keep_processing;
    
    protected:
        std::string getVideoName();
        void drawSVMDetections(cv::Mat &image);
        void drawPaths(cv::Mat &image);
        void trackPedestrians(std::string video_file);
        void count();
        void writeCounts();
        void recordPositions();

        std::unique_ptr<Tracker> tracker;
        std::vector<TrainingModel> models;
        Mode mode;
        cv::VideoCapture cap;
        std::vector<cv::Rect> svm_detections;
        std::vector<std::shared_ptr<Object> > pedestrians;
        std::vector<std::shared_ptr<Object> > exit_pedestrians;
        Playback vidStatus;
        int sliderPos;
        int frameCount;
        std::string win_name;
        cv::Rect countZone;
        cv::Point line_pt1;
        cv::Point line_pt2;

        //Counts
        std::vector<int> in_zone;
        int in_zone_count;
        int exited_up;
        int exited_down;
        int exited_left;
        int exited_right;
};

#endif
