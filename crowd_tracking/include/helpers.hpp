#ifndef HELPERS_H
#define HELPERS_H

#include <stdio.h>
#include <ctime>
//#include <dirent.h>
#include <boost/filesystem.hpp>
#include <ios>
#include <fstream>
#include <stdexcept>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/ml/ml.hpp>

#ifdef SYS_TEGRA
#include <opencv2/gpu/gpu.hpp>
#endif

#include <array>
#include <utility>
//#include "background_sub.hpp"

#define SVMLIGHT 1
#define LIBSVM 2

//#define TRAINHOG_USEDSVM SVMLIGHT
#define TRAINHOG_USEDSVM SVMLIGHT

#if TRAINHOG_USEDSVM == SVMLIGHT
#include "../svmlight/svmlight.h"
#define TRAINHOG_SVM_TO_TRAIN SVMlight
#elif TRAINHOG_USEDSVM == LIBSVM
#include "libsvm/libsvm.h"
#define TRAINHOG_SVM_TO_TRAIN libSVM
#endif

std::string toLowerCase(const std::string& in);
void storeCursor(void);
void resetCursor(void);

class MeanshiftGrouping
{
public:
  MeanshiftGrouping(const cv::Point3d& densKer, const std::vector<cv::Point3d>& posV,
    const std::vector<double>& wV, double eps, int maxIter = 20)
  {
    densityKernel = densKer;
    weightsV = wV;
    positionsV = posV;
    positionsCount = (int)posV.size();
    meanshiftV.resize(positionsCount);
    distanceV.resize(positionsCount);
    iterMax = maxIter;
    modeEps = eps;

    for (unsigned i = 0; i<positionsV.size(); i++)
    {
      meanshiftV[i] = getNewValue(positionsV[i]);
      distanceV[i] = moveToMode(meanshiftV[i]);
      meanshiftV[i] -= positionsV[i];
    }
  }

  void getModes(std::vector<cv::Point3d>& modesV, std::vector<double>& resWeightsV, const double eps)
  {
    for (size_t i = 0; i <distanceV.size(); i++)
    {
      bool is_found = false;
      for (size_t j = 0; j<modesV.size(); j++)
      {
        if (getDistance(distanceV[i], modesV[j]) < eps)
        {
          is_found = true;
          break;
        }
      }
      if (!is_found)
      {
        modesV.push_back(distanceV[i]);
      }
    }

    resWeightsV.resize(modesV.size());

    for (size_t i = 0; i<modesV.size(); i++)
    {
      resWeightsV[i] = getResultWeight(modesV[i]);
    }
  }

protected:
  std::vector<cv::Point3d> positionsV;
  std::vector<double> weightsV;

  cv::Point3d densityKernel;
  int positionsCount;

  std::vector<cv::Point3d> meanshiftV;
  std::vector<cv::Point3d> distanceV;
  int iterMax;
  double modeEps;

  cv::Point3d getNewValue(const cv::Point3d& inPt) const
  {
    cv::Point3d resPoint(.0);
    cv::Point3d ratPoint(.0);
    for (size_t i = 0; i<positionsV.size(); i++)
    {
      cv::Point3d aPt = positionsV[i];
      cv::Point3d bPt = inPt;
      cv::Point3d sPt = densityKernel;

      sPt.x *= exp(aPt.z);
      sPt.y *= exp(aPt.z);

      aPt.x /= sPt.x;
      aPt.y /= sPt.y;
      aPt.z /= sPt.z;

      bPt.x /= sPt.x;
      bPt.y /= sPt.y;
      bPt.z /= sPt.z;

      double w = (weightsV[i])*std::exp(-((aPt - bPt).dot(aPt - bPt)) / 2) / std::sqrt(sPt.dot(cv::Point3d(1, 1, 1)));

      resPoint += w*aPt;

      ratPoint.x += w / sPt.x;
      ratPoint.y += w / sPt.y;
      ratPoint.z += w / sPt.z;
    }
    resPoint.x /= ratPoint.x;
    resPoint.y /= ratPoint.y;
    resPoint.z /= ratPoint.z;
    return resPoint;
  }

  double getResultWeight(const cv::Point3d& inPt) const
  {
    double sumW = 0;
    for (size_t i = 0; i<positionsV.size(); i++)
    {
      cv::Point3d aPt = positionsV[i];
      cv::Point3d sPt = densityKernel;

      sPt.x *= exp(aPt.z);
      sPt.y *= exp(aPt.z);

      aPt -= inPt;

      aPt.x /= sPt.x;
      aPt.y /= sPt.y;
      aPt.z /= sPt.z;

      sumW += (weightsV[i])*std::exp(-(aPt.dot(aPt)) / 2) / std::sqrt(sPt.dot(cv::Point3d(1, 1, 1)));
    }
    return sumW;
  }

  cv::Point3d moveToMode(cv::Point3d aPt) const
  {
    cv::Point3d bPt;
    for (int i = 0; i<iterMax; i++)
    {
      bPt = aPt;
      aPt = getNewValue(bPt);
      if (getDistance(aPt, bPt) <= modeEps)
      {
        break;
      }
    }
    return aPt;
  }

  double getDistance(cv::Point3d p1, cv::Point3d p2) const
  {
    cv::Point3d ns = densityKernel;
    ns.x *= exp(p2.z);
    ns.y *= exp(p2.z);
    p2 -= p1;
    p2.x /= ns.x;
    p2.y /= ns.y;
    p2.z /= ns.z;
    return p2.dot(p2);
  }
};

inline void UnwarpQuads(cv::Mat &affine_transform, std::vector< std::array< cv::Point, 4> > &quads){

  std::vector< std::array< cv::Point, 4> > unwarped_quads;

  cv::Mat invert_transform;
  cv::invertAffineTransform(affine_transform, invert_transform);

  for (const auto &quad : quads){

    std::array<cv::Point, 4> unwarped_quad;
    for (int i = 0; i < 4; ++i){
      
      std::vector<cv::Point> in; in.push_back(quad[i]);
      std::vector<cv::Point> out;
      cv::transform(in, out, invert_transform);
      
      unwarped_quad[i] = out[0];

    }

    unwarped_quads.push_back(unwarped_quad);


  }


  quads = unwarped_quads;

}

inline cv::Rect SafeDetectionOnFrame(const cv::Rect &roi, const cv::Size &frame_size){

  cv::Rect safe_roi = roi;

  //makes assumption box is not wider than image...
  while (safe_roi.tl().x < 0){
    safe_roi.x++;
    safe_roi.width--;
  }

  while (safe_roi.tl().y < 0){
    safe_roi.y++;
    safe_roi.height--;
  }

  while (safe_roi.br().x >= frame_size.width)
    safe_roi.width--;

  while (safe_roi.br().y >= frame_size.height)
    safe_roi.height--;

  return safe_roi;
}

inline cv::Point GetCenter(cv::Point &tl, const size_t width, const size_t height){

  return tl + cv::Point(width / 2, height / 2);

}

inline cv::Point FlipPoint(cv::Point &pt, const cv::Size image_size){

  pt.x = image_size.width - pt.x;
  return pt;

}

inline cv::Rect FlipRect(cv::Rect &pt, const cv::Size image_size){

  cv::Rect r;

  r.x = image_size.width - pt.x;
  r.y = pt.y;
  r.width = pt.width;
  r.height = pt.height;
  
  return r;

}

#define MK2

struct QuickImageGrid {

  explicit QuickImageGrid(const float scale, cv::Size &frame_size){

    std::array<cv::Point, 4> a;

#ifdef MK1
    
    a[0] = cv::Point(157, 242) * scale;
    a[1] = cv::Point(204, 168) * scale;
    a[2] = cv::Point(403, 194) * scale;
    a[3] = cv::Point(383, 289) * scale;
    quads.push_back(a);

    a[0] = cv::Point(299, 42) * scale;
    a[1] = cv::Point(315, 21) * scale;
    a[2] = cv::Point(439, 26) * scale;
    a[3] = cv::Point(434, 51) * scale;
    quads.push_back(a);

    a[0] = cv::Point(436, 51) * scale;
    a[1] = cv::Point(440, 27) * scale;
    a[2] = cv::Point(567, 48) * scale;
    a[3] = cv::Point(580, 76) * scale;
    quads.push_back(a);

    a[0] = cv::Point(65, 59) * scale;
    a[1] = cv::Point(86, 41) * scale;
    a[2] = cv::Point(139, 34) * scale;
    a[3] = cv::Point(115, 52) * scale;
    
    quads.push_back(a);
    
    //center block - 3
    a[0] = cv::Point(271, 75) * scale;
    a[1] = cv::Point(296, 44) * scale;
    a[2] = cv::Point(436, 53) * scale;
    a[3] = cv::Point(428, 86) * scale;

    quads.push_back(a);

    //center block - 4
    a[0] = cv::Point(429, 84) * scale;
    a[1] = cv::Point(435, 52) * scale;
    a[2] = cv::Point(580, 77) * scale;
    a[3] = cv::Point(594, 113) * scale;

    quads.push_back(a);

    //center block - 5
    a[0] = cv::Point(241, 115) * scale;
    a[1] = cv::Point(273, 74) * scale;
    a[2] = cv::Point(428, 86) * scale;
    a[3] = cv::Point(419, 131) * scale;

    quads.push_back(a);

    //center block - 6
    a[0] = cv::Point(418, 131) * scale;
    a[1] = cv::Point(428, 86) * scale;
    a[2] = cv::Point(593, 114) * scale;
    a[3] = cv::Point(611, 165) * scale;

    quads.push_back(a);

    //center block - 7
    a[0] = cv::Point(203, 167) * scale;
    a[1] = cv::Point(241, 115) * scale;
    a[2] = cv::Point(418, 132) * scale;
    a[3] = cv::Point(405, 193) * scale;

    quads.push_back(a);

    //center block - 8
    a[0] = cv::Point(404, 194) * scale;
    a[1] = cv::Point(417, 113) * scale;
    a[2] = cv::Point(610, 163) * scale;
    a[3] = cv::Point(629, 230) * scale;

    quads.push_back(a);

    //center block - 9
    a[0] = cv::Point(156, 242) * scale;
    a[1] = cv::Point(204, 168) * scale;
    a[2] = cv::Point(404, 194) * scale;
    a[3] = cv::Point(384, 289) * scale;

    quads.push_back(a);

    //center block - 10
    a[0] = cv::Point(385, 289) * scale;
    a[1] = cv::Point(404, 195) * scale;
    a[2] = cv::Point(628, 228) * scale;
    a[3] = cv::Point(647, 325) * scale;

    quads.push_back(a);

    //left block - 2
    a[0] = cv::Point(116, 53) * scale;
    a[1] = cv::Point(142, 31) * scale;
    a[2] = cv::Point(217, 24) * scale;
    a[3] = cv::Point(187, 50) * scale;

    quads.push_back(a);

    //left block - 3
    a[0] = cv::Point(47, 76) * scale;
    a[1] = cv::Point(55, 57) * scale;
    a[2] = cv::Point(114, 53) * scale;
    a[3] = cv::Point(89, 78) * scale;

    quads.push_back(a);

    //left block - 4
    a[0] = cv::Point(86, 80) * scale;
    a[1] = cv::Point(113, 54) * scale;
    a[2] = cv::Point(188, 49) * scale;
    a[3] = cv::Point(159, 77) * scale;

    quads.push_back(a);

    //left block - 5
    a[0] = cv::Point(5, 115) * scale;
    a[1] = cv::Point(41, 80) * scale;
    a[2] = cv::Point(85, 80) * scale;
    a[3] = cv::Point(51, 113) * scale;

    quads.push_back(a);

    //left block - 6
    a[0] = cv::Point(51, 114) * scale;
    a[1] = cv::Point(84, 81) * scale;
    a[2] = cv::Point(157, 77) * scale;
    a[3] = cv::Point(121, 114) * scale;

    quads.push_back(a);

    //left block - 8 (7 not visible)
    a[0] = cv::Point(18, 151) * scale;
    a[1] = cv::Point(50, 113) * scale;
    a[2] = cv::Point(120, 113) * scale;
    a[3] = cv::Point(83, 156) * scale;

    quads.push_back(a);


    //left left block - 1 
    a[0] = cv::Point(28, 39) * scale;
    a[1] = cv::Point(45, 24) * scale;
    a[2] = cv::Point(86, 14) * scale;
    a[3] = cv::Point(58, 37) * scale;

    quads.push_back(a);

    //left left block - 2
    a[0] = cv::Point(1, 65) * scale;
    a[1] = cv::Point(13, 49) * scale;
    a[2] = cv::Point(40, 47) * scale;
    a[3] = cv::Point(20, 64) * scale;

    quads.push_back(a);
#elif defined MK2

//back row (right to left)

    a[0] = cv::Point(430, 6) * scale;
    a[1] = cv::Point(484, 6) * scale;
    a[2] = cv::Point(515, 24) * scale;
    a[3] = cv::Point(487, 23) * scale;
    quads.push_back(a);

    a[0] = cv::Point(428, 6) * scale;
    a[1] = cv::Point(457, 5) * scale;
    a[2] = cv::Point(487, 24) * scale;
    a[3] = cv::Point(455, 23) * scale;
    quads.push_back(a);

    a[0] = cv::Point(398, 7) * scale;
    a[1] = cv::Point(430, 6) * scale;
    a[2] = cv::Point(451, 23) * scale;
    a[3] = cv::Point(416, 24) * scale;
    quads.push_back(a);

    a[0] = cv::Point(364, 8) * scale;
    a[1] = cv::Point(398, 7) * scale;
    a[2] = cv::Point(415, 23) * scale;
    a[3] = cv::Point(376, 27) * scale;
    quads.push_back(a);

    a[0] = cv::Point(327, 12) * scale;
    a[1] = cv::Point(363, 8) * scale;
    a[2] = cv::Point(376, 27) * scale;
    a[3] = cv::Point(334, 29) * scale;
    quads.push_back(a);

    a[0] = cv::Point(291, 15) * scale;
    a[1] = cv::Point(327, 12) * scale;
    a[2] = cv::Point(334, 30) * scale;
    a[3] = cv::Point(291, 33) * scale;
    quads.push_back(a);

    a[0] = cv::Point(255, 20) * scale;
    a[1] = cv::Point(290, 15) * scale;
    a[2] = cv::Point(291, 34) * scale;
    a[3] = cv::Point(249, 38) * scale;
    quads.push_back(a);

    a[0] = cv::Point(220, 25) * scale;
    a[1] = cv::Point(256, 19) * scale;
    a[2] = cv::Point(247, 41) * scale;
    a[3] = cv::Point(205, 49) * scale;
    quads.push_back(a);

    a[0] = cv::Point(184, 33) * scale;
    a[1] = cv::Point(218, 25) * scale;
    a[2] = cv::Point(203, 50) * scale;
    a[3] = cv::Point(166, 56) * scale;
    quads.push_back(a);

    a[0] = cv::Point(151, 40) * scale;
    a[1] = cv::Point(183, 33) * scale;
    a[2] = cv::Point(160, 59) * scale;
    a[3] = cv::Point(128, 65) * scale;
    quads.push_back(a);

    a[0] = cv::Point(110, 53) * scale;
    a[1] = cv::Point(151, 41) * scale;
    a[2] = cv::Point(123, 68) * scale;
    a[3] = cv::Point(91, 76) * scale;
    quads.push_back(a);

    a[0] = cv::Point(84, 59) * scale;
    a[1] = cv::Point(113, 51) * scale;
    a[2] = cv::Point(84, 80) * scale;
    a[3] = cv::Point(54, 89) * scale;
    quads.push_back(a);

    a[0] = cv::Point(48, 73) * scale;
    a[1] = cv::Point(75, 63) * scale;
    a[2] = cv::Point(57, 86) * scale;
    a[3] = cv::Point(24, 100) * scale;
    quads.push_back(a);

    //middle row (right to left)

    a[0] = cv::Point(552, 44) * scale;
    a[1] = cv::Point(582, 42) * scale;
    a[2] = cv::Point(639, 86) * scale;
    a[3] = cv::Point(605, 85) * scale;
    quads.push_back(a);


    a[0] = cv::Point(515, 45) * scale;
    a[1] = cv::Point(550, 44) * scale;
    a[2] = cv::Point(604, 85) * scale;
    a[3] = cv::Point(567, 84) * scale;
    quads.push_back(a);
    
    a[0] = cv::Point(476, 43) * scale;
    a[1] = cv::Point(516, 44) * scale;
    a[2] = cv::Point(567, 83) * scale;
    a[3] = cv::Point(525, 85) * scale;
    quads.push_back(a);

    a[0] = cv::Point(437, 44) * scale;
    a[1] = cv::Point(480, 44) * scale;
    a[2] = cv::Point(525, 85) * scale;
    a[3] = cv::Point(474, 87) * scale;
    quads.push_back(a);

    a[0] = cv::Point(391, 47) * scale;
    a[1] = cv::Point(436, 45) * scale;
    a[2] = cv::Point(474, 86) * scale;
    a[3] = cv::Point(418, 90) * scale;
    quads.push_back(a);

    a[0] = cv::Point(340, 51) * scale;
    a[1] = cv::Point(390, 49) * scale;
    a[2] = cv::Point(417, 89) * scale;
    a[3] = cv::Point(356, 93) * scale;
    quads.push_back(a);

    a[0] = cv::Point(290, 57) * scale;
    a[1] = cv::Point(341, 52) * scale;
    a[2] = cv::Point(355, 96) * scale;
    a[3] = cv::Point(290, 104) * scale;
    quads.push_back(a);

    a[0] = cv::Point(238, 66) * scale;
    a[1] = cv::Point(289, 58) * scale;
    a[2] = cv::Point(290, 102) * scale;
    a[3] = cv::Point(224, 114) * scale;
    quads.push_back(a);

    a[0] = cv::Point(189, 70) * scale;
    a[1] = cv::Point(238, 65) * scale;
    a[2] = cv::Point(226, 115) * scale;
    a[3] = cv::Point(167, 127) * scale;
    quads.push_back(a);

    a[0] = cv::Point(143, 85) * scale;
    a[1] = cv::Point(142, 87) * scale;
    a[2] = cv::Point(166, 130) * scale;
    a[3] = cv::Point(110, 140) * scale;
    quads.push_back(a);

    a[0] = cv::Point(102, 96) * scale;
    a[1] = cv::Point(145, 85) * scale;
    a[2] = cv::Point(111, 139) * scale;
    a[3] = cv::Point(62, 154) * scale;
    quads.push_back(a);

    a[0] = cv::Point(63, 107) * scale;
    a[1] = cv::Point(101, 96) * scale;
    a[2] = cv::Point(62, 155) * scale;
    a[3] = cv::Point(24, 165) * scale;
    quads.push_back(a);

    //front row

    a[0] = cv::Point(700, 162) * scale;
    a[1] = cv::Point(728, 157) * scale;
    a[2] = cv::Point(798, 224) * scale;
    a[3] = cv::Point(787, 242) * scale;
    quads.push_back(a);

    a[0] = cv::Point(664, 168) * scale;
    a[1] = cv::Point(699, 163) * scale;
    a[2] = cv::Point(787, 249) * scale;
    a[3] = cv::Point(752, 261) * scale;
    quads.push_back(a);

    a[0] = cv::Point(617, 176) * scale;
    a[1] = cv::Point(664, 168) * scale;
    a[2] = cv::Point(752, 263) * scale;
    a[3] = cv::Point(712, 284) * scale;
    quads.push_back(a);

    a[0] = cv::Point(558, 184) * scale;
    a[1] = cv::Point(616, 174) * scale;
    a[2] = cv::Point(713, 285) * scale;
    a[3] = cv::Point(649, 308) * scale;
    quads.push_back(a);

    a[0] = cv::Point(483, 195) * scale;
    a[1] = cv::Point(560, 186) * scale;
    a[2] = cv::Point(652, 311) * scale;
    a[3] = cv::Point(566, 342) * scale;
    quads.push_back(a);

    a[0] = cv::Point(396, 210) * scale;
    a[1] = cv::Point(483, 197) * scale;
    a[2] = cv::Point(569, 346) * scale;
    a[3] = cv::Point(454, 379) * scale;
    quads.push_back(a);

    a[0] = cv::Point(288, 227) * scale;
    a[1] = cv::Point(395, 210) * scale;
    a[2] = cv::Point(495, 393) * scale;
    a[3] = cv::Point(319, 411) * scale;
    quads.push_back(a);

    a[0] = cv::Point(202, 242) * scale;
    a[1] = cv::Point(296, 226) * scale;
    a[2] = cv::Point(318, 415) * scale;
    a[3] = cv::Point(188, 415) * scale;
    quads.push_back(a);

    a[0] = cv::Point(117, 257) * scale;
    a[1] = cv::Point(198, 242) * scale;
    a[2] = cv::Point(181, 419) * scale;
    a[3] = cv::Point(87, 416) * scale;
    quads.push_back(a);

    a[0] = cv::Point(48, 272) * scale;
    a[1] = cv::Point(117, 257) * scale;
    a[2] = cv::Point(83, 419) * scale;
    a[3] = cv::Point(4, 415) * scale;
    quads.push_back(a);

  
#endif
    for (const auto &grid_sq : quads){

      cv::Mat quad_frame = cv::Mat::zeros(frame_size, CV_8UC1);
      for (int r = 0; r < frame_size.height; r++){
        for (int c = 0; c < frame_size.width; c++){
          cv::Point pt(c, r);
          if (IsInQuad(pt, grid_sq)){
            quad_frame.at<unsigned char>(r, c) = 255;
          }
        }
      }

      quick_intersection_cache.push_back(quad_frame);
    }


    InitKalmanFilter();


  }


  void InitKalmanFilter(){

    const size_t number_of_spaces = quads.size();

    filter_.init(number_of_spaces, number_of_spaces, 0, CV_32F); //dynamic params, measurement params, control params
    
    //transition matrix is 'decaying' linear function
    filter_.transitionMatrix = 0.9*cv::Mat::eye(number_of_spaces, number_of_spaces, CV_32F);
        
    filter_.measurementMatrix = cv::Mat::eye(number_of_spaces, number_of_spaces, CV_32F);
    
    for (size_t i = 0; i < number_of_spaces; ++i){
      filter_.statePost.at<float>(i) = 0; 
    }


    cv::setIdentity(filter_.processNoiseCov, cv::Scalar::all(1)); //uncertainty in the model
    cv::setIdentity(filter_.measurementNoiseCov, cv::Scalar::all(1e-2)); //uncertainty in the measurement
    cv::setIdentity(filter_.errorCovPost, cv::Scalar::all(1));

  }

  void drawQuadsOnFrame(cv::Mat &frame, const std::vector<std::array<cv::Point, 4> > &quads){

    cv::Mat alpha_frame = cv::Mat::zeros(frame.size(), CV_8UC3);

    for (const auto &quad : quads){
      std::vector<cv::Point> quad_v;
      for (int i = 0; i < 4; ++i) quad_v.push_back(quad[i]);

      cv::fillConvexPoly(alpha_frame, quad_v, cv::Scalar(255, 0, 0));

    }
    double alpha = 0.3;
    cv::Mat out_frame;
    cv::addWeighted(alpha_frame, alpha, frame, 1.0 - alpha, 0.0, out_frame);
    frame = out_frame;

  }

  bool IsInQuad(cv::Point &center, const std::array<cv::Point,4> &quad){

    return IsInTriangle(center, quad[0], quad[1], quad[2]) || IsInTriangle(center, quad[2], quad[3], quad[0]);
    
  }

  float LineSign(const cv::Point2f p1, const cv::Point2f p2, const cv::Point2f p3) {
    return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
  }

  bool IsInTriangle(const cv::Point &pt, const cv::Point &v1, const cv::Point &v2, const cv::Point &v3){
    bool b1, b2, b3;
    b1 = LineSign(pt, v1, v2) < 0.0f;
    b2 = LineSign(pt, v2, v3) < 0.0f;
    b3 = LineSign(pt, v3, v1) < 0.0f;
    return ((b1 == b2) && (b2 == b3));
  }

  void UpdateOccupancyWithMeasurement(std::vector<std::array<cv::Point, 4> > &detected_spaces){

    std::vector<float> measurements;

    for (size_t i = 0; i < quads.size(); ++i){

      measurements.push_back(0);

      for (size_t j = 0; j < detected_spaces.size(); ++j){

        if (quads[i] == detected_spaces[j]){
          measurements.back() = 1;
          break;
        }
        
      }
      
    }

    cv::Mat measurements_(measurements);

    const cv::Mat prediction = filter_.predict(); //compute prior P(w_{t}|x_{1},...,x_{t-1}) (using Chapman-Kolomogorov e.q.)
    const cv::Mat estimation = filter_.correct(measurements_); //compute posterior by combining the measurement likelihood with the prior

    std::vector< std::array<cv::Point, 4> > filtered_detections;
    for (int i = 0; i < estimation.total(); ++i){
      if (estimation.at<float>(i) > 0.5) filtered_detections.push_back(quads[i]);
    }

    detected_spaces = filtered_detections;

  }


  cv::KalmanFilter filter_;

  std::vector< std::array<cv::Point, 4> > quads;
  std::vector< cv::Mat > quick_intersection_cache;

};

inline void ApplyNonMaxSuppression(std::vector<cv::Rect>& rectList, double detectThreshold, std::vector<double>* foundWeights, std::vector<double>& scales, cv::Size winDetSize){
  
  int detectionCount = (int)rectList.size();
  std::vector<cv::Point3d> hits(detectionCount), resultHits;
  std::vector<double> hitWeights(detectionCount), resultWeights;
  cv::Point2d hitCenter;

  for (int i = 0; i < detectionCount; i++)
  {
    hitWeights[i] = (*foundWeights)[i];
    hitCenter = (rectList[i].tl() + rectList[i].br())*(0.5); //center of rectangles
    hits[i] = cv::Point3d(hitCenter.x, hitCenter.y, std::log(scales[i]));
  }

  rectList.clear();
  if (foundWeights)
    foundWeights->clear();

  double logZ = std::log(1.3);
  cv::Point3d smothing(8, 16, logZ);

  MeanshiftGrouping msGrouping(smothing, hits, hitWeights, 1e-5, 100);

  msGrouping.getModes(resultHits, resultWeights, 1);

  for (unsigned i = 0; i < resultHits.size(); ++i)
  {

    double scale = exp(resultHits[i].z);
    hitCenter.x = resultHits[i].x;
    hitCenter.y = resultHits[i].y;
    cv::Size s(int(winDetSize.width * scale), int(winDetSize.height * scale));
    cv::Rect resultRect(int(hitCenter.x - s.width / 2), int(hitCenter.y - s.height / 2),
      int(s.width), int(s.height));

    if (resultWeights[i] > detectThreshold)
    {
      rectList.push_back(resultRect);
      foundWeights->push_back(resultWeights[i]);
    }
  }
}

struct TrainingModel;

void AddDetectionsToFrame(std::vector<std::pair<cv::Rect, TrainingModel *> > detections, cv::Mat &frame, QuickImageGrid qig, int MODE, std::vector< std::array<cv::Point, 4> > &quads_to_draw, cv::Mat &affine_transform);



/**
* Saves the given descriptor vector to a file
* @param descriptorVector the descriptor vector to save
* @param _vectorIndices contains indices for the corresponding vector values (e.g. descriptorVector(0)=3.5f may have index 1)
* @param fileName
* @TODO Use _vectorIndices to write correct indices
*/

void saveDescriptorVectorToFile(std::vector<float>& descriptorVector, std::vector<unsigned int>& _vectorIndices, std::string fileName);


/**
* For unixoid systems only: Lists all files in a given directory and returns a vector of path+name in string format
* @param dirName
* @param fileNames found file names in specified directory
* @param validExtensions containing the valid file extensions for collection in lower case
*/
void getFilesInDirectory(const std::string& dirName, std::vector<std::string>& fileNames, const std::vector<std::string>& validExtensions);

size_t filterDetectionsWithBackgroundSubtractor(const cv::Mat &image, const std::vector<cv::Rect> &detections, const cv::Rect bg_subtractor, const cv::Mat &optional_mask, const float overlap_fraction);

void rotate2D(const cv::Mat & src, cv::Mat & dst, const double degrees);

/**
* This is the actual calculation from the (input) image data to the HOG descriptor/feature vector using the hog.compute() function
* @param imageFilename file path of the image file to read and calculate feature vector from
* @param descriptorVector the returned calculated feature vector<float> ,
*      I can't comprehend why openCV implementation returns std::vector<float> instead of cv::MatExpr_<float> (e.g. Mat<float>)
* @param hog HOGDescriptor containin HOG settings
*/
#ifdef SYS_TEGRA
void calculateFeaturesFromInput(const std::string& imageFilename, std::vector<float>& featureVector, cv::gpu::HOGDescriptor& hog, cv::Size winStride, cv::Size trainingPadding);
#else
void calculateFeaturesFromInput(const std::string& imageFilename, std::vector<float>& featureVector, cv::HOGDescriptor& hog, cv::Size winStride, cv::Size trainingPadding);
#endif

void runDetection(TrainingModel *detector, const cv::Mat &image, std::vector<cv::Rect> &detections);

/**
* Shows the detections in the image
* @param found vector containing valid detection rectangles
* @param imageData the image in which the detections are drawn
*/
void showDetections(const std::vector<cv::Point>& found, cv::Mat& imageData);


/**
* Shows the detections in the image
* @param found vector containing valid detection rectangles
* @param imageData the image in which the detections are drawn
*/
void showDetections(const std::vector<cv::Rect>& found, cv::Mat& imageData);

void mineVideoForFalsePositives(const std::string &video_file, TrainingModel &detector);
void viewOutputOnVideo(const std::string &video_file, std::vector<TrainingModel> &detectors,const std::string &bgsubtractor_file, const std::string &optional_mask_car, const std::string &optional_mask_person);

/**
* Test detection with custom HOG description vector
* @param hog
* @param hitThreshold threshold value for detection
* @param imageData
*/
void detectTest(TrainingModel *car_detector, TrainingModel *people_detector, TrainingModel *bike_detector, cv::Mat& imageData, cv::Mat &background_image, cv::Mat &optional_mask);

void detectTest2(TrainingModel *car_detector, TrainingModel *people_detector, TrainingModel *bike_detector, cv::Mat& imageData, cv::Mat &background_image, std::vector<cv::Rect> &car_detections, std::vector<cv::Rect> &people_detections, std::vector<cv::Rect> &bike_detections, cv::Mat &optional_mask_car, cv::Mat &optional_mask_person);

void detectTest3(TrainingModel *car_detector, TrainingModel *people_detector, TrainingModel *bike_detector, cv::Mat& imageData, cv::Mat &background_image, std::vector<cv::Rect> &car_detections, std::vector<cv::Rect> &people_detections, std::vector<cv::Rect> &bike_detections);

/**
* Test the trained detector against the same training set to get an approximate idea of the detector.
* Warning: This does not allow any statement about detection quality, as the detector might be overfitting.
* Detector quality must be determined using an independent test set.
* @param hog
*/
void detectTrainingSetTest(const cv::HOGDescriptor& hog, const double hitThreshold, const std::vector<std::string>& posFileNames, const std::vector<std::string>& negFileNames);

bool FindConnectedRegions(const cv::Mat &image, std::vector<std::vector<cv::Vec2i> >&connected_regions);
void GetContours(const cv::Mat &image, std::vector<std::vector<cv::Point> > &contours);
void FindSingleRegionFromContour(const std::vector<cv::Point> &contour, std::vector<cv::Vec2i> &connected_region, const cv::Size image_size); 

template<typename T>
T GetCenter(const std::vector<T> &contour) {
  cv::Vec2i center(0, 0);
  for (size_t i = 0; i < contour.size(); i++){
    center = center + (cv::Vec2i)contour[i];
  }
  center[0] = center[0] / (int)contour.size();
  center[1] = center[1] / (int)contour.size();
  return center;
}


inline cv::Point GetCenter(const cv::Rect &r){

  return r.tl() + cv::Point(r.width / 2, r.height / 2);

}

#endif
