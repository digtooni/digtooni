#pragma once

#include "helpers.hpp"
#include "config_reader.hpp"

#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/ml/ml.hpp>
#include <opencv2/gpu/gpu.hpp>


#include <iostream>

struct HOGSVM {

  explicit HOGSVM(const std::string &config_file);
  HOGSVM(const HOGSVM &other);
  HOGSVM &operator=(const HOGSVM &rhs);

  //HOGSVM(const std::string &object_type, cv::Size hog_size_);
  void SetTrainingDataDirs(const std::vector<std::string> &positive_dirs, const std::vector<std::string> &negative_dirs);
  void CollectTrainingData(const HOGSVM &other_models);
  void CollectTrainingData();
  
  void CollectExtraNegatives(const std::string &directory);
  void CollectExtraNegatives(const std::vector<std::string> &directories);

  void Train(bool retrain);
  void Retrain();
  void LoadModel();
  
  void GenerateFeaturesFromData(bool append);

  const std::string &name() { return name_; }

  void copyHOG(cv::gpu::HOGDescriptor lhs, cv::gpu::HOGDescriptor& rhs);

  /// WARNING - any new variables must be added to copy constructor and assignment operator!
  cv::Size win_stride;
  cv::Size training_padding;
  cv::gpu::HOGDescriptor hog;  
  cv::HOGDescriptor cpuHog;  
  std::string name_;
  std::string features_file;
  std::string descriptor_vector_file;
  std::string svm_model_file;
  std::vector<std::string> positive_training_data_dirs;
  std::vector<std::string> negative_training_data_dirs;
  std::vector<std::string> positive_training_images;
  std::vector<std::string> negative_training_images;
  std::vector<std::string> valid_extensions;
  std::vector<float> descriptor_vector;
  unsigned long number_of_samples;
  float hit_threshold;

};



