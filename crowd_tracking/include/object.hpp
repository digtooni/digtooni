#ifndef OBJECT_H
#define OBJECT_H

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <cmath>
#include <forward_list>

class Object {
  public:
    Object(cv::Rect detection, cv::Point center, cv::Mat mask, int objectCount, cv::Mat hist, int validThresh);
    virtual ~Object() {}

    int getId() const;
    int getLastSeen();
    int getAge();
    cv::Mat getHist();
    cv::Point getCentroid();
    void update();
    void update(cv::Rect detection, cv::Point, cv::Mat colorHist, cv::Mat mask);
    double histDistance(Object obj2);
    double histDist(cv::Mat otherHist);
    void pedestrianCheck(double histDiff);  // Convert object to pedestrian if satisfies criteria
    bool getIsPedestrian();      // return the "isPedestrian" flag
    cv::Scalar getColor();
    std::string getName();
    double dist(Object obj2);
    double dist(cv::Point pt);
    void printHist();
    void setEntered();
    bool getEntered();
    void str();
    bool getPositionsRecorded();
    void setPositionsRecorded();
    
    void addPoint(const cv::Point pt);

    bool updated;
    std::vector<cv::Point> pointHistory;

  protected:
    // These change for every detection
    cv::Mat image;
    cv::Mat binaryMask;
    cv::Rect boundingBox;
    cv::Point centroid;
    cv::Mat colorHist;  // BGR order
    cv::Mat contour;
    cv::Vec2d direction;
    double aspectRatio;

    // Updated every frame
    int lastSeen;
    bool occluded;

    // Persistent values
    int id;
    std::string name;
    cv::Scalar drawColor;
    int age;
    bool isPedestrian;
    bool entered_zone;
    int validationFrames;
    bool oldPositionsRecorded;

    const int VALID_THRESH;
};

#endif
