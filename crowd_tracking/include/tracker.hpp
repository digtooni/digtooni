#ifndef TRACKER_H
#define TRACKER_H

#include "object.hpp"

#include "opencv2/video/background_segm.hpp"

#ifdef SYS_TEGRA
#include "opencv2/gpu/gpu.hpp"
#endif

#include <memory>
#include <climits>

class Tracker {
  public:
    Tracker(double distThresh, double histThresh, int objEOL, int validThresh);
    cv::Mat getForeground();
    cv::Mat computeColorHistogram(cv::Mat image, cv::Rect roi);
    std::vector<std::shared_ptr<Object> > track(cv::Mat image,
        std::vector<cv::Rect> newDetections,
        std::vector<std::shared_ptr<Object> > &exitPedestrians);
    std::string type2str(int type);
    void reset();

    cv::Mat fgMask;

  protected:
    cv::Rect trimDetectionBox(cv::Rect box, cv::Size dims);
    void subtractBackground(cv::Mat image, cv::Mat &mask);
    std::vector<std::shared_ptr<Object> > objects;

    const double DIST_THRESH;
    const int HIST_THRESH;
    const int VALID_THRESH;
    const int OBJ_EOL;

#ifdef SYS_TEGRA
    cv::Ptr<cv::gpu::MOG2_GPU> bgSubtractor;
#else
    cv::Ptr<cv::BackgroundSubtractorMOG2> bgSubtractor;
#endif
    int objectCount;

};

#endif
