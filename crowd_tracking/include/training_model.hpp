#pragma once

#include "helpers.hpp"
#include "config_reader.hpp"

struct TrainingModel
{
    explicit TrainingModel(  const std::string &config_file );
    TrainingModel( const TrainingModel &other );
    TrainingModel &operator=( const TrainingModel &rhs );

    //TrainingModel( const std::string &object_type, cv::Size hog_size_ );
    void setTrainingDataDirs( const std::vector<std::string> &positive_dirs, const std::vector<std::string> &negative_dirs );
    void collectTrainingData( const TrainingModel &other_models );
    void collectTrainingData();

    void collectExtraNegatives( const std::string &directory );
    void collectExtraNegatives( const std::vector<std::string> &directories );

    void train( bool retrain );
    void retrain();
    void loadModel();
    void detect( const cv::Mat &image, std::vector<cv::Rect> &detections, float scaling_factor );
    void cleanOverlaps( std::vector<cv::Rect> &detections );

    void generateFeaturesFromData( bool append );

    const std::string &name() { return name_; }

#ifdef SYS_TEGRA
    void copyHOG( cv::gpu::HOGDescriptor lhs, cv::gpu::HOGDescriptor& rhs );
#endif

    // WARNING - any new variables must be added to copy constructor and assignment operator!
    cv::Size win_stride;
    cv::Size training_padding;
#ifdef SYS_TEGRA
    cv::gpu::HOGDescriptor hog;  
#else
    cv::HOGDescriptor hog;  
#endif
    std::string name_;
    std::string features_file;
    std::string descriptor_vector_file;
    std::string svm_model_file;
    std::vector<std::string> positive_training_data_dirs;
    std::vector<std::string> negative_training_data_dirs;
    std::vector<std::string> positive_training_images;
    std::vector<std::string> negative_training_images;
    std::vector<std::string> valid_extensions;
    std::vector<float> descriptor_vector;
    unsigned long number_of_samples;
    float hit_threshold;
};



