video_dir = videos/oxford_road/
video_number = 119
model_path = models/heads/
output_path = output/counts.csv
mode = 0
frame_start = 8995

# Scene parameters
count_rect_x = 750
count_rect_y = 0
count_rect_width = 800
count_rect_height = 550

# Set to 1 to continue processing videos after first one finishes
process_next_videos = 1

line_pt1_x = 1000
line_pt1_y = 230
line_pt2_x = 1500
line_pt2_y = 230
