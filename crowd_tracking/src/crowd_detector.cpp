#include "crowd_detector.hpp"

const std::string config_file = "config.txt";

// Set the frame to the one selected with the trackbar
void onSlideTrackbar(  int pos, void* vCap  ) 
{
    // dereference the void pointer back to a videoCapture
    cv::VideoCapture cap = *static_cast<cv::VideoCapture*>(  vCap  );
    cap.set(  CV_CAP_PROP_POS_FRAMES, pos  );
}

CrowdDetector::CrowdDetector(  const std::string &config_path, const std::string &vid  )
    : win_name(  "Crowd Tracking"  )
    , in_zone( 0 )
    , exited_up( 0 )
    , exited_down( 0 )
    , exited_left( 0 )
    , exited_right( 0 )
{
    ConfigReader cfg( config_path );

    model_config << cfg.get_element( "model_path" );
    model_config << "config.txt";
    video_dir = cfg.get_element( vid );
    video_number = cfg.get_element_as_type<int>( "video_number" );
    output_path = cfg.get_element( "output_path" );
    keep_processing = static_cast<bool>( cfg.get_element_as_type<int>( "process_next_videos" ) );
      
    std::stringstream ss;
    ss << video_dir << "TegraVid_" << video_number << ".mp4";
    video_path = ss.str();
    setOutputFiles();

    mode = static_cast<Mode>( cfg.get_element_as_type<int>( "mode" ) );
    frameStart = cfg.get_element_as_type<int>( "frame_start" );

    // Crossing rectangle
    int tl_x = cfg.get_element_as_type<int>( "count_rect_x" );
    int tl_y = cfg.get_element_as_type<int>( "count_rect_y" );
    int width = cfg.get_element_as_type<int>( "count_rect_width" );
    int height = cfg.get_element_as_type<int>( "count_rect_height" );

    // Line dividing the two pedestrian trajectories
    int pt1_x = cfg.get_element_as_type<int>( "line_pt1_x" );
    int pt1_y = cfg.get_element_as_type<int>( "line_pt1_y" );
    int pt2_x = cfg.get_element_as_type<int>( "line_pt2_x" );
    int pt2_y = cfg.get_element_as_type<int>( "line_pt2_y" );

    countZone = cv::Rect( tl_x, tl_y, width, height );
    line_pt1 = cv::Point( pt1_x, pt1_y );
    line_pt2 = cv::Point( pt2_x, pt2_y );

    tracker = std::unique_ptr<Tracker>( new Tracker( 30, 0.5, 6, 12 ) );
    vidStatus = PLAY;
}

void CrowdDetector::setVidNumber( int number )
{
    video_number = number;
    std::stringstream ss;
    ss << video_dir << "TegraVid_" << video_number << ".mp4";
    video_path = ss.str();
  
    setOutputFiles();
}

void CrowdDetector::setOutputFiles()
{
    std::stringstream ss;
    ss << "output/counts_";
    ss << video_number;
    ss << ".csv";
    counts_output = ss.str();

    ss.str( "" );
    ss << "output/positions_";
    ss << video_number;
    ss << ".csv";
    positions_output = ss.str();
}

std::string CrowdDetector::getVideoName()
{
    int namePos = 0;
    int extPos = video_path.size() - 5;
    for ( int i=video_path.size(); i>0; --i )
    {
        if( video_path[i] == '/' )
        {
            namePos = i + 1;
            break;
        }
    }
    return video_path.substr( namePos, 12 );
}

void CrowdDetector::drawSVMDetections( cv::Mat &image )
{
    const cv::Scalar green( 0, 255, 0 );

    for ( int i=0; i<svm_detections.size(); ++i )
    {
        cv::rectangle( image, svm_detections[i], green, 2 );
    }
}

// Draw the cyclist paths on the display image
void CrowdDetector::drawPaths( cv::Mat& image )
{
    cv::Point lastPoint;
    for ( int i=0; i<pedestrians.size(); ++i )
    {
        std::vector<cv::Point> points( pedestrians[i]->pointHistory );
        lastPoint = points[0];
        for ( int j=1; j<points.size(); ++j )
        {
            cv::line( image, lastPoint, points[j], pedestrians[i]->getColor(), 4 );
            lastPoint = points[j];
        }
    }
}

void CrowdDetector::trackPedestrians( std::string video_path )
{
    for ( auto &model : models )
    {
        model.loadModel();
    }

    // Reset values upon new video
    pedestrians.clear();
    exit_pedestrians.clear();
    exited_up = 0;
    exited_down = 0;
    exited_left = 0;
    exited_right = 0;
    tracker->reset();

    std::string videoName = getVideoName();

    std::stringstream ss;
    ss << "Performing tracking on video file: ";
    ss << video_path;
    std::cout << ss.str() << std::endl;

    std::ofstream outFile;
    outFile.open( counts_output );
    outFile << videoName << std::endl;
    outFile.close();

    outFile.open( positions_output );
    outFile << videoName << std::endl;
    outFile.close();

    cap.open( video_path );
    if ( !cap.isOpened() )
    {
        std::cout << "WARNING: " << video_path << " could not be found!" << std::endl;
        return;
    }
    cap.set( CV_CAP_PROP_POS_FRAMES, frameStart );
    sliderPos = frameStart;
    frameCount = ( int ) cap.get( CV_CAP_PROP_FRAME_COUNT );
    cv::namedWindow( win_name, cv::WINDOW_AUTOSIZE );
    cv::createTrackbar( "Position", win_name, &sliderPos, frameCount, 
        onSlideTrackbar, &cap );

    cv::Mat frame, fgMask, outputFrame;
    bool end = false;
    while ( true )
    {
        if ( vidStatus == PLAY || vidStatus == NEXT )
        {
            //Trackbar control
            int currentPos = ( int ) cap.get( CV_CAP_PROP_POS_FRAMES );
            cv::setTrackbarPos( "Position", win_name, currentPos );
   
            cap >> frame;
            if ( frame.empty() )
            {
                break;
            }
            const cv::Mat image( frame );
      
            for ( auto &model : models )
            {
                model.detect( image, svm_detections, 1 );
            }

            pedestrians = tracker->track( image, svm_detections, exit_pedestrians );

            fgMask = tracker->getForeground();
            //cv::cvtColor( fgMask, outputFrame, CV_GRAY2RGB );
            cv::Mat outputFrame( frame );
            drawSVMDetections( outputFrame );
            drawPaths( outputFrame );

            cv::imshow( win_name, outputFrame );
            svm_detections.clear();
        }
        // increment video by one frame then pause
        if ( vidStatus == NEXT )
        {
            vidStatus = PAUSE;
        }
        // Video playback controls
        char keyPress = ( char ) cv::waitKey( 100 );
        switch( keyPress )
        {
            case 'p' : 
                if ( vidStatus == PLAY )
                { 
                    vidStatus = PAUSE;
                    std::cout << "Playback paused" << std::endl;
                }
                else
                {
                    vidStatus = PLAY;
                    std::cout << "Playback resumed" << std::endl;
                }
                break;
            case 'n' :
                vidStatus = NEXT;
                break;
            case 27 : // Esc key
                end = true;
                break;
        }
        if ( end )
        {
            break;
        }
        count();
        writeCounts();
        recordPositions();
    }
}

void CrowdDetector::count() {

    in_zone.clear();
    in_zone_count = 0;

    for ( int i=0; i<pedestrians.size(); ++i )
    {
        if ( countZone.contains( pedestrians[i]->getCentroid() ) )
        {
            in_zone.push_back( pedestrians[i]->getId() );
            ++in_zone_count;
        }
    }
  
    // Count exit_pedestrians
    for ( auto it = exit_pedestrians.begin(); it != exit_pedestrians.end(); )
    {
        std::shared_ptr<Object> objectPtr = *it;

        //Check the pedestrian went across the crossing
        bool entered = false;
        for ( int i=0; i<objectPtr->pointHistory.size(); ++i )
        {
            if ( countZone.contains( objectPtr->pointHistory[i] ) )
            {
                entered = true;
                break;
            }
        }
        if (entered)
        {
            // Get the first and last points in the point history of the pedestrian
            cv::Point firstPoint = objectPtr->pointHistory[0];
            cv::Point lastPoint = objectPtr->pointHistory[objectPtr->pointHistory.size() - 1];

            // Pedestrian left via the bottom of the area
            if (firstPoint.x > lastPoint.x)
            {
                ++exited_down;
            }
            // Pedestrian left via the top of the area
            else
            {
                ++exited_up;
                if (lastPoint.y > line_pt1.y)
                {
                    ++exited_right;
                }
                else
                {
                    ++exited_left;
                }
            }
            // Remove the pedestrian from the list
            it = exit_pedestrians.erase(it);
        }
    }
}

void CrowdDetector::writeCounts()
{
    std::stringstream ss;
    ss << "frame " << sliderPos << ",";
    ss << "InZone,";
    for ( int i=0; i<in_zone.size(); ++i )
    {
        ss << in_zone[i] << ",";
    }
    ss << "InZoneCount,";
    ss << in_zone_count << ",";
    ss << "ExitUp,";
    ss << exited_up << ",";
    ss << "ExitDown,";
    ss << exited_down << ",";
    ss << "ExitLeft,";
    ss << exited_left << ",";
    ss << "ExitRight,";
    ss << exited_right << std::endl;

    std::ofstream outFile;
    outFile.open( counts_output, std::ios::ate | std::ios::app );
    outFile << ss.str();
    outFile.close();

    ss.str( "" );
}

void CrowdDetector::recordPositions()
{

    std::ofstream outFile;
    std::stringstream ss;
    outFile.open( positions_output, std::ios::ate | std::ios::app );

    for ( int i=0; i<pedestrians.size(); ++i )
    {
        std::shared_ptr<Object> pedestrian( pedestrians[i] );
        if ( pedestrian->getPositionsRecorded() )
        {
            // Record current position
            ss << sliderPos << ",";
            ss << pedestrian->getId() << ",";

            cv::Point pt = pedestrian->pointHistory[pedestrian->pointHistory.size()-1];
            ss << pt.x << ",";
            ss << pt.y << std::endl; 

            outFile << ss.str();
            ss.str( "" );
        }
        else
        {
            // write all previous positions
            int count( 0 );
            for ( int j=pedestrian->pointHistory.size()-1; j<pedestrian->pointHistory.size();
                --j )
            {
                cv::Point pt( pedestrian->pointHistory[j] );
                ss << ( sliderPos - count ) << ",";
                ss << pedestrian->getId() << ",";
                ss << pt.x << ",";
                ss << pt.y << std::endl; 
                outFile << ss.str();
                ss.str( "" );
                ++count;
            }

            pedestrian->setPositionsRecorded();
        }
    }
    outFile.close();
}


void CrowdDetector::run()
{
    models.push_back( TrainingModel( model_config.str() ) );
    TrainingModel &crowd_head = models.back();
  
    // @WARNING: This is really important, some libraries ( e.g. ROS )
    // seems to set the system locale which takes decimal commata instead
    // of points which causes the file input parsing to fail
    setlocale( LC_ALL, "C" ); // Do not use the system locale
    setlocale( LC_NUMERIC, "C" );
    setlocale( LC_ALL, "POSIX" );

    switch ( mode )
    {
        case TRAIN:
            for ( auto &model : models )
            {
                model.collectTrainingData();
                model.train( false );
            }
            break;

        case RETRAIN:
            for ( auto &model : models )
            {
                model.collectTrainingData();
                model.retrain();
            }
	        break;

        case MINE_FPS:
            for ( auto &model : models )
            {
                model.loadModel();
            }
            std::cout << "Beginning labeling of false positives." << std::endl;
            mineVideoForFalsePositives( video_path, crowd_head );
            break;

        case RUN:
            trackPedestrians( video_path );
            break;
    } 
    models.clear();
}


int main( int argc, char** argv )
{
    cv::CommandLineParser parser( argc, argv, 
        "{ v | video_path |  | path to the input video file }" 
        "{ x | video_number | 0 | video file number }" 
        "{ t | mode |  | set the detector mode ( Run = 0; Train = 1; Retrain = 2; Mine = 3 )}"
        "{ f | frame_start | 0 | starting frame in the video}"
        "{ n | process_next_videos | 0 | Set to true to continually process videos}" );

    const std::string vid = parser.get<std::string>( "v" );
    const int vid_number = parser.get<size_t>( "x" );
    const size_t mode = parser.get<size_t>( "t" );
    const int frame_start = parser.get<size_t>( "f" );
    const int keep_processing = parser.get<bool>( "n" );

    if ( argc >= 2 )
    {
        std::stringstream input;
        input << argv[1];

        if ( input.str() == "help" )
        {
            std::cout << "Usage : trainer [options]" << std::endl;
            std::cout << "Available options:" << std::endl;
            parser.printParams();
            std::cout << std::endl << "For permanent parameter changes,"
                " make edits to \"config.txt\"." << std::endl;

            return 1;
        }
    }

    bool has_params = true;

    std::cout << "/..........................................................\\" << std::endl;
    if ( vid.length() == 0 && mode == 0 && vid_number == 0 )
    {
#ifdef SYS_TEGRA
        std::cout << "Running GPU SVM crowd detection with default parameters." << std::endl;
#else
        std::cout << "Running CPU SVM crowd detection with default parameters." << std::endl;
#endif
        std::cout << "To see optional parameters, re-run the application followed\n"
                     "by the command line keyword \"help\"" << std::endl;
        std::cout << "Enter 'q' to quit now, or press ENTER to continue." << std::endl;
        std::stringstream interrupt;
        interrupt << std::cin.get();
        if ( interrupt.str() == "113" )
        {
            return 1;
        }
        has_params = false;
    }
    std::string vid_dir( "video_dir" );
    CrowdDetector detector( config_file, vid_dir );

    if ( has_params )
    {

        std::cout << "SVM crowd detection.\n";
        detector.video_path = vid;
        detector.setVidNumber( vid_number );
        detector.train = mode;
        if ( frame_start != 0 )
        {
            detector.frameStart = frame_start;
        }
        detector.keep_processing = keep_processing;
    }

    detector.run();

    int video_count = detector.video_number + 1;
    std::stringstream stream;

    while ( detector.keep_processing )
    {
        stream.str( "" );
        stream << detector.video_dir << "TegraVid_";
        stream << video_count << ".mp4";

        detector.video_path = stream.str();
        detector.run();

        ++video_count;
    }

    std::cout << "finished" <<std::endl;
    return EXIT_SUCCESS;
}
