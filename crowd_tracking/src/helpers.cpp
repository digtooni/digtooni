#include "helpers.hpp"
#include "miner.hpp"
//#include "hog_svm.hpp"
#include "training_model.hpp"
#include <boost/date_time.hpp>
#include <stdio.h>
#include <math.h>

std::clock_t timeNow;

void storeCursor(void) {
  printf("\033[s");
}

void resetCursor(void) {
  printf("\033[u");
}

std::string toLowerCase(const std::string& in) {
  std::string t;
  for (std::string::const_iterator i = in.begin(); i != in.end(); ++i) {
    t += tolower(*i);
  }
  return t;
}

void saveDescriptorVectorToFile(std::vector<float>& descriptorVector, std::vector<unsigned int>& _vectorIndices, std::string fileName) {
  printf("Saving descriptor vector to file '%s'\n", fileName.c_str());
  std::string separator = " "; // Use blank as default separator between single features
  std::fstream File;
  float percent;
  File.open(fileName.c_str(), std::ios::out);
  if (File.good() && File.is_open()) {
    printf("Saving %lu descriptor vector features:\t", descriptorVector.size());
    storeCursor();
    for (size_t feature = 0; feature < descriptorVector.size(); ++feature) {
      if ((feature % 10 == 0) || (feature == (descriptorVector.size() - 1))) {
        percent = (float)((1 + feature) * 100 / descriptorVector.size());
        printf("%4u (%3.0f%%)", feature, percent);
        fflush(stdout);
        resetCursor();
      }
      File << descriptorVector.at(feature) << separator;
    }
    printf("\n");
    File << std::endl;
    File.flush();
    File.close();
  }
}


void getFilesInDirectory(const std::string& dirName, std::vector<std::string>& fileNames, const std::vector<std::string>& validExtensions) {

  printf("Opening directory %s\n", dirName.c_str());
  boost::filesystem::path dirPath(dirName);

  //DIR* dp = opendir(dirName.c_str());
  if (!exists(boost::filesystem::path(dirName))) throw std::runtime_error("Error, directory " + dirName + " does not exist.\n");
  boost::filesystem::directory_iterator endItr;
  boost::filesystem::directory_iterator dirItr(dirPath);
  for (; dirItr != endItr; ++dirItr){

    boost::filesystem::path filePath = dirItr->path();
    const std::string extension = filePath.extension().string();

    if (find(validExtensions.begin(), validExtensions.end(), extension) != validExtensions.end()) {
      //printf("Found matching data file '%s'\n", filePath.string().c_str());
      fileNames.push_back(filePath.string());
    }
    else {
      printf("Found file does not match required file type, skipping: '%s'\n", filePath.string().c_str());
    }

  }

}

#ifdef SYS_TEGRA
void calculateFeaturesFromInput(const std::string& imageFilename, std::vector<float>& featureVector, cv::gpu::HOGDescriptor& hog, cv::Size winStride, cv::Size trainingPadding) {
#else
void calculateFeaturesFromInput(const std::string& imageFilename, std::vector<float>& featureVector, cv::HOGDescriptor& hog, cv::Size winStride, cv::Size trainingPadding) {
#endif
  /** for imread flags from openCV documentation,
  * @see http://docs.opencv.org/modules/highgui/doc/reading_and_writing_images_and_video.html?highlight=imread#Mat imread(const string& filename, int flags)
  * @note If you get a compile-time error complaining about following line (esp. imread),
  * you either do not have a current openCV version (>2.0)
  * or the linking order is incorrect, try g++ -o openCVHogTrainer main.cpp `pkg-config --cflags --libs opencv`
  */
  cv::Mat imageData = cv::imread(imageFilename, 0);
  if (imageData.empty()) {
    featureVector.clear();
    printf("Error: HOG image '%s' is empty, features calculation skipped!\n", imageFilename.c_str());
    return;
  }
  // Check for mismatching dimensions

#ifdef SYS_TEGRA
  if (imageData.size() != hog.win_size){
    printf("Warning: resizing image from %i,%i to %i,%i.\n", imageData.rows, imageData.cols, hog.win_size.height, hog.win_size.width);
    cv::Mat resizedImage;
    cv::resize(imageData, resizedImage, hog.win_size);
    imageData = resizedImage;
  }
#else
  if (imageData.size() != hog.winSize){
    printf("Warning: resizing image from %i,%i to %i,%i.\n", imageData.rows, imageData.cols, hog.winSize.height, hog.winSize.width);
    cv::Mat resizedImage;
    cv::resize(imageData, resizedImage, hog.winSize);
    imageData = resizedImage;
  }
#endif 

  // Check for mismatching dimensions
#ifdef SYS_TEGRA
  if (imageData.cols != hog.win_size.width || imageData.rows != hog.win_size.height) {
    featureVector.clear();
    printf("Error: Image '%s' dimensions (%u x %u) do not match HOG window size (%u x %u)!\n", imageFilename.c_str(), imageData.cols, imageData.rows, hog.win_size.width, hog.win_size.height);
  }
#else
  if (imageData.cols != hog.winSize.width || imageData.rows != hog.winSize.height) {
    featureVector.clear();
    printf("Error: Image '%s' dimensions (%u x %u) do not match HOG window size (%u x %u)!\n", imageFilename.c_str(), imageData.cols, imageData.rows, hog.winSize.width, hog.winSize.height);
  }
#endif

#ifdef SYS_TEGRA
  cv::gpu::GpuMat gpuImageData;
  gpuImageData.upload(imageData);

  std::vector<cv::Point> locations;
  cv::gpu::GpuMat gpuFeatureMat;
  hog.getDescriptors(gpuImageData, winStride, gpuFeatureMat, 0);
  imageData.release(); // Release the image again after features are extracted
  gpuImageData.release(); 

  featureVector.resize(gpuFeatureMat.cols);
  cv::Mat cpuFeatureMat(gpuFeatureMat);
  cv::Mat topRow = cpuFeatureMat.row(0);
  std::copy(topRow.data, topRow.data + gpuFeatureMat.cols, featureVector.begin());
  std::cout << "feature vector size: " << featureVector.size() << std::endl;

#else
  std::vector<cv::Point> locations;
  hog.compute(imageData, featureVector, winStride, trainingPadding, locations);
  std::cout << "feature vector size: " << featureVector.size() << std::endl;
#endif

}


void showDetections(const std::vector<cv::Point>& found, cv::Mat& imageData) {
   for (size_t i = 0; i < found.size(); ++i) {
    cv::Point r = found[i];
    // Rect_(_Tp _x, _Tp _y, _Tp _width, _Tp _height);
    cv::rectangle(imageData, cv::Rect(r.x - 16, r.y - 32, 32, 64), cv::Scalar(64, 255, 64), 3);
  }
}

void showDetections(const std::vector<cv::Rect>& found, cv::Mat& imageData) {
  std::vector<cv::Rect> found_filtered;
  size_t i, j;
  for (i = 0; i < found.size(); ++i) {
    cv::Rect r = found[i];
    for (j = 0; j < found.size(); ++j)
      if (j != i && (r & found[j]) == r)
        break;
    if (j == found.size())
      found_filtered.push_back(r);
  }
  for (i = 0; i < found_filtered.size(); i++) {
    cv::Rect r = found_filtered[i];
    rectangle(imageData, r.tl(), r.br(), cv::Scalar(64, 255, 64), 3);
  }
}

void detectTest2(std::vector<TrainingModel> detectors, cv::Mat& imageData, cv::Mat &background_image, std::vector<std::pair<cv::Rect, TrainingModel *> > &detections, cv::Mat &optional_mask_cars, cv::Mat &optional_mask_person) {

  std::vector<cv::Rect> found;
  cv::Size padding(0, 0);
  cv::Size winStride(8, 8);
  cv::Size blockStride(8,8);


  std::vector<std::vector<cv::Vec2i> > connected_regions;
  FindConnectedRegions(background_image, connected_regions);

  std::vector<cv::Rect> regions_of_interest;

  for (auto connected_region : connected_regions){

    cv::Rect bounding_box = cv::boundingRect(connected_region);
    cv::Point center = bounding_box.tl() + cv::Point(bounding_box.width / 2, bounding_box.height / 2);
    //if (!optional_mask.empty() && optional_mask.at<unsigned char>(center) == 0) continue;

    regions_of_interest.push_back(bounding_box);

  }

  cv::Mat testImageGray;

  cvtColor(imageData, testImageGray, CV_BGR2GRAY);

#if SYS_TEGRA
  cv::gpu::GpuMat gpuTestImageGray;
  gpuTestImageGray.upload(testImageGray);
#endif 

  for (auto &detector : detectors){

#if SYS_TEGRA
    detector.hog.block_stride = blockStride;
#else
    detector.hog.blockStride = blockStride;
#endif

    std::vector<cv::Rect> found_objects;
    std::vector<double> confidences;
    std::vector<cv::Point> locations;

    cv::Rect roi(100, 100, 100, 100);

    double timeLapse = (std::clock() - timeNow) / (double) CLOCKS_PER_SEC;
    std::cout << timeLapse <<std::endl;
    timeNow = std::clock();
    detector.hog.setSVMDetector(detector.descriptor_vector);
    std::cout << "descriptor: " << detector.descriptor_vector.size() << std::endl;
    std::cout << "detecting..." << std::endl;

#ifdef SYS_TEGRA
    padding = cv::Size(0,0);
    detector.hog.detectMultiScale(gpuTestImageGray, found_objects, detector.hit_threshold, winStride, padding, 1.02, 1.5);
#else
    detector.hog.detectMultiScale(testImageGray, found_objects, detector.hit_threshold, padding, winStride, 1.02, 1.5, true);
#endif

    std::cout << "HOG detections: " << found_objects.size() << std::endl;
    for (auto &detection : found_objects){
      if (background_image.empty()){
        detections.push_back(std::make_pair(detection, &detector));
      }
      else{
        throw(std::runtime_error("Fix this!"));
      }
    }

  }
}


size_t filterDetectionsWithBackgroundSubtractor(const cv::Mat &image, const std::vector<cv::Rect> &detections, const cv::Rect bg_subtractor, const cv::Mat &optional_mask, const float overlap_fraction){

  size_t score = 0;

  for (auto &detection : detections){

    if (!cv::Rect(0, 0, image.cols, image.rows).contains(detection.tl())
      || !cv::Rect(0, 0, image.cols, image.rows).contains(detection.br())) continue;

    if (!optional_mask.empty() && optional_mask.at<unsigned char>(GetCenter(detection)) == 0) continue;


    cv::Mat roi_bg = cv::Mat::zeros(image.size(), CV_8UC1);
    cv::Mat roi_det = cv::Mat::zeros(image.size(), CV_8UC1);

    roi_bg(bg_subtractor).setTo(cv::Scalar(255));
    roi_det(detection).setTo(cv::Scalar(255));

    cv::Mat combined = roi_bg & roi_det;
    double size_combined = cv::sum(combined)[0];

    if (size_combined == 0) continue;

    double size_det = cv::sum(roi_det)[0];

    double fraction = size_det / size_combined;

    if (fraction > overlap_fraction) score++;

  }

  return score;

}

void runDetection(TrainingModel *detector, const cv::Mat &image, std::vector<cv::Rect> &detections){

  cv::Size padding(0, 0);
  cv::Size winStride(4, 4);

  std::vector<double> confidences;

#ifdef SYS_TEGRA
  cv::gpu::GpuMat gpuTestImageGray;
  cvtColor(image, gpuTestImageGray, CV_BGR2GRAY);
#else
  cv::Mat testImageGray;
  cvtColor(image, testImageGray, CV_BGR2GRAY);
#endif

#ifdef SYS_TEGRA
    padding = cv::Size(0,0);
    detector->hog.detectMultiScale(gpuTestImageGray, detections, detector->hit_threshold, winStride, padding, 1.02, 1.5);
#else
    detector->hog.detectMultiScale(testImageGray, detections, detector->hit_threshold, padding, winStride, 1.02, 1.5, true);
#endif

}

void detectTest3(TrainingModel *car_detector, TrainingModel *people_detector, TrainingModel *bike_detector, cv::Mat& imageData, cv::Mat &background_image, std::vector<cv::Rect> &car_detections, std::vector<cv::Rect> &people_detections, std::vector<cv::Rect> &bike_detections) {

  cv::Size padding(32, 32);
  cv::Size winStride(8, 8);

  std::vector<double> confidences_cars;
  std::vector<double> confidences_people;
  std::vector<double> confidences_bikes;

  cv::Mat testImageGray;
  cvtColor(imageData, testImageGray, CV_BGR2GRAY);

  //car_detector->hog.detectMultiScale(testImageGray, car_detections, confidences_cars, car_detector->hit_threshold, winStride, padding, 1.02, 2, false);
  //bike_detector->hog.detectMultiScale(testImageGray, bike_detections, confidences_bikes, bike_detector->hit_threshold, padding, bike_detector->hog.blockStride);
  //people_detector->hog.detectMultiScale(testImageGray, people_detections, confidences_people, people_detector->hit_threshold, padding, people_detector->hog.blockStride);

}

void detectTest(TrainingModel *car_detector, TrainingModel *people_detector, TrainingModel *bike_detector, cv::Mat& imageData, cv::Mat &background_image, cv::Mat &optional_mask) {



  std::vector<cv::Rect> found;
  cv::Size padding(32, 32);
  cv::Size winStride(4, 4);

  std::vector<double> scores;

  std::vector<std::vector<cv::Vec2i> > connected_regions;
  FindConnectedRegions(background_image, connected_regions);

  std::vector<cv::Point> locations;


  std::vector<std::pair<cv::Mat, cv::Rect> > car_regions_of_interest;

  for (auto connected_region : connected_regions){

    cv::Rect bounding_box = cv::boundingRect(connected_region);
    cv::Point center =  bounding_box.tl() + cv::Point(bounding_box.width / 2, bounding_box.height / 2);
    if (!optional_mask.empty() && optional_mask.at<unsigned char>(center) == 0) continue;

    car_regions_of_interest.push_back( std::make_pair(imageData(bounding_box).clone(), bounding_box));

  }
  
  cv::Mat testImageGray;
  cvtColor(imageData, testImageGray, CV_BGR2GRAY);
  for (auto region : car_regions_of_interest){

    std::vector<cv::Point> found_cars;
    std::vector<cv::Point> found_people;
    std::vector<cv::Point> found_bikes;

    std::vector<double> confidences_cars;
    std::vector<double> confidences_people;
    std::vector<double> confidences_bikes;
    
    cv::Mat resized_region_car;
    cv::Mat resized_region_bike;
    cv::Mat resized_region_people;
#ifdef SYS_TEGRA
    cv::resize(region.first, resized_region_car, car_detector->hog.win_size);
    cv::resize(region.first, resized_region_bike, bike_detector->hog.win_size);
    cv::resize(region.first, resized_region_people, people_detector->hog.win_size);
#else
    cv::resize(region.first, resized_region_car, car_detector->hog.winSize);
    cv::resize(region.first, resized_region_bike, bike_detector->hog.winSize);
    cv::resize(region.first, resized_region_people, people_detector->hog.winSize);
#endif

    for (auto car : found_cars){

      cv::rectangle(imageData, region.second, cv::Scalar(152, 123, 51), 3);

    }

    for (auto person : found_people){

      cv::rectangle(imageData, region.second, cv::Scalar(10, 123, 121), 3);

    }

    for (auto bike : found_bikes){

      cv::rectangle(imageData, region.second, cv::Scalar(192, 3, 51), 3);

    }

  }
  //people_detector->hog.detectMultiScaleROI(testImageGray, found_people, locations, people_detector->hit_threshold);
  //bike_detector->hog.detectMultiScaleROI(testImageGray, found_bikes, locations, bike_detector->hit_threshold);



  /*for (auto i = 0; i < found.size(); ++i){

    if (!found[i].tl().inside(cv::Rect(0, 0, imageData.cols, imageData.rows))
      || !found[i].br().inside(cv::Rect(0, 0, imageData.cols, imageData.rows))){
      continue;
    }
    cv::Mat roi = background_image(found[i]);

    int size = roi.rows * roi.cols;
    int count = cv::countNonZero(roi);

    if (count < 0.3*size){
      continue;
    }

    cv::rectangle(imageData, found[i].tl(), found[i].br(), cv::Scalar(152, 123, 51), 3);

    std::stringstream ss;
    ss << scores[i];
    cv::putText(imageData, ss.str(), found[i].tl() + cv::Point(5, 5), cv::FONT_HERSHEY_COMPLEX_SMALL, 1.2, cvScalar(20, 200, 250), 1, CV_AA);

  }*/

}

void detectTrainingSetTest(const cv::HOGDescriptor& hog, const double hitThreshold, const std::vector<std::string>& posFileNames, const std::vector<std::string>& negFileNames) {
  unsigned int truePositives = 0;
  unsigned int trueNegatives = 0;
  unsigned int falsePositives = 0;
  unsigned int falseNegatives = 0;
  std::vector<cv::Point> foundDetection;
  // Walk over positive training samples, generate images and detect
  for (std::vector<std::string>::const_iterator posTrainingIterator = posFileNames.begin(); posTrainingIterator != posFileNames.end(); ++posTrainingIterator) {
    const cv::Mat imageData = cv::imread(*posTrainingIterator, 0);
    //hog.dete
    //hog.detect(imageData, foundDetection, hitThreshold, winStride, trainingPadding);
    if (foundDetection.size() > 0) {
      ++truePositives;
      falseNegatives += foundDetection.size() - 1;
    }
    else {
      ++falseNegatives;
    }

    //cv::Mat imageData_= imageData.clone();
    //detectTest(hog, hitThreshold, imageData_);
    //while ((cvWaitKey(10) & 255) != 27) {
    //  imshow("HOG custom detection", imageData_);
    //}

  }
  // Walk over negative training samples, generate images and detect
  for (std::vector<std::string>::const_iterator negTrainingIterator = negFileNames.begin(); negTrainingIterator != negFileNames.end(); ++negTrainingIterator) {
    const cv::Mat imageData = cv::imread(*negTrainingIterator, 0);
    //hog.detect(imageData, foundDetection, hitThreshold, winStride, trainingPadding);
    if (foundDetection.size() > 0) {
      falsePositives += foundDetection.size();
    }
    else {
      ++trueNegatives;
    }

  }

  printf("Results:\n\tTrue Positives: %u\n\tTrue Negatives: %u\n\tFalse Positives: %u\n\tFalse Negatives: %u\n", truePositives, trueNegatives, falsePositives, falseNegatives);
}

bool FindConnectedRegions(const cv::Mat &image, std::vector<std::vector<cv::Vec2i> >&connected_regions){

  //draw contours around the connected regions returning a vector of each connected region
  std::vector<std::vector<cv::Point> >contours;

  GetContours(image, contours);

  //iterate over the contours, finding the number of pixels they enclose and store those above a heuristic threshold
  for (size_t i = 0; i<contours.size(); i++){

    std::vector<cv::Vec2i> connected_region;
    FindSingleRegionFromContour(contours[i], connected_region, image.size());

    if (connected_region.size() > 0.001*image.rows*image.cols) connected_regions.push_back(connected_region);

  }

  //did we find any that were big enough?
  return connected_regions.size() > 0;

}

void GetContours(const cv::Mat &image, std::vector<std::vector<cv::Point> > &contours) {

  if (image.empty()) return;

  //threshold the image to 0 & 255 values
  cv::Mat thresholded, thresholded8bit;
  threshold(image, thresholded, 0.4, 1.0, cv::THRESH_BINARY);
  thresholded.convertTo(thresholded8bit, CV_8U);

  //find contours around the 255 'blobs' in the image
  findContours(thresholded8bit, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

}

void FindSingleRegionFromContour(const std::vector<cv::Point> &contour, std::vector<cv::Vec2i> &connected_region, const cv::Size image_size) {

  //we're not intested in the small contours as they correspond to small blobs
  if (contour.size() < 100) return;

  //draw the interior region of the contour on a blank frame
  cv::Point center = GetCenter<cv::Point>(contour);
  cv::Mat mask = cv::Mat::zeros(image_size.height + 2, image_size.width + 2, CV_8UC1);
  std::vector<std::vector<cv::Point> >t;
  t.push_back(contour);
  drawContours(mask, t, -1, cv::Scalar(255), CV_FILLED, 8);


  //push the interior pixels of the drawn region (faster than polygon intersection?)
  unsigned char *mask_ptr = (unsigned char *)mask.data;
  for (int r = 0; r<mask.rows; r++){
    for (int c = 0; c<mask.cols; c++){
      if (mask_ptr[r*mask.cols + c] == static_cast<unsigned char>(255))
        connected_region.push_back(cv::Vec2i(c, r));
    }
  }

}

void mineVideoForFalsePositives(const std::string &video_file, TrainingModel &detector){

  cv::VideoCapture cap(video_file); 

  if (!cap.isOpened()) { // check if we succeeded
    printf("Error opening camera!\n");
    return;
  }

  const std::string win_name("HOG custom detection");
  cv::namedWindow(win_name);

  AnnotationWrapper aw("false_positives", detector);

  cv::setMouseCallback(win_name, MinerMouseCallback, &aw);

  /*
  cv::Mat affine_transform = cv::Mat::zeros(2, 3, CV_32FC1);
  const float angle = 0.25*3.141592;
  affine_transform.at<float>(0, 0) = cos(angle);
  affine_transform.at<float>(0, 1) = -sin(angle);
  affine_transform.at<float>(1, 0) = sin(angle);
  affine_transform.at<float>(1, 1) = cos(angle);
  affine_transform.at<float>(0, 2) = 0;
  affine_transform.at<float>(1, 2) = 100;
  */

  cv::Mat testImageA;
  cap >> testImageA;

  bool run = true;

  while (run) {
    cv::Mat testImage;
    cap >> testImage;
    //cv::resize(testImage, testImage, cv::Size(960, 540));

    for (int rot_angle = -40; rot_angle < 40; rot_angle += 5){

      rot_angle = 0;

      cv::Mat rotatedImage;
      rotate2D(testImage, rotatedImage, rot_angle);

      cv::Mat testImageBIG;
      cv::resize(rotatedImage, testImageBIG, cv::Size(0, 0), 2, 2, cv::INTER_CUBIC);

      std::vector<cv::Rect> detections;

      runDetection(&detector, testImage, detections);
      //cv::Mat writerImageSmall;
      //cv::resize(testImageBIG, writerImageSmall, testImageA.size(), cv::INTER_CUBIC);

      aw.Clear();
      aw.Setup(detections);

      while (1){

        cv::Mat frame = testImage.clone();

        int key = cv::waitKey(5);
        if (key == ' ') break;
        if (key == 'q') run = false;

        for (auto detection : aw.positive){
          cv::rectangle(frame, detection, cv::Scalar(255, 0, 0), 2);
        }

        imshow(win_name, frame);

      }

      aw.SaveAll(testImage);

      break;

    }
  }


}


void cleanDetection2(std::vector<cv::Rect> &detections){

  std::vector<cv::Rect> new_detections;

  for (auto detection1 : detections){
    
    bool found_inside = false;

    cv::Point detection1_center = detection1.tl() + cv::Point(detection1.width / 2, detection1.height / 2);

    for (auto detection2 : detections){

      if (detection1 == detection2) continue;

      if (detection2.contains(detection1_center)){

        if (detection1.area() < detection2.area()){
          found_inside = true;
        }
      }
    }

    if (!found_inside) new_detections.push_back(detection1);

  }
  
  detections = new_detections;

}

void cleanDetection(std::vector<cv::Rect> &detections){

  std::vector<cv::Rect> new_detections;

  for (auto detection1 : detections){

    bool found_overlapping = false;
    for (auto detection2 : detections){

      if (detection1 == detection2) continue;

      if (detection2.contains(detection1.tl()) && detection2.contains(detection1.br())){
        found_overlapping = true;
        break;
      }
    }

    if (!found_overlapping) new_detections.push_back(detection1);

  }

  detections = new_detections;

}

void cleanDetections(std::vector<std::pair<cv::Rect, TrainingModel *> > &detections){

  std::vector<std::pair<cv::Rect, TrainingModel *> > new_detections;

  for (auto detection1 : detections){

    bool found_overlapping = false;
    for (auto detection2 : detections){

      if (detection1 == detection2) continue;

      if (detection2.first.contains(detection1.first.tl()) && detection2.first.contains(detection1.first.br()) && (detection2.second == detection1.second)){
        found_overlapping = true;
        break;
      }
    }

    if (!found_overlapping) new_detections.push_back(detection1);

  }

  detections = new_detections;

  //detections.insert(detections.end(), car_detections.begin(), car_detections.end());
  //detections.insert(detections.end(), bike_detections.begin(), bike_detections.end());
  //detections.insert(detections.end(), people_detections.begin(), people_detections.end());
  
}

struct Camera {

  explicit Camera(const std::string &calib_file){

    cv::FileStorage fs;
    fs.open(calib_file, cv::FileStorage::READ);

    if (!fs.isOpened()) throw std::runtime_error("");

    fs["Camera_Matrix"] >> camera_matrix;
    fs["Distortion_Coefficients"] >> distortion;

  }


  void unwarpImage(cv::Mat &image){

    cv::Mat undistorted_image;
    cv::undistort(image, undistorted_image, camera_matrix, distortion);

    //image = undistorted_image;
  }

  cv::Mat remapping_function;

  cv::Mat camera_matrix;
  cv::Mat distortion;

};

void viewOutputOnVideo(const std::string &video_file, std::vector<TrainingModel> &detectors, const std::string &bgsubtractor_file, const std::string &optional_mask_car, const std::string &optional_mask_person){

  timeNow = std::clock();
  cv::Mat affine_transform = cv::Mat::zeros(2, 3, CV_32FC1);
  const float angle = -0.27;// *3.141592;
  affine_transform.at<float>(0, 0) = cos(angle);
  affine_transform.at<float>(0, 1) = -sin(angle);
  affine_transform.at<float>(1, 0) = sin(angle);
  affine_transform.at<float>(1, 1) = cos(angle);
  affine_transform.at<float>(0, 2) = 0;
  affine_transform.at<float>(1, 2) = 0;

  const float SCALE_BY_FACTOR = 1;
  
  cv::VideoCapture cap(video_file);
  if (!cap.isOpened()) return;
  cv::Mat testImage;
    
  cap >> testImage;

  cv::Size newSize(testImage.size().width * SCALE_BY_FACTOR, testImage.size().height * SCALE_BY_FACTOR);
  QuickImageGrid qig(SCALE_BY_FACTOR, newSize);

  cv::VideoWriter writer("output_videos/video.avi", CV_FOURCC('M','J','P','G'), 5, testImage.size());
  if (!cap.isOpened()) { // check if we succeeded
    printf("Error opening camera!\n");
    return;
  }

  const std::string win_name("HOG custom detection"), detection_frame("HOG Detector RAW OUTPUT");
  //cv::namedWindow(win_name);
  cv::namedWindow(detection_frame);


  cv::Mat background_mask_cars, background_mask_person;
  if (optional_mask_car.length() != 0){
    background_mask_cars = cv::imread(optional_mask_car, 0);
    cv::resize(background_mask_cars, background_mask_cars, cv::Size(0, 0), SCALE_BY_FACTOR, SCALE_BY_FACTOR);
  }
  if (optional_mask_person.length() != 0){
    background_mask_cars = cv::imread(optional_mask_person, 0);
    cv::resize(background_mask_person, background_mask_person, cv::Size(0, 0), SCALE_BY_FACTOR, SCALE_BY_FACTOR);
  }  


  //BackgroundSubtractorWrapper bsw(MOG2);
  //bool using_bgsubtractor = !bgsubtractor_file.empty();
  //if (using_bgsubtractor)
  //  bsw.Setup(cv::imread(bgsubtractor_file), cv::imread(bgsubtractor_file));

  while ((cvWaitKey(10) & 255) != 27) {

    cap >> testImage; // get a new frame from camera

    //for (int i = 0; i < 100; ++i){
    //  cap >> testImage;
    //}

    if (testImage.empty()) break;

    cv::Mat testImageFlipped;
    cv::flip(testImage, testImageFlipped, 1);

    //camera.unwarpImage(testImage);

    static int64 framenum = 0;
    //if (using_bgsubtractor){
    //  cv::Mat newMat;
    //  bsw.Update(testImage, framenum, newMat);
    //}
    framenum++;

    //double size the image
    cv::Mat testImageBIG, testImageFlippedBig;
    cv::resize(testImage, testImageBIG, cv::Size(0, 0), SCALE_BY_FACTOR, SCALE_BY_FACTOR, cv::INTER_CUBIC);
    cv::resize(testImageFlipped, testImageFlippedBig, cv::Size(0, 0), SCALE_BY_FACTOR, SCALE_BY_FACTOR, cv::INTER_CUBIC);

    cv::Mat background_image;
    //if (using_bgsubtractor){
    //  background_image = bsw.GetForegroundMask();
    //  cv::resize(background_image, background_image, cv::Size(0, 0), SCALE_BY_FACTOR, SCALE_BY_FACTOR, cv::INTER_NEAREST);
    //}

    std::vector< std::array<cv::Point, 4> > all_detections;

    cv::Mat testImageDrawOnBig = testImageBIG.clone();
    
    //std::cerr << "starting detection" << std::endl;

    //do over both frames (flipped)
    for (int image_dir = 0; image_dir < 1; image_dir++){

      cv::Mat image;
      if (image_dir == 0 || image_dir == 2)
        image = testImageBIG.clone();
      else
        image = testImageFlippedBig.clone();


      //rotate2D(image, image, -16);
      cv::imwrite("output_frames/frame_new.png", image);

      if (image_dir == 2)
        cv::warpAffine(image, image, affine_transform, image.size());
      
      std::vector<std::pair<cv::Rect, TrainingModel *> > detections;


      detectTest2(detectors, image, background_image, detections, background_mask_cars, background_mask_person);

      cleanDetections(detections);
      
      //std::cerr << "found " << car_detections.size() + bike_detections.size() + people_detections.size() << " detections in frame " << framenum << std::endl;

      //AddDetectionsToFrame(car_detections, testImageDrawOnBig, qig, image_dir, all_detections, affine_transform, car_detector.name());
      //AddDetectionsToFrame(bike_detections, testImageDrawOnBig, qig, image_dir, all_detections, affine_transform, bike_detector.name());
      AddDetectionsToFrame(detections, testImageDrawOnBig, qig, image_dir, all_detections, affine_transform);

    }

    qig.drawQuadsOnFrame(testImageBIG, all_detections);

    cv::resize(testImageBIG, testImageBIG, cv::Size(0, 0), (float)1 / SCALE_BY_FACTOR, (float)1 / SCALE_BY_FACTOR);

    //cv::imshow(win_name, testImageBIG);

    cv::resize(testImageDrawOnBig, testImageDrawOnBig, cv::Size(0, 0), (float)1 / SCALE_BY_FACTOR, (float)1 / SCALE_BY_FACTOR);

    cv::imshow(detection_frame, testImageDrawOnBig);

    writer << testImageDrawOnBig;

  }
}

inline float GaussianScore(const cv::Point &mean, const cv::Point &point, const float sigma_x, const float sigma_y){

  const float norm = sqrt((2 * 3.141592)*(2 * 3.141592) * (sigma_x * sigma_y));
  return (1.0f/norm) * exp(-((((mean.x - point.x)*(mean.x - point.x)) / 2 * sigma_x*sigma_x) + (((mean.y - point.y)*(mean.y - point.y)) / 2 * sigma_y*sigma_y)));

}

inline float ScoreWithGaussianWeight(const cv::Mat &quad_frame, const cv::Point center, const float sigma_x, const float sigma_y){

  float score = 0;

  for (int r = 0; r < quad_frame.rows; ++r){
    for (int c = 0; c < quad_frame.cols; ++c){
    
      const cv::Point poi(c, r);
      if (quad_frame.at<unsigned char>(poi) == 255){

        score += GaussianScore(center, poi, sigma_x, sigma_y);

      }
    
    }
  }

  return score;


}


void AddDetectionsToFrame(std::vector<std::pair<cv::Rect, TrainingModel *> > detections, cv::Mat &frame, QuickImageGrid qig, int MODE, std::vector< std::array<cv::Point, 4> > &quads_to_draw, cv::Mat &affine_transform){

  for (const auto &det : detections){

   
    cv::Rect ndet = det.first; 

    if (MODE == 1){
      //pt = FlipPoint(pt, frame.size());
      ndet = FlipRect(ndet, frame.size());
    }

    ndet = SafeDetectionOnFrame(ndet, frame.size());

    cv::Mat overlap_frame = cv::Mat::zeros(frame.size(), CV_8UC1);

    overlap_frame(ndet).setTo(255);
    
    if (MODE == 2){

      cv::Mat invert_transform;
      cv::invertAffineTransform(affine_transform, invert_transform);
      
      std::vector<cv::Point> in; in.push_back(ndet.tl()); in.push_back(ndet.br());
      std::vector<cv::Point> out;
      cv::transform(in, out, invert_transform);
      cv::Point tl = out[0];
      cv::Point br = out[1];

      float hypotenuse = std::sqrt((ndet.width * ndet.width) + (ndet.height * ndet.height));

      float angle = acos(ndet.width / hypotenuse);

      const float pi = 3.141592;

      cv::Point p1 = tl;
      cv::Point p2 = tl + cv::Point(ndet.width * cos(angle), ndet.width * sin(angle));
      cv::Point p3 = br;
      cv::Point p4 = tl + cv::Point(ndet.width * cos((pi / 2) - angle), -ndet.width * sin((pi/2) - angle));
      
      cv::line(frame, p1, p2, cv::Scalar(255, 0, 0), 2);
      cv::line(frame, p3, p2, cv::Scalar(255, 0, 0), 2);
      cv::line(frame, p4, p3, cv::Scalar(255, 0, 0), 2);
      cv::line(frame, p1, p4, cv::Scalar(255, 0, 0), 2);

      cv::warpAffine(overlap_frame, overlap_frame, invert_transform, overlap_frame.size());

    }
    else{

      cv::rectangle(frame, ndet, cv::Scalar(0, 255, 0), 2);
      //cv::putText(frame, det.second->name(), ndet.tl() + cv::Point(5, 5), CV_FONT_HERSHEY_PLAIN, 4, cv::Scalar(255, 0, 0), 4);

    }

    cv::Point topLeft(det.first.tl());
    cv::Point centre = GetCenter(topLeft, det.first.width, det.first.height);

    //cv::circle(frame, pt, 8, cv::Scalar(255, 0, 0));

    std::array<cv::Point, 4> best;
    float best_sum = std::numeric_limits<float>::min();
    
    for (size_t q_idx = 0; q_idx < qig.quads.size(); q_idx++){

      cv::Mat &quad_frame = qig.quick_intersection_cache[q_idx];

      //float sum = cv::sum(quad_frame & overlap_frame)[0] / cv::sum(quad_frame)[0];
      float sum = ScoreWithGaussianWeight(quad_frame, centre, det.first.width, det.first.height);

      if (sum > best_sum){
        best = qig.quads[q_idx];
        best_sum = sum;
      }

    }

    if (best_sum != std::numeric_limits<float>::min()){
        quads_to_draw.push_back(best);
    }

  }

}

void rotate2D(const cv::Mat & src, cv::Mat & dst, const double degrees){
  cv::Mat frame, frameRotated;

  int diagonal = (int)sqrt(src.cols * src.cols + src.rows * src.rows);
  int newWidth = diagonal;
  int newHeight = diagonal;

  int offsetX = (newWidth - src.cols) / 2;
  int offsetY = (newHeight - src.rows) / 2;
  cv::Mat targetMat(newWidth, newHeight, src.type(), cv::Scalar(0));
  cv::Point2f src_center(targetMat.cols / 2.0f, targetMat.rows / 2.0f);

  src.copyTo(frame);

  frame.copyTo(targetMat.rowRange(offsetY, offsetY +
    frame.rows).colRange(offsetX, offsetX + frame.cols));
  cv::Mat rot_mat = cv::getRotationMatrix2D(src_center, degrees, 1.0);
  cv::warpAffine(targetMat, frameRotated, rot_mat, targetMat.size());

  cv::Rect bound_Rect(frame.cols, frame.rows, 0, 0);
  int x1 = offsetX;
  int x2 = offsetX + frame.cols;
  int x3 = offsetX;
  int x4 = offsetX + frame.cols;
  int y1 = offsetY;
  int y2 = offsetY;
  int y3 = offsetY + frame.rows;
  int y4 = offsetY + frame.rows;
  cv::Mat co_Ordinate = (cv::Mat_<double>(3, 4) << x1, x2, x3, x4,
    y1, y2, y3, y4,
    1, 1, 1, 1);

  cv::Mat RotCo_Ordinate = rot_mat * co_Ordinate;

  for (int i = 0; i < 4; ++i) {
    if (RotCo_Ordinate.at<double>(0, i) < bound_Rect.x)
      bound_Rect.x = (int)RotCo_Ordinate.at<double>(0, i);
    if (RotCo_Ordinate.at<double>(1, i) < bound_Rect.y)
      bound_Rect.y = RotCo_Ordinate.at<double>(1, i);
  }

  for (int i = 0; i < 4; ++i) {
    if (RotCo_Ordinate.at<double>(0, i) > bound_Rect.width)
      bound_Rect.width = (int)RotCo_Ordinate.at<double>(0, i);
    if (RotCo_Ordinate.at<double>(1, i) > bound_Rect.height)
      bound_Rect.height = RotCo_Ordinate.at<double>(1, i);
  }

  bound_Rect.width = bound_Rect.width - bound_Rect.x;
  bound_Rect.height = bound_Rect.height - bound_Rect.y;

  if (bound_Rect.x < 0)
    bound_Rect.x = 0;
  if (bound_Rect.y < 0)
    bound_Rect.y = 0;
  if (bound_Rect.width > frameRotated.cols)
    bound_Rect.width = frameRotated.cols;
  if (bound_Rect.height > frameRotated.rows)
    bound_Rect.height = frameRotated.rows;

  cv::Mat ROI = frameRotated(bound_Rect);
  ROI.copyTo(dst);
}

