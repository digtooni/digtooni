#include "hog_svm.hpp"

/**
* For unixoid systems only: Lists all files in a given directory and returns a vector of path+name in string format
* @param dirName
* @param fileNames found file names in specified directory
* @param validExtensions containing the valid file extensions for collection in lower case
*/
void getFilesInDirectory(const std::string& dirName, std::vector<std::string>& fileNames, const std::vector<std::string>& validExtensions) {

}

HOGSVM::HOGSVM(const std::string &config_file){

  ConfigReader cfg(config_file);

  name_ = cfg.get_element("name");

  win_stride = cv::Size(8, 8);
  training_padding = cv::Size(0, 0);
  hog.win_size.width = cfg.get_element_as_type<int>("width");
  hog.win_size.height = cfg.get_element_as_type<int>("height");

  std::string positive_dirs = cfg.get_element("positive_dirs");
  std::string negative_dirs = cfg.get_element("negative_dirs");

  positive_training_data_dirs = cfg.split(positive_dirs, ',');
  negative_training_data_dirs = cfg.split(negative_dirs, ',');

  for (size_t i = 0; i < positive_training_data_dirs.size(); ++i){
    positive_training_data_dirs[i] = cfg.trim(positive_training_data_dirs[i]);
  }
  for (size_t i = 0; i < negative_training_data_dirs.size(); ++i){
    negative_training_data_dirs[i] = cfg.trim(negative_training_data_dirs[i]);
  }

  namespace fs = boost::filesystem;

  fs::path abs_config_file_path(config_file);// , fs::current_path());
  features_file = (abs_config_file_path.parent_path() / fs::path("features.dat")).string();
  svm_model_file = (abs_config_file_path.parent_path() / fs::path("model_file.dat")).string();
  descriptor_vector_file = (abs_config_file_path.parent_path() / fs::path("descriptor_vector.dat")).string();
  
  valid_extensions.push_back(".jpg");
  valid_extensions.push_back(".png");
  valid_extensions.push_back(".ppm");
  valid_extensions.push_back(".pgm");


}

void HOGSVM::copyHOG(cv::gpu::HOGDescriptor lhs, cv::gpu::HOGDescriptor& rhs) {
  rhs.win_size = lhs.win_size;
  rhs.block_size = lhs.block_size;
  rhs.block_stride = lhs.block_stride;
  rhs.cell_size = lhs.cell_size;
  rhs.nbins = lhs.nbins;
  rhs.win_sigma = lhs.win_sigma;
  rhs.threshold_L2hys = lhs.threshold_L2hys;
  rhs.gamma_correction = lhs.gamma_correction;
  rhs.nlevels = lhs.nlevels;
}

HOGSVM::HOGSVM(const HOGSVM &other) {

  win_stride = other.win_stride;
  training_padding = other.training_padding;
  copyHOG(other.hog, hog);

  name_ = other.name_;
  features_file = other.features_file;
  descriptor_vector_file = other.descriptor_vector_file;
  svm_model_file = other.svm_model_file;

  positive_training_data_dirs = other.positive_training_data_dirs;
  negative_training_data_dirs = other.negative_training_data_dirs;
  positive_training_images = other.positive_training_images;
  negative_training_images = other.negative_training_images;
  valid_extensions = other.valid_extensions;

  descriptor_vector = other.descriptor_vector;
  number_of_samples = other.number_of_samples;
  hit_threshold = other.hit_threshold;

}

// Copy constructor
HOGSVM &HOGSVM::operator=(const HOGSVM &rhs){

  HOGSVM tmp(rhs);
  //exception safe
  std::swap(*this, tmp);
  //std::swap(hog_size, tmp.hog_size);
  std::swap(win_stride, tmp.win_stride);
  std::swap(win_stride, tmp.win_stride);
  std::swap(training_padding, tmp.training_padding);
  std::swap(hog, tmp.hog);
  std::swap(name_, tmp.name_);
  std::swap(features_file, tmp.features_file);
  std::swap(descriptor_vector_file, tmp.descriptor_vector_file);
  std::swap(svm_model_file, tmp.svm_model_file);
  std::swap(positive_training_data_dirs, tmp.positive_training_data_dirs);
  std::swap(negative_training_data_dirs, tmp.negative_training_data_dirs);
  std::swap(positive_training_images, tmp.positive_training_images);
  std::swap(negative_training_images, tmp.negative_training_images);
  std::swap(valid_extensions, tmp.valid_extensions);
  std::swap(descriptor_vector, tmp.descriptor_vector);
  std::swap(number_of_samples, tmp.number_of_samples);
  std::swap(hit_threshold, tmp.hit_threshold);
  return *this;

}

void HOGSVM::SetTrainingDataDirs(const std::vector<std::string> &positive_dirs, const std::vector<std::string> &negative_dirs){

  positive_training_data_dirs = positive_dirs;
  negative_training_data_dirs = negative_dirs;
}

void HOGSVM::CollectExtraNegatives(const std::string &directory){

  getFilesInDirectory(directory, negative_training_images, valid_extensions);
  number_of_samples = positive_training_images.size() + negative_training_images.size();
}

void HOGSVM::CollectExtraNegatives(const std::vector<std::string> &directories){

  for (auto dir : directories) {
    CollectExtraNegatives(dir);
  }
}

void HOGSVM::CollectTrainingData(){

  for (const auto &pos_dir : positive_training_data_dirs)
    getFilesInDirectory(pos_dir, positive_training_images, valid_extensions);

  for (const auto &neg_dir : negative_training_data_dirs)
    getFilesInDirectory(neg_dir, negative_training_images, valid_extensions);

  number_of_samples = positive_training_images.size() + negative_training_images.size();

}


void HOGSVM::CollectTrainingData(const HOGSVM &other_model){

  for (const auto &pos_dir : other_model.positive_training_data_dirs)
    getFilesInDirectory(pos_dir, negative_training_images, valid_extensions);

  number_of_samples = positive_training_images.size() + negative_training_images.size();
}

void HOGSVM::Retrain(){

  if (svm_model_file == ""){
    throw std::runtime_error("Error, SVM file is not defined");
  }

  TRAINHOG_SVM_TO_TRAIN::getInstance()->loadModelFromFile(svm_model_file);

  //will be features_file eventually - save SV's here and then load new data appending to this.
  std::ofstream sv_file;
  sv_file.open(features_file);

  if (sv_file.good() && sv_file.is_open()) {
    //write the training data 
    TRAINHOG_SVM_TO_TRAIN::getInstance()->writeSupportVectorsToFile(sv_file);
  }
  sv_file.close();
  Train(true);
}

void HOGSVM::GenerateFeaturesFromData(bool append){

  float percent;
  printf("Reading files, generating HOG features and save them to file '%s':\n", features_file.c_str());

  std::fstream File;
  if (append)
    File.open(features_file.c_str(), std::ios::app);
  else
    File.open(features_file.c_str(), std::ios::out);

  if (File.good() && File.is_open()) {
    // Remove following line for libsvm which does not support comments
    // File << "# Use this file to train, e.g. SVMlight by issuing $ svm_learn -i 1 -a weights.txt " << featuresFile.c_str() << endl;
    // Iterate over sample images
    for (unsigned long currentFile = 0; currentFile < number_of_samples; ++currentFile) {
      storeCursor();
      std::vector<float> featureVector;
      // Get positive or negative sample image file path
      const std::string currentImageFile = (currentFile < positive_training_images.size() ? positive_training_images.at(currentFile) : negative_training_images.at(currentFile - positive_training_images.size()));
      // Output progress
      if ((currentFile + 1) % 10 == 0 || (currentFile + 1) == number_of_samples) {
        percent = ((currentFile + 1) * 100 / number_of_samples);
        printf("%5lu (%3.0f%%):\tFile '%s'", (currentFile + 1), percent, currentImageFile.c_str());
        fflush(stdout);
        resetCursor();
      }
      // Calculate feature vector from current image file
      calculateFeaturesFromInput(currentImageFile, featureVector, hog, win_stride, training_padding);
      if (!featureVector.empty()) {
        /* Put positive or negative sample class to file,
        * true=positive, false=negative,
        * and convert positive class to +1 and negative class to -1 for SVMlight
        */
        File << ((currentFile < positive_training_images.size()) ? "+1" : "-1");
        // Save feature vector components
        for (unsigned int feature = 0; feature < featureVector.size(); ++feature) {
          File << " " << (feature + 1) << ":" << featureVector.at(feature);
        }
        File << std::endl;
      }
    }
    printf("\n");
    File.flush();
    File.close();
  }
  else {
    printf("Error opening file '%s'!\n", features_file.c_str());
    return;
  }
}

void HOGSVM::Train(bool retrain=false){

  GenerateFeaturesFromData(retrain);

  printf("Calling %s\n", TRAINHOG_SVM_TO_TRAIN::getInstance()->getSVMName());

  std::cout << "Loading " << number_of_samples << " samples.\n" << std::endl;

  TRAINHOG_SVM_TO_TRAIN::getInstance()->read_problem(const_cast<char*> (features_file.c_str()));
  TRAINHOG_SVM_TO_TRAIN::getInstance()->train(); // Call the core libsvm training procedure
  printf("Training done, saving model file!\n");
  TRAINHOG_SVM_TO_TRAIN::getInstance()->saveModelToFile(svm_model_file);

  printf("Generating representative single HOG feature vector using svmlight!\n");
  std::vector<unsigned int> descriptorVectorIndices;
  // Generate a single detecting feature vector (v1 | b) from the trained support vectors, for use e.g. with the HOG algorithm
  TRAINHOG_SVM_TO_TRAIN::getInstance()->getSingleDetectingVector(descriptor_vector, descriptorVectorIndices);
  // And save the precious to file system
  saveDescriptorVectorToFile(descriptor_vector, descriptorVectorIndices, descriptor_vector_file);

  TRAINHOG_SVM_TO_TRAIN::getInstance()->Refresh();
}

void HOGSVM::LoadModel(){

  //TRAINHOG_SVM_TO_TRAIN::getInstance()->loadModelFromFile(svm_model_file);

  std::ifstream ifs(descriptor_vector_file);

  if (!ifs.is_open()){
    throw std::runtime_error("Error, could not open " + descriptor_vector_file);
  }

  descriptor_vector.clear();

  while (!ifs.eof()){
    float x;
    ifs >> x;
    descriptor_vector.push_back(x);
  }


  std::ifstream model_file(svm_model_file);

  if (!model_file.is_open()){
    throw std::runtime_error("Error, could not open " + svm_model_file);
  }

  for (int i = 0; i < 10; ++i){
    std::string line;
    std::getline(model_file, line);
  }

  model_file >> hit_threshold;

  hog.setSVMDetector(descriptor_vector);

}
