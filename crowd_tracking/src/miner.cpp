#include "miner.hpp"

void MinerMouseCallback(int event, int x, int y, int, void *annotations){

  if (event != CV_EVENT_LBUTTONDBLCLK) return;

  AnnotationWrapper *aw = (AnnotationWrapper *)annotations;

  for (auto positive : aw->positive){

    if (positive.contains(cv::Point(x, y))){

      aw->to_save.push_back(positive);
      aw->positive.remove(positive);

      return;
    }

    
  }


}
