#include "object.hpp"
#include <iostream>

double computeAngle(cv::Vec2d v1, cv::Vec2d v2) {

  double dotProduct = v1[0] * v2[0] + v1[1] * v2[1];
  double norm = cv::norm(v1) * cv::norm(v2);
  double cosine = dotProduct / norm;
  double angle = acos(cosine);

  return angle;
}

Object::Object(cv::Rect detection, cv::Point center, cv::Mat mask, int objectCount, cv::Mat hist, int validThresh)
  : boundingBox(detection)
  , centroid(center)
  , binaryMask(mask)
  , age(0)
  , lastSeen(0)
  , occluded(false)
  , updated(false)
  , id(objectCount)
  , validationFrames(0)
  , isPedestrian(false)
  , colorHist(hist)
  , VALID_THRESH(validThresh)
  , oldPositionsRecorded(false)
{
    // Assign a name
    std::stringstream ss;
    ss << "Object " << id;
    name = ss.str(); 

    // Assign a random color for drawing
    cv::RNG rng(rand() % 10000);
    drawColor = cv::Scalar(rng.uniform(0, 255), rng.uniform(0,255), 
                rng.uniform(0,255));
} 

bool Object::getPositionsRecorded() {
  return oldPositionsRecorded;
}

void Object::setPositionsRecorded() {
  oldPositionsRecorded = true;
}

// Return the number of frames since the
// object was last seen in the video
int Object::getLastSeen() {
    return lastSeen;
}

void Object::setEntered() {
    entered_zone = true;
}

bool Object::getEntered() {
    return entered_zone;
}

// Update the history of an occluded
// object
void Object::update() {
    ++age;
    ++lastSeen;
    occluded = true;
    addPoint(getCentroid());
    updated = true;
}

// Update the object with newly detected information
void Object::update(cv::Rect detection, cv::Point center, cv::Mat newHist, cv::Mat mask) {
  ++age;
  lastSeen = 0;
  occluded = false;
  updated = true;
  colorHist = newHist;
  binaryMask = mask;
  boundingBox = detection;

  cv::Point ptDir = (center - centroid);
  direction = cv::Vec2d(ptDir.x, ptDir.y);
  centroid = center;
  addPoint(center);

}


bool Object::getIsPedestrian() {
    return isPedestrian;
}

int Object::getId() const {
    return id;
}

int Object::getAge() {
    return age;
}

cv::Mat Object::getHist() {
    return colorHist;
}

cv::Point Object::getCentroid() {
    return centroid;
}

cv::Scalar Object::getColor() {
    return drawColor;
}

std::string Object::getName() {
    return name;
}

// Add the current position to the point history
void Object::addPoint(const cv::Point pt) {
    pointHistory.push_back(pt);
}

// Distance in pixels between this object
// and another object
double Object::dist(Object obj2) {
    cv::Vec2d v1(centroid.x, centroid.y);
    cv::Vec2d v2(obj2.getCentroid().x, obj2.getCentroid().y);

    return cv::norm(v1 - v2);
}

double Object::dist(cv::Point pt) {
    cv::Vec2d v1(centroid.x, centroid.y);
    cv::Vec2d v2(pt.x, pt.y);

    return cv::norm(v1 - v2);
}

// Compute the Bhattacharrya distance between
// this and another object's histograms
double Object::histDistance(Object obj2) {
    cv::Mat hist2 = obj2.getHist();
    cv::normalize(colorHist, colorHist, 1, 0, cv::NORM_L1);
    cv::normalize(hist2, hist2, 1, 0, cv::NORM_L1);
    double dist = cv::compareHist(colorHist, hist2, CV_COMP_BHATTACHARYYA);
    return dist;
}

// Compute the Bhattacharrya distance between
// this and another object's histograms
double Object::histDist(cv::Mat otherHist) {

  cv::normalize(colorHist, colorHist, 1, 0, cv::NORM_L1);
  cv::normalize(otherHist, otherHist, 1, 0, cv::NORM_L1);
  double dist = cv::compareHist(colorHist, otherHist, CV_COMP_BHATTACHARYYA);
  return dist;
}

// Check whether this object qualifies
// as a cyclist
void Object::pedestrianCheck(double histDiff) {
  if (cv::norm(direction) < 25 && histDiff < 0.3) {
    ++validationFrames;
  }
  if (validationFrames > VALID_THRESH) {
    isPedestrian = true;
    std::stringstream ss;
    ss << "Pedestrian ";
    ss << id;
    name = ss.str();
  }
}

void Object::str() {
  std::stringstream ss;
  ss << std::endl;
  ss << name << std::endl;
  ss << "Position: " << centroid << std::endl;
  ss << "Age: " << age << std::endl;
  ss << "Last seen: " << lastSeen << std::endl;
  ss << "Occluded: " << occluded << std::endl;
  ss << "Shift: " << cv::norm(direction) << std::endl;
  ss << "Valid frames: " << validationFrames << std::endl;
  ss << std::endl;

  std::cout << ss.str();
}
