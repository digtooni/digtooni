#include "../svmlight/svmlight.h"

/// Singleton
SVMlight* SVMlight::getInstance() {
  static SVMlight theInstance;
  return &theInstance;
}
