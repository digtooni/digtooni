#include "tracker.hpp"

#include <iostream> 

Tracker::Tracker(double dist_thresh, double hist_thresh, int obj_EOL, int valid_thresh)
    : DIST_THRESH(dist_thresh)
    , HIST_THRESH(hist_thresh)
    , VALID_THRESH(valid_thresh)
    , OBJ_EOL(obj_EOL)
    , objectCount(0)
{

#ifdef SYS_TEGRA
    bgSubtractor = new cv::gpu::MOG2_GPU(-1);
    bgSubtractor->nShadowDetection = 0;
#else
    bgSubtractor = new cv::BackgroundSubtractorMOG2(150, 9, 1);
    bgSubtractor->setInt("nShadowDetection", 0);
#endif
}

cv::Mat Tracker::getForeground() {
  return fgMask;
}

cv::Rect Tracker::trimDetectionBox(cv::Rect box, cv::Size dims) {

  int x = box.tl().x;
  int y = box.tl().y;
  int width = box.width;
  int height = box.height;

  if( box.tl().x < 0) {
    x = 0;
  }
  if (box.tl().y < 0) {
    y = 0;
  }
  if (box.tl().x + box.width > dims.width) {
    width = dims.width - box.tl().x;
  }
  if (box.tl().y + box.height > dims.height) {
    height = dims.height - box.tl().y;
  }

  cv::Rect trimmedBox(x, y, width, height);
  return trimmedBox;

}

void Tracker::subtractBackground(cv::Mat image, cv::Mat &mask) {

#ifdef SYS_TEGRA
  cv::gpu::GpuMat gpuImage;
  cv::gpu::GpuMat gpuMask;
  gpuImage.upload(image);
  bgSubtractor->operator()(gpuImage, gpuMask);
  mask = cv::Mat(gpuMask);
#else
  bgSubtractor->operator()(image, mask);
#endif

  cv::Mat erosionElement = cv::getStructuringElement(cv::MORPH_RECT, 
      cv::Size(2,2), cv::Point(0,0));

  cv::erode(mask, mask, erosionElement);
}

cv::Mat Tracker::computeColorHistogram(cv::Mat image, cv::Rect roi) {

  // crop image to region of interest
  const cv::Mat imageTile(image, roi);

  // Matrix for storing the histogram
  cv::Mat colorHist;

  int channels[] = {0, 1, 2};
  int histSize[] = {8, 8, 8};

  /// Set the ranges ( for B,G,R) )
  float range[] = {0, 255} ;
  const float* histRange[] = { range, range, range };

  // initialise the histogram matrices
  for (int i=0;i<3;i++) { 
      colorHist.push_back(cv::Mat(imageTile.size(),CV_8UC3)); 
  }

  cv::Mat mat; // TODO: change this to be a mask
  
  /// Compute the histograms:
  cv::calcHist(&imageTile, 1, channels, mat, colorHist, 3, histSize, 
          histRange, true);

  if (colorHist.size().height == 0) {
    std::cin.ignore();
  }
  return colorHist;
}

std::vector<std::shared_ptr<Object> > Tracker::track(cv::Mat image, 
    std::vector<cv::Rect> newDetections, 
    std::vector<std::shared_ptr<Object> > &exitPedestrians) {

  // Mark all objects as non-updated
  for (int i=0; i<objects.size(); ++i) {
    objects[i]->updated = false;
  }

  std::vector<std::shared_ptr<Object> > pedestrians;
  std::vector<std::shared_ptr<Object> > closeObjects;

  subtractBackground(image, fgMask);

  for (int i=0; i<newDetections.size(); ++i) {
    cv::Rect detection = trimDetectionBox(newDetections[i], image.size());
    bool objectExists = false;
    // center of the box
    cv::Point detectionCenter(newDetections[i].x + (newDetections[i].width / 2.0),
        newDetections[i].y + (newDetections[i].height / 2.0));
    // Add nearby objects to a shortlist of potential candidates
    for (int j=0; j<objects.size(); ++j) {
      double objDist = objects[j]->dist(detectionCenter);
      if (objDist < DIST_THRESH && detection.size().height > 15 && 
          detection.size().width > 15) {
        closeObjects.push_back(objects[j]);
      }
    }

    cv::Mat colorHist = computeColorHistogram(image, detection);
    cv::Mat objectMask(fgMask, detection);

    // Match new detection with the right object (if any)
    double minDist = INT_MAX;
    std::shared_ptr<Object> mostSimilar;
    for (int j=0; j<closeObjects.size(); ++j) {
      double histDist = closeObjects[j]->histDist(colorHist);
      if (histDist < minDist && histDist > HIST_THRESH && !closeObjects[j]->updated) {
        minDist = histDist;
        mostSimilar = closeObjects[j];
      }
    }

    if (mostSimilar) {
      mostSimilar->update(detection, detectionCenter, colorHist, objectMask);
      mostSimilar->pedestrianCheck(minDist);
      objectExists = true;
    }
    if (!objectExists) {
      objectCount++;
      std::shared_ptr<Object> newObject(new Object(detection, detectionCenter, objectMask, objectCount, colorHist, VALID_THRESH));
      objects.push_back(newObject);
    }
    closeObjects.clear();
  }

  for (auto it=objects.begin(); it!=objects.end();) {
    std::shared_ptr<Object> tempPtr = *it;
    if (!tempPtr->updated) {
      tempPtr->update();
      // If the object hasn't been seen in X frames, delete it
      if (tempPtr->getLastSeen() > OBJ_EOL) {
        // Store pedestrians that have exited the scene (for stats)
        if (tempPtr->getIsPedestrian()) {
          exitPedestrians.push_back(std::move(tempPtr));
        }
        it = objects.erase(it);
        continue;
      }
    }
    ++it;
  }

  for (int i=0; i<objects.size(); ++i) {
    if (objects[i]->getIsPedestrian()) {
      pedestrians.push_back(objects[i]);
    }
  }

  return pedestrians;
}

void Tracker::reset() {
  objects.clear();
  objectCount = 0;
}
