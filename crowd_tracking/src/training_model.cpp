#include "training_model.hpp"

TrainingModel::TrainingModel( const std::string &config_file )
{
    ConfigReader cfg( config_file );
    name_ = cfg.get_element( "name" );

    win_stride = cv::Size( 8, 8 );
    training_padding = cv::Size( 0, 0 );

#ifdef SYS_TEGRA
    hog.win_size.width = cfg.get_element_as_type<int>( "width" );
    hog.win_size.height = cfg.get_element_as_type<int>( "height" );
#else
    hog.winSize.width = cfg.get_element_as_type<int>( "width" );
    hog.winSize.height = cfg.get_element_as_type<int>( "height" );
#endif

    std::string positive_dirs = cfg.get_element( "positive_dirs" );
    std::string negative_dirs = cfg.get_element( "negative_dirs" );

    positive_training_data_dirs = cfg.split( positive_dirs, ',' );
    negative_training_data_dirs = cfg.split( negative_dirs, ',' );

    for ( size_t i = 0; i < positive_training_data_dirs.size(); ++i )
    {
        positive_training_data_dirs[i] = cfg.trim( positive_training_data_dirs[i] );
    }
    for ( size_t i = 0; i < negative_training_data_dirs.size(); ++i )
    {
        negative_training_data_dirs[i] = cfg.trim( negative_training_data_dirs[i] );
    }

    namespace fs = boost::filesystem;

    fs::path abs_config_file_path( config_file );
    features_file = ( abs_config_file_path.parent_path() / fs::path( "features.dat" ) ).string();
    svm_model_file = ( abs_config_file_path.parent_path() /
        fs::path( "model_file.dat" ) ).string();
    descriptor_vector_file = ( abs_config_file_path.parent_path() / 
        fs::path( "descriptor_vector.dat" ) ).string();

    valid_extensions.push_back( ".jpg" );
    valid_extensions.push_back( ".png" );
    valid_extensions.push_back( ".ppm" );
    valid_extensions.push_back( ".pgm" );
}

#ifdef SYS_TEGRA
void TrainingModel::copyHOG( cv::gpu::HOGDescriptor lhs, cv::gpu::HOGDescriptor& rhs )
{
    rhs.win_size = lhs.win_size;
    rhs.block_size = lhs.block_size;
    rhs.block_stride = lhs.block_stride;
    rhs.cell_size = lhs.cell_size;
    rhs.nbins = lhs.nbins;
    rhs.win_sigma = lhs.win_sigma;
    rhs.threshold_L2hys = lhs.threshold_L2hys;
    rhs.gamma_correction = lhs.gamma_correction;
    rhs.nlevels = lhs.nlevels;
}
#endif

TrainingModel::TrainingModel( const TrainingModel &other ) {
    win_stride = other.win_stride;
    training_padding = other.training_padding;
#ifdef SYS_TEGRA
    copyHOG( other.hog, hog );
#else
    other.hog.copyTo( hog );
#endif

    name_ = other.name_;
    features_file = other.features_file;
    descriptor_vector_file = other.descriptor_vector_file;
    svm_model_file = other.svm_model_file;

    positive_training_data_dirs = other.positive_training_data_dirs;
    negative_training_data_dirs = other.negative_training_data_dirs;
    positive_training_images = other.positive_training_images;
    negative_training_images = other.negative_training_images;
    valid_extensions = other.valid_extensions;

    descriptor_vector = other.descriptor_vector;
    number_of_samples = other.number_of_samples;
    hit_threshold = other.hit_threshold;
}


TrainingModel &TrainingModel::operator=( const TrainingModel &rhs )
{
  TrainingModel tmp( rhs );

  //exception safe
  std::swap( *this, tmp );
  //std::swap( hog_size, tmp.hog_size );
  std::swap( win_stride, tmp.win_stride );
  std::swap( win_stride, tmp.win_stride );
  std::swap( training_padding, tmp.training_padding );
  std::swap( hog, tmp.hog );
  std::swap( name_, tmp.name_ );
  std::swap( features_file, tmp.features_file );
  std::swap( descriptor_vector_file, tmp.descriptor_vector_file );
  std::swap( svm_model_file, tmp.svm_model_file );
  std::swap( positive_training_data_dirs, tmp.positive_training_data_dirs );
  std::swap( negative_training_data_dirs, tmp.negative_training_data_dirs );
  std::swap( positive_training_images, tmp.positive_training_images );
  std::swap( negative_training_images, tmp.negative_training_images );
  std::swap( valid_extensions, tmp.valid_extensions );
  std::swap( descriptor_vector, tmp.descriptor_vector );
  std::swap( number_of_samples, tmp.number_of_samples );
  std::swap( hit_threshold, tmp.hit_threshold );
  return *this;
}

void TrainingModel::setTrainingDataDirs( const std::vector<std::string> &positive_dirs, 
    const std::vector<std::string> &negative_dirs )
{
    positive_training_data_dirs = positive_dirs;
    negative_training_data_dirs = negative_dirs;
}

void TrainingModel::collectExtraNegatives( const std::string &directory )
{
    getFilesInDirectory( directory, negative_training_images, valid_extensions );
    number_of_samples = positive_training_images.size() + negative_training_images.size();
}

void TrainingModel::collectExtraNegatives( const std::vector<std::string> &directories )
{
    for (auto dir : directories)
    {
        collectExtraNegatives(dir);
    }
}

void TrainingModel::collectTrainingData()
{
    for (const auto &pos_dir : positive_training_data_dirs)
    {
        getFilesInDirectory(pos_dir, positive_training_images, valid_extensions);
    }

    for (const auto &neg_dir : negative_training_data_dirs)
    {
        getFilesInDirectory(neg_dir, negative_training_images, valid_extensions);
    }

    number_of_samples = positive_training_images.size() + negative_training_images.size();
}


void TrainingModel::collectTrainingData( const TrainingModel &other_model )
{
    for ( const auto &pos_dir : other_model.positive_training_data_dirs )
    {
        getFilesInDirectory( pos_dir, negative_training_images, valid_extensions );
    }

    number_of_samples = positive_training_images.size() + negative_training_images.size();
}

void TrainingModel::retrain()
{

    if ( svm_model_file == "" )
    {
        throw std::runtime_error( "Error, SVM file is not defined" );
    }

    TRAINHOG_SVM_TO_TRAIN::getInstance()->loadModelFromFile( svm_model_file );

    //will be features_file eventually - save SV's here and then load new data appending to this.
    std::ofstream sv_file;
    sv_file.open( features_file );

    if ( sv_file.good() && sv_file.is_open() )
    {
        //write the training data 
        TRAINHOG_SVM_TO_TRAIN::getInstance()->writeSupportVectorsToFile( sv_file );
    }

    sv_file.close(); 
    train( true );
}

void TrainingModel::generateFeaturesFromData( bool append )
{
    float percent;
    printf( "Reading files, generating HOG features and save them to file '%s':\n", features_file.c_str() );

    std::fstream file;
    if (append)
    {
        file.open(features_file.c_str(), std::ios::app);
    }
    else
    {
        file.open(features_file.c_str(), std::ios::out);
    }

    if ( file.good() && file.is_open() )
    {
        // Remove following line for libsvm which does not support comments
        // file << "# Use this file to train, 
        // e.g. SVMlight by issuing $ svm_learn -i 1 -a weights.txt " << featuresFile.c_str() << endl;
        // Iterate over sample images
        for ( unsigned long current_file = 0; current_file < number_of_samples; ++current_file )
        {
            storeCursor();
            std::vector<float> feature_vector;
            // Get positive or negative sample image file path
            const std::string current_image_file = ( current_file < positive_training_images.size() 
                ? positive_training_images.at( current_file )
                : negative_training_images.at( current_file - positive_training_images.size() ) );
            // Output progress
            if ( ( current_file + 1 ) % 10 == 0 || ( current_file + 1 ) == number_of_samples )
            {
                percent = ( ( current_file + 1 ) * 100 / number_of_samples );
                printf( "%5lu ( %3.0f%% ):\tFile '%s'", ( current_file + 1 ), percent, current_image_file.c_str() );
                fflush( stdout );
                resetCursor();
            }
      
            // Calculate feature vector from current image file
            calculateFeaturesFromInput( current_image_file, feature_vector, hog, win_stride, training_padding );
            if ( !feature_vector.empty() )
            {
                /* Put positive or negative sample class to file,
                * true=positive, false=negative,
                * and convert positive class to +1 and negative class to -1 for SVMlight
                */
                file << ( ( current_file < positive_training_images.size() ) ? "+1" : "-1" );

                // Save feature vector components
                for ( unsigned int feature = 0; feature < feature_vector.size(); ++feature )
                {
                    file << " " << ( feature + 1 ) << ":" << feature_vector.at( feature );
                }
                file << std::endl;
            }
        }
        printf( "\n" );
        file.flush();
        file.close();
    }
    else
    {
        printf( "Error opening file '%s'!\n", features_file.c_str() );
        return;
    }
}

void TrainingModel::train( bool retrain )
{

    generateFeaturesFromData( retrain );

    printf( "Calling %s\n", TRAINHOG_SVM_TO_TRAIN::getInstance()->getSVMName() );
    std::cout << "Loading " << number_of_samples << " samples.\n" << std::endl;

    TRAINHOG_SVM_TO_TRAIN::getInstance()->read_problem( const_cast<char*> ( features_file.c_str() ) );
    std::cout << "made it here" << std::endl;
    TRAINHOG_SVM_TO_TRAIN::getInstance()->train(); // Call the core libsvm training procedure
    std::cout << "and here" << std::endl;
    printf( "Training done, saving model file!\n" );
    TRAINHOG_SVM_TO_TRAIN::getInstance()->saveModelToFile( svm_model_file );

    printf( "Generating representative single HOG feature vector using svmlight!\n" );
    std::vector<unsigned int> descriptor_vector_indices;
    // Generate a single detecting feature vector ( v1 | b ) from the trained support vectors, for use e.g. with the HOG algorithm
    TRAINHOG_SVM_TO_TRAIN::getInstance()->getSingleDetectingVector( descriptor_vector, descriptor_vector_indices );
    // And save the precious to file system
    saveDescriptorVectorToFile( descriptor_vector, descriptor_vector_indices, descriptor_vector_file );

    TRAINHOG_SVM_TO_TRAIN::getInstance()->Refresh();
}

void TrainingModel::loadModel()
{
    //TRAINHOG_SVM_TO_TRAIN::getInstance()->loadModelFromFile( svm_model_file );
    std::ifstream ifs(descriptor_vector_file);

    if (!ifs.is_open())
    {
        throw std::runtime_error("Error, could not open " + descriptor_vector_file);
    }

    descriptor_vector.clear();

    while (!ifs.eof())
    {
        float x;
        ifs >> x;
        descriptor_vector.push_back(x);
    }

    std::ifstream model_file(svm_model_file);

    if (!model_file.is_open())
    {
        throw std::runtime_error("Error, could not open " + svm_model_file);
    }

    for (int i = 0; i < 10; ++i)
    {
        std::string line;
        std::getline( model_file, line );
    }
    model_file >> hit_threshold;
    hog.setSVMDetector( descriptor_vector );
}

void TrainingModel::cleanOverlaps( std::vector<cv::Rect> &detections )
{
    std::vector<cv::Rect> new_detections;

    for ( auto detection1 : detections )
    {
        bool found_overlapping = false;
        for ( auto detection2 : detections )
        {
            if ( detection1 == detection2 )
            {
                continue;
            }
            if ( detection2.contains( detection1.tl() ) && detection2.contains( detection1.br() ) )
            {
                found_overlapping = true;
                break;
            }
        }

        if ( !found_overlapping )
        {
            new_detections.push_back(detection1);
        }
    }
    detections = new_detections;
}

void TrainingModel::detect( const cv::Mat &image, std::vector<cv::Rect> &detections ,float scaling_factor=1 )
{
    cv::Size newSize( image.size().width * scaling_factor, image.size().height * scaling_factor );
    QuickImageGrid qig( scaling_factor, newSize );

    std::vector<cv::Rect> found_objects;

    // Scale the image
    cv::Mat imageScaled;
    cv::resize( image, imageScaled, cv::Size( 0, 0 ), scaling_factor, scaling_factor, cv::INTER_CUBIC );

    cv::Size padding( 0, 0 );
    cv::Size winStride( 8, 8 );
    cv::Size blockStride( 8,8 );
    cv::Mat testImageGray;

    cvtColor( imageScaled, testImageGray, CV_BGR2GRAY );

#if SYS_TEGRA
    cv::gpu::GpuMat gpuTestImageGray;
    gpuTestImageGray.upload( testImageGray );
#endif 

#if SYS_TEGRA
    hog.block_stride = blockStride;
#else
    hog.blockStride = blockStride;
#endif

    std::vector<double> confidences;
    std::vector<cv::Point> locations;

    hog.setSVMDetector( descriptor_vector );

#ifdef SYS_TEGRA
    padding = cv::Size( 0,0 );
    std::cout << "Vector size: " << descriptor_vector.size() << std::endl;
    hog.detectMultiScale( gpuTestImageGray, found_objects, hit_threshold, winStride, padding, 1.02, 1.5 );
#else
    hog.detectMultiScale( testImageGray, found_objects, hit_threshold, padding, winStride, 1.02, 1.5, true );
#endif

    for ( int i=0; i<found_objects.size(); ++i )
    {
        detections.push_back( found_objects[i] );
    }
    cleanOverlaps( detections );
    std::cout << "HOG detections: " << detections.size() << std::endl;
}
