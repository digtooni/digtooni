#include <opencv2/opencv.hpp>
#include <fstream>
#include <random>

void rotate2D(const cv::Mat & src, cv::Mat & dst, const double degrees);

struct PersonAnnotation {

  PersonAnnotation(cv::Rect bb) : bounding_box(bb) {}

  cv::Rect bounding_box;

};

class AnnotationHelper {
  
public:

  AnnotationHelper() : current_annotation_(cv::Rect(0,0,0,0)) { 
    currently_selecting_annotation_ = false; 
  }


  bool ShouldDrawRectangleWithMouseMovement() const {
    
    return currently_selecting_annotation_;
  
  };

  void CleanUp(){

    currently_selecting_annotation_ = false;
    current_annotation_ = cv::Rect(0, 0, 0, 0);
    annotations_.clear();

  }

  void GotMovement(cv::Point point){

    if (currently_selecting_annotation_){

      cv::Point tl = current_annotation_.bounding_box.tl();
      if (point.x < tl.x){
        std::swap(tl.x, point.x);
      }
      if (point.y < tl.y){
        std::swap(point.y, tl.y);
      }

      current_annotation_.bounding_box = cv::Rect(tl, point);

    }
    else{
      //do nothing
    }

  }

  void GotClick(cv::Point point){

    if (currently_selecting_annotation_){

      currently_selecting_annotation_ = false;
      cv::Point tl = current_annotation_.bounding_box.tl();
      if (point.x < tl.x){
        std::swap(tl.x, point.x);
      }
      if (point.y < tl.y){
        std::swap(point.y, tl.y);
      }

      current_annotation_.bounding_box = cv::Rect(tl, point);
      annotations_.push_back(current_annotation_);

    }
    else{

      currently_selecting_annotation_ = true;
      current_annotation_.bounding_box = cv::Rect(point, point + cv::Point(1, 1));

    }    
  }

  std::vector<PersonAnnotation> &GetAnnotations() { return annotations_; }
  const std::vector<PersonAnnotation> &GetAnnotations() const { return annotations_; }
  cv::Rect GetCurrentlyDrawnPerson() { return current_annotation_.bounding_box; }

protected:

  std::vector<PersonAnnotation> annotations_;
  PersonAnnotation current_annotation_;
  bool currently_selecting_annotation_;

};

void SelectFromUserInput(const cv::Mat &frame, const std::string &window_name, AnnotationHelper &ah);
void SelectFromOpticalFlowTracking(const cv::Mat &current_frame, const cv::Mat &previous_frame, AnnotationHelper &ah);

void SaveImageWindow(const cv::Mat &frame, const std::string &foreground_directory, const std::string &background_directory, const size_t frame_number, const AnnotationHelper &ah, const size_t number_of_negative_example, const cv::Size hog_window_size){

  std::stringstream filename_no_extension; filename_no_extension << "image_" << frame_number << "_";
  
  static const float dimension_ratio = (float)hog_window_size.width / hog_window_size.height;

  const cv::Rect frame_size(cv::Point(0, 0), frame.size());

  // create a 'mask' which is white where we have no foreground examples (i.e. people) and black where we have extracted people - this can be used to generate background examples from the same frame
  cv::Mat background_mask = cv::Mat::zeros(frame.size(), CV_8UC1);

  for (size_t i = 0; i < ah.GetAnnotations().size(); ++i){

    const PersonAnnotation &annotation = ah.GetAnnotations()[i];

    cv::Rect roi_resized = annotation.bounding_box;

    while (roi_resized.tl().x < 0) roi_resized.x++;
    while (roi_resized.tl().y < 0) roi_resized.y++;
    while (roi_resized.br().x > frame.cols) roi_resized.width--;
    while (roi_resized.br().y > frame.rows) roi_resized.height--;

    cv::Mat roi = frame(roi_resized).clone();


    ////check dimensions match
    //float new_dimension_ratio = (float)annotation.bounding_box.width / annotation.bounding_box.height;
    //float ideal_dimension_ratio = (float)hog_window_size.width / hog_window_size.height;

    //while (new_dimension_ratio > ideal_dimension_ratio) { roi_resized.width--; roi_resized.x++; new_dimension_ratio = (float)annotation.bounding_box.width / annotation.bounding_box.height; }
    //while (new_dimension_ratio < ideal_dimension_ratio) { roi_resized.width--; roi_resized.x++; new_dimension_ratio = (float)annotation.bounding_box.width / annotation.bounding_box.height; }
  
    //float width_factor = 1.0;
    //float height_factor = 1.0;
    //if (new_dimension_ratio > dimension_ratio){
    //  height_factor = (float)annotation.bounding_box.width*hog_window_size.height / (hog_window_size.width*roi_resized.height);
    //}
    //else if (new_dimension_ratio < dimension_ratio){
    //  width_factor = (float)annotation.bounding_box.height*hog_window_size.width / (hog_window_size.height*roi_resized.width);
    //}

    //int new_height = height_factor * roi_resized.height;
    //int new_width = width_factor * roi_resized.width;

    //cv::Point box_centre = cv::Point(annotation.bounding_box.x + annotation.bounding_box.width / 2, annotation.bounding_box.y + annotation.bounding_box.height / 2);
    //cv::Point new_top_left(box_centre.x - (0.5*width_factor*annotation.bounding_box.width), box_centre.y - (0.5*height_factor*annotation.bounding_box.height));
    //
    //cv::Rect new_bounding_box(new_top_left.x, new_top_left.y, width_factor*annotation.bounding_box.width, height_factor*annotation.bounding_box.height);

    //while (!frame_size.contains(new_bounding_box.tl())) {
    //  new_bounding_box.x++;
    //  new_bounding_box.y++;
    //}
    //while (!frame_size.contains(new_bounding_box.br())){
    //  new_bounding_box.width--;
    //  new_bounding_box.height--;
    //}

    //cv::Mat roi = frame(new_bounding_box).clone();

    //if (new_bounding_box.area() > hog_window_size.area()){

    //  cv::Mat roi_resized;
    //  cv::resize(roi, roi_resized, hog_window_size, 0, 0, cv::INTER_CUBIC);
    //  roi = roi_resized;

    //}
    //else{

    //  box_centre = cv::Point(new_bounding_box.x + new_bounding_box.width / 2, new_bounding_box.y + new_bounding_box.height / 2);
    //  new_top_left = cv::Point(box_centre.x - (hog_window_size.width / 2), box_centre.y - (hog_window_size.height / 2));

    //  new_bounding_box = cv::Rect(new_top_left.x, new_top_left.y, hog_window_size.width, hog_window_size.height);
    //  roi = frame(new_bounding_box).clone();

    //}

    
    cv::Mat background_roi = background_mask(roi_resized);
    background_roi.setTo(255);

    std::stringstream ss; ss << filename_no_extension.str() << i;
    cv::imwrite(foreground_directory + "/" + ss.str() + ".png", roi);

  }

  return;

  //random number generator stuff
  static std::random_device rd;
  static std::mt19937 rng(rd());
  
  static std::uniform_int_distribution<std::mt19937::result_type> row_distribution(0, frame.rows - hog_window_size.height);
  static std::uniform_int_distribution<std::mt19937::result_type> col_distribution(0, frame.cols - hog_window_size.width);
  static std::uniform_int_distribution<std::mt19937::result_type> scale_distribution(15, 25);

  const cv::Rect image_dims(cv::Point(0, 0), frame.size());

  for (size_t i = 0; i < number_of_negative_example; ){

    const int random_row_start = row_distribution(rng);
    const int random_col_start = col_distribution(rng);
    const float scale = (float)scale_distribution(rng) / 10;

    const cv::Rect window(random_col_start, random_row_start, (int)(scale*hog_window_size.width), (int)(scale*hog_window_size.height));

    if (!image_dims.contains(window.tl()) || !image_dims.contains(window.br())) continue;

    const cv::Mat is_overlapping_positive = background_mask(window);
    if (cv::countNonZero(is_overlapping_positive) > 0) continue;

    cv::Mat roi = frame(window).clone();
    
    std::stringstream ss; ss << filename_no_extension.str() << i;
    cv::imwrite(background_directory + "/" + ss.str() + ".png", roi);

    //do this at the end so we don't count cases where we skip due to a bad region being selected
    ++i;

  }

} 

static void MouseCallback(int event, int x, int y, int, void *ah_){

  AnnotationHelper *ah = (AnnotationHelper *)ah_;

  if (event == cv::EVENT_MOUSEMOVE){

    ah->GotMovement(cv::Point(x, y));
    return;

  }

  else if (event == cv::EVENT_LBUTTONDOWN){

    ah->GotClick(cv::Point(x, y));
    return;

  }

}

int main(int argc, const char **argv){

  cv::CommandLineParser parser(argc, argv, 
    "{ v | video-file |  | path to the input video file }" 
    "{ p | positive-examples-path |  | path to the directory where the positive samples will be saved }"
    "{ n | negative-examples-path |  | path to the directory where the negative samples will be saved }"
    "{ t | skip-seconds | 0 | number of seconds of video to skip from the start }"
    "{ w | target-width | 150 | width of the extracted template in pixels }"
    "{ h | target-height | 120 | height of the extracted template in pixels }"
    "{ s | skip-frames | 0 | number of frames to skip }");


  if (parser.get<bool>("help") || parser.get<std::string>("video-file").length() == 0){
    std::cout << "Usage : trainer [options]" << std::endl;
    std::cout << "Available options:" << std::endl;
    parser.printParams();
    return 1;
  }
    
  std::cout << "training data generator\n-------------------------------\n\nFor each frame of video, click, drag out a rectangle and click again to make bounding boxes around the targets.\n\nThe click-drag-click process should be from top left to bottom right.\nIf you accidentally start creating a bounding box in the wrong location, you can drag the box to a new start point before the second click.\n";
  std::cout << "\nControls\n--------\n\n\tq : quit the program\n\tc : clear the selections for a given frame\n\t'space' : go to the next frame\n" << std::endl;
 

  std::cout << "WARNING... NOT SAVING NEGATIVE DATA.\n";

  const std::string video_file(parser.get<std::string>("video-file"));
  const std::string positive_examples(parser.get<std::string>("positive-examples-path"));
  const std::string negative_examples(parser.get<std::string>("negative-examples-path"));
  const size_t skip_seconds = parser.get<size_t>("skip-seconds");
  const size_t skip_frames = parser.get<size_t>("skip-frames");
  const size_t target_width = parser.get<size_t>("target-width");
  const size_t target_height = parser.get<size_t>("target-height");

  const std::string window_name("Training Data Generator");

  cv::VideoCapture cap(video_file);
  if (!cap.isOpened()){
    std::cerr << "Error, video file " + video_file + " couldn't open.\n";
    return 1;
  }


  size_t num_frames_to_skip;
  if (skip_frames == 0 && skip_seconds > 0)
    num_frames_to_skip = skip_seconds * (size_t)cap.get(CV_CAP_PROP_FPS);
  else{
    num_frames_to_skip = skip_frames;
  }

  cv::namedWindow(window_name);
  AnnotationHelper ah;
  cv::setMouseCallback(window_name, MouseCallback, &ah);
  cv::Mat previous_frame;
  
  cv::Mat affine_transform = cv::Mat::zeros(2, 3, CV_32FC1);
  const float angle = 0.25*3.141592;
  affine_transform.at<float>(0, 0) = cos(angle);
  affine_transform.at<float>(0, 1) = -sin(angle);
  affine_transform.at<float>(1, 0) = sin(angle);
  affine_transform.at<float>(1, 1) = cos(angle);
  affine_transform.at<float>(0, 2) = 0;
  affine_transform.at<float>(1, 2) = 0;
    

  for(size_t frame_num = 0; ; ++frame_num){

    cv::Mat frame;
    cap >> frame;
    if (frame.empty()) {
      std::cout << "finishing reading video frames.\n";
      return 0;
    }

    if ((frame_num) < num_frames_to_skip) continue;


    //rotate2D(frame, frame, -35);


    if (frame_num % 10 && frame_num > 0){

      //SelectFromOpticalFlowTracking(frame, previous_frame, ah);
      //SaveImageWindow(frame, positive_examples, negative_examples, frame_num, ah, 2000, cv::Size(64, 128));

    }
    else{

      ah.CleanUp();
      SelectFromUserInput(frame, window_name, ah);
      SaveImageWindow(frame, positive_examples, negative_examples, frame_num, ah, 100, cv::Size(target_width, target_height));

    }
    
    
    previous_frame = frame;

  }

  return 0;

}


void SelectFromUserInput(const cv::Mat &frame, const std::string &window_name, AnnotationHelper &ah){

  while (1){

    int key = cv::waitKey(5);
    if (key == ' ') break;
    if (key == 'c') {
      std::cout << "clearing values for this window!" << std::endl;
      ah.CleanUp();
    }
    if (key == 'q'){
      exit(0);
    }

    cv::Mat draw_on_frame = frame.clone();

    for (const auto &annotation : ah.GetAnnotations()){
      cv::rectangle(draw_on_frame, annotation.bounding_box, cv::Scalar(255, 0, 0), 2);
    }

    if (ah.ShouldDrawRectangleWithMouseMovement()){
      cv::rectangle(draw_on_frame, ah.GetCurrentlyDrawnPerson(), cv::Scalar(255, 0, 255), 2);
    }

    cv::imshow(window_name, draw_on_frame);

  }
}


void SelectFromOpticalFlowTracking(const cv::Mat &current_frame, const cv::Mat &previous_frame, AnnotationHelper &ah){

  if (previous_frame.empty()){
    throw std::runtime_error("Error! Previous frame should never be empty!\n");
  }

  cv::Mat current_frame_gray;
  cv::Mat previous_frame_gray;

  cv::cvtColor(previous_frame, previous_frame_gray, CV_BGR2GRAY);
  cv::cvtColor(current_frame, current_frame_gray, CV_BGR2GRAY);

  for (const auto annotation : ah.GetAnnotations()){

    cv::Mat mask = cv::Mat::zeros(current_frame.size(), CV_8UC1);
    cv::Mat region = mask(annotation.bounding_box);
    region.setTo(cv::Scalar(255));

    if (cv::sum(region)[0] == 0) continue; //sanity check

    std::vector<unsigned char> status;
    std::vector<float> err;
    std::vector<cv::Point2f> points[2];
    cv::TermCriteria termination_critera(CV_TERMCRIT_EPS | CV_TERMCRIT_ITER, 20, 0.03);
    cv::Size window_size(annotation.bounding_box.width, annotation.bounding_box.height);

    points[0].push_back(cv::Point(annotation.bounding_box.tl().x + (annotation.bounding_box.width / 2), (annotation.bounding_box.tl().y + (annotation.bounding_box.height / 2))));

    cv::calcOpticalFlowPyrLK(previous_frame_gray, current_frame_gray, points[0], points[1], status, err, window_size, 3, termination_critera, 0, 0.001);

    cv::Rect r = annotation.bounding_box;
    r.x = points[1][0].x - (annotation.bounding_box.width / 2);
    r.y = points[1][0].y - (annotation.bounding_box.height / 2);

    cv::Mat f = current_frame.clone();
    cv::rectangle(f, r, cv::Scalar(255, 0, 0));
    cv::rectangle(f, annotation.bounding_box, cv::Scalar(0, 0, 255));

  }

}



void rotate2D(const cv::Mat & src, cv::Mat & dst, const double degrees){
  cv::Mat frame, frameRotated;

  int diagonal = (int)sqrt(src.cols * src.cols + src.rows * src.rows);
  int newWidth = diagonal;
  int newHeight = diagonal;

  int offsetX = (newWidth - src.cols) / 2;
  int offsetY = (newHeight - src.rows) / 2;
  cv::Mat targetMat(newWidth, newHeight, src.type(), cv::Scalar(0));
  cv::Point2f src_center(targetMat.cols / 2.0f, targetMat.rows / 2.0f);

  src.copyTo(frame);

  frame.copyTo(targetMat.rowRange(offsetY, offsetY +
    frame.rows).colRange(offsetX, offsetX + frame.cols));
  cv::Mat rot_mat = cv::getRotationMatrix2D(src_center, degrees, 1.0);
  cv::warpAffine(targetMat, frameRotated, rot_mat, targetMat.size());

  cv::Rect bound_Rect(frame.cols, frame.rows, 0, 0);
  int x1 = offsetX;
  int x2 = offsetX + frame.cols;
  int x3 = offsetX;
  int x4 = offsetX + frame.cols;
  int y1 = offsetY;
  int y2 = offsetY;
  int y3 = offsetY + frame.rows;
  int y4 = offsetY + frame.rows;
  cv::Mat co_Ordinate = (cv::Mat_<double>(3, 4) << x1, x2, x3, x4,
    y1, y2, y3, y4,
    1, 1, 1, 1);

  cv::Mat RotCo_Ordinate = rot_mat * co_Ordinate;

  for (int i = 0; i < 4; ++i) {
    if (RotCo_Ordinate.at<double>(0, i) < bound_Rect.x)
      bound_Rect.x = (int)RotCo_Ordinate.at<double>(0, i);
    if (RotCo_Ordinate.at<double>(1, i) < bound_Rect.y)
      bound_Rect.y = RotCo_Ordinate.at<double>(1, i);
  }

  for (int i = 0; i < 4; ++i) {
    if (RotCo_Ordinate.at<double>(0, i) > bound_Rect.width)
      bound_Rect.width = (int)RotCo_Ordinate.at<double>(0, i);
    if (RotCo_Ordinate.at<double>(1, i) > bound_Rect.height)
      bound_Rect.height = RotCo_Ordinate.at<double>(1, i);
  }

  bound_Rect.width = bound_Rect.width - bound_Rect.x;
  bound_Rect.height = bound_Rect.height - bound_Rect.y;

  if (bound_Rect.x < 0)
    bound_Rect.x = 0;
  if (bound_Rect.y < 0)
    bound_Rect.y = 0;
  if (bound_Rect.width > frameRotated.cols)
    bound_Rect.width = frameRotated.cols;
  if (bound_Rect.height > frameRotated.rows)
    bound_Rect.height = frameRotated.rows;

  cv::Mat ROI = frameRotated(bound_Rect);
  ROI.copyTo(dst);
}
