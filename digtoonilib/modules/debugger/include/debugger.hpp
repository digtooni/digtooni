#ifndef DEBUGGER_HPP
#define DEBUGGER_HPP

#include <iostream>
#include <string>

#include "opencv2/highgui/highgui.hpp"

namespace digtooni
{
    class Debug
    {
        public:

            Debug() {}
            Debug( std::string file_path, bool display = false, bool verbosity = false,
                bool show_fps = false );

            static void cvVersion();

        private:

            cv::Mat image;
            cv::VideoCapture video;
            bool is_image;
            bool is_video;

            
    };
}


#endif
