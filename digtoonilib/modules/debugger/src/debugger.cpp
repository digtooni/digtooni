#include "debugger.hpp"

namespace digtooni
{
    Debug::Debug( std::string file_path, bool display_images , bool verbosity ,
        bool show_fps )
    {
        if ( ( file_path.find( ".jpg" ) != std::string::npos ) ||
             ( file_path.find( ".png" ) != std::string::npos ) )
        {
            image = cv::imread( file_path );
            is_image = true;
        }
        else if ( ( file_path.find( ".mp4" ) != std::string::npos ) ||
                  ( file_path.find( ".avi" ) != std::string::npos ) )
        {
            video.open( file_path );
            is_video = true;
        }
        else
        {
            if ( file_path.find( "." ) == std::string::npos )
            {
                std::cout << "Error: unable to open file. File has no " <<
                    "extension!" << std::endl;
                std::cin.ignore();
                throw std::exception();
            }
            else
            {
                std::size_t dot_pos = file_path.find_last_of( "." );
                std::cout << "Error: " << file_path.substr( dot_pos ) <<
                    " is not a valid file extension!" << std::endl;
                std::cin.ignore();
                throw std::exception();
            }
        }

        if ( display_images )
        {
            cv::namedWindow( "Digtooni Debug" );
        }
    }

    void Debug::cvVersion()
    {
        std::cout << "OpenCV version: " << CV_MAJOR_VERSION << "." << CV_MINOR_VERSION << "." << CV_SUBMINOR_VERSION << std::endl;
    }
}
