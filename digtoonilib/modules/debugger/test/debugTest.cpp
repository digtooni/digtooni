#define BOOST_TEST_MODULE debug_test //must be defined before #include unit_test

#include <boost/test/unit_test.hpp>

#include "debugger.hpp"

#ifdef WIN32
#include "../../stdafx.h"
#endif



namespace digtooni
{
    BOOST_AUTO_TEST_CASE( debug_test_1 )
    {
        digtooni::Debug d("a bad path");
        BOOST_CHECK_THROW( d, std::exception );
        d.cvVersion();
    }
}