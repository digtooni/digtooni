#ifndef _LOGGER_HPP_
#define _LOGGER_HPP_

#ifdef _WIN32
#define DllExport   __declspec( dllexport )
#else
#define DllExport
#endif

/**

1) On launch should create a directory where everything will be stored.

2) Overloaded Log(...) method that accepts cv::Mat

3) Can maintain multiple timers (for outer loops and inner loops)

4) 

*/

#include <boost/filesystem.hpp>

#include "timer.hpp"

namespace digtooni {
    class DllExport Logger {

    public:

        explicit Logger(const std::string &path);

        virtual ~Logger();

        void CreateTimer(const std::string &name) const;

        void StartTimer(const std::string &name) const;

        void StopTimer(const std::string &name) const;

    protected:

      boost::filesystem::path save_dir;

 

    };
}


#endif