#ifndef _TIMER_HPP_
#define _TIMER_HPP_

#include <memory>
#include <vector>
#include <string>
#include <chrono>
#include <map>

#ifdef _WIN32
#define DllExport   __declspec( dllexport )
#else
#define DllExport
#endif

namespace digtooni {

    class DllExport Timer {

    public:

        typedef std::chrono::milliseconds Interval;

        void Start();

        void Stop();

        friend class TimerManager;

        size_t GetNumberOfRuns() const { return loop_durations_.size(); }
        float GetAverageRunTime() const;
        float GetStdDevRunTime() const;

    protected:

        Timer(const std::string &name) : name_(name), timing_(false) {}
        Timer(const Timer &rhs) : name_(rhs.name_) {}
        Timer &operator=(const Timer &rhs) { return *this; }

        bool timing_;
        const std::string name_; /**!<  */

        std::chrono::time_point<std::chrono::high_resolution_clock> clock_;
        std::vector< std::chrono::duration<float> > loop_durations_;

    };


    class DllExport TimerManager {

    public:

        static TimerManager &Instance();
        
        Timer *CreateTimer(const std::string &timer_name);
        
        void StartTimer(const std::string &timer_name);
        
        void StopTimer(const std::string &timer_name);

        void WriteLog(std::string &file_name) const ;

        ~TimerManager();

    private:

        TimerManager() {}
        
        TimerManager(const TimerManager &rhs){}
        TimerManager &operator=(const TimerManager &tm) {
            return *this;
        }

        typedef std::map<std::string, Timer *> TimerMap;
        TimerMap timers_;

        static std::shared_ptr<TimerManager> singleton_;

    };

}
#endif