#ifndef _VALIDATE_HPP_
#define _VALIDATE_HPP_

#include <string>
#include <fstream>
#include <vector>
#include <queue>

#include "opencv2/opencv.hpp"

#ifdef _WIN32
#define DllExport   __declspec( dllexport )
#else
#define DllExport
#endif

namespace digtooni {

  class DllExport Report {




  };

  class DllExport Validate {

  public:

    explicit Validate(const std::string &ground_truth_file) : ground_truth_file_(ground_truth_file) {}

  protected:

    virtual void LoadGroundTruth() = 0;

    std::ifstream ground_truth_file_;

  };


  class DllExport BoundingBoxValidate : public Validate {

  public:

    typedef cv::Rect MeasurementType;

    typedef std::vector<MeasurementType> GroundTruthType;

    explicit BoundingBoxValidate(const std::string &ground_truth_file);

    void AddResultsFromFrame(const GroundTruthType &bounding_box);

    void CreateGroundTruth(const std::vector<digtooni::BoundingBoxValidate::GroundTruthType> &ground_truth, const std::string &output_file_path) const;

    void ComputeResultsForFrame(const GroundTruthType &results_for_frame);

  protected:

    float ComputeOverlap(const MeasurementType &a, const MeasurementType &b) const;

    virtual void LoadGroundTruth() override;

    GroundTruthType GetGroundTruthForCurrentFrame() ;
    
    std::queue< GroundTruthType > sequence_ground_truth;
    
    float overlap_threshold;
    std::vector<double> accuracy_per_frame;
    std::vector<double> precision_per_frame;

    const std::string RootNodeName;
    const std::string ItemName;

  };



 

}

#endif