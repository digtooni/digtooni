#include "logger.hpp"

#include <iostream>
#include <sstream>

digtooni::Logger::Logger(const std::string &path){

  size_t cnt = 0;
  while (save_dir == "" || boost::filesystem::exists(save_dir)){

    std::stringstream ss; 
    ss << "run" << cnt++;
    save_dir = boost::filesystem::path(path) / boost::filesystem::path(ss.str());
    
  }

  boost::filesystem::create_directories(save_dir);

}
digtooni::Logger::~Logger(){

  try{
    TimerManager::Instance().WriteLog(save_dir.string() + "/timings.txt");
  }
  catch (std::exception & e){
    std::cerr << "Error writing timer logs: " << e.what() << "\n";
  }
  

}

void digtooni::Logger::CreateTimer(const std::string &name) const {

  TimerManager::Instance().CreateTimer(name);

}

void digtooni::Logger::StartTimer(const std::string &name) const {

  TimerManager::Instance().StartTimer(name);

}


void digtooni::Logger::StopTimer(const std::string &name) const {

  TimerManager::Instance().StopTimer(name);

}