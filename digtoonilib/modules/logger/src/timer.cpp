#include "timer.hpp"
#include <iostream>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>


std::shared_ptr<digtooni::TimerManager> digtooni::TimerManager::singleton_;

void digtooni::Timer::Stop() {

    timing_ = false;

    //get duration
    auto duration = (std::chrono::high_resolution_clock::now() - clock_);
    loop_durations_.push_back(duration);

}

float digtooni::Timer::GetAverageRunTime() const{

  if (loop_durations_.size() == 0) return 0.0f;

  float average = 0.0f;
  for (auto &v : loop_durations_){
    //average = average + v;
  }

  return average / loop_durations_.size();

}



float digtooni::Timer::GetStdDevRunTime() const{

  if (loop_durations_.size() == 0) return 0.0f;

  float average = GetAverageRunTime();

  float dev = 0.0f;
  for (auto &v : loop_durations_){
    //dev = dev + ((v - average) * (v - average));
  }

  return std::sqrt(average / loop_durations_.size());



}

void digtooni::Timer::Start() {

    timing_ = true;
    clock_ = std::chrono::high_resolution_clock::now();

}

digtooni::TimerManager &digtooni::TimerManager::Instance() {
    if (!singleton_){
        singleton_.reset(new TimerManager);
    }
    return *singleton_;
}


digtooni::Timer *digtooni::TimerManager::CreateTimer(const std::string &timer_name){

    if (timers_.find(timer_name) == timers_.end()){
        timers_.insert(TimerMap::value_type(timer_name, new Timer(timer_name)));
    }

    return timers_[timer_name];

}

void digtooni::TimerManager::StartTimer(const std::string &timer_name){

  if (timers_.find(timer_name) == timers_.end()){
    std::cerr << "Error, invalid timer name: " << timer_name << "\n";
    return;
  }
  
  timers_[timer_name]->Start();

}

void digtooni::TimerManager::StopTimer(const std::string &timer_name){

  if (timers_.find(timer_name) == timers_.end()){
    std::cerr << "Error, invalid timer name: " << timer_name << "\n";
    return;
  }

  timers_[timer_name]->Stop();

}

void digtooni::TimerManager::WriteLog(std::string &file_name) const {

  using boost::property_tree::ptree;

  ptree pt;

  for (auto timer : timers_){

    std::string node_name = "TimerData." + timer.first;

    pt.put(node_name + ".NumberOfRuns", timer.second->GetNumberOfRuns());
    pt.put(node_name + ".NumberOfRuns", timer.second->GetNumberOfRuns());
    pt.put(node_name + ".NumberOfRuns", timer.second->GetNumberOfRuns());

    //boost::property_tree::xml_writer_settings<char> settings('\t', 1);
    

  }

  boost::property_tree::write_xml(file_name, pt, std::locale());

}


digtooni::TimerManager::~TimerManager(){

    for (auto i : timers_){
        delete i.second;
        i.second = nullptr;
    }


}
