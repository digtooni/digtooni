#include "validate.hpp"

#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/foreach.hpp>

digtooni::BoundingBoxValidate::BoundingBoxValidate(const std::string &ground_truth_file) : Validate(ground_truth_file), overlap_threshold(0.8f), RootNodeName("Frame"), ItemName("BoundingBox"){
 
  LoadGroundTruth();

}

void digtooni::BoundingBoxValidate::AddResultsFromFrame(const std::vector<cv::Rect> &bounding_boxes){

  GroundTruthType current_frame_results = GetGroundTruthForCurrentFrame();


}

digtooni::BoundingBoxValidate::GroundTruthType digtooni::BoundingBoxValidate::GetGroundTruthForCurrentFrame() {


  GroundTruthType ret = sequence_ground_truth.front(); //copy
  sequence_ground_truth.pop();
  return ret;


}

float digtooni::BoundingBoxValidate::ComputeOverlap(const digtooni::BoundingBoxValidate::MeasurementType &a, const digtooni::BoundingBoxValidate::MeasurementType &b) const {

  //compute sorensen-dice score

  uint32_t count_a = 0;
  for (int r = a.tl().y; r < a.br().y; ++r){
    for (int c = a.tl().x; c < a.br().x; ++c){
    
      if (b.contains(cv::Point(c, r))){
        ++count_a;
      }

    }
  }

  return (2.0f * count_a) / (a.area() + b.area());
  
}

void digtooni::BoundingBoxValidate::ComputeResultsForFrame(const digtooni::BoundingBoxValidate::GroundTruthType &results_for_frame){

  GroundTruthType ground_truth = GetGroundTruthForCurrentFrame();

  for (auto &gt_value : ground_truth){

    //check each for overlap with results (set up recall)
    float max_overlap = 0.0f;
    for (auto &res_value : results_for_frame){

      float overlap = ComputeOverlap(gt_value, res_value);
      if (overlap > max_overlap){


      }

    }

  }

  for (auto &res_value : results_for_frame){

    //check each for overlap with results (set up precision)
    for (auto &gt_value : ground_truth){

    }
  }

}


void digtooni::BoundingBoxValidate::CreateGroundTruth(const std::vector<digtooni::BoundingBoxValidate::GroundTruthType> &ground_truth, const std::string &output_file_path) const{

  using boost::property_tree::ptree;
  ptree pt;

  size_t frame_num = 0;
  BOOST_FOREACH(GroundTruthType gt, ground_truth){

    std::stringstream root_ss;
     
    root_ss << RootNodeName << frame_num;

    for (size_t idx = 0; idx < gt.size(); ++idx){

      auto &gt_element = gt[idx];

      std::stringstream item_ss; 
      item_ss << ItemName << idx;

      ptree &node = pt.add(root_ss.str() + "." + item_ss.str(), "");

      node.put("top-left-x", gt_element.tl().x);
      node.put("top-left-y", gt_element.tl().y);
      node.put("width", gt_element.width);
      node.put("height", gt_element.height);

    }

    frame_num++;

  }

  boost::property_tree::xml_writer_settings<char> settings(' ', 4);
  std::ofstream ofs(output_file_path);
  boost::property_tree::write_xml(ofs, pt, settings);

}


void digtooni::BoundingBoxValidate::LoadGroundTruth(){

  using boost::property_tree::ptree;
  ptree pt;
  read_xml(ground_truth_file_, pt);

  for (ptree::value_type const &frames : pt){

    GroundTruthType t;

    for (ptree::value_type const &bb : frames.second){

      t.push_back(cv::Rect(bb.second.get<size_t>("top-left-x"), bb.second.get<size_t>("top-left-y"), bb.second.get<size_t>("width"), bb.second.get<size_t>("height")));
      
    }

    sequence_ground_truth.push(t);

  }

}
