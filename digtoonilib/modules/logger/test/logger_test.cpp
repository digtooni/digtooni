#include "logger.hpp"
#include "validate.hpp"

int main(int argc, char **argv){

  /*
  
  Fake dataset contains 5 frames. Each frame contains a few measurements (bounding boxes) as ground truth
  
  
  
  */

  digtooni::Logger l("sometestpath");

  digtooni::BoundingBoxValidate bbv("output.txt");

  std::vector< digtooni::BoundingBoxValidate::GroundTruthType > res;

  digtooni::BoundingBoxValidate::GroundTruthType v1;
  v1.push_back(cv::Rect(10, 15, 12, 5));
  v1.push_back(cv::Rect(42, 110, 13, 81));
  v1.push_back(cv::Rect(341, 23, 7, 12));
  v1.push_back(cv::Rect(431, 231, 23, 14));

  digtooni::BoundingBoxValidate::GroundTruthType v2;
  v2.push_back(cv::Rect(110, 25, 15, 15));
  v2.push_back(cv::Rect(64, 54, 3, 22));
  v2.push_back(cv::Rect(311, 43, 2, 42));
  v2.push_back(cv::Rect(42, 22, 24, 44));

  res.push_back(v1);
  res.push_back(v2);

  bbv.CreateGroundTruth(res, "output2.txt");

  digtooni::BoundingBoxValidate bbv2("output.txt");



  return 0;
}