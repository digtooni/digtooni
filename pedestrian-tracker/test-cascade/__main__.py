import argparse

METHOD = "USING NO SIZE FILTER ON DETECTIONS. ADDING OUTER PAVEMENTS TO ROAD REGIONS"

import cv2
from classifier import Classifier
from backgrd_sub import BackgroundSubtractor
from tracker import Tracker
from utils import DEBUG, draw_rectangle, combine_frames

arg_parser = argparse.ArgumentParser("Run validation on Manchester Data.\n")

#arg_parser.add_argument("--root-dir", type=str, help="Root directory on HD where Manchester data is stored.", required=True)
arg_parser.add_argument("--video-file",  type=str, help="Full path to video file to process.", required=True)
arg_parser.add_argument("--mask-file", type=str, help="Full path to road mask.", required=True)
arg_parser.add_argument("--road-file", type=str, help="Full path to road segment file.", required=True)
arg_parser.add_argument("--pavement-file", type=str, help="Full path to center pavement file.", required=True)
arg_parser.add_argument("--classifier-file", type=str, help="Full path to classifier file.")

arg_parser.add_argument("--output-dir", type=str, help="Full path to output directory to save data about this video if saving is required.")

args = arg_parser.parse_args()

#setup

mask = cv2.imread(args.mask_file,0)
input_video = cv2.VideoCapture(args.video_file)
road_segments = cv2.imread(args.road_file,0)
pavement_file = cv2.imread(args.pavement_file,0)

try:
  classifier = Classifier(args.classifier_file, 3)
except:
  classifier = None
  
bgs = BackgroundSubtractor()
track = Tracker(road_segments, pavement_file, args.output_dir)

with open(args.output_dir + "/method.txt","w") as f:
  f.write(METHOD)


if args.output_dir:
  writer = cv2.VideoWriter(args.output_dir + "/output_video.avi", cv2.cv.FOURCC(*"XVID"), 25, (int(input_video.get(cv2.cv.CV_CAP_PROP_FRAME_WIDTH)), int(input_video.get(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT))))

#run processing
cv2.namedWindow("output")

while True:

  frame = input_video.read()
  
  if frame[0] is False:
    break
    
  frame = frame[1]
  
  #if classifier is not None
  #  classifier.classify_image(frame, mask)
  #  classifier.draw_detections(frame)
    
  background_sub_detections = bgs.detect_people_from_image(frame, mask)

  if DEBUG:
    for detection in background_sub_detections:
      draw_rectangle(frame, detection, False, (255,127,0))

  track.brute_force_match_detections(frame, background_sub_detections)
   
  track.draw_tracked_people(frame)
   
  track.clean_up()
   
  cv2.putText(frame, "Count: {0}".format(Tracker.person_count), (10,20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255,0,0))

  if DEBUG:
    viewer_frame = combine_frames( frame, bgs.filtered_background )
  else:
    viewer_frame = frame

  cv2.imshow("output", viewer_frame)
        
  key = cv2.waitKey(5)

  if key & 255 == ord("q"):
    print "Goodbye"
    break
   

  if key & 255 == ord(" "):
    while True:
        key = cv2.waitKey(5)
        if key & 255 == ord(" "):
            break

  writer.write(frame)
