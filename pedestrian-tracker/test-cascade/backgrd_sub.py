import cv2
import numpy as np

class BackgroundSubtractor:

  def __init__(self):
  
    #self.bg = cv2.BackgroundSubtractorMOG2(500,5,True)
    self.bg = cv2.BackgroundSubtractorMOG()
    #print self.bg.bShadowDetection
    
  def detect_people_from_image(self, image, mask=None):
  
    blur_im = cv2.blur(image, (3, 3))

    fgmask = self.bg.apply(blur_im, learningRate=0.005)
  
    fgmask = cv2.blur(fgmask, (4, 4))
  
    fgmask = cv2.erode(fgmask, (3,3))
  
    fgmask = cv2.blur(fgmask, (4, 4))
  
    fgmask = cv2.dilate(fgmask, (25,25))
  
    fgmask = cv2.blur(fgmask, (4, 4))
  
    (_,fgmask) = cv2.threshold(fgmask, 127, 255, cv2.THRESH_BINARY)
  
    contours = self.find_blobs(fgmask)

    self.filtered_background = fgmask.copy()

    contours = self.filter_contours(contours)
  
    contours = self.filter_detections(contours, fgmask, mask)
  
    good_detections = []
    bad_detections = []
  
    for (x,y,w,h) in contours:
      if (w * 1.2) < h:
        good_detections.append( (x,y,w,h) )
      else:
        bad_detections.append( (x,y,w,h) )
  
    #return good_detections, bad_detections, fgmask
    return good_detections

    
  def find_blobs(self, foreground_mask):
  
    f = foreground_mask.copy()
    c = cv2.Canny(foreground_mask, 20 , 60)
    (contours,_) = cv2.findContours(c, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

    return contours
    
  def filter_contours(self, contours):
  
    short_contours = []

    for contour in contours:

      if len(contour) > 20 and len(contour) < 300:
        short_contours.append(cv2.boundingRect(contour))

    return short_contours
    

  def filter_detections(self, contours, bg_mask, frame_mask):

    return contours

    if frame_mask is None:
      return contours
      
    filtered_contours = []

    for (x, y, w, h) in contours:

      sub_mask = bg_mask[y:y+h,x:x+w]

      if np.sum(sub_mask)/255 > (0.3 * w * h):
        filtered_contours.append((x,y,w,h))

    return filtered_contours