import cv2
import os


class Classifier:
  
  def __init__(self, classifier_file, image_scale):
  
    if not os.path.exists(classifier_file):
      raise Exception("Error, could not find classifier file {0}".format(classifier_file))
    
    self.classifier = cv2.CascadeClassifier(classifier_file)
    self.image_scale = image_scale
    

  def classify_image(self, image, mask=None):
    
    large_image = cv2.resize(image, (0,0), fx = self.scale, fy = self.scale)
    if mask is not None:
      large_mask = cv2.resize(mask, (0,0), fx = self.scale, fy = self.scale)
    
    gray = cv2.cvtColor(large_image, cv2.COLOR_BGR2GRAY)
    gray = cv2.equalizeHist(gray)
    
        
    detections = detector.detectMultiScale(gray, scaleFactor=1.05, minNeighbors=0, minSize=(10*scale,20*scale), maxSize=(40*scale,80*scale), flags = cv2.cv.CV_HAAR_SCALE_IMAGE)
    
    #scale down detections
    detections = [( detection[0]/self.scale, detection[1]/self.scale, detection[2]/self.scale, detection[3]/self.scale) for detection in detections]
        
    if mask is None:
      return detections
    
      
    filtered_detections = []
    
    for (x, y, w, h) in detections:
      sub_mask = mask[y:y+h,x:x+w]
          
      if np.sum(sub_mask)/255 > 0:
        filtered_detections.append((x,y,w,h))
        
    return filtered_detections
    
  def draw_detections(self, image):
    
     for (x, y, w, h) in detections:
      cv2.rectangle(im, (x, y), (x+w, y+h), (0, 255, 0), 2)
    
      