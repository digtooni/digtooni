#hard coded - BAD
#the intensity values used to mark out each road region
ROAD_REGIONS = [26, 51, 77, 102, 128, 153, 179, 204]

import random
from utils import get_center, get_distance, draw_rectangle
import cv2
import os
from utils import DEBUG

class TrackedPerson:

  def __init__(self, initial_square):

    self.detections = []
    self.detections.append(initial_square)
    self.color = (random.randint(0,255), random.randint(0,255), random.randint(0,255))
    self.number_times_drawn = 0

  def get_last_detection(self):

    for detection in reversed(self.detections):
      if detection != [-1,-1]:
        return detection
    return [-1,-1]

  def detected_recently(self):

    history_length = 12

    if len(self.detections) < history_length:
      return True

    if self.detections[-history_length:].count([-1,-1]) == history_length:
      return False
    else:
      return True


  def draw_trajectory(self, frame):

    good_trajectory_values = []
    #good_detection_values = []

    for i in range(len(self.detections)):
      if i == 0:
        continue
      detection_1 = self.detections[i]
      if detection_1 == [-1,-1]:
        continue
      k = i-1
      detection_2 = [-1,-1]
      while k > 0:
        detection_2 = self.detections[k]
        if detection_2 != [-1,-1]:
          break
        k = k - 1

      if detection_2 == [-1, -1]:
        continue

      good_trajectory_values.append( (get_center(detection_1), get_center(detection_2)) )

    

    scores = [False]*len(ROAD_REGIONS)

    for (start,end) in good_trajectory_values:

      val = Tracker.road_segments[start[1],start[0]]
      try:
        idx = ROAD_REGIONS.index(val)
      except ValueError as e:
        if val == 0:
          continue
        else:
          print val
          print start

          raise e

      scores[idx] = True

    if DEBUG:
          for (start,end) in good_trajectory_values:
            cv2.line(frame, start, end, (0, 0, 255), 2)

    if sum(scores) > 2 or (sum(scores) == 2 and self.walked_on_pavement()):

        if self.number_times_drawn < 5:
          draw_rectangle(frame, self.get_last_detection())

          if self.number_times_drawn == 0:
            Tracker.person_count += 1
            cv2.imwrite(Tracker.output_dir + "/detection_{0}.jpg".format(Tracker.person_count), frame)
            Tracker.detection_file.write("{0} {1} ({2},{3},{4},{5}) [{6}]\n".format(Tracker.person_count, Tracker.frame_count, self.get_last_detection()[0], self.get_last_detection()[1], self.get_last_detection()[2],self.get_last_detection()[3], good_trajectory_values))

          self.number_times_drawn += 1


  def walked_on_pavement(self):

    if Tracker.pavements is None:
      return False

    good_trajectory_values = []
    #good_detection_values = []

    for detection in self.detections:

      if detection == [-1,-1]:
        continue

      count = 0
      for r in range(detection[1], detection[1]+detection[3]):
        for c in range(detection[0], detection[0]+detection[2]):

          if Tracker.pavements[r,c] == 255:
            count += 1

      if count > (0.8 * detection[3] * detection[2]):
        return True


    return False





        
class Tracker:

  person_count = 0
  output_dir = None
  detection_file = None
  frame_count = 0
  road_segments = None
  pavements = None

  def __init__(self, road_segments, pavements, output_dir):

    self.tracked_people = []
    Tracker.road_segments = road_segments
    Tracker.pavements = pavements

    Tracker.output_dir = output_dir
    if not os.path.exists(output_dir):
      os.mkdir(output_dir)
    Tracker.detection_file = open(output_dir + "/detections.txt", "w")
    Tracker.detection_file.write("Count - Frame No - Detection Coords\n")
  def brute_force_match_detections(self, frame, detections):

    #increment frame count
    Tracker.frame_count += 1

    matched_detections = []

    #iterate over the currently tracked people and see if any of them can be matched to new detections
    for person in self.tracked_people:

      #where we last found them
      last_detection = person.get_last_detection()
      if last_detection == [-1,-1]:
        raise Exception("Shouldn't happen!")

      best_detection = [-1,-1]
      best_distance = 1000000

      #iterate over the new detections and match to the last frame detection by how far away they are
      for detection in detections:
        dist = get_distance(last_detection, detection)
        if dist < best_distance:
          best_detection = detection
          best_distance = dist

      if best_detection != [-1,-1] and best_distance < 30 :
        if best_detection not in matched_detections:

          if DEBUG:
            draw_rectangle(frame, last_detection, False, (255,0,0))
            draw_rectangle(frame, best_detection, False, (255,0,255))
            cv2.line(frame, get_center(last_detection), get_center(best_detection), (255,0,0), 2)

          person.detections.append(best_detection)
          matched_detections.append(best_detection)

      else:
      
        person.detections.append([-1,-1])

    new_people = [TrackedPerson(d) for d in detections if d not in matched_detections]
    self.tracked_people = self.tracked_people + new_people
    return self.tracked_people

    
  def draw_tracked_people(self, image):

    for person in self.tracked_people:
      person.draw_trajectory(image)
    
   
  def clean_up(self):
    #get rid of old tracked people
    self.tracked_people = [t for t in self.tracked_people if t.detected_recently()]
  