import cv2
import math
import numpy as np

DEBUG = True

def combine_frames(frame_left, frame_right):

  r = np.ndarray(shape=(frame_left.shape[0],frame_left.shape[1]*2,frame_left.shape[2]), dtype=frame_left.dtype)

  r[:,:frame_left.shape[1],:] = frame_left

  if len(frame_right.shape) == 2:
    for k in range(frame_left.shape[2]):
      r[:,frame_left.shape[1]:,k] = frame_right

  return r


def squares_close( sq_x, sq_y):

  c_x = (sq_x[0] + sq_x[2], sq_x[1] + sq_x[3])
  c_y = (sq_y[0] + sq_y[2], sq_y[1] + sq_y[3])
  a_x = sq_x[2] * sq_x[3]
  a_y = sq_y[2] * sq_y[3]
  if math.sqrt( (c_x[0]-c_y[0])**2 + (c_x[1]-c_y[1])**2 ) > 20:# or float(a_x)/a_y > 0.4 or float(a_y)/a_x > 0.4:
    return False
  else:
    return True


def get_center(detection):
  return (detection[0]+detection[2], detection[1]+detection[3])

def get_distance(detection_1, detection_2):

  c_1 = get_center(detection_1)
  c_2 = get_center(detection_2)

  return math.sqrt( (c_1[0]-c_2[0])**2 + (c_1[1]-c_2[1])**2 )

    
def draw_rectangle(frame, rect, single_channel=False, color=(0,255,0)):

    x = rect[0]
    y = rect[1]
    w = rect[2]
    h = rect[3]

    if not single_channel:
      cv2.rectangle(frame, (x, y), (x+w, y+h), color, 2)
    else:
      cv2.rectangle(frame, (x, y), (x+w, y+h), (255), 2)