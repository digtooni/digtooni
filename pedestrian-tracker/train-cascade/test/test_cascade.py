import cv2
import os
import numpy as np
import math
import random

ROAD_REGIONS = [26, 51, 77, 102, 128, 153, 179, 204]

class LKTracker:

  def __init__(self):

    self.feature_params = dict( maxCorners = 100,
                           qualityLevel = 0.3,
                           minDistance = 7,
                           blockSize = 7 )

    # Parameters for lucas kanade optical flow
    self.lk_params = dict( winSize  = (15,15), maxLevel = 2, criteria = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.03))

    self.previous_gray = None

  def track_points(self, new_frame, detections):

    gray = cv2.cvtColor(new_frame, cv2.COLOR_BGR2GRAY)
    if self.previous_gray is None:
      self.previous_gray = gray
      return

    mask = np.zeros(shape=(new_frame.shape[0],new_frame.shape[1]), dtype=np.uint8)

    for detection in detections:
      draw_rectangle(mask, detection, True)

    p0 = cv2.goodFeaturesToTrack(self.previous_gray, mask=None, **self.feature_params)
    p1, st, err = cv2.calcOpticalFlowPyrLK(self.previous_gray, gray, p0, None, **self.lk_params)

    good_new = p1[st==1]
    good_old = p0[st==1]


    self.previous_gray = gray.copy()
    #self.p0 = good_new.reshape(-1,1,2)

def brute_force_match_people(people, detections):

  matched_detections = []

  for person in people:

    last_detection = person.get_last_detection()
    if last_detection == [-1,-1]:
      raise Exception("Shouldn't happen!")
    best_detection = [-1,-1]
    best_distance = 1000000
    for detection in detections:
      dist = get_distance(last_detection, detection)
      if dist < best_distance:
        best_detection = detection
        best_distance = dist

    if best_detection != [-1,-1] and best_distance < 30 :
      if best_detection not in matched_detections:
        person.detections.append(best_detection)
        matched_detections.append(best_detection)

    else:
      #print("Distance too great for {0}->{1} ==> {2}".format(last_detection, best_detection, best_distance))
      #print("count not match person to detections")
      person.detections.append([-1,-1])

  new_people = [TrackedPerson(d) for d in detections if d not in matched_detections]
  return people + new_people

def get_center(detection):
  return (detection[0]+detection[2], detection[1]+detection[3])

def get_distance(detection_1, detection_2):

  c_1 = get_center(detection_1)
  c_2 = get_center(detection_2)

  return math.sqrt( (c_1[0]-c_2[0])**2 + (c_1[1]-c_2[1])**2 )

class TrackedPerson:

  def __init__(self, initial_square):

    self.detections = []
    self.detections.append(initial_square)
    self.color = (random.randint(0,255), random.randint(0,255), random.randint(0,255))
    self.number_times_drawn = 0

  def get_last_detection(self):

    for detection in reversed(self.detections):
      if detection != [-1,-1]:
        return detection
    return [-1,-1]

  def detected_recently(self):

    history_length = 12

    if len(self.detections) < history_length:
      return True

    if self.detections[-history_length:].count([-1,-1]) == history_length:
      return False
    else:
      return True


  def draw_trajectory(self, frame):

    good_trajectory_values = []
    #good_detection_values = []

    for i in range(len(self.detections)):
      if i == 0:
        continue
      detection_1 = self.detections[i]
      if detection_1 == [-1,-1]:
        continue
      k = i-1
      detection_2 = [-1,-1]
      while k > 0:
        detection_2 = self.detections[k]
        if detection_2 != [-1,-1]:
          break
        k = k - 1

      if detection_2 == [-1, -1]:
        continue

      good_trajectory_values.append( (get_center(detection_1), get_center(detection_2)) )

    #for (start,end) in good_trajectory_values:
    #      cv2.line(frame, start, end, (0, 0, 255), 2)

    scores = [False]*len(ROAD_REGIONS)

    for (start,end) in good_trajectory_values:

      val = ROAD_REGION_IM[start[1],start[0]]
      try:
        idx = ROAD_REGIONS.index(val)
      except ValueError as e:
        if val == 0:
          continue
        else:
          raise e

      scores[idx] = True

    if sum(scores) > 2:

        if self.number_times_drawn < 5:
          draw_rectangle(frame, self.get_last_detection())

          if self.number_times_drawn == 0:
            global COUNT_CROSSING
            COUNT_CROSSING += 1
            print "ADding person + {0}".format(self)
            print "Adding at {0}".format(self.get_last_detection())
          self.number_times_drawn += 1

        #for (start,end) in good_trajectory_values:
        #  cv2.line(frame, start, end, (255, 0, 0), 2)
    #else:
    #    for (start,end) in good_trajectory_values:
    #      cv2.line(frame, start, end, (0, 0, 255), 2)
        #global COUNT_CROSSING
        #COUNT_CROSSING += 1
        #print "Crossing count: {0}".format(COUNT_CROSSING)


def squares_close( sq_x, sq_y):

  c_x = (sq_x[0] + sq_x[2], sq_x[1] + sq_x[3])
  c_y = (sq_y[0] + sq_y[2], sq_y[1] + sq_y[3])
  a_x = sq_x[2] * sq_x[3]
  a_y = sq_y[2] * sq_y[3]
  if math.sqrt( (c_x[0]-c_y[0])**2 + (c_x[1]-c_y[1])**2 ) > 20:# or float(a_x)/a_y > 0.4 or float(a_y)/a_x > 0.4:
    return False
  else:
    return True


def draw_rectangle(frame, rect, single_channel=False, color=(0,255,0)):

    x = rect[0]
    y = rect[1]
    w = rect[2]
    h = rect[3]

    if not single_channel:
      cv2.rectangle(frame, (x, y), (x+w, y+h), color, 2)
    else:
      cv2.rectangle(frame, (x, y), (x+w, y+h), (255), 2)

def filter_contours(contours):

  short_contours = []

  for contour in contours:

    if len(contour) > 20 and len(contour) < 230:
      #short_contours.append(contour)
      short_contours.append(cv2.boundingRect(contour))

  return short_contours

def filter_detections(detections, bg_output):

  dets = []

  for (x, y, w, h) in detections:

    sub_mask = bg_output[y:y+h,x:x+w]

    if np.sum(sub_mask)/255 > (0.3 * w * h):
      dets.append((x,y,w,h))

  return dets

def draw_detections(detections, im, mask=None):

  for (x, y, w, h) in detections:
    if mask is not None:
      sub_mask = mask[y:y+h,x:x+w]

      if np.sum(sub_mask)/255 > 0:
        cv2.rectangle(im, (x, y), (x+w, y+h), (0, 255, 0), 2)

def run_detection(detector, im, scale):

  gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
  gray = cv2.equalizeHist(gray)
  #detections = detector.detectMultiScale(gray, scaleFactor=1.05, minNeighbors=0, minSize=(10*scale,20*scale), maxSize=(24*scale,50*scale), flags = cv2.cv.CV_HAAR_SCALE_IMAGE)
  detections = detector.detectMultiScale(gray, scaleFactor=1.05, minNeighbors=0, minSize=(10*scale,20*scale), maxSize=(40*scale,80*scale), flags = cv2.cv.CV_HAAR_SCALE_IMAGE)
  return detections

def draw_blob_detection(detections, im):

  im_with_keypoints = cv2.drawKeypoints(im, detections, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
  return im_with_keypoints

def run_blob_detection(detector, im):

  keypoints = detector.detect(im)
  return keypoints

def init_blob_detector(scale):

    # Setup SimpleBlobDetector parameters.
  params = cv2.SimpleBlobDetector_Params()

  # Change thresholds
  params.minThreshold = 10
  params.maxThreshold = 200




  # Filter by Area.
  params.filterByArea = True
  params.minArea = 20*scale
  params.maxArea = 150*scale

  # Filter by Circularity
  params.filterByCircularity = False
  #params.minCircularity = 0.1

  # Filter by Convexity
  params.filterByConvexity = False
  #params.minConvexity = 0.87

  # Filter by Inertia
  params.filterByInertia = True
  params.minInertiaRatio = 0.01

  # Create a detector with the parameters
  detector = cv2.SimpleBlobDetector(params)
  return detector

def run_usa(detector):

  dir = r"C:\images"
  
  os.chdir(dir)
  
  for root, dirs, files in os.walk("."):
  
    print "Processing " + root
    
    for file in files:
    
      im = cv2.imread(os.path.join(root, file))
      
      if im is None:
        continue
    
      #print "Reading " + file
    
      im = cv2.resize(im, (0,0), fx = 2, fy = 2)
    
      detections = run_detections(detector, im, scale)
      
      draw_detections(detections, im)
      
      im = cv2.resize(im, (0,0), fx = 0.5, fy = 0.5)
      
      cv2.imshow("output",im)
      
      key = cv2.waitKey(300)
      
      
      if chr(key & 255) == 'q':
        print "Goodbye"
        import sys
        sys.exit(0)

def find_blobs(background_sub):

  f = background_sub.copy()
  c = cv2.Canny(background_sub, 20 , 60)
  (contours,_) = cv2.findContours(c, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

  return contours

def get_cascade_detections(im, scale, mask):

  frame = cv2.resize(im, (0,0), fx = scale, fy = scale)
  nmask = cv2.resize(mask, (0,0), fx = scale, fy = scale)

  detections = run_detection(detector, frame, scale)

  detections = filter_detections(detections, nmask)

  scaled_detections = []
  for detection in detections:
    scaled_detections.append( ( detection[0]/scale, detection[1]/scale, detection[2]/scale, detection[3]/scale) )


  return scaled_detections

def get_background_subtractor_detections(im, bg, mask):

  blur_im = cv2.blur(im, (3, 3))

  fgmask = bg.apply(blur_im, learningRate=0.005)

  fgmask = cv2.blur(fgmask, (4, 4))

  fgmask = cv2.erode(fgmask, (3,3))

  fgmask = cv2.blur(fgmask, (4, 4))

  fgmask = cv2.dilate(fgmask, (25,25))

  fgmask = cv2.blur(fgmask, (4, 4))

  (_,fgmask) = cv2.threshold(fgmask, 127, 255, cv2.THRESH_BINARY)

  contours = find_blobs(fgmask)

  contours = filter_contours(contours)

  contours = filter_detections(contours, mask)

  good_detections = []
  bad_detections = []

  for rect in contours:
    tl = (rect[0],rect[1])
    br = (rect[0]+rect[2],rect[1]+rect[3])
    x = rect[0]
    y =rect[1]
    w = rect[2]
    h = rect[3]

    if (w * 1.2) < h:

      good_detections.append( (x,y,w,h) )
    else:
      bad_detections.append( (x,y,w,h) )

  return good_detections, bad_detections, fgmask


def run_video(detector):

  mask = cv2.imread(r"P:\TfGM\FOOTAGE\background.png",0)
  cap = cv2.VideoCapture(r"P:\TfGM\FOOTAGE\Great_Ancoats_1/testvid1_13.mp4")

  writer = cv2.VideoWriter("P:/TfGM/FOOTAGE/testvid1_13_output.avi", cv2.cv.FOURCC(*"MJPG"), 20, (640, 480))

  bg = cv2.BackgroundSubtractorMOG()

  if mask is None:
    print "mask is none"

  scale = 3

  cnt = 0

  tracked_people = []

  while True:
    
    frame = cap.read()
    
    if not frame[0]:
      break
    
    frame = frame[1]  

    #c_detections = get_cascade_detections(frame, scale, mask)

    good_detections, bad_detections, bg_frame = get_background_subtractor_detections(frame, bg, mask)

    #for detection in good_detections:
    #  draw_rectangle(frame, detection, False, (0,255,0))

    #for detection in bad_detections:
    #  draw_rectangle(frame, detection, False, (0,0,255))

    tracked_people = brute_force_match_people(tracked_people, good_detections)

    for person in tracked_people:

      person.draw_trajectory(frame)

    tracked_people = [t for t in tracked_people if t.detected_recently()]

    global COUNT_CROSSING
    cv2.putText(frame, "Count: {0}".format(COUNT_CROSSING), (10,20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255,0,0))

    cv2.imshow("output", frame)

    cv2.imshow("bg_frame", bg_frame)



    key = cv2.waitKey(5)

    while True:
      if key & 255 == ord(" "):
        break
      elif key & 255 == ord("q"):
        print "goodbye"
        return
      key = cv2.waitKey(5)

    if key & 255 == ord("q"):
      print "GOodbye"
      return
    writer.write(frame)
  
if __name__ == '__main__':


  cv2.namedWindow("output")

  detector = cv2.CascadeClassifier(r"P:\TfGM\FOOTAGE\models\classifier\cascade.xml")


  run_video(detector)
