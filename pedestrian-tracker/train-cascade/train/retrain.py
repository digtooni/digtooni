import os
import subprocess
from sys import exit

def cmd_train_model(output_dir, training_data_file, negative_image_file_name, num_positive, num_negative, num_stages):

  return "C:/sdks/opencv/build-x64/install/x64/vc12/bin/opencv_traincascade.exe -data {0} -bg {1} -vec {2} -numPos {3} -numNeg {4} -numStages {5} -precalcValBufSize 1024 -precalcIdxBufSize 1024 -featureType HAAR -minHitRate 0.995 -maxFalseAlarmRate 0.1 -w 30 -h 100".format(output_dir, negative_image_file_name, training_data_file, num_positive, num_negative, num_stages)

def cmd_create_training_data(output_file, positive_file, negative_file):

    cmd = "C:/sdks/opencv/build-x64/install/x64/vc12/bin/opencv_createsamples.exe -info {0} -bg {1} -vec {2} -num 8000 -w 30 -h 100".format(positive_file, negative_file, output_file)
    print("Runing: " + cmd)
    return cmd

def is_image(filename):

    name,ext = os.path.splitext(filename)
    return ext == ".png" or ext == ".bmp" or ext == ".jpg" or ext == ".jpeg"

#add an extra layer to the haar cascade
haar_dir = "C://models/output2_8stage/"
num_stages = len([ f for f in os.listdir(haar_dir) if f.find("stage") != -1])

new_negatives_file_path = "new_negatives.txt"
new_negatives_file = open(new_negatives_file_path, "w")

# should be a directory containing only background images
new_data_dir = "C:/data-MAX/"

for root, dirs, files in os.walk(new_data_dir):

    for file in files:
        if is_image(file):
            new_negatives_file.write(os.path.join(root,file) + "\n")


print("Added negative data.\n")

new_data_file_path = "new_data.vec"
new_data_file = open(new_data_file_path,"w")
new_positives_file_path = "new_positives.txt"
new_positives_file = open(new_positives_file_path, "w")

print("Launching create training data.\n")
p = subprocess.Popen(cmd_create_training_data( new_data_file_path, new_positives_file_path, new_negatives_file_path), shell=True)
p.wait()
if p.returncode == 0:
    print("Done.\n")
else:
    print("Error, could not complete.\n")
    exit(1)

num_positive = 0
num_negative = 100
print("Launching training\n")
p = subprocess.Popen(cmd_train_model(haar_dir, new_data_file_path, new_negatives_file_path, num_positive, num_negative, num_stages+1))
p.wait()
print("Done")






