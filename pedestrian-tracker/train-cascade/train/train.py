import os 
import subprocess
import cv2

"""

Training data directories should contain 2 directories \'images\' (containing image files) and \'annotations\' containing annotation files. The annotation files should have the same name as the image file but with a .txt extension.
These can be within a directory structure, e.g. the Caltech dataset where image files are stored in subdirectories such as \'images/set01/V000/I00029.jpg\' in which case the corresponding annotation file should be in the directory \'annotations/set01/V000/I00029.txt\'.

The annotation file should list annotations in the format
\{object category\} x y w h N M O P ...
where x1 is the x coordinate of the top left of a bounding box around the person, y is the y coordinate, w is the width of the bounding box and h is the height. Additional lines correspond to additional people.
Object category is the type of object (e.g. person)

The application will find these images and in the case of positive examples they will be extracted using the annotation information and saved as subwindows a new directory in the training data directory called \'positive_samples\'.
To create negative examples the application will mask out any positive examples in each frame and save the entire frame to a  new directory in the training data directory called \'negative_samples\'.

"""

class Parser:

    def __init__(self):
        pass

    def is_image(self, filename):
        import os
        ext = os.path.splitext(filename)[1]
        return ext == ".jpg" or ext == ".png" or ext == ".jpeg" or ext == ".bmp"

class Trainer:

    def __init__(self, width, height, output_dir):

        try:
            os.mkdir(os.path.join(output_dir,"models"))
        except OSError:
            pass

        self.working_directory = os.path.join(output_dir,"models/model_0")
        count = 0
        while os.path.exists(self.working_directory):
            self.working_directory = os.path.join(output_dir,"models/model_{0}".format(count))
            count += 1

        os.mkdir(self.working_directory)

        self.output_model_directory = os.path.join(self.working_directory, "classifier")
        os.mkdir(self.output_model_directory)

        self.image_width = width
        self.image_height = height

        self.positive_images = []
        self.negative_images = []

        self.parser = Parser()


    def Retrain(self, positive_vec_file, negative_data_dirs, model_dir):

        from distutils.dir_util import copy_tree

        num_stages = len([ f for f in os.listdir(model_dir) if f.find("stage") != -1])
        if num_stages == 0:
            print("Error, no stages found in model dir: " + model_dir)
            exit(1)

        copy_tree(model_dir, self.output_model_directory)
        os.unlink(os.path.join(self.output_model_directory,"cascade.xml"))

        # all image files should be checked against their corresponding annotation file
        # if there are no annotations then the image should be automati

        self.positive_vec_file_name = positive_vec_file

        self.load_negative_data_files(negative_data_dirs)

        if not os.path.exists(self.positive_vec_file_name):
            raise Exception("Error, positive .vec file should have already been created.")

        self.run_training(num_stages+1)


    def Train(self, positive_data_dirs, negative_data_dirs):

        self.load_positive_data_files(positive_data_dirs)
        self.load_negative_data_files(negative_data_dirs)

        self.create_vec_file()

        self.run_training()

    def run_training(self, num_stages = 10):

        cwd = os.getcwd()
        #os.chdir(self.working_directory)
        cmd = self.cmd_train_model(os.path.relpath(self.output_model_directory), os.path.relpath(self.positive_vec_file_name), os.path.relpath(self.negative_image_file_name), 500, 200, num_stages, self.image_height, self.image_width)

        print cmd
        p = subprocess.Popen(cmd, shell=True)
        p.wait()
        if p.returncode != 0:
            print("Error, could not run training.\n")
            exit(1)

        os.chdir(cwd)

    def create_vec_file(self):

        """
        This takes a set of positive pre-marked up positive images and turns them into a compact .vec file format
        :return:
        """
        cmd = self.cmd_create_training_data(self.positive_vec_file_name, self.positive_image_file_name, self.image_height, self.image_width)
        p = subprocess.Popen(cmd, shell=True)
        p.wait()
        if p.returncode != 0:
            print("Error, could not create vec file.\n")
            exit(1)


    def load_negative_data_files(self, negative_data_dirs):


        for dir in negative_data_dirs:
            self.negative_images += [os.path.join(os.path.relpath(dir, self.working_directory), f) for f in os.listdir(dir) if self.parser.is_image(f)]

        #create some model file directory?
        self.negative_image_file_name = os.path.join(self.working_directory, "negatives.txt")
        with open(self.negative_image_file_name,"w") as negative_image_file:
            for negative_image in self.negative_images:
                negative_image_file.write(negative_image.replace("\\","/") + "\n") #in case windows

    def load_positive_data_files(self, positive_data_dirs):

        for dir in positive_data_dirs:
            self.positive_images += [os.path.join(dir, f) for f in os.listdir(dir) if self.parser.is_image(f)]

        self.positive_image_file_name = os.path.join(self.working_directory,"positives.txt")

        with open(self.positive_image_file_name,"w") as positive_image_file:

            for positive_image in self.positive_images:

                image = cv2.imread(positive_image)

                if image is None:
                    print "Error, could not open image {0}".format(positive_image)
                    continue

                image_width = image.shape[1]
                image_height = image.shape[0]

            positive_image_file.write("{0} 1 0 0 {1} {2}\n".format(positive_image, image_width, image_height))

    def cmd_create_training_data(self, output_file, positive_file, height, width):

        num_samples = len(open(positive_file, 'r').readlines())

        return "opencv_createsamples -info {0} -vec {1} -num {2} -w {3} -h {4}".format(positive_file, output_file, num_samples, width, height)

    def cmd_train_model(self, output_dir, training_data_file, negative_image_file_name, num_positive, num_negative, num_stages, height, width):

        return "opencv_traincascade -data {0} -bg {1} -vec {2} -numPos {3} -numNeg {4} -numStages {5} -precalcValBufSize 1024 -precalcIdxBufSize 1024 -featureType HAAR -minHitRate 0.995 -maxFalseAlarmRate 0.05 -w {6} -h {7}".format(output_dir, negative_image_file_name, training_data_file, num_positive, num_negative, num_stages, width, height)


#def cmd_train(width, height, positive_image_file, negative_images, output_file):
  # all pixels in the positive image with intensity between bgcolor-bgthresh and bgcolor + bgthresh are set to transparent
#  return "C:/sdks/opencv/build-x86/install/x86/vc12/bin/opencv_createsamples.exe -bgcolor -1 -bgthresh 0 -maxxangle 0.3 -maxyangle 0.2 -maxzangle 0 -maxidev 40 -img {2} -bg {3} -info {4} -num {5} -show".format(width, height, positive_image_file, negative_images, output_file, num_samples_to_generate)
#def cmd_test(width, height, positive_image_file, negative_images, output_file):
#  return "C:/sdks/opencv/build-x86/install/x86/vc12/bin/opencv_createsamples.exe -info samples.dat -bgcolor -1 -bgthresh 0 -maxxangle 1.1 -maxyangle 1.1 -maxzangle 0.5 -maxidev 40 -img {2} -bg {3} -num {4} -show".format(width, height, positive_image_file, negative_images, num_samples_to_generate)
#def get_vec_file_name(positive_image_file):
#
#  name,ext = os.path.splitext(os.path.basename(positive_image_file))
#  return name + ".txt"
  


