import os, sys
import copy
from train import Trainer

import process.process as process

def is_exe(fpath):

    return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

def check_opencv_found():

    exe_name = "opencv_traincascade"
    if os.name == "nt":
        exe_name += ".exe"

    for path in os.environ["PATH"].split(os.pathsep):
        path = path.strip('"')
        exe_file = os.path.join(path, exe_name)
        if is_exe(exe_file):
            return

    print("Error, could not find opencv binaries on path. Add the opencv binary directory to your path and relaunch")
    sys.exit(1)

    
def test(model_dir, video_file, image_scale = 1):

    from classifier.classifier import Classifier
    import cv2
    
    c = Classifier(model_dir + "/cascade.xml", image_scale)
  
    v = cv2.VideoCapture(video_file)
    w = cv2.VideoWriter("test_output.avi", cv2.cv.CV_FOURCC(*"DIB "), 25, (640, 480))

    #run processing
    cv2.namedWindow("output")

    while True:

        frame = v.read()
        
        if not frame[0]:
          break
          
        frame = frame[1]
        
        detections = c.classify_image(frame)
        
        c.draw_detections(frame, detections)
        
        cv2.imshow("output", frame)
        
        key = cv2.waitKey(10)
                
        if key & 255 == ord("q"):
          break
          
        while key & 255 != ord(" "):
          key = cv2.waitKey(10)

        w.write(frame)
        

if __name__ == "__main__":

    import argparse
    check_opencv_found()

    parser = argparse.ArgumentParser(description='Train or retrain a cascade type classifier with minimum fuss. Also can process a caltech type dataset.')

    parser.add_argument('--process-dataset', action='store_true', help='Process a dataset with annotation files into positive and negative directories')
    parser.add_argument('--train', action='store_true', help='Train a classifier for the first time.')
    parser.add_argument('--retrain', action="store_true", help='Retrain rather than train.')
    parser.add_argument('--test', action="store_true", help='Very simple testing tool to display the output of a classifier on a video')
    args,unknown = parser.parse_known_args()

    if args.train or args.retrain:

        parser.add_argument('--output-dir', type=str, help='Directory to save the output files.', required=True)
        parser.add_argument('--negative-data-dirs', type=str, nargs='+', help='Directory containing negative training data.', required=True)
        parser.add_argument('--positive-data-dirs', type=str, nargs='+', help='Directory containing positive training data.', required=True)
        parser.add_argument('--width', type=int, help='Width of positive training samples.', required=True)
        parser.add_argument('--height', type=int, help='Height of positive training samples.', required=True)
        parser.add_argument('--num-negative', type=int, help='Number of negative examples to create (by random sampling from the negative frames) for each cascade training round.', required=True)
        parser.add_argument('--use-haar-features', action="store_true", help='Use Haar features for training. If omitted the fast LBP features are used instead. If retraining, ensure you are using the same feature type as in previous rounds.')

    elif args.train:

        parser.add_argument('--num-stages', type=int, help='Number of cascade stages to train.', required=True)

    elif args.process_dataset:

        parser.add_argument('--root-data-dir', type=str, help='The root dataset which will be processed into positive and negative datasets.', required=True)

    elif args.test:
      
        parser.add_argument('--model-dir', type=str, help='Directory containing the already trained model.', required=True)
        parser.add_argument('--video-file', type=str, help='Video file to test on.', required=True)
        
    else:

        parser.print_help()
        sys.exit(1)

    args2 = parser.parse_args()

    if args.train:
        t = Trainer(args2.width, args2.height, args2.output_dir, args2.use_haar_features)
        t.Train(args2.positive_data_dirs, args2.negative_data_dirs, args2.num_stages, args2.num_negative)
    elif args.retrain:
        t = Trainer(args2.width, args2.height, args2.output_dir, args2.use_haar_features)
        t.Retrain(args2.positive_data_dirs, args2.negative_data_dirs, args2.num_negative)
    elif args.process_dataset:
        process.process_caltech_datasets(args2.root_data_dir)
    elif args.test:
        test(args2.model_dir, args2.video_file)
    
    else:
        raise Exception("Should never get here...")



