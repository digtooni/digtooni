import cv2
import os


class Classifier:
  
  def __init__(self, classifier_file, image_scale):
  
    if not os.path.exists(classifier_file):
      raise Exception("Error, could not find classifier file {0}".format(classifier_file))
    
    self.classifier = cv2.CascadeClassifier(classifier_file)
    self.image_scale = image_scale
    

  def classify_image(self, image, mask=None):
    
    large_image = cv2.resize(image, (0,0), fx = self.image_scale, fy = self.image_scale)
    if mask is not None:
      large_mask = cv2.resize(mask, (0,0), fx = self.image_scale, fy = self.image_scale)
    
    gray = cv2.cvtColor(large_image, cv2.COLOR_BGR2GRAY)
    gray = cv2.equalizeHist(gray)
            
    detections = self.classifier.detectMultiScale(gray, scaleFactor=1.01, minNeighbors=0, minSize=(10,10), maxSize=(120,120), flags = cv2.cv.CV_HAAR_SCALE_IMAGE)
    
    #scale down detections
    detections = [( detection[0]/self.image_scale, detection[1]/self.image_scale, detection[2]/self.image_scale, detection[3]/self.image_scale) for detection in detections]
        
    if mask is None:
      return detections
    
      
    filtered_detections = []
    
    for (x, y, w, h) in detections:
      sub_mask = mask[y:y+h,x:x+w]
          
      if np.sum(sub_mask)/255 > 0:
        filtered_detections.append((x,y,w,h))
        
    return filtered_detections
    
  def draw_detections(self, image, detections):
    
     for (x, y, w, h) in detections:
      cv2.rectangle(image, (x, y), (x+w, y+h), (0, 255, 0), 2)
    
      