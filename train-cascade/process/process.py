import os
import sys
import numpy as np
import cv2

import random
random.seed(None)


def fill_frame(image, rectangle, val):

  image[rectangle[1]:rectangle[1]+rectangle[3],rectangle[0]:rectangle[0]+rectangle[2],:] = val
  return image

def parse_annotations(annotation_line):

  split = annotation_line.split(" ")
  type = split[0]
  vals = map(int,split[1:5])
  if type != "person":
    print "Warning, type was : " + type
    return []

  return vals

def get_positive_examples(image_file, annotation_file):

  image = cv2.imread(image_file)
  if image is None:
    print "Warning, could not open image file: " + image_file
    return []

  with open(annotation_file, 'r') as annotation_file_handle:

    annotations = [parse_annotations(line) for line in annotation_file_handle.readlines() if line[0] != '%']

    positive_examples = []

    for annotation in annotations:

      if len(annotation) == 0:
        continue

      start_c = annotation[0]
      end_c = annotation[0]+annotation[2]
      start_r = annotation[1]
      end_r = annotation[1]+annotation[3]

      if end_c - start_c < 10 or end_r - start_r < 35:
        continue

      positive_examples.append(image[start_r:end_r,start_c:end_c,:])

    return positive_examples

def get_negative_examples(image_file, annotation_file):

  image = cv2.imread(image_file)
  if image is None:
    print "Warning, could not open image file: " + image_file
    return []


  with open(annotation_file, 'r') as annotation_file_handle:

    annotations = [parse_annotations(line) for line in annotation_file_handle.readlines() if line[0] != '%']

    negative_examples = []

    empty_frame = np.zeros(shape=image.shape, dtype=np.uint8)

    for annotation in annotations:

      if len(annotation) == 0:
        continue

      empty_frame = fill_frame(empty_frame, annotation, 255)


    for i in range(20):

      while True:
        start_r = int(random.uniform(0, image.shape[0]))
        start_c = int(random.uniform(0, image.shape[1]))
        height = int(random.uniform(25, 0.2*image.shape[0]))
        width = int(random.uniform(0.25*height, 0.4*height))


        if np.sum(empty_frame[start_r:start_r+height,start_c:start_c+width,:]) > 0:
          continue
        else:
          negative_examples.append(image[start_r:start_r+height,start_c:start_c+width,:])
          break

    return negative_examples

def get_negative_frame(image_file, annotation_file):

  image = cv2.imread(image_file)

  if image is None:
    print "Warning, could not open image file: " + image_file
    return None


  with open(annotation_file, 'r') as annotation_file_handle:

    annotations = [parse_annotations(line) for line in annotation_file_handle.readlines() if line[0] != '%']

    for annotation in annotations:

      if annotation == []:
        continue

      image = fill_frame(image, annotation, 127)

  return image


def process_caltech_datasets(root_dataset):

    annotations_dirs = os.path.join(root_dataset, "annotations")
    images_dirs = os.path.join(root_dataset, "images")
    #res are results from other methods
    #videos is some kind of database

    if not os.path.exists(annotations_dirs) or not os.path.exists(images_dirs):
      print "\nError, could not find the annotation or image dir!\n"
      sys.exit(1)


    sequences = os.listdir(images_dirs)
    assert(len( set(sequences) & set(os.listdir(annotations_dirs)) ) == len(sequences))

    positive_idx = 0
    negative_idx = 0

    positive_save_dir = os.path.join(root_dataset, "training_positive")
    negative_save_dir = os.path.join(root_dataset, "training_negative")

    try:
      os.mkdir(positive_save_dir)
    except:
      pass
    try:
      os.mkdir(negative_save_dir)
    except:
      pass


    for sequence in sequences:


      image_set_dir = os.path.join(images_dirs,sequence)
      annotation_set_dir = os.path.join(annotations_dirs,sequence)

      image_subsets = os.listdir(image_set_dir)

      for image_subset in image_subsets:

        image_subset_dir = os.path.join(image_set_dir, image_subset)
        annotation_subset_dir = os.path.join(annotation_set_dir, image_subset)

        images = os.listdir(image_subset_dir)

        for image in images:

          image_file = os.path.join(image_subset_dir, image)
          annotation_file = os.path.join(annotation_subset_dir, image.replace(image[-4:],".txt"))

          positive_images = get_positive_examples(image_file, annotation_file)
          negative_frame = get_negative_frame(image_file, annotation_file)

          for positive_image in positive_images:

            if positive_image.shape[0] == 0 or positive_image.shape[1] == 0:
              continue
            cv2.imwrite(os.path.join(positive_save_dir, "image{0}.png".format(positive_idx)), positive_image)
            positive_idx += 1

          #for negative_image in negative_images:

          if negative_frame is not None:

            if negative_frame.shape[0] == 0 or negative_frame.shape[1] == 0:
              continue

            cv2.imwrite(os.path.join(negative_save_dir, "image{0}.png".format(negative_idx)), negative_frame)
            negative_idx += 1



