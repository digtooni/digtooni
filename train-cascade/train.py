import os 
import subprocess
import cv2

"""

Training data directories should contain 2 directories \'images\' (containing image files) and \'annotations\' containing annotation files. The annotation files should have the same name as the image file but with a .txt extension.
These can be within a directory structure, e.g. the Caltech dataset where image files are stored in subdirectories such as \'images/set01/V000/I00029.jpg\' in which case the corresponding annotation file should be in the directory \'annotations/set01/V000/I00029.txt\'.

The annotation file should list annotations in the format
\{object category\} x y w h N M O P ...
where x1 is the x coordinate of the top left of a bounding box around the person, y is the y coordinate, w is the width of the bounding box and h is the height. Additional lines correspond to additional people.
Object category is the type of object (e.g. person)

The application will find these images and in the case of positive examples they will be extracted using the annotation information and saved as subwindows a new directory in the training data directory called \'positive_samples\'.
To create negative examples the application will mask out any positive examples in each frame and save the entire frame to a  new directory in the training data directory called \'negative_samples\'.

"""

class Parser:

    def __init__(self):
        pass

    def is_image(self, filename):
        import os
        ext = os.path.splitext(filename)[1]
        return ext == ".jpg" or ext == ".png" or ext == ".jpeg" or ext == ".bmp"

class Trainer:

    def __init__(self, width, height, output_dir, use_haar_features):

        try:
            os.mkdir(os.path.join(output_dir,"models"))
        except OSError:
            pass

        self.working_directory = output_dir

        if not os.path.exists(self.working_directory):
            os.mkdir(self.working_directory)

        self.output_model_directory = os.path.join(self.working_directory, "classifier")
        if not os.path.exists(self.output_model_directory):
            os.mkdir(self.output_model_directory)

        self.image_width = width
        self.image_height = height

        self.positive_images = []
        self.negative_images = []

        self.parser = Parser()

        self.use_haar_features = use_haar_features

    def Retrain(self, positive_data_dirs, negative_data_dirs, num_negative):

        from shutil import copy

        num_stages = len([ f for f in os.listdir(self.output_model_directory) if f.find("stage") != -1])
        if num_stages == 0:
            print("Error, no stages found in model dir: " + self.output_model_directory)
            exit(1)

        backup_cascade_file = self.output_model_directory + "/cascade_old.xml"
        if os.path.exists(backup_cascade_file):
            backup_cascade_file = raw_input("Please enter a new filename for the old cascade.xml to be renamed (as a backup). Type enter to discard it. ")
            if backup_cascade_file != "":
                filename, ext = os.path.splitext(backup_cascade_file)
                if ext != ".xml":
                    backup_cascade_file == filename + ".xml"
                backup_cascade_file = self.output_model_directory + "/" + backup_cascade_file
                copy(self.output_model_directory + "/cascade.xml", backup_cascade_file)

        self.load_positive_data_files(positive_data_dirs)
        self.load_negative_data_files(negative_data_dirs)
        
        self.create_vec_file()

        self.run_training(num_stages+1, num_negative)


    def Train(self, positive_data_dirs, negative_data_dirs, num_stages, num_negative):

        self.load_positive_data_files(positive_data_dirs)
        self.load_negative_data_files(negative_data_dirs)

        self.create_vec_file()

        self.run_training(num_stages, num_negative)

    def run_training(self, num_stages, num_negative):

        cwd = os.getcwd()

        num_negatives = num_negative
        num_positives = int(len(open(self.positive_image_file_name).readlines())/3)
        
        cmd = self.cmd_train_model(os.path.relpath(self.output_model_directory), os.path.relpath(self.positive_vec_file_name), os.path.relpath(self.negative_image_file_name), num_positives, num_negatives, num_stages, self.image_height, self.image_width)

        print cmd
        p = subprocess.Popen(cmd, shell=True)
        p.wait()
        if p.returncode != 0:
            print("Error, could not run training.\n")
            exit(1)

        os.chdir(cwd)

    def create_vec_file(self):

        """
        This takes a set of positive pre-marked up positive images and turns them into a compact .vec file format
        :return:
        """
        
        self.positive_vec_file_name = self.working_directory + "/positive.vec"

        cmd = self.cmd_create_training_data(self.positive_vec_file_name, self.positive_image_file_name, self.image_height, self.image_width)
        p = subprocess.Popen(cmd, shell=True)
        p.wait()
        if p.returncode != 0:
            print("Error, could not create vec file.\n")
            exit(1)


    def load_negative_data_files(self, negative_data_dirs):


        for dir in negative_data_dirs:
            self.negative_images += [os.path.join(os.path.relpath(dir, self.working_directory), f) for f in os.listdir(dir) if self.parser.is_image(f)]

            
        print "Found {0} negative images".format(len(self.negative_images))
        
        #create some model file directory?
        self.negative_image_file_name = os.path.join(self.working_directory, "negatives.txt")

        with open(self.negative_image_file_name,"w") as negative_image_file:
            for negative_image in self.negative_images:
                negative_image_file.write(negative_image.replace("\\","/") + "\n") #in case windows

    def load_positive_data_files(self, positive_data_dirs):

        for dir in positive_data_dirs:
            self.positive_images += [os.path.join(os.path.relpath(dir, self.working_directory), f) for f in os.listdir(dir) if self.parser.is_image(f)]#[os.path.join(dir, f) for f in os.listdir(dir) if self.parser.is_image(f)]

        print "Found {0} positive images".format(len(self.positive_images))
            
        self.positive_image_file_name = os.path.join(self.working_directory,"positives.txt")

        with open(self.positive_image_file_name,"w") as positive_image_file:

            for positive_image in self.positive_images:

                image = cv2.imread(os.path.join(self.working_directory, positive_image))

                if image is None:
                    print "Error, could not open image {0}".format(positive_image)
                    continue

                image_width = image.shape[1]
                image_height = image.shape[0]

                positive_image_file.write("{0} 1 0 0 {1} {2}\n".format(positive_image, image_width, image_height))

    def cmd_create_training_data(self, output_file, positive_file, height, width):

        num_samples = len(open(positive_file, 'r').readlines())

        return "opencv_createsamples -info {0} -vec {1} -num {2} -w {3} -h {4}".format(positive_file, output_file, num_samples, width, height)

    def cmd_train_model(self, output_dir, training_data_file, negative_image_file_name, num_positive, num_negative, num_stages, height, width):

        features_substring = ""
        if self.use_haar_features:
            features_substring = "-featureType HAAR -mode ALL"
        else:
            features_substring = "-featureType LBP"

        return "opencv_traincascade -numThreads 4 -data {0} -bg {1} -vec {2} -numPos {3} -numNeg {4} -numStages {5} -precalcValBufSize 1024 -precalcIdxBufSize 1024 {6} -minHitRate 0.995 -maxFalseAlarmRate 0.0005 -w {7} -h {8}".format(output_dir, negative_image_file_name, training_data_file, num_positive, num_negative, num_stages, features_substring, width, height)




