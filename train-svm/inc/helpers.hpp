#pragma once

#include <stdio.h>
#include <boost/filesystem.hpp>
#include <ios>
#include <fstream>
#include <stdexcept>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/ml/ml.hpp>
#include <array>
#include <utility>

#include "svmlight.h"
#include "training_model.hpp"

inline void storeCursor(void) {
  printf("\033[s");
}

inline void resetCursor(void) {
  printf("\033[u");
}

cv::Rect SafeDetectionOnFrame(const cv::Rect &roi, const cv::Size &frame_size);

cv::Point GetCenter(cv::Point &tl, const size_t width, const size_t height);

cv::Point FlipPoint(cv::Point &pt, const cv::Size image_size);

cv::Rect FlipRect(cv::Rect &pt, const cv::Size image_size);

void RotateFrame2D(const cv::Mat & src, cv::Mat & dst, const double degrees);

void RunAllDetectorsOnVideo(std::string &video_file, const std::string &output_dir, std::vector<TrainingModel *> &detectors);

void MineVideoForFalsePositives(const std::string &video_file, const std::string &mining_output, TrainingModel *detector);
