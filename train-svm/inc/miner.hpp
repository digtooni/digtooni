#pragma once

#include <list>
#include <string>
#include <opencv.hpp>

#include "training_model.hpp"

struct AnnotationWrapper {

  AnnotationWrapper(const std::string &s, TrainingModel &tm_) : output_positive_dir(s), positive_cnt(0) , tm(tm_) {}

  void Clear(){
    positive.clear();
    to_save.clear();
  }


  void Setup(const std::vector<cv::Rect> &positive_){

    for (const auto &p : positive_)
      positive.push_back(p);

  }

  void SaveAll(const cv::Mat &frame){
    for (const auto &roi : to_save){
      cv::Rect safe_roi = roi;
      
      //makes assumption box is not wider than image...
      if (roi.tl().x < 0){
        safe_roi.x = 0;
        safe_roi.width = roi.br().x - safe_roi.tl().x;
      }

      if (roi.tl().y < 0){
        safe_roi.y = 0;
        safe_roi.height = roi.br().y - safe_roi.tl().y;
      }

      if (roi.br().x >= frame.cols) 
        safe_roi.width = (frame.cols - safe_roi.x - 1);

      if (roi.br().y >= frame.rows) 
        safe_roi.y = (frame.rows - safe_roi.y - 1);

      cv::Mat roi_data = frame(safe_roi).clone();
      cv::resize(roi_data, roi_data, tm.HoGSize());
      Save(roi_data);

    }
  }

  void Save(const cv::Mat &roi){

    std::stringstream ss;
    ss << output_positive_dir << "/image" << positive_cnt << ".png";
    cv::imwrite(ss.str(), roi);
    positive_cnt++;

  }

  TrainingModel &tm;

  std::list<cv::Rect> positive;
  std::vector<cv::Rect> to_save;

  std::string output_positive_dir;

  size_t positive_cnt;

};

void MinerMouseCallback(int event, int x, int y, int, void *annotations);