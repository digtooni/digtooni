/**
 * @file:   svmlight.h
 * @author: Jan Hendriks (dahoc3150 [at] gmail.com)
 * @date:   Created on 11. Mai 2011
 * @brief:  Wrapper interface for SVMlight,
 * @see http://www.cs.cornell.edu/people/tj/svm_light/ for SVMlight details and terms of use
 *
 */

#ifndef SVMLIGHT_H
#define	SVMLIGHT_H

#include <stdio.h>
#include <vector>
#include <fstream>
#include <iostream>
#include <string>

// svmlight related
// namespace required for avoiding collisions of declarations (e.g. LINEAR being declared in flann, svmlight and libsvm)
namespace svmlight {
  extern "C" {
#include "svmlight/svm_common.h"
#include "svmlight/svm_learn.h"
  }
}

using namespace svmlight;

class SVMlight {

public:

  SVMlight();
  virtual ~SVMlight();

  LEARN_PARM* learn_parm;
  KERNEL_PARM* kernel_parm;

  inline void saveModelToFile(const std::string _modelFileName) {
    write_model(const_cast<char*>(_modelFileName.c_str()), model);
  }


  void loadModelFromFile(const std::string _modelFileName) {
    this->model = read_model(const_cast<char*>(_modelFileName.c_str()));
  }

  // read in a problem (in svmlight format)
  void read_problem(char* filename) {
    // Reads and parses the specified file
    read_documents(filename, &docs, &target, &totwords, &totdoc);
  }

  // Calls the actual machine learning algorithm
  void train() {

    model = (MODEL *)my_malloc(sizeof(MODEL));

    //svm_learn_regression(docs, target, totdoc, totwords, learn_parm, kernel_parm, &kernel_cache, model);
    svm_learn_classification(docs, target, totdoc, totwords, learn_parm, kernel_parm, kernel_cache, model, NULL);
  }

  /**
   * Generates a single detecting feature vector (vec1) from the trained support vectors, for use e.g. with the HOG algorithm
   * vec1 = sum_1_n (alpha_y*x_i). (vec1 is a 1 x n column vector. n = feature vector length)
   * @param singleDetectorVector resulting single detector vector for use in openCV HOG
   * @param singleDetectorVectorIndices dummy vector for this implementation
   */
  void getSingleDetectingVector(std::vector<float>& singleDetectorVector, std::vector<unsigned int>& singleDetectorVectorIndices);

  //mimic a 'features' file with support vectors - we then append new training data to this file
  void writeSupportVectorsToFile(std::ofstream &file);
  
  /**
   * Return model detection threshold / bias
   * @return detection threshold / bias
   */
  float getThreshold() const {
    return (float)model->b;
  }

  const char* getSVMName() const {
    return "SVMlight";
  }

private:
  DOC** docs; // training examples
  long totwords, totdoc, i; // support vector stuff
  double* target;
  double* alpha_in;
  KERNEL_CACHE* kernel_cache;
  MODEL* model; // SVM model


};

#endif	/* SVMLIGHT_H */
