#pragma once

#include <opencv2/opencv.hpp>

#include "svmlight.h"

class TrainingModel {

public:
  
  explicit TrainingModel(const std::string &config_file);
  ~TrainingModel();

  void Train(bool retrain);
  void Retrain();
  void LoadModel();
  
  void CollectTrainingData();

  void RunDetection(const cv::Mat &frame, std::vector<cv::Rect> &detections) const;

  const std::string &name() const { return name_; }
  cv::Size HoGSize() const { return hog_.winSize; }
  float FrameScale() const { return frame_scale_; }

protected:


  void GenerateFeaturesFromData(bool append);


  /**
  * This is the actual calculation from the (input) image data to the HOG descriptor/feature vector using the hog.compute() function
  * @param imageFilename file path of the image file to read and calculate feature vector from
  * @param descriptorVector the returned calculated feature vector<float> ,
  *      I can't comprehend why openCV implementation returns std::vector<float> instead of cv::MatExpr_<float> (e.g. Mat<float>)
  * @param hog HOGDescriptor containin HOG settings
  */
  void CalculateFeaturesFromInput(const std::string& imageFilename, std::vector<float>& featureVector, cv::HOGDescriptor& hog, cv::Size winStride, cv::Size trainingPadding);


  /**
  * For unixoid systems only: Lists all files in a given directory and returns a vector of path+name in string format
  * @param dirName
  * @param fileNames found file names in specified directory
  * @param validExtensions containing the valid file extensions for collection in lower case
  */
  void GetFilesInDirectory(const std::string& dirName, std::vector<std::string>& fileNames, const std::vector<std::string>& validExtensions);


  void SetTrainingDataDirs(const std::vector<std::string> &positive_dirs, const std::vector<std::string> &negative_dirs);


  void CollectTrainingData(const TrainingModel &other_models);
  
  void CollectExtraNegatives(const std::string &directory);
  void CollectExtraNegatives(const std::vector<std::string> &directories);


  /**
  * Saves the given descriptor vector to a file
  * @param descriptorVector the descriptor vector to save
  * @param _vectorIndices contains indices for the corresponding vector values (e.g. descriptorVector(0)=3.5f may have index 1)
  * @param fileName
  * @TODO Use _vectorIndices to write correct indices
  */

  void SaveSVMToFile(std::vector<float>& descriptor_vector, const float svm_margin, std::vector<unsigned int> & vector_indicies, const std::string &filename);

  
  std::string name_;
  std::string features_file_;
  std::string descriptor_vector_file_;
  std::string svmlight_model_file_;
  std::vector<std::string> positive_training_data_dirs_;
  std::vector<std::string> negative_training_data_dirs_;
  std::vector<std::string> positive_training_images_;
  std::vector<std::string> negative_training_images_;
  std::vector<std::string> valid_extensions_;
  std::vector<float> descriptor_vector_;
  unsigned long number_of_samples_;
  
  cv::HOGDescriptor hog_;

  //svm + hog parameters
  float hit_threshold_;
  cv::Size win_stride_;
  cv::Size training_padding_;
  float shift_threshold_;
  float scale_increase_;
  float max_scale_;
  float frame_scale_;

  //don't use
  TrainingModel::TrainingModel(const TrainingModel &other) {}
  TrainingModel &TrainingModel::operator=(const TrainingModel &rhs) { return *this; }

  SVMlight *svm_light_;

};



