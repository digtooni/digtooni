#include <boost/date_time.hpp>
#include <math.h>

#include "../inc/helpers.hpp"
#include "../inc/miner.hpp"
#include "../inc/training_model.hpp"

void MineVideoForFalsePositives(const std::string &video_file, const std::string &mining_output, TrainingModel *detector){

  cv::VideoCapture cap(video_file); 

  if (!cap.isOpened()) { // check if we succeeded
    printf("Error opening camera!\n");
    return;
  }

  const std::string win_name("HOG custom detection");
  cv::namedWindow(win_name);
  AnnotationWrapper aw(mining_output, *detector);

  cv::setMouseCallback(win_name, MinerMouseCallback, &aw);

  bool run = true;

  while (run) {
    
    cv::Mat frame;
    cap >> frame;

    if (frame.empty()) break;

    std::vector<cv::Rect> detections;

    detector->RunDetection(frame, detections);
    
    //for (auto &detection : detections)
    //  cv::rectangle(frame, detection, cv::Scalar(255, 0, 255), 2);


    aw.Clear();
    aw.Setup(detections);

    while (1){

      cv::Mat frame_with_detections = frame.clone();

      int key = cv::waitKey(5);
      if (key == ' ') break;
      if (key == 'q') run = false;

      for (auto detection : aw.positive){
        cv::rectangle(frame_with_detections, detection, cv::Scalar(255, 0, 0), 2);
      }

      imshow(win_name, frame_with_detections);

    }

    aw.SaveAll(frame);

  }
}

void RunAllDetectorsOnVideo(std::string &video_file, const std::string &output_directory, std::vector<TrainingModel *> &detectors){

  std::cout << "Running detection on video file: " << video_file << " using " << detectors.size() << " detectors." << std::endl;

  cv::VideoCapture cap(video_file);
  if (!cap.isOpened()){

    std::cerr << "Error, could not open file: " << video_file << "\n";

  }

  const std::string win_name("HOG custom detection");
  cv::VideoWriter writer;
  
  if (output_directory == "")
    cv::namedWindow(win_name);  
  else
    writer.open(output_directory + "/output.avi", CV_FOURCC('D', 'I', 'B', ' '), 25, cv::Size(cap.get(CV_CAP_PROP_FRAME_WIDTH), cap.get(CV_CAP_PROP_FRAME_HEIGHT)));

  std::vector<cv::Rect> detections;

  while ((cv::waitKey(10) & 255) != 27) {

    cv::Mat frame;
    cap >> frame;

    if (frame.empty()) break;

    for (auto &detector : detectors){
      
      detector->RunDetection(frame, detections);

    }

    for (const auto &detection : detections){
      cv::rectangle(frame, detection, cv::Scalar(255, 0, 0), 2);
    }

    if (output_directory == "")
      cv::imshow(win_name, frame);
    else
      writer.write(frame);
    
  }
}

void RotateFrame2D(const cv::Mat & src, cv::Mat & dst, const double degrees){
  cv::Mat frame, frameRotated;

  int diagonal = (int)sqrt(src.cols * src.cols + src.rows * src.rows);
  int newWidth = diagonal;
  int newHeight = diagonal;

  int offsetX = (newWidth - src.cols) / 2;
  int offsetY = (newHeight - src.rows) / 2;
  cv::Mat targetMat(newWidth, newHeight, src.type(), cv::Scalar(0));
  cv::Point2f src_center(targetMat.cols / 2.0f, targetMat.rows / 2.0f);

  src.copyTo(frame);

  frame.copyTo(targetMat.rowRange(offsetY, offsetY +
    frame.rows).colRange(offsetX, offsetX + frame.cols));
  cv::Mat rot_mat = cv::getRotationMatrix2D(src_center, degrees, 1.0);
  cv::warpAffine(targetMat, frameRotated, rot_mat, targetMat.size());

  cv::Rect bound_Rect(frame.cols, frame.rows, 0, 0);
  int x1 = offsetX;
  int x2 = offsetX + frame.cols;
  int x3 = offsetX;
  int x4 = offsetX + frame.cols;
  int y1 = offsetY;
  int y2 = offsetY;
  int y3 = offsetY + frame.rows;
  int y4 = offsetY + frame.rows;
  cv::Mat co_Ordinate = (cv::Mat_<double>(3, 4) << x1, x2, x3, x4,
    y1, y2, y3, y4,
    1, 1, 1, 1);

  cv::Mat RotCo_Ordinate = rot_mat * co_Ordinate;

  for (int i = 0; i < 4; ++i) {
    if (RotCo_Ordinate.at<double>(0, i) < bound_Rect.x)
      bound_Rect.x = (int)RotCo_Ordinate.at<double>(0, i);
    if (RotCo_Ordinate.at<double>(1, i) < bound_Rect.y)
      bound_Rect.y = RotCo_Ordinate.at<double>(1, i);
  }

  for (int i = 0; i < 4; ++i) {
    if (RotCo_Ordinate.at<double>(0, i) > bound_Rect.width)
      bound_Rect.width = (int)RotCo_Ordinate.at<double>(0, i);
    if (RotCo_Ordinate.at<double>(1, i) > bound_Rect.height)
      bound_Rect.height = RotCo_Ordinate.at<double>(1, i);
  }

  bound_Rect.width = bound_Rect.width - bound_Rect.x;
  bound_Rect.height = bound_Rect.height - bound_Rect.y;

  if (bound_Rect.x < 0)
    bound_Rect.x = 0;
  if (bound_Rect.y < 0)
    bound_Rect.y = 0;
  if (bound_Rect.width > frameRotated.cols)
    bound_Rect.width = frameRotated.cols;
  if (bound_Rect.height > frameRotated.rows)
    bound_Rect.height = frameRotated.rows;

  cv::Mat ROI = frameRotated(bound_Rect);
  ROI.copyTo(dst);
}

