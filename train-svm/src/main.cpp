#include "../inc/training_model.hpp"
#include "../inc/helpers.hpp"

#include <opencv.hpp>
#include <sstream>

int main(int argc, char** argv) {

  cv::CommandLineParser cmd(argc, argv,
    "{ m | model      |                  | model config file.     }"
    "{ t | train      | false            | train new model.       }"
    "{ r | retrain    | false            | retrain model.         }"
    "{ n | mining     | false            | perform hard negative mining. }"
    "{ o | output     |                  | output directory for saving mining or detector ouput. Optional for dectector output. If omitted will display output on window.}"
    "{ v | video      |                  | video testing file.    }"
    );

  if (argc == 1 || cmd.get<std::string>("model") == ""){
    cmd.printParams();
    return 0;
  }

  const bool train = cmd.get<bool>("train");
  const bool retrain = cmd.get<bool>("retrain");
  const bool mine_for_false_positives = cmd.get<bool>("mining");

  std::vector<TrainingModel *> models;
  models.push_back(new TrainingModel(cmd.get<std::string>("model")));
 
  
  /// @WARNING: This is really important, some libraries (e.g. ROS) seems to set the system locale which takes decimal commata instead of points which causes the file input parsing to fail
  setlocale(LC_ALL, "C"); // Do not use the system locale
  setlocale(LC_NUMERIC, "C");
  setlocale(LC_ALL, "POSIX");

  if (train){

    for (auto &model : models){
      model->CollectTrainingData();
      model->Train(false);
    }


  } 
  else if (retrain){

    for (auto &model : models){
      model->CollectTrainingData();
      model->Retrain();
    }

  }
  
  else{

    const std::string output_dir = cmd.get<std::string>("output");
    const std::string video_file = cmd.get<std::string>("video");

    if (!boost::filesystem::exists(video_file)) {
      std::cerr << "Error, could not find video file: " << video_file << "\n";
      cmd.printParams();
      return 1;
    }

    for (auto &model : models){
      model->LoadModel();
    }

    std::cout << "Testing custom detection using camera" << std::endl;

    if (mine_for_false_positives){

      if (!boost::filesystem::exists(output_dir)) {
        std::cerr << "Error, could not find output directory: " << output_dir << "\n";
        cmd.printParams();
        return 1;
      }

      for (auto &model : models){
        MineVideoForFalsePositives(cmd.get<std::string>("video"), output_dir, model);
      }
    }
    else{
      RunAllDetectorsOnVideo(cmd.get<std::string>("video"), output_dir, models);
    }

  }


  for (auto &model : models){
    delete model;
  }

  return EXIT_SUCCESS;
}
