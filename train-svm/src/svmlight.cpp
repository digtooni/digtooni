#include "../inc/svmlight.h"

SVMlight::SVMlight() {

  alpha_in = NULL;
  kernel_cache = NULL; // Cache not needed with linear kernel
  //model = (MODEL *)my_malloc(sizeof(MODEL));
  model = NULL;
  learn_parm = new LEARN_PARM;
  kernel_parm = new KERNEL_PARM;
  // Init parameters
  verbosity = 1; // Show some messages -v 1
  learn_parm->alphafile[0] = '\0'; // NULL; // Important, otherwise files with strange/invalid names appear in the working directory
  learn_parm->biased_hyperplane = 1;
  learn_parm->sharedslack = 0; // 1
  learn_parm->skip_final_opt_check = 0;
  learn_parm->svm_maxqpsize = 10;
  learn_parm->svm_newvarsinqp = 0;
  learn_parm->svm_iter_to_shrink = 2; // 2 is for linear;
  learn_parm->kernel_cache_size = 40;
  learn_parm->maxiter = 80000;
  learn_parm->svm_costratio = 1.0;
  learn_parm->svm_costratio_unlab = 1.0;
  learn_parm->svm_unlabbound = 1E-5;
  learn_parm->eps = 0.01;
  learn_parm->transduction_posratio = -1.0;
  learn_parm->epsilon_crit = 0.0001;
  learn_parm->epsilon_a = 1E-15;
  learn_parm->compute_loo = 0;
  learn_parm->rho = 1.0;
  learn_parm->xa_depth = 0;
  // The HOG paper uses a soft classifier (C = 0.01), set to 0.0 to get the default calculation
  learn_parm->svm_c = 0.01; // -c 0.01
  learn_parm->type = CLASSIFICATION;
  learn_parm->remove_inconsistent = 0; // -i 0 - Important
  kernel_parm->rbf_gamma = 1.0;
  kernel_parm->coef_lin = 1;
  kernel_parm->coef_const = 1;
  kernel_parm->kernel_type = LINEAR; // -t 0
  kernel_parm->poly_degree = 3;

}



SVMlight::~SVMlight() {
  // Cleanup area
  // Free the memory used for the cache
  if (kernel_cache){
    kernel_cache_cleanup(kernel_cache);
    kernel_cache = 0x0;
  }
  if (alpha_in){
    free(alpha_in);
    alpha_in = 0x0;
  }
  if (model){
    free_model(model, 0);
    model = 0x0;
  }
  if (docs){
    for (i = 0; i < totdoc; i++)
      free_example(docs[i], 1);
    free(docs);
    docs = 0x0;
  }
  if (target){
    free(target);
    target = 0x0;
  }
}

void SVMlight::writeSupportVectorsToFile(std::ofstream &file){

  DOC** supveclist = model->supvec;

  // Walk over every support vector
  for (long ssv = 1; ssv < model->sv_num; ++ssv) { // Don't know what's inside model->supvec[0] ?!
    // Get a single support vector
    DOC* singleSupportVector = supveclist[ssv]; // Get next support vector
    SVECTOR* singleSupportVectorValues = singleSupportVector->fvec;
    WORD singleSupportVectorComponent;

    double alpha = model->alpha[ssv];
    if (alpha > 0.0){
      file << "+1 ";
    }
    else{
      file << "-1 ";
    }
    for (unsigned long singleFeature = 0; singleFeature < model->totwords; ++singleFeature) {
      singleSupportVectorComponent = singleSupportVectorValues->words[singleFeature];
      //file << (singleSupportVectorComponent.weight * model->alpha[ssv]);

      //write the dimension value
      file << singleFeature + 1 << ":" << singleSupportVectorComponent.weight << " ";
    }

    file << "\n";
  }
}

void SVMlight::getSingleDetectingVector(std::vector<float>& singleDetectorVector, std::vector<unsigned int>& singleDetectorVectorIndices) {
  // Now we use the trained svm to retrieve the single detector vector
  DOC** supveclist = model->supvec;
  printf("Calculating single descriptor vector out of support vectors (may take some time)\n");
  // Retrieve single detecting vector (v1) from returned ones by calculating vec1 = sum_1_n (alpha_y*x_i). (vec1 is a n x1 column vector. n = feature vector length)
  singleDetectorVector.clear();
  singleDetectorVector.resize(model->totwords, 0.);
  printf("Resulting vector size %lu\n", singleDetectorVector.size());

  // Walk over every support vector
  for (long ssv = 1; ssv < model->sv_num; ++ssv) { // Don't know what's inside model->supvec[0] ?!
    // Get a single support vector
    DOC* singleSupportVector = supveclist[ssv]; // Get next support vector
    SVECTOR* singleSupportVectorValues = singleSupportVector->fvec;
    WORD singleSupportVectorComponent;
    // Walk through components of the support vector and populate our detector vector
    for (unsigned long singleFeature = 0; singleFeature < model->totwords; ++singleFeature) {
      singleSupportVectorComponent = singleSupportVectorValues->words[singleFeature];
      singleDetectorVector.at(singleSupportVectorComponent.wnum - 1) += (singleSupportVectorComponent.weight * model->alpha[ssv]);
    }
  }
}
