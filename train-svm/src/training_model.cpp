#include "../inc/training_model.hpp"
#include "../inc/config_reader.hpp"
#include "../inc/svmlight.h"
#include "../inc/helpers.hpp"

#include <iostream>

TrainingModel::TrainingModel(const std::string &config_file){

  ConfigReader cfg(config_file);

  name_ = cfg.get_element("name");

  win_stride_ = cv::Size(cfg.get_element_as_type<int>("stride-x"), cfg.get_element_as_type<int>("stride-y"));
  training_padding_ = cv::Size(cfg.get_element_as_type<int>("train-padding-x"), cfg.get_element_as_type<int>("train-padding-y"));
  hog_.winSize.width = cfg.get_element_as_type<int>("width");
  hog_.winSize.height = cfg.get_element_as_type<int>("height");

  hog_.nlevels = cfg.get_element_as_type<int>("num-scales");
  shift_threshold_ = cfg.get_element_as_type<float>("shift-threshold");
  scale_increase_ = cfg.get_element_as_type<float>("scale-increase");
  max_scale_ = cfg.get_element_as_type<float>("max-scale");
  frame_scale_ = cfg.get_element_as_type<float>("frame-scale");

  if ((hog_.winSize.width - hog_.blockSize.width) % hog_.blockStride.width != 0){
    throw std::runtime_error("Error, incompatible block and window size!\n");
  }
  if ((hog_.winSize.height - hog_.blockSize.height) % hog_.blockStride.height != 0){
    throw std::runtime_error("Error, incompatible block and window size!\n");
  }

  //hog.blockSize = cv::Size(cfg.get_element_as_type<int>("block-size-x"), cfg.get_element_as_type<int>("block-size-y"));
  //hog.cellSize = cv::Size(cfg.get_element_as_type<int>("cell-size-x"), cfg.get_element_as_type<int>("cell-size-y"));
  std::string positive_dirs = cfg.get_element("positive_dirs");
  std::string negative_dirs = cfg.get_element("negative_dirs");

  positive_training_data_dirs_ = cfg.split(positive_dirs, ',');
  negative_training_data_dirs_ = cfg.split(negative_dirs, ',');

  for (size_t i = 0; i < positive_training_data_dirs_.size(); ++i){
    positive_training_data_dirs_[i] = cfg.trim(positive_training_data_dirs_[i]);
  }
  for (size_t i = 0; i < negative_training_data_dirs_.size(); ++i){
    negative_training_data_dirs_[i] = cfg.trim(negative_training_data_dirs_[i]);
  }

  namespace fs = boost::filesystem;

  fs::path abs_config_file_path(config_file);// , fs::current_path());
  features_file_ = (abs_config_file_path.parent_path() / fs::path("features.dat")).string();
  svmlight_model_file_ = (abs_config_file_path.parent_path() / fs::path("svmlight_model_file.dat")).string();
  descriptor_vector_file_ = (abs_config_file_path.parent_path() / fs::path("descriptor_vector.dat")).string();
  
  valid_extensions_.push_back(".jpg");
  valid_extensions_.push_back(".png");
  valid_extensions_.push_back(".ppm");
  valid_extensions_.push_back(".pgm");

  svm_light_ = new SVMlight();

}

TrainingModel::~TrainingModel(){

  delete svm_light_;

}

void TrainingModel::SetTrainingDataDirs(const std::vector<std::string> &positive_dirs, const std::vector<std::string> &negative_dirs){

  positive_training_data_dirs_ = positive_dirs;
  negative_training_data_dirs_ = negative_dirs;

}

void TrainingModel::CollectExtraNegatives(const std::string &directory){

  GetFilesInDirectory(directory, negative_training_images_, valid_extensions_);
  number_of_samples_ = positive_training_images_.size() + negative_training_images_.size();

}

void TrainingModel::CollectExtraNegatives(const std::vector<std::string> &directories){

  for (auto dir : directories) CollectExtraNegatives(dir);

}

void TrainingModel::CollectTrainingData(){

  for (const auto &pos_dir : positive_training_data_dirs_)
    GetFilesInDirectory(pos_dir, positive_training_images_, valid_extensions_);

  for (const auto &neg_dir : negative_training_data_dirs_)
    GetFilesInDirectory(neg_dir, negative_training_images_, valid_extensions_);

  number_of_samples_ = positive_training_images_.size() + negative_training_images_.size();

}


void TrainingModel::CollectTrainingData(const TrainingModel &other_model){

  for (const auto &pos_dir : other_model.positive_training_data_dirs_)
    GetFilesInDirectory(pos_dir, negative_training_images_, valid_extensions_);

  number_of_samples_ = positive_training_images_.size() + negative_training_images_.size();

}

void TrainingModel::Retrain(){

  if (svmlight_model_file_ == ""){
  
    throw std::runtime_error("Error, SVM file is not defined");
  
  }

  svm_light_->loadModelFromFile(svmlight_model_file_);

  //will be features_file eventually - save SV's here and then load new data appending to this.
  std::ofstream sv_file;
  sv_file.open(features_file_);

  if (sv_file.good() && sv_file.is_open()) {

    //write the training data (i.e. the support vectors) into the features file. We will then append new training examples to this. 
    svm_light_->writeSupportVectorsToFile(sv_file);

  }

  sv_file.close();

  Train(true);

}


void TrainingModel::CalculateFeaturesFromInput(const std::string& imageFilename, std::vector<float>& featureVector, cv::HOGDescriptor& hog, cv::Size winStride, cv::Size trainingPadding) {
  /** for imread flags from openCV documentation,
  * @see http://docs.opencv.org/modules/highgui/doc/reading_and_writing_images_and_video.html?highlight=imread#Mat imread(const string& filename, int flags)
  * @note If you get a compile-time error complaining about following line (esp. imread),
  * you either do not have a current openCV version (>2.0)
  * or the linking order is incorrect, try g++ -o openCVHogTrainer main.cpp `pkg-config --cflags --libs opencv`
  */
  cv::Mat imageData = cv::imread(imageFilename, 0);
  if (imageData.empty()) {
    featureVector.clear();
    printf("Error: HOG image '%s' is empty, features calculation skipped!\n", imageFilename.c_str());
    return;
  }
  // Check for mismatching dimensions

  if (imageData.size() != hog.winSize){
    printf("Warning: resizing image from %i,%i to %i,%i.\n", imageData.rows, imageData.cols, hog.winSize.height, hog.winSize.width);
    cv::Mat resizedImage;
    cv::resize(imageData, resizedImage, hog.winSize);
    imageData = resizedImage;
  }

  // Check for mismatching dimensions
  if (imageData.cols != hog.winSize.width || imageData.rows != hog.winSize.height) {
    featureVector.clear();
    printf("Error: Image '%s' dimensions (%u x %u) do not match HOG window size (%u x %u)!\n", imageFilename.c_str(), imageData.cols, imageData.rows, hog.winSize.width, hog.winSize.height);
    //  return;
  }

  std::vector<cv::Point> locations;
  hog.compute(imageData, featureVector, winStride, trainingPadding, locations);
  imageData.release(); // Release the image again after features are extracted
}


void TrainingModel::SaveSVMToFile(std::vector<float>& descriptorVector, const float svm_margin, std::vector<unsigned int>& _vectorIndices, const std::string &fileName) {
  
  printf("Saving descriptor vector to file '%s'\n", fileName.c_str());
  std::string separator = " "; // Use blank as default separator between single features
  std::fstream File;
  float percent;
  File.open(fileName.c_str(), std::ios::out);
  if (File.good() && File.is_open()) {
  
    File << svm_margin << "\n";
    
    printf("Saving %lu descriptor vector features:\t", descriptorVector.size());
    storeCursor();
    for (size_t feature = 0; feature < descriptorVector.size(); ++feature) {
      if ((feature % 10 == 0) || (feature == (descriptorVector.size() - 1))) {
        percent = (float)((1 + feature) * 100 / descriptorVector.size());
        printf("%4u (%3.0f%%)", feature, percent);
        fflush(stdout);
        resetCursor();
      }
      File << descriptorVector.at(feature) << separator;
    }
    printf("\n");
    File << std::endl;
    File.flush();
    File.close();
  }
}

void TrainingModel::GetFilesInDirectory(const std::string& dirName, std::vector<std::string>& fileNames, const std::vector<std::string>& validExtensions) {

  std::cout << "Opening directory " << dirName << "\n";
  boost::filesystem::path dirPath(dirName);

  if (!exists(boost::filesystem::path(dirName))) throw std::runtime_error("Error, directory " + dirName + " does not exist.\n");
  boost::filesystem::directory_iterator endItr;
  boost::filesystem::directory_iterator dirItr(dirPath);
  for (; dirItr != endItr; ++dirItr){

    boost::filesystem::path filePath = dirItr->path();
    const std::string extension = filePath.extension().string();

    if (find(validExtensions.begin(), validExtensions.end(), extension) != validExtensions.end()) {
      std::cout << "Found matching data file " << filePath << "\n";
      fileNames.push_back(filePath.string());
    }
    else {
      std::cout << "Found file does not match required file type, skipping: " << filePath << "\n";
    }

  }

}


void TrainingModel::GenerateFeaturesFromData(bool append){

  float percent;
  std::cout << "Reading files, generating HOG features and save them to file " << features_file_ << std::endl;

  std::fstream File;
  if (append)
    File.open(features_file_, std::ios::app);
  else
    File.open(features_file_, std::ios::out);

  if (File.good() && File.is_open()) {
    // Remove following line for libsvm which does not support comments
    // File << "# Use this file to train, e.g. SVMlight by issuing $ svm_learn -i 1 -a weights.txt " << featuresFile.c_str() << endl;
    // Iterate over sample images
    for (unsigned long currentFile = 0; currentFile < number_of_samples_; ++currentFile) {
      storeCursor();
      std::vector<float> featureVector;
      // Get positive or negative sample image file path
      const std::string currentImageFile = (currentFile < positive_training_images_.size() ? positive_training_images_.at(currentFile) : negative_training_images_.at(currentFile - positive_training_images_.size()));
      // Output progress
      if ((currentFile + 1) % 10 == 0 || (currentFile + 1) == number_of_samples_) {
        percent = (float)((currentFile + 1) * 100 / number_of_samples_);
        printf("%5lu (%3.0f%%):\tFile '%s'", (currentFile + 1), percent, currentImageFile.c_str());
        fflush(stdout);
        resetCursor();
      }
      // Calculate feature vector from current image file
      CalculateFeaturesFromInput(currentImageFile, featureVector, hog_, win_stride_, training_padding_);
      if (!featureVector.empty()) {
        /* Put positive or negative sample class to file,
        * true=positive, false=negative,
        * and convert positive class to +1 and negative class to -1 for SVMlight
        */
        File << ((currentFile < positive_training_images_.size()) ? "+1" : "-1");
        // Save feature vector components
        for (unsigned int feature = 0; feature < featureVector.size(); ++feature) {
          File << " " << (feature + 1) << ":" << featureVector.at(feature);
        }
        File << std::endl;
      }
    }
    printf("\n");
    File.flush();
    File.close();
  }
  else {
    std::cout << "Error opening file " << features_file_ << "'!\n";
    return;
  }


}

void TrainingModel::RunDetection(const cv::Mat &frame, std::vector<cv::Rect> &detections) const {

  std::vector<double> confidences;

  cv::Mat resized_frame;
  cv::resize(frame, resized_frame, cv::Size(0, 0), frame_scale_, frame_scale_);

  cv::Mat frame_gray;
  cvtColor(resized_frame, frame_gray, CV_BGR2GRAY);

  hog_.detectMultiScale(frame_gray, detections, confidences, shift_threshold_ * hit_threshold_, win_stride_, cv::Size(0,0), scale_increase_, max_scale_, false);

  for (size_t i = 0; i < detections.size(); ++i){
    detections[i] = cv::Rect(detections[i].x / frame_scale_, detections[i].y / frame_scale_, detections[i].width / frame_scale_, detections[i].height / frame_scale_);
  }

}

void TrainingModel::Train(bool retrain){

  std::cout << "Generating features from data" << std::endl;
  GenerateFeaturesFromData(retrain);

  std::cout << "Calling " << svm_light_->getSVMName() << std::endl;
  std::cout << "Loading " << number_of_samples_ << " samples.\n" << std::endl;
  svm_light_->read_problem(const_cast<char*> (features_file_.c_str()));

  std::cout << "Training model" << std::endl;
  svm_light_->train(); // Call the core libsvm training procedure
  
  std::cout << "Training done, saving model file!" << std::endl;
  svm_light_->saveModelToFile(svmlight_model_file_);
  

  std::vector<unsigned int> descriptorVectorIndices;
  // Generate a single detecting feature vector (v1 | b) from the trained support vectors, for use e.g. with the HOG algorithm
  svm_light_->getSingleDetectingVector(descriptor_vector_, descriptorVectorIndices);
  // And save the precious to file system
  
  SaveSVMToFile(descriptor_vector_, svm_light_->getThreshold(), descriptorVectorIndices, descriptor_vector_file_);

}

void TrainingModel::LoadModel(){

  std::ifstream ifs(descriptor_vector_file_);

  if (!ifs.is_open()){
    throw std::runtime_error("Error, could not open " + descriptor_vector_file_);
  }

  descriptor_vector_.clear();

  ifs >> hit_threshold_;

  while (!ifs.eof()){
    float x;
    ifs >> x;
    descriptor_vector_.push_back(x);
  }

  hog_.setSVMDetector(descriptor_vector_);

}
